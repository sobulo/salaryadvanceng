/**
 * 
 */
package com.fertiletech.addosser.server.loanmktdata;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.fertiletech.addosser.server.ServiceImplUtilities;
import com.fertiletech.addosser.shared.exceptions.DuplicateEntitiesException;
import com.fertiletech.addosser.shared.exceptions.MissingEntitiesException;
import com.fertiletech.testutils.Order;
import com.fertiletech.testutils.OrderedRunner;
import com.google.appengine.api.datastore.QueryResultIterable;
import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 * @author Segun Razaq Sobulo
 *
 */
@RunWith(OrderedRunner.class)
public class LoanMktDAOTest {
	private static final LocalServiceTestHelper helper = new LocalServiceTestHelper(
            new LocalDatastoreServiceTestConfig());
    private static Objectify ofy;
    

    public static final String TEST_KEY1 = "abc";
    public static final String TEST_KEY2 = "def";    
    public static final String TEST_VAL1 = "hello";
    public static final String TEST_VAL2 = "10";
    
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		helper.setUp();
		ofy = ObjectifyService.begin();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		helper.tearDown();
	}
	
	@Test(expected=MissingEntitiesException.class)
	@Order(order = 1)
	public void updateApplicationParameters() throws MissingEntitiesException
	{
		HashMap<String, String> params = new HashMap<String, String>();
		params.put(TEST_KEY1, TEST_VAL2);
		LoanMktDAO.updateApplicationParameters(ServiceImplUtilities.getLoanAppConfigurationKey(), params);
	}	
	
	@Test
	@Order(order = 2)
	public void testCreateApplicationParameters() throws DuplicateEntitiesException
	{
		HashMap<String, String> params = new HashMap<String, String>();
		params.put(TEST_KEY1, TEST_VAL1);
		params.put(TEST_KEY2, TEST_VAL2);
		//create and store a param obj in the datastore
		ApplicationParameters createdParamObj = LoanMktDAO.createApplicationParameters(EntityConstants.MARKETING_PARAM_ID, 
				params);
		
		assertTrue(createdParamObj.getKey() != null);
	}
	
	@Test
	@Order(order = 3)
	public void readCreatedApplicationParameters()
	{
		ApplicationParameters paramObj = ofy.get(new Key<ApplicationParameters>(ApplicationParameters.class, 
				EntityConstants.MARKETING_PARAM_ID)); 
		assertTrue(paramObj != null);
		assertEquals(paramObj.getParams().get(TEST_KEY1), TEST_VAL1);
	}
	
	@Test(expected=DuplicateEntitiesException.class)
	@Order(order = 4)
	public void testDuplicateException() throws DuplicateEntitiesException
	{
		HashMap<String, String> params = new HashMap<String, String>();
		params.put(TEST_VAL1, TEST_VAL2);
		params.put(TEST_KEY2, TEST_VAL1);
		//create and store a param obj in the datastore
		ApplicationParameters createdParamObj = LoanMktDAO.createApplicationParameters(EntityConstants.MARKETING_PARAM_ID, 
				params);
	}
	
	@Test
	@Order(order = 5)
	public void readCreatedApplicationParametersAgain()
	{
		ApplicationParameters paramObj = ofy.get(new Key<ApplicationParameters>(ApplicationParameters.class, 
				EntityConstants.MARKETING_PARAM_ID)); 
		assertTrue(paramObj != null);
		assertEquals(paramObj.getParams().get(TEST_KEY1), TEST_VAL1);
	}
	
	@Test
	@Order(order = 6)
	public void updateApplicationParametersAgain() throws MissingEntitiesException
	{
		HashMap<String, String> params = new HashMap<String, String>();
		params.put(TEST_KEY1, TEST_VAL2);
		LoanMktDAO.updateApplicationParameters(ServiceImplUtilities.getLoanAppConfigurationKey(), params);
	}
	
	@Test
	@Order(order = 7)
	public void readUpdatedApplicationParameters()
	{
		ApplicationParameters paramObj = ofy.get(new Key<ApplicationParameters>(ApplicationParameters.class, 
				EntityConstants.MARKETING_PARAM_ID)); 
		assertTrue(paramObj != null);
		assertEquals(paramObj.getParams().get(TEST_KEY1), TEST_VAL2);
	}	
	
	@Test
	@Order(order = 8)
	public void createConsumerLoansSalesLeadTest() throws DuplicateEntitiesException
	{
		String[] result = LoanMktDAO.getOrCreateLoanID("test@123.com");
		assertTrue(result.length > 0);
		
	}

	@Test
	@Order(order = 9)
	public void createConsumerLoansSalesLeadTest2() throws DuplicateEntitiesException
	{
		String[] result = LoanMktDAO.getOrCreateLoanID("test2@123.com");
		assertTrue(result.length > 0);		
	}			
	
	@Test
	@Order(order = 11)
	public void readConsumerLoansSalesLeadTest()
	{
		Calendar cal = GregorianCalendar.getInstance();
		Date endDate = cal.getTime();
		System.out.println(endDate);
		cal.roll(Calendar.DAY_OF_MONTH, -1);
		Date startDate = cal.getTime();
		System.out.println(startDate);
		QueryResultIterable<ConsumerLoanSalesLead> saleLeads = LoanMktDAO.getSalesLeadsByDate(startDate, endDate);
		int count = 0;
		System.out.println(saleLeads.iterator().hasNext());
		for(ConsumerLoanSalesLead sl : saleLeads)
			count++;
		assertEquals(count, 2);
	}

}
 