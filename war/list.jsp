<!--
Copyright 2016 Google Inc.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
-->
<!-- [START list] -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="com.fertiletech.addosser.shared.DTOConstants"%>
<style>
.bodySlide1
{
	background: url(sang.jpg) no-repeat scroll center -72px / auto 716px rgba(0, 0, 0, 0);
}

.home-search {
		font-size: smaller;
		margin: 0 auto;
		background: rgba(255, 255, 255, 0.75);
		padding: 10px;
		border: 1px solid #e0e0e0;
	}
</style>
<div class= "bodySlide1 bodyPad">
<div class="container">


<div class ="row">
<div class = "col-sm-6">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</div>
<div class="col-sm-6">
	<h3 style="color:#44ace9">Salary Advance NG (S.A.NG)</h3>
	<br/>
<p>S.A.NG provides loans of up to &#8358;4,000,000 for salary earners.<p>
  <c:choose>
  <c:when test="${empty eligible}">
  <form method="GET" action="list">
			<div class="form-group">
				<label for="monthly-salary">How much is your monthly salary?
					<input type="text" name="monthly-salary" id="monthly-salary" class="form-control"/>
			</div>
			<button type="submit" class="btn btn-info">Check eligibility</button>

</form>
  </c:when>
  <c:otherwise>
  <b>${eligible}</b>
  <br/>
  	<p>Get started now &nbsp; &nbsp;
	<a href="/update" class="btn btn-success btn-sm">
    <i class="glyphicon glyphicon-plus"></i>
    request a loan
  </a></p>
    </c:otherwise>
  </c:choose>
  
    <hr/>
            <small style="font-size: smaller;">Product Terms & Condtions</b></small>   
		

				<select name="page-type" id="page-type" class="form-control">
					<%
						String[] pageUrls = DTOConstants.getLoanTypeUrl();
						for (int i = 0; i < DTOConstants.LOAN_TYPES.length; i++) {
							String lt = DTOConstants.LOAN_TYPES[i];
							String pg = pageUrls[i];
							String optionTag = "<option value='"+ pg +"'>" + lt + "</option>";
							out.print(optionTag);
						}
					%>
				</select>
			
			<button onclick="goType()" class="btn btn-link col btn-sm">Click to visit selected product page</button>
			    	<hr/>
      	<div class="home-search"><b>Why choose S.A.NG?</b><br/>
  	 For excellent, sanguine and friendly customer service. We talk you through the process<br/>
  	 Call <a href='tel:09037414749'>0903-741-4749</a><br/>  
  	 Or&nbsp;&nbsp; <a href='tel:08176251709'>0817-625-1709</a><br/>
  	 </div>
<br/><br/><br/><br/><br/>
  </div>

	<script>
		function goType() {
			var e = document.getElementById("page-type");
			var abouturl = e.options[e.selectedIndex].value;
			window.location.assign(abouturl)
		}
	</script>			
</div>
<br/><br/>
<div>
<!--  
    <small>
  <c:forEach items="${books}" var="book">
      S.A.NG ID: <c:out value="${fn:escapeXml(book.id)}" />
      Salary: <span>&#8358;</span>${book.salary}<br/>Requested Loan: <span>&#8358;</span><c:out value="${fn:escapeXml(book.amount)}" />&nbsp;
        Status:  <c:out value="${fn:escapeXml(book.ofykey)}" />&nbsp;
        <i><c:out value="${fn:escapeXml(book.company)}" /></i>&nbsp;&nbsp;&nbsp;&nbsp;
  </c:forEach>
  </small>
-->
  </div>
</div>
</div>
<!-- [END list] -->
