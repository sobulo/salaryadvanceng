<!--
Copyright 2016 Google Inc.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
-->
<!-- [START base] -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html lang="en">
  <head>
    <title>Salary Advance - Nigeria</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <style>
.bodyPad
{
margin-bottom: 0px!important;
margin-top:0px!important;
}

.navbar-default .navbar-toggle .icon-bar {
    background-color: white;
}

</style>
  </head>
  <body>
  
    <nav class="navbar navbar-default bodyPad"  style="background-color:#44ace9;">
      <div class="container-fluid">
        <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
          <img src="logosmall.png" class="navbar-brand"/>
          <div class="navbar-brand"><a href = "/" style="color:white!important;">S.A.NG</a></div>
          
        </div>
         <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav">
          <li><a href="/update" style="color:white!important;">Apply</a></li>
          <li><a href="/sangstate" style="color:white!important;">Check Status</a></li>
        </ul>
        <p class="navbar-text navbar-right">
          <a href="http://info.salaryadvanceng.com/products" style="color:white!important;">Products</a>
        </p></div>
      </div>
    </nav>
    <c:import url="/${page}.jsp" />
 <p style="font-size: smaller; text-align: center">27 Ribadu Street, off Awolowo rd. Ikoyi, Lagos, Nigeria<br/>
 <a href="mailto:hello@salaryadvanceng.com>">hello@salaryadvanceng.com</a> and <a href="https://www.facebook.com/SalaryAdvanceNG/">facebook.com/salaryadvanceng</a><br/></p>
  </body>
</html>
<!-- [END base]-->
