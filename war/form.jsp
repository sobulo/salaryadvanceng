<!--
Copyright 2016 Google Inc.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
-->
<!-- [START form] -->
<%@page import="com.fertiletech.addosser.shared.FormConstants.HirePeriods"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="com.fertiletech.addosser.shared.NigeriaConstants"%>
<%@ page import="com.fertiletech.addosser.shared.DTOConstants"%>
<%@ page import="com.fertiletech.addosser.shared.FormConstants"%>
<div class="container">
	<h3>
		<c:out value="${action}" />
	</h3>

	<form method="POST" action="${destination}">

		<div class=row>
			<div class="form-group col-sm-4">
				<label for="state">Resident State </label><select name="state" id="state" class="form-control">
					<%
						for (String st : NigeriaConstants.STATE_MAP.keySet()) {
							String startTag = "<option>";
							if(st.equals("Lagos State"))
								startTag = "<option selected>";
							out.println(startTag +st + "</option>");
							
							
						}
					%>
				</select>
			</div>

			<div class="form-group col-sm-4">
				<label for="company">Company </label><input type="text" name="company" id="company"
					class="form-control">${fn:escapeXml(book.company)}</input>
			</div>

			<div class="form-group col-sm-4">
				<label for="name">Surname, First name</label>  <input
					type="text" name="name" id="name"
					value="${fn:escapeXml(book.name)}" class="form-control" />
			</div>
		</div>
		<div class=row>

			<div class="form-group col-sm-4">
				<label for="email">Email</label> <input type="text" name="email" id="email"
					value="${fn:escapeXml(book.email)}" class="form-control" />
			</div>

			<div class="form-group col-sm-4">
				<label for="phone">Phone No.</label> <input type="text" name="phone" id="phone"
					value="${fn:escapeXml(book.phone)}" class="form-control" />
			</div>

			<div class="form-group col-sm-4">
				<label for="type">Type </label><select name="type" id="type" class="form-control">
					<%
						for (String lt : DTOConstants.LOAN_TYPES) {
					%>
					<option>
						<%
							out.print(lt);
						%>
					</option>
					<%
						}
					%>
				</select>
			</div>
		</div>
		<div class=row>


			<div class="form-group col-sm-4">
				<label for="salary">Salary </label>  <input type="text" name="salary" id="salary"
					class="form-control">${fn:escapeXml(book.salary)}</input>
			</div>

			<div class="form-group col-sm-4">
				<label for="amount">Requested Amount</label><input type="text" name="amount"
					id="amount" class="form-control">${fn:escapeXml(book.amount)}</input>
			</div>


			<div class="form-group col-sm-4">
				<label for="tenor">Requested Tenor</label>
                <select name="tenor" id="tenor" class="form-control">
                <option>1 month</option>
                <option>6 months</option>
                <option>12 months</option>
                </select>
			</div>
		</div>
		
				<div class=row>

			<div class="form-group col-sm-4">
				<label for="gender">Gender</label>
                <select name="gender" id="gender" class="form-control">
                <%
                	for(String sx : FormConstants.SEX)
                		out.print("<option>"+sx+"</option>");
                %>
                </select>
			</div>

			<div class="form-group col-sm-4">
				<label for="age">Age Range</label> 
				<select name="age" id="age" class="form-control">
                <%
                	for(String range : FormConstants.AGES)
                		out.print("<option>" + range + "</option>");
                
                %>
                </select>
			</div>

			<div class="form-group col-sm-4">
				<label for="hire">Hire Periods </label>
				<select name="hire" id="hire" class="form-control">
		         <%
                	for(HirePeriods p : HirePeriods.values())
                		out.print("<option>" + p.toString() + "</option>");
                  
                %>
				</select>
			</div>
		</div>




		<button type="submit" class="btn btn-success col btn-lg">Confirm Request</button>
</div>
</form>
</div>
<!-- [END form] -->
