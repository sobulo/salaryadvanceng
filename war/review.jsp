<!--
Copyright 2016 Google Inc.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
-->
<!-- [START view] -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<div class="container">
	<h3>Review</h3>
${dpbook.message}<br/><br/>
<button onclick="goBack()" class="btn ${book.style}">${book.message}</button>
<br/><hr/><br/><div style="text-decoration: underline;">Loan Request</div>
	<form method="POST" action="create">

		<div class="form-group">
			<label for="state">State:</label> <span>${dpbook.state}</span> <span style="font-size: smaller;"> (the state that you
					live and work in)</span>
			<input type="hidden" name="state" value="${book.state}"/>
		</div>

		<div class="form-group">
			<label for="company">Company:</label> <span>${dpbook.company}</span> <span style="font-size: smaller;"> (where you 
					work and earn your income)</span> 
			<input type="hidden" name="company" value="${book.company}"/>
		</div>

		<div class="form-group">
			<label for="name">Name: </label> <span>${dpbook.name}</span><span style="font-size: smaller;"> (last name, first name)</span> 
			<input type="hidden" name="name" value="${book.name}"/>
		</div>

		<div class="form-group">
			<label for="email">Email: </label> <span>${dpbook.email}</span> <span style="font-size: smaller;"> (your personal
					email address)</span>
			<input type="hidden" name="email" value="${book.email}"/>
		</div>

		<div class="form-group">
			<label for="phone">Phone No: </label> <span>${dpbook.phone}</span> <span style="font-size: smaller;"> (
					phone number to reach you) </span>
			<input type="hidden" name="phone" value="${book.phone}"/>
		</div>

		<div class="form-group">
			<label for="type">Product: </label>${dpbook.type}
			<input type="hidden" name="type" value="${book.type}"/>
		</div>

		<div class="form-group">
			<label for="salary">Salary: </label>  <span>&#8358;</span> <span>${dpbook.salary}</span><span style="font-size: smaller;"> (your
					monthly income at ${book.company})</span> 
			<input type="hidden" name="salary" value="${book.salary}"/>
		</div>

		<div class="form-group">
			<label for="amount">Loan Amount: </label>  <span>&#8358;</span> <span>${dpbook.amount}</span> <span style="font-size: smaller;">(the amount
					of money you want to borrow?)</span>
			<input type="hidden" name="amount" value="${book.amount}"/>
		</div>

		<div class="form-group">
			<label for="tenor">Tenor: </label> <span>${dpbook.tenor}</span> <span style="font-size: smaller;"> (the number of 
					monthly repayments to settle the loan)</span> 
			<input type="hidden" name="tenor" value="${book.tenor}"/>
		</div>
		
		<div class="form-group">
			<label for="gender">Sex: </label> <span>${dpbook.gender}</span> <span style="font-size: smaller;"> (your gender, male or female)</span> 
			<input type="hidden" name="gender" value="${book.gender}"/>
		</div>
		
		<div class="form-group">
			<label for="age">Age Range: </label> <span>${dpbook.age}</span> <span style="font-size: smaller;"> (Age grouping, 20s, 30s etc)</span> 
			<input type="hidden" name="age" value="${book.age}"/>
		</div>
		
				<div class="form-group">
			<label for="hire">Hire Period: </label> <span>${dpbook.hire}</span> <span style="font-size: smaller;"> (for how long have you been  
					employed at ${book.company})</span> 
			<input type="hidden" name="hire" value="${book.hire}"/>
		</div>
		
		<button type="submit" class="btn btn-succcess btn-large" ${dpbook.style}>  Submit Request  ${dpbook.style}</button>
	</form>

	<script>
		function goBack() {
			window.history.back();
		}
	</script>
	

	




</div>

<!-- [END view] -->
