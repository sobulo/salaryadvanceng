<!--
Copyright 2016 Google Inc.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
-->
<!-- [START view] -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="com.fertiletech.addosser.server.pub.AuditHelper"%>
<div class="container">
	<h3>Status</h3>
	
  <c:choose>
  <c:when test="${empty sang}">
  <form method="POST" action="sangstate">
  <div class="row">
			<div class="form-group col-sm-4">
				<label for="email">Email
					<input value="${oldmail}" type="text" name="email" id="email" class="form-control"/>
			</div>
			<div class="form-group col-sm-4">
				<label for="id">S.A.NG ID
					<input value="${oldid}" type="text" name="id" id="id" class="form-control"/>
			</div>
			<div class="col-sm-4">
			<button type="submit" class="btn btn-info col btn-lg">Check Request Queue</button>
			</div>
			</div>
			<p>${sangerror}</p>

</form>
   <hr/>
   <b>Request Trail</b><br/>
  </c:when>
  <c:otherwise>

	<div class="media">
		<div class="media-left">
			<img alt="logo" src="logosmall.png">
		</div>
		<div class="media-left">
			S.A.NG ID:
			<c:out value="${fn:escapeXml(book.id)}" />
		</div>
		<div class="media-body">

			<small>Salary: <span>&#8358;</span><c:out value="${fn:escapeXml(book.salary)}" /></small>

			<p class="book-author">
				Requested Amount:
				<span>&#8358;</span><c:out value="${fn:escapeXml(book.amount)}" /><br/>
			
			
				Requested Tenor:
				<c:out value="${fn:escapeXml(book.tenor)}" />
				<br /> Requested on:
				<c:out value="${fn:escapeXml(book.name)}" />
			</p>
			<small class="book-added-by">Status 
			<c:out value="${fn:escapeXml(book.ofykey)}" /><br /> <i>
			<c:out value="${fn:escapeXml(book.company)}" /></i></small>
		<p style=" ${fn:escapeXml(book.style)}"><b>
        ${fn:escapeXml(book.message)}</b></p>
		</div>
	</div>
	   <hr/>
   <b>Request Trail</b><br/>
	<% out.println(AuditHelper.getAudutTrail((String) request.getAttribute("sang"))); %>

    </c:otherwise>
  </c:choose>

</div>

<!-- [END view] -->
