/**
 * 
 */
package com.fertiletech.addosser.admin;



import com.fertiletech.addosser.shared.FormConstants;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class ImageUploadPanel extends Composite implements ClickHandler, FormPanel.SubmitCompleteHandler{

	@UiField Button submit;
	@UiField HTML status;
	@UiField FileUpload selectFile;
	@UiField FormPanel imageForm;
	@UiField ListBox purpose;
	
	private static ImageUploadUiBinder uiBinder = GWT
			.create(ImageUploadUiBinder.class);

	interface ImageUploadUiBinder extends UiBinder<Widget, ImageUploadPanel> {
	}

	public ImageUploadPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		for(String purposeType : FormConstants.ADMIN_UPLOADS)
			purpose.addItem(purposeType);
		
        submit.addClickHandler(this);
        imageForm.setAction("/upload/images");
        imageForm.addSubmitCompleteHandler(this);
        imageForm.setMethod(FormPanel.METHOD_POST);
        imageForm.setEncoding(FormPanel.ENCODING_MULTIPART);
        selectFile.setName("chooseFile");
	}
	
    @Override
    public void onSubmitComplete(FormPanel.SubmitCompleteEvent event)
    {
        String results = event.getResults();
        status.setHTML("Received " + results.length() + " chars: " + results);
        GWT.log(results);
    }

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		status.setText("Sending file to server...");
		imageForm.submit();
	}	

}
