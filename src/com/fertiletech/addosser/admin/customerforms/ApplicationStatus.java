package com.fertiletech.addosser.admin.customerforms;

import com.fertiletech.addosser.admin.WebAppHelper;
import com.fertiletech.addosser.admin.app.Showcase;
import com.fertiletech.addosser.admin.app.content.ApplicationFormCW;
import com.fertiletech.addosser.admin.app.content.GuarantorFormCW;
import com.fertiletech.addosser.admin.app.content.OtherGuarantorCW;
import com.fertiletech.addosser.client.utils.PanelUtilities;
import com.fertiletech.addosser.client.utils.SearchIDBox;
import com.fertiletech.addosser.shared.DTOConstants;
import com.fertiletech.addosser.shared.FormConstants;
import com.fertiletech.addosser.shared.table.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Widget;

public class ApplicationStatus extends Composite{

	@UiField
	SearchIDBox idBox;
	@UiField
	Hyperlink formLink;
	@UiField
	Anchor formDownload;
	@UiField
	Hyperlink guardLink;
	@UiField
	Anchor guardDownload;
	@UiField
	Hyperlink guard2Link;
	@UiField
	Anchor guard2Download;
	@UiField
	Hyperlink checkLink;
	@UiField
	Anchor checkDownload;
	@UiField
	Hyperlink opsLink;
	@UiField
	Anchor opsDownload;
	@UiField
	Hyperlink offerLink;
	@UiField
	Anchor offerDownload;
	
	
		
	private AsyncCallback<TableMessage> loanStateCallBack = new AsyncCallback<TableMessage>() {

		@Override
		public void onFailure(Throwable caught) {
			Window.alert("Error occured. " + caught.getMessage());
			idBox.setEnabled(true);
		}

		@Override
		public void onSuccess(TableMessage result) {
			idBox.setEnabled(true);
			if(result == null)
			{	
				String[] id = idBox.getText().split(":");
				String msg = "Unable to find loan application with id ";
				if(id.length > 1)
					msg += id[0];
				PanelUtilities.errorBox.show(msg);
				return;
			}
			Boolean[] formStates = FormConstants.getSubmissionValues(result);
			changePanelState(result, formStates[0], formStates[1], formStates[2]);
		}
	};
	
	private AsyncCallback<String> appDownloadLinkCallback = new AsyncCallback<String>() {

		@Override
		public void onFailure(Throwable caught) {
			Window.alert(caught.getMessage());
			formDownload.setText("Error - refresh browser?");
		}

		@Override
		public void onSuccess(String result) {
			setupDownloadLink(result, formDownload);
		}
	};
	
	private AsyncCallback<String> guard1DownloadCallback = new AsyncCallback<String>() {

		@Override
		public void onFailure(Throwable caught) {
			Window.alert(caught.getMessage());
			guardDownload.setText("Error - refreh browser?");
		}

		@Override
		public void onSuccess(String result) {
			setupDownloadLink(result, guardDownload);
			
		}
	};
	
	private AsyncCallback<String> guard2DownloadCallback = new AsyncCallback<String>() {

		@Override
		public void onFailure(Throwable caught) {
			Window.alert(caught.getMessage());
			guard2Download.setText("Error - refresh browser");
		}

		@Override
		public void onSuccess(String result) {
			setupDownloadLink(result, guard2Download);
		}
	};
	
	private AsyncCallback<String> checklistDownloadCallback = new AsyncCallback<String>() {

		@Override
		public void onFailure(Throwable caught) {
			Window.alert(caught.getMessage());
			checkDownload.setText("Error - refresh browser");
		}

		@Override
		public void onSuccess(String result) {
			setupDownloadLink(result, checkDownload);
		}
	};
	
	private static ApplicationStatusUiBinder uiBinder = GWT
			.create(ApplicationStatusUiBinder.class);

	interface ApplicationStatusUiBinder extends
			UiBinder<Widget, ApplicationStatus> {
	}
	
	public ApplicationStatus(boolean showSearch) {
		this();
		idBox.setVisible(showSearch);
	}

	public ApplicationStatus() {
		initWidget(uiBinder.createAndBindUi(this));
		idBox.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				searchLoanID();
			}
		});
		//Window.alert("Status me na");
		GWT.log("Status panel loaded");
	}
	
	private void setupDownloadLink(String href, Anchor a)
	{
		a.setHref(href);
		a.setEnabled(true);
		a.setText("Download PDF");
	}
	
	public void setLoanID(String ID)
	{
		if(ID == null || ID.trim().length() == 0) return;
		idBox.setText("Loading, please wait ...");
		idBox.setEnabled(false);
		WebAppHelper.getLoanMktService().getLoanState(ID, loanStateCallBack);
	}
	
	private void searchLoanID()
	{
		Long id = idBox.getValue();
		if(id == null)
		{
			PanelUtilities.errorBox.show(idBox.getText() + " [id should consist of numbers only]");
			return;
		}
		idBox.setText(id + ": searching, please wait ...");
		idBox.setEnabled(false);
		WebAppHelper.getLoanMktService().getLoanState(id, loanStateCallBack);
	}
	
	public void killAnchor(Anchor a)
	{
		GWT.log("Killing: " + a.getHref());
		a.setHref(Window.Location.getHref());
		a.setEnabled(false);
		GWT.log("Killed: " + a.getHref());
	}
	
	public void killL1ink(Hyperlink h)
	{
		h.setTargetHistoryToken(History.getToken());
	}
	
	private void changePanelState(TableMessage loanState, boolean appSubmitted, Boolean guarantor1Submitted, Boolean guarantor2Submitted)
	{
		String loanID = loanState.getMessageId();
		String appFormID = loanState.getText(DTOConstants.LOAN_KEY_IDX);
		String guardFormID = loanState.getText(DTOConstants.GUARANTOR_ONE_KEY_IDX);
		String otherGuardFormID = loanState.getText(DTOConstants.GUARANTOR_TW0_KEY_IDX);
		
		formLink.setTargetHistoryToken(Showcase.getContentWidgetToken(ApplicationFormCW.class) + "/" + appFormID);
		if(appSubmitted)
		{
			formLink.setText("VIEW/edit");
			formDownload.setText("Fetching link ...");
			WebAppHelper.getLoanMktService().getApplicationFormDownloadLink(appFormID, appDownloadLinkCallback);
		}
		else
		{
			formLink.setText("view/EDIT");
			formDownload.setText("N/A - Application not submitted");
			killAnchor(formDownload);
		}
		
		guardLink.setTargetHistoryToken(Showcase.getContentWidgetToken(GuarantorFormCW.class) + "/" + guardFormID);
		boolean hasNext = (guarantor2Submitted != null); 
		killAnchor(guardDownload);
		if(guarantor1Submitted == null)
		{
			guardLink.setText(appSubmitted?"N/A - Guarantor Form not supported": "N/A - Application not submitted");
			guardDownload.setText(appSubmitted?"N/A - Guarantor Form not supported": "N/A - Application not submitted");
			killL1ink(guardLink);
		}
		else if(guarantor1Submitted)
		{
			guardLink.setText("VIEW/edit");
			guardDownload.setText("Fetching Link ... ");
			WebAppHelper.getLoanMktService().getGuarantorFormDownloadLink(guardFormID, guard1DownloadCallback);
		}
		else
		{
			guardLink.setText("view/EDIT");
			guardDownload.setText("N/A - Form not submitted");
		}
		
		guard2Link.setTargetHistoryToken(Showcase.getContentWidgetToken(OtherGuarantorCW.class) + "/" + loanID);
		killAnchor(guard2Download);
		if(guarantor2Submitted == null)
		{
			String text = "N/A - Application not submitted";
			if(appSubmitted)
			{
				if(guarantor1Submitted == null)
					text = "N/A - Guarantor Form not supported";
				else
					text = "N/A - Only 1 Guarantor Required";
			}
			guard2Link.setText(text);
			guard2Download.setText(text);
			killL1ink(guard2Link);
		}
		else if(guarantor2Submitted)
		{
			guard2Link.setText("VIEW/edit");
			guard2Download.setText("Fetching Link ... ");
			WebAppHelper.getLoanMktService().getGuarantorFormDownloadLink(otherGuardFormID, guard2DownloadCallback);
		}
		else
		{
			guard2Link.setText("view/EDIT");
			guard2Download.setText("N/A - Form not submitted");
		}
		
		boolean allSubmitted = appSubmitted && (guarantor1Submitted != null && guarantor1Submitted) && (!hasNext || guarantor2Submitted);
		killAnchor(checkDownload);
		if(allSubmitted)
		{
			checkDownload.setText("Fetching Link ...");
			WebAppHelper.getLoanMktService().getRequirementsChecklistLink(loanID, checklistDownloadCallback);
		}
	}	
	
}
