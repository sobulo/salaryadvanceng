package com.fertiletech.addosser.admin.customerforms;

import java.util.HashMap;

import com.fertiletech.addosser.admin.WebAppHelper;
import com.fertiletech.addosser.client.GUIConstants;
import com.fertiletech.addosser.client.utils.PanelUtilities;
import com.fertiletech.addosser.shared.FormConstants;
import com.fertiletech.addosser.shared.FormConstants.FormValidators;
import com.fertiletech.addosser.shared.FormConstants.HirePeriods;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

public class GuarantorsFormPanel extends Composite{
	@UiField
	TextBox surnameTextBox;
	
	@UiField
	TextBox otherNamesTextBox;
	
	@UiField
	ListBox maritalStatus;
	
	
	@UiField
	TextBox primaryPhone;
	
	@UiField
	TextBox email;
	
	@UiField
	TextArea address;
	
	@UiField
	TextBox area;
	
	@UiField
	TextBox secondaryPhone;
		
	@UiField
	TextArea branch;
	
	@UiField
	TextBox officeNumber;
	
	@UiField
	TextBox officeEmail;
	
	@UiField
	TextBox rank;
	
	@UiField
	ListBox duration;
	
	@UiField
	TextArea job;
	
	@UiField
	TextBox employeeNumber;
	
	@UiField
	TextBox salary;
	
	@UiField
	ListBox salaryDueDate;
		
	@UiField
	DateBox dateOfBirth;
	
	
	@UiField
	ListBox companyList;
	
	@UiField
	Button saveButton;
	
	@UiField
	Button submitButton;
	
	@UiField
	ListBox sex;
	
	@UiField
	TextBox otherCompany;
	
	@UiField
	CheckBox companyType;
	
	@UiField
	TextBox annualSalary;
	
	public final static String NO_COMPANY_SELECTED = "SELECT EMPLOYER";
	public final static String ENTER_COMPANY = "OR enter company name here instead (click the checkbox to your right)";
	public final String INITIAL_SAVED_STYLE_NAME, INITIAL_SUBMIT_STYLE_NAME;
	
	private String loanId = null;
    
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<HashMap<String, String>> fetchFormDataCallback = new AsyncCallback<HashMap<String, String>>() {

        @Override
        public void onSuccess(HashMap<String, String> result) {
        	if(result.size() == 1 && result.containsKey(FormConstants.EMAIL_KEY))
        		email.setText(result.get(FormConstants.EMAIL_KEY));
        	else
        		setFormData(result); //TODO this will fail when we allow saving illegal input
        	
        	enableButtons(true);
        }

        @Override
        public void onFailure(Throwable caught) {
        	PanelUtilities.errorBox.show("Unable to retrieve your application form. Try refreshing your browser. " +
        			"Contact info@fertiletech.com if problems persist. <p> Error msg: <b>"+ caught.getMessage() + "</b></p>");
        	enableButtons(false);
        }
    };  
    
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<String[]> saveCallback = new AsyncCallback<String[]>() {

        @Override
        public void onSuccess(String[] result) {
        	PanelUtilities.infoBox.show("Values saved successfully. You may continue editing the guarantor form");
        	GWT.log("Save Loan ID: " + loanId);
        	enableButtons(true);
        }

        @Override
        public void onFailure(Throwable caught) {
        	PanelUtilities.errorBox.show("Unable to save values. Error was: " + caught.getMessage());
        	enableButtons(true);
        }
    };
    
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<String[]> submitCallback = new AsyncCallback<String[]>() {

        @Override
        public void onSuccess(String[] result) {
        	PanelUtilities.infoBox.show("Guarantor has been submmitted");
        }

        @Override
        public void onFailure(Throwable caught) {
        	PanelUtilities.errorBox.show("Unable to submit values. Error was: " + caught.getMessage());
        }
    };    

	private static GuarantorsFormPanelUiBinder uiBinder = GWT
			.create(GuarantorsFormPanelUiBinder.class);

	interface GuarantorsFormPanelUiBinder extends
			UiBinder<Widget, GuarantorsFormPanel> {
	}

	public GuarantorsFormPanel(boolean enableSaving) {
		initWidget(uiBinder.createAndBindUi(this));
		INITIAL_SAVED_STYLE_NAME = saveButton.getStyleName();
		INITIAL_SUBMIT_STYLE_NAME = submitButton.getStyleName();
		setupListBoxes(FormConstants.MARRIAGE_STATES, FormConstants.SEX);
		setupCompanyBox(FormConstants.COMPANY_LIST);
        FormConstants.setDateParameters(dateOfBirth);

        //setup button handlers
        if(enableSaving)
        {
	        saveButton.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					submitHandler(false);		
				}
			});
	        
	        submitButton.addClickHandler(new ClickHandler() {			
				@Override
				public void onClick(ClickEvent event) {
					submitHandler(true);	
				}
			});
        }
        else
        	disableEditing();
        setHelpTips();		
	}
	
	public GuarantorsFormPanel()
	{
		this(true);
	}

	private void submitFormData(HashMap<String, String> data, boolean isSubmit)
	{
		GWT.log("Loan ID: " + loanId);
		enableButtons(false); //prevent user from hitting save multiple times while waiting on server
			WebAppHelper.getLoanMktService().storeGurantorApplicationData(
					data, loanId, isSubmit, isSubmit? submitCallback : saveCallback);
	}
	
	private void fetchFormData()
	{
		WebAppHelper.getLoanMktService().getStoredGuarantorApplication(
				loanId, fetchFormDataCallback);
	}
	
	
	public HashMap<String, String> getFormData()
	{
		HashMap<String, String> userData = new HashMap<String, String>();
		
		//personal info
		userData.put(FormConstants.APPLICANT_SURNAME_KEY, surnameTextBox.getText().trim());
		userData.put(FormConstants.APPLICANT_OTHERNAME_KEY, otherNamesTextBox.getText().trim());	
		userData.put(FormConstants.MARITAL_STATUS_KEY, maritalStatus.getValue(maritalStatus.getSelectedIndex()));
		userData.put(FormConstants.PRIMARY_PHONE_KEY, primaryPhone.getText().trim());
		userData.put(FormConstants.EMAIL_KEY, email.getText());
		userData.put(FormConstants.ADDRESS_KEY, address.getText().trim());
		userData.put(FormConstants.BUS_STOP_KEY, area.getText().trim());
		userData.put(FormConstants.SECONDARY_PHONE_KEY, secondaryPhone.getText().trim());
		String dateStr = GUIConstants.DEFAULT_DATEBOX_FORMAT.format(dateOfBirth, dateOfBirth.getValue());
		userData.put(FormConstants.DATE_OF_BIRTH_KEY, dateStr);
		userData.put(FormConstants.SEX_KEY, sex.getValue(sex.getSelectedIndex()));

		
		//employment info
		String employer = companyList.getValue(companyList.getSelectedIndex());
		if(employer.equals(NO_COMPANY_SELECTED))
			employer = companyType.getValue()? otherCompany.getText().trim() : null; 
		userData.put(FormConstants.EMPLOYER_KEY, employer);		
		userData.put(FormConstants.BRANCH_KEY, branch.getText().trim());
		userData.put(FormConstants.OFFICE_PHONE_KEY, officeNumber.getText().trim());
		userData.put(FormConstants.OFFICE_EMAIL_KEY, officeEmail.getText().trim());
		userData.put(FormConstants.JOB_RANK_KEY, rank.getText().trim());
		userData.put(FormConstants.HIRE_DURATION_KEY, duration.getValue(duration.getSelectedIndex()));
		userData.put(FormConstants.JOB_TITLE_KEY, job.getText().trim());
		userData.put(FormConstants.EMPLOYEE_ID_KEY, employeeNumber.getText().trim());		
		userData.put(FormConstants.SALARY_KEY, salary.getText().trim());
		userData.put(FormConstants.SALARY_DATE_KEY, salaryDueDate.getValue(salaryDueDate.getSelectedIndex()));
		userData.put(FormConstants.ANNUAL_SALARY_KEY, annualSalary.getText().trim());		
		return userData;
	}
	
	public void setFormData( HashMap<String, String> myData) {
		//personal info
		surnameTextBox.setText(myData.get(FormConstants.APPLICANT_SURNAME_KEY));
		otherNamesTextBox.setText(myData.get(FormConstants.APPLICANT_OTHERNAME_KEY));
		selectAListBoxItem(maritalStatus, myData.get(FormConstants.MARITAL_STATUS_KEY));
		primaryPhone.setText(myData.get(FormConstants.PRIMARY_PHONE_KEY));
		email.setText(myData.get(FormConstants.EMAIL_KEY));
		address.setText(myData.get(FormConstants.ADDRESS_KEY));
		area.setText(myData.get(FormConstants.BUS_STOP_KEY));
		secondaryPhone.setText(myData.get(FormConstants.SECONDARY_PHONE_KEY));
		dateOfBirth.getTextBox().setValue( myData.get(FormConstants.DATE_OF_BIRTH_KEY));
		selectAListBoxItem(sex, myData.get(FormConstants.SEX_KEY));
		
		//employee info
		branch.setText(myData.get(FormConstants.BRANCH_KEY));
		officeNumber.setText(myData.get(FormConstants.OFFICE_PHONE_KEY));
		officeEmail.setText(myData.get(FormConstants.OFFICE_EMAIL_KEY));
		rank.setText(myData.get(FormConstants.JOB_RANK_KEY));
		selectAListBoxItem(duration, myData.get(FormConstants.HIRE_DURATION_KEY));
		job.setText(myData.get(FormConstants.JOB_TITLE_KEY));
		employeeNumber.setText(myData.get(FormConstants.EMPLOYEE_ID_KEY));		
		salary.setText(myData.get(FormConstants.SALARY_KEY));
		selectAListBoxItem(salaryDueDate, myData.get(FormConstants.SALARY_DATE_KEY));
		selectAListBoxItem(companyList, myData.get(FormConstants.EMPLOYER_KEY));
		GWT.log("RECEIVED: " + myData.get(FormConstants.EMPLOYER_KEY));
		GWT.log("LISTBOX val: " + companyList.getValue(companyList.getSelectedIndex()));
		if(companyList.getValue(companyList.getSelectedIndex()).equals(NO_COMPANY_SELECTED))
		{
			GWT.log("made it in here");
			companyList.setEnabled(false);
			otherCompany.setEnabled(true);
			otherCompany.setText(myData.get(FormConstants.EMPLOYER_KEY));
			companyType.setValue(true);
		}
		else
		{
			companyList.setEnabled(true);
			otherCompany.setEnabled(false);
			otherCompany.setText(ENTER_COMPANY);
			companyType.setValue(false);
		}
		annualSalary.setText(myData.get(FormConstants.ANNUAL_SALARY_KEY));		
	}
	
	public void selectAListBoxItem(ListBox l, String val)
	{
		for( int i = 0; i < l.getItemCount(); i++)
			if(l.getValue(i).equals(val))
			{
				l.setSelectedIndex(i);
				break;
			}
	}
	
	
	public void setupListBoxes(String[] marriageStates, String[] sexTypes) {
		for(int i = 1; i <= 31; i++) //magic number 31 is max number of days in a month
			salaryDueDate.addItem(String.valueOf(i));
				
		for(int i= 0; i< marriageStates.length; i++)
			maritalStatus.addItem(marriageStates[i]);
		
		for(String s : sexTypes)
			sex.addItem(s);
		
		for( HirePeriods p : HirePeriods.values())
			duration.addItem(p.toString());
	}
	
	public void setupCompanyBox(final String[] companyListVals)
	{
		companyType.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
			
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				if(event.getValue() == true)
				{
					//disable list
					companyList.setEnabled(false);
					selectAListBoxItem(companyList, NO_COMPANY_SELECTED);
					
					//enable textbox
					otherCompany.setEnabled(true);
					otherCompany.setText(null);
				}
				else
				{
					//enable list
					companyList.setEnabled(true);
					
					//disable textBox
					otherCompany.setText(ENTER_COMPANY);
					otherCompany.setEnabled(false);
				}
				
			}
		});

		companyList.addItem(NO_COMPANY_SELECTED);
		for (int counter = 0; counter < companyListVals.length; counter++) {
			companyList.addItem(companyListVals[counter]);
		}		
	}
	
	public void enableButtons(boolean enabled)
	{
		if(enabled)
		{
			submitButton.setStyleName("blue button");
			saveButton.setStyleName("black button");
		}
		else
		{
			saveButton.setStyleName(INITIAL_SAVED_STYLE_NAME);
			submitButton.setStyleName(INITIAL_SUBMIT_STYLE_NAME);
		}
		saveButton.setEnabled(enabled);
		submitButton.setEnabled(enabled);
	}

	public void submitHandler(boolean submmitted) {
		HashMap<String, String> customerInfo = getFormData();
		StringBuilder errorMessage = new StringBuilder();
		
		for(String key : FormConstants.nameFieldsMap.keySet())
		{
			FormValidators[] validators = FormConstants.requiredFieldsMap.get(key);
			
			for(FormValidators v : validators)
				v.validate(key, customerInfo.get(key), errorMessage, submmitted);
		}
		
		if(errorMessage.length() > 0)
		{
			PanelUtilities.errorBox.show("Please fix the issues listed below then try again" +
					"<ol>" + errorMessage.toString() + "</ol>");
			return;
		}
		
		//go ahead and persist on the server
		submitFormData(customerInfo, submmitted);
	}

	public void setLoanID(String loanID) {
		this.loanId = loanID;
        fetchFormData();  
	}
	
	private void setHelpTips()
	{
		surnameTextBox.setTitle("Enter your surname/last name here");
		otherNamesTextBox.setTitle("Enter your first name & other names here");
		maritalStatus.setTitle("Select your marital status form the drop down list");
		primaryPhone.setTitle("Enter your personal phone number here. Customer reps will call you at this number");
		email.setTitle("SPEED BOOSTER: Please ensure you check your inbox regularly. First contact by a rep will be via this email address");
		address.setTitle("Enter your house/flat address here, i.e. where your bed is");
		area.setTitle("Enter the nearest bustop/area closest to your house/flat");
		secondaryPhone.setTitle("Enter another phone number here that you can be reached here in case your primary number is unreachable");
		sex.setTitle("Select your gender from drop down list");
		dateOfBirth.setTitle("Click on the datebox and select any date. Then edit the displayed date to match your date of birth.");
		companyList.setTitle("Select the company you currently work for from the drop down list");
		salary.setTitle("Enter your monthly salary here");
		salaryDueDate.setTitle("Enter the day your monthly salary is paid here");
		employeeNumber.setTitle("Enter your employment number here");
		branch.setTitle("Enter your company address/branch here");
		officeNumber.setTitle("Enter your company's office telephone number here");
		officeEmail.setTitle("SPEED BOOSTER: Enter your personal company email address here and speed up employment verification");
		rank.setTitle("Enter the position you occupy in your company here");
		duration.setTitle("Enter how long you've been employed in the company from the drop down list");
		job.setTitle("Enter a brief description of what your job entails here");
		annualSalary.setTitle("Enter your Annual Salary here");
	}
	
	private void disableEditing()
	{
		surnameTextBox.setEnabled(false);
		otherNamesTextBox.setEnabled(false);
		maritalStatus.setEnabled(false);
		primaryPhone.setEnabled(false);
		email.setEnabled(false);
		address.setEnabled(false);
		area.setEnabled(false);
		secondaryPhone.setEnabled(false);
		sex.setEnabled(false);
		dateOfBirth.setEnabled(false);
		companyList.setEnabled(false);
		salary.setEnabled(false);
		salaryDueDate.setEnabled(false);
		employeeNumber.setEnabled(false);
		branch.setEnabled(false);
		officeNumber.setEnabled(false);
		officeEmail.setEnabled(false);
		rank.setEnabled(false);
		duration.setEnabled(false);
		job.setEnabled(false);
		annualSalary.setEnabled(false);
	}
}
