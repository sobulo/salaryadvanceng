/**
 * 
 */
package com.fertiletech.addosser.admin.customerforms;

import java.util.Date;

import com.fertiletech.addosser.admin.WebAppHelper;
import com.fertiletech.addosser.client.DisplayWrapperChild;
import com.fertiletech.addosser.client.GUIConstants;
import com.fertiletech.addosser.client.utils.PanelUtilities;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.summatech.gwt.client.HourMinutePicker;
import com.summatech.gwt.client.HourMinutePicker.PickerFormat;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class BookAppointMent extends Composite implements DisplayWrapperChild{

	@UiField
	FlowPanel timeSlotPanel;
	@UiField
	Button bookButton;
	@UiField
	DateBox appointmentDate;
	HourMinutePicker picker;

    private final AsyncCallback<Date> getDateCallback = new AsyncCallback<Date>() {

        @Override
        public void onSuccess(Date result) {
        	GWT.log("received appointment date: " + result);
        	int hours = 0;
        	int minutes = 0;
        	if(result == null)
        	{
        		result = new Date();
        		hours = 9;
        		minutes = 30;
        	}
        	else
        	{
            	hours = result.getHours();
            	minutes = result.getMinutes();
        	}
        	
        	result.setHours(0);
        	result.setMinutes(0);
        	String suffix = "am";
        	if(hours >= 12)
        		suffix = "pm";
        	GWT.log("hours: " + hours + " minutes: " + minutes + " suffix: " + suffix);
        	picker.setTime(suffix, hours, minutes);
        	appointmentDate.setValue(result);
        	
        	appointmentDate.setEnabled(true);
        	timeSlotPanel.add(picker);
        	bookButton.setEnabled(true);
        }

        @Override
        public void onFailure(Throwable caught) {
        	PanelUtilities.errorBox.show("Unable to retrieve appointment date. Try refreshing browser. Error was: " + caught.getMessage());
        }
    };	
	
    private final AsyncCallback<Void> setDateCallback = new AsyncCallback<Void>() {

        @Override
        public void onSuccess(Void result) {
        	PanelUtilities.infoBox.show("Appointment booked succesfully. Please contact a loan officer if you need to reschedule");
        	bookButton.setEnabled(false);
        	appointmentDate.setEnabled(false);
        }

        @Override
        public void onFailure(Throwable caught) {
        	PanelUtilities.errorBox.show("Unable to book appointment. Try refreshing browser. Error was: " + caught.getMessage());
        }
    };	    
    
	private static BookAppointMentUiBinder uiBinder = GWT
			.create(BookAppointMentUiBinder.class);

	interface BookAppointMentUiBinder extends UiBinder<Widget, BookAppointMent> {
	}

	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	 *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public BookAppointMent() {
		initWidget(uiBinder.createAndBindUi(this));
		bookButton.setEnabled(false);
		picker = new HourMinutePicker(PickerFormat._24_HOUR, 8, 16, 4);
		appointmentDate.setFormat(GUIConstants.DEFAULT_DATEBOX_FORMAT);
		appointmentDate.setEnabled(false);
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.HasValueChangeHandlers#addValueChangeHandler(com.google.gwt.event.logical.shared.ValueChangeHandler)
	 */
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<Boolean> handler) {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.client.apply.forms.DisplayWrapperChild#getFormWidget()
	 */
	@Override
	public Widget getFormWidget() {
		return this;
	}

	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.client.apply.forms.DisplayWrapperChild#setLoanID(java.lang.String)
	 */
	@Override
	public void setLoanID(final String loanID) {
		GWT.log("**Calling get appointment date**");
		WebAppHelper.getLoanMktService().getAppointmentDate(loanID, getDateCallback);
		bookButton.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				Date d = appointmentDate.getValue();					
				d.setHours(picker.getHour());
				d.setMinutes(picker.getMinute());
				if(d.before(new Date()))
				{
					PanelUtilities.errorBox.show("Error: selected appointment slot is in the past");
					return;
				}

				GWT.log("Sending date: " + d);
				bookButton.setEnabled(false);
				WebAppHelper.getLoanMktService().setAppointmentDate(loanID, d, setDateCallback);
			}
		});
	}
}
