/**
 * 
 */
package com.fertiletech.addosser.admin.customerforms;

import com.fertiletech.addosser.admin.WebAppHelper;
import com.fertiletech.addosser.client.DisplayWrapperChild;
import com.fertiletech.addosser.client.utils.PanelUtilities;
import com.fertiletech.addosser.shared.DTOConstants;
import com.fertiletech.addosser.shared.FormConstants.FormTypes;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.SimplePanel;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class DisplayWrapperPanel extends Composite implements ValueChangeHandler<Boolean>{
	private Panel displayArea;
	DisplayWrapperChild formPanel;
	FormTypes type;
	private final static HTML TEMP_DISPLAY = new HTML("Loading display, please wait ...");
	String[] loanIDs;

    // Create an asynchronous callback to handle the result.
    final AsyncCallback<String> defaultCallback = new AsyncCallback<String>() 
    {
        @Override
        public void onSuccess(String result) {
        	displayArea.clear();
        	if(result == null)
        		displayArea.add(new HTML("Panel not properly setup, try refreshing your browser contact support if problem persists"));
        	else
        		displayArea.add(new ScrollPanel(new HTML(result)));
        }

        @Override
        public void onFailure(Throwable caught) {
        	PanelUtilities.errorBox.show("Error displaying your application form: " + caught.getMessage());
        }
    };  
    
    final AsyncCallback<String[]> idCallback = new AsyncCallback<String[]>() 
    {
        @Override
        public void onSuccess(String[] result) {
        	displayArea.clear();
    		loanIDs = result;
        	if(Boolean.valueOf(result[DTOConstants.SUBMITTED_KEY_IDX]) == true && FormTypes.LOAN_FORM == type)
        		setToReadOnlyMode(loanIDs[DTOConstants.FORM_KEY_IDX]);
        	else
        	{
        		GWT.log("in here 2 to create");
        		if(type == FormTypes.LOAN_FORM)
        		{
        			formPanel = new LoanApplicationFormPanel();
        			formPanel.setLoanID(loanIDs[DTOConstants.FORM_KEY_IDX]);
        		}
        		else if(type == FormTypes.ACKNOWLEDGEMENT_FORM)
        		{
        			formPanel = new CustomeAcknowledgementPanel();
        			formPanel.setLoanID(loanIDs[DTOConstants.LOAN_KEY_IDX]);
    			}
        		else if(type == FormTypes.PRINT_FORM)
        		{
        			formPanel = new PrintFormsPanel();
        			formPanel.setLoanID(loanIDs[DTOConstants.LOAN_KEY_IDX]);
        		}
        		else if(type == FormTypes.APPOINTMENT_FORM)
        		{
        			formPanel = new BookAppointMent();
        			formPanel.setLoanID(loanIDs[DTOConstants.LOAN_KEY_IDX]);
        		}
        		else
        		{
        			displayArea.add(new HTML("Error, panel configuration looks wrong. Please contact support"));
        			return;
        		}
        		
    			formPanel.addValueChangeHandler(getSwitchHandler());
    			
    			displayArea.add(formPanel.getFormWidget());
    			//d
        	}
        }

        @Override
        public void onFailure(Throwable caught) {
        	PanelUtilities.errorBox.show("Error displaying your application form: " + caught.getMessage());
        }
    };    
    
	public DisplayWrapperPanel(FormTypes t)
	{
		type = t;
		if(FormTypes.LOAN_FORM == t)
			displayArea = new LayoutPanel();
		else
			displayArea = new SimplePanel();
		initWidget(displayArea);
		displayArea.add(TEMP_DISPLAY);
		GWT.log("calling get loan id for " + t);
		WebAppHelper.getLoanMktService().getLoanID(idCallback);
		
	}
	
	private void setToReadOnlyMode(String formIdentifier)
	{
		displayArea.clear();
		displayArea.add(TEMP_DISPLAY);
		GWT.log("formPanel: " + formPanel);
		if(type == FormTypes.LOAN_FORM)
			WebAppHelper.getLoanMktService().loadFormDisplay(formIdentifier, true, defaultCallback);
	}
	
	private ValueChangeHandler<Boolean> getSwitchHandler()
	{
		return this;
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.ValueChangeHandler#onValueChange(com.google.gwt.event.logical.shared.ValueChangeEvent)
	 */
	@Override
	public void onValueChange(ValueChangeEvent<Boolean> event) {
		if(event.getValue() == true)
			setToReadOnlyMode(loanIDs[DTOConstants.FORM_KEY_IDX]);
	}
}
