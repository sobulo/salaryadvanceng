/**
 * 
 */
package com.fertiletech.addosser.admin.customerforms;

import com.fertiletech.addosser.admin.WebAppHelper;
import com.fertiletech.addosser.client.DisplayWrapperChild;
import com.fertiletech.addosser.client.utils.PanelUtilities;
import com.fertiletech.addosser.shared.FormConstants;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class PrintFormsPanel extends Composite implements DisplayWrapperChild{
	
	private static final String PRINT_URL = GWT.getHostPageBaseURL() + "print/form?" + FormConstants.LOAN_ID_PARAM + "="; 
	private final String HTML_STYLING = "<p style='border: 1px dashed white; padding:30px; margin:50px' >";
	private final HTML WAIT_FOR_PRINT = new HTML("Please wait, checking print status ...");
	private String loanID;
	private SimplePanel readyToShow;

	@UiField
	Anchor applicationFormLink;
	
    private final AsyncCallback<Boolean> showPrintCallback = new AsyncCallback<Boolean>() {

        @Override
        public void onSuccess(Boolean result) {
        	readyToShow.clear();
        	if(result == null)
        	{
        		readyToShow.add(new HTML(HTML_STYLING + "Please return to the first tab and submit aknowledgement. Try refreshing the browser if you recently submitted your aknowledgement</p>"));
        	}
        	else if(result)
        		readyToShow.add(PrintFormsPanel.this);
        	else
        		readyToShow.add(new HTML(HTML_STYLING + "Awaiting update from bank representative. Please check back later to print</p>"));
        }

        @Override
        public void onFailure(Throwable caught) {
        	PanelUtilities.errorBox.show("Unable to retrieve documents for printing. Try refreshing browser. Error was: " + caught.getMessage());
        }
    };
	

	private static PrintFormsPanelUiBinder uiBinder = GWT
			.create(PrintFormsPanelUiBinder.class);

	interface PrintFormsPanelUiBinder extends UiBinder<Widget, PrintFormsPanel> {
	}

	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	 *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public PrintFormsPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		readyToShow = new SimplePanel(WAIT_FOR_PRINT);
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.HasValueChangeHandlers#addValueChangeHandler(com.google.gwt.event.logical.shared.ValueChangeHandler)
	 */
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<Boolean> handler) {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.client.apply.forms.DisplayWrapperChild#getFormWidget()
	 */
	@Override
	public Widget getFormWidget() {	
		return readyToShow;
	}

	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.client.apply.forms.DisplayWrapperChild#setLoanID(java.lang.String)
	 */
	@Override
	public void setLoanID(String loanID) {
		this.loanID = loanID;
		applicationFormLink.setHref(PRINT_URL + loanID);
		WebAppHelper.getLoanMktService().okToPrint(loanID, showPrintCallback);
	}
}
