/**
 * 
 */
package com.fertiletech.addosser.admin.customerforms;

import com.fertiletech.addosser.admin.WebAppHelper;
import com.fertiletech.addosser.client.DisplayWrapperChild;
import com.fertiletech.addosser.client.utils.PanelUtilities;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class CustomeAcknowledgementPanel extends Composite implements DisplayWrapperChild{
	@UiField
	Button saveButton;
	
	@UiField
	CheckBox agreeToTerms;
	
	@UiField
	Anchor needHelp;
	
	String loanId;
	
	private final static String SAVED_ALREADY = "Saved acknowledgement already";
	private final static String NEED_HELP_MESSAGE = "Running into difficulties obtaining the required documentation? Please alert customer service when " +
			"we contact you. We've helped hundreds of customers with issues get the documentation they needed (pls note this may delay your application though";

	private static CustomeAcknowledgementPanelUiBinder uiBinder = GWT
			.create(CustomeAcknowledgementPanelUiBinder.class);

	interface CustomeAcknowledgementPanelUiBinder extends
			UiBinder<Widget, CustomeAcknowledgementPanel> {
	}
    
	// Create an asynchronous callback to handle the result.
    final AsyncCallback<Void> saveCallback = new AsyncCallback<Void>() {

        @Override
        public void onSuccess(Void result) {
        	PanelUtilities.infoBox.show("Acknowledgement saved. Please have your documents ready when you visit the bank");
    		saveButton.setText(SAVED_ALREADY);
        }

        @Override
        public void onFailure(Throwable caught) {
        	PanelUtilities.errorBox.show("Unable to save values. Error was: " + caught.getMessage());
        	saveButton.setEnabled(true);
        }
    };
    
    // Create an asynchronous callback to handle the result.
    private final AsyncCallback<Boolean> readCallback = new AsyncCallback<Boolean>() {

        @Override
        public void onSuccess(Boolean result) {
        	if(result)
        	{
        		agreeToTerms.setValue(true);
        		enableEdit(false);
        		saveButton.setText(SAVED_ALREADY);
        	}
        	else
        		enableEdit(true);
        }

        @Override
        public void onFailure(Throwable caught) {
        	PanelUtilities.errorBox.show("Unable to save values. Error was: " + caught.getMessage());
        	saveButton.setEnabled(true);
        }
    };
    
    public void enableEdit(boolean editable)
    {
		agreeToTerms.setEnabled(editable);
		saveButton.setEnabled(editable);    	
    }
    
	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	 *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public CustomeAcknowledgementPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		enableEdit(false);
		GWT.log("In here but not showing");
		saveButton.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				if(agreeToTerms.getValue())
				{
					enableEdit(false);
					WebAppHelper.getLoanMktService().setCustomerAck(loanId, saveCallback);
				}
				else
					PanelUtilities.errorBox.show("You must confirm your documents are ready by clicking the checkbox");
			}
		});
		needHelp.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				PanelUtilities.infoBox.show(NEED_HELP_MESSAGE);
			}
		});
	}
	
	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.HasValueChangeHandlers#addValueChangeHandler(com.google.gwt.event.logical.shared.ValueChangeHandler)
	 */
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<Boolean> handler) {
		return null;
	}
	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.client.apply.forms.DisplayWrapperChild#getFormWidget()
	 */
	@Override
	public Widget getFormWidget() {
		return this;
	}
	
	
	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.client.apply.forms.DisplayWrapperChild#setLoanID(java.lang.String)
	 */
	@Override
	public void setLoanID(String loanID) {
		this.loanId = loanID;
		
		//finally, lets go get the rest of the data
		WebAppHelper.getLoanMktService().getCustomerAck(loanID, readCallback);
	}
}
