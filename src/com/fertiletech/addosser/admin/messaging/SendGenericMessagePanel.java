/**
 * 
 */
package com.fertiletech.addosser.admin.messaging;



import com.fertiletech.addosser.admin.util.text.RichTextToolbar;
import com.fertiletech.addosser.client.MessagingManager;
import com.fertiletech.addosser.client.MessagingManagerAsync;
import com.fertiletech.addosser.shared.FormConstants.CustomMessageTypes;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class SendGenericMessagePanel extends Composite implements ClickHandler
{

	@UiField RadioButton sendEmail;
	@UiField RadioButton sendSMS;
	@UiField SimplePanel messageSlot;
	@UiField Button sendMessage;
	@UiField Label status;
	
	private RichTextArea area;
	

	
	private static SendGenericMessagePanelUiBinder uiBinder = GWT
			.create(SendGenericMessagePanelUiBinder.class);

	interface SendGenericMessagePanelUiBinder extends
			UiBinder<Widget, SendGenericMessagePanel> {
	}	
	
	MessagingManagerAsync messageService = GWT.create(MessagingManager.class);


    
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<Void> sendMessageCallBack =
            new AsyncCallback<Void>() {
        @Override
        public void onFailure(Throwable caught) {
        	status.setText(caught.getMessage());
            setEnableStatus4Clickables(true);
        }

        @Override
        public void onSuccess(Void result) {
        	status.setText("Scheduled sending of messages succesfully");
            setEnableStatus4Clickables(true);
        }
    };	    
	
	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	 *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public SendGenericMessagePanel() {
		initWidget(uiBinder.createAndBindUi(this));
		
		//default values for radio Buttons
		sendEmail.setValue(true);
		
		//initialize click handlers for radioButtons and send message button

		sendEmail.addClickHandler(this);
		sendSMS.addClickHandler(this);
		sendMessage.addClickHandler(this);
		        
        //setup text area for generic messages
        messageSlot.add(getRichTextArea());
        messageSlot.setVisible(true);
		
	}
	
	public void setEnableStatus4Clickables(boolean status)
	{
		sendMessage.setEnabled(status);
		sendSMS.setEnabled(status);
		sendEmail.setEnabled(status);
	}


	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		/**
		 * Noteworthy, addresses are hacky. studentBox gives you a unique list of addresses
		 * but then there are cases where we want to email different reports to the same addresses
		 * in which case we rely on addressToLoginMap to help identify the different students that 
		 * we'll be generating custom reports for. And oh did we mention messaging for custom reports
		 * is slightly hacky as well but that's partly because of the problem defination. Only thing to know
		 * on that front is d messages are built on the server. This widget simply goes, hey i need a report emailed
		 * for segun and then on server side we figure out how to build a report 4 segun.
		 */
		boolean isEmail = sendEmail.getValue();
		if(event.getSource() == sendMessage)
		{
			String[] msgParts = new String[2];
			msgParts[0] = area.getText();
			if(isEmail)
				msgParts[1] = area.getHTML();
			
			if(msgParts[0].length() == 0)
			{
				status.setText("Nothing to send, message is empty");
				return;
			}
			
			//build addresses
			String[] addyList = {"segun.sobulo@gmail.com", "sobulo@fertiletech.com"};
			
			//send message
			setEnableStatus4Clickables(false);
			status.setText("Making server request to send messages, please wait ...");
			messageService.sendMessage(CustomMessageTypes.GENERIC_MESSAGE, addyList, msgParts, isEmail, sendMessageCallBack);
		}
	}

	
	public Widget getRichTextArea()
	{
		area = new RichTextArea();
	    RichTextToolbar tb = new RichTextToolbar(area, "Enter Message below");
	    VerticalPanel p = new VerticalPanel();
	    p.add(tb);
	    p.add(area);
	    
	    area.setHeight("7em");
	    area.setWidth("99%");
	    tb.setWidth("100%");
	    p.setWidth("100%");		
	    return p;	
	}

	

}
