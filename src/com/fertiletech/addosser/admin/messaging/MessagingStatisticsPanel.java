/**
 * 
 */
package com.fertiletech.addosser.admin.messaging;

import java.util.HashMap;
import java.util.List;

import com.fertiletech.addosser.client.GUIConstants;
import com.fertiletech.addosser.client.MessagingManager;
import com.fertiletech.addosser.client.MessagingManagerAsync;
import com.fertiletech.addosser.shared.table.TableMessage;
import com.fertiletech.addosser.shared.table.TableMessageHeader;
import com.fertiletech.addosser.shared.table.TableMessageHeader.TableMessageContent;
import com.fertiletech.addosser.table.MessageListToGrid;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class MessagingStatisticsPanel extends Composite implements ClickHandler{
	
	@UiField ListBox type;
	@UiField TextBox description;
	@UiField TextBox max;
	@UiField TextBox daily;
	@UiField TextBox lastDate;
	@UiField TextBox remaining;
	@UiField TextBox used;
	@UiField Button fetchInfo;
	@UiField ScrollPanel tableSlot;
	@UiField Label status;
	
	MessagingManagerAsync messagingService = GWT.create(MessagingManager.class);

	private static MessagingStatisticsPanelUiBinder uiBinder = GWT
			.create(MessagingStatisticsPanelUiBinder.class);

	interface MessagingStatisticsPanelUiBinder extends
			UiBinder<Widget, MessagingStatisticsPanel> {
	}

    // Create an asynchronous callback to handle the result.
    final AsyncCallback<HashMap<String, String>> listboxCallBack =
            new AsyncCallback<HashMap<String, String>>() {
        @Override
        public void onFailure(Throwable caught) {
        	status.setText(caught.getMessage());
        }

        @Override
        public void onSuccess(HashMap<String, String> result) {
        	type.setEnabled(true);
        	fetchInfo.setEnabled(true);
        	for(String controllerId : result.keySet())
        		type.addItem(result.get(controllerId), controllerId);
            status.setText("Fetched list of controllers successfully");
        }
    };	
	
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<List<TableMessage>> infoCallBack =
            new AsyncCallback<List<TableMessage>>() {
        @Override
        public void onFailure(Throwable caught) {
        	fetchInfo.setEnabled(true);
        	status.setText(caught.getMessage());
        }

        @Override
        public void onSuccess(List<TableMessage> result) {
        	TableMessage headerMessage = result.remove(0);
        	TableMessageHeader.sort(result, 0, TableMessageContent.DATE, false);
        	result.add(0, headerMessage);
        	fetchInfo.setEnabled(true);
        	displayInfo(result);
            status.setText("Fetched controller information successfully");
        }
    };
    
    

	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	 *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public MessagingStatisticsPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		type.setEnabled(false);
		fetchInfo.setEnabled(false);
		disableTextFields();
		fetchInfo.addClickHandler(this);
		messagingService.getMessagingControllerNames(listboxCallBack);
	}
	
	private void displayInfo(List<TableMessage> messages)
	{
		TableMessage info = messages.remove(0);
		description.setText(info.getText(0));
		lastDate.setText(GUIConstants.DEFAULT_DATE_FORMAT.format(info.getDate(0)));
		max.setValue(GUIConstants.DEFAULT_NUMBER_FORMAT.format(info.getNumber(0)));
		daily.setValue(GUIConstants.DEFAULT_NUMBER_FORMAT.format(info.getNumber(1)));
		used.setValue(GUIConstants.DEFAULT_NUMBER_FORMAT.format(info.getNumber(2)));
		remaining.setValue(GUIConstants.DEFAULT_NUMBER_FORMAT.format(info.getNumber(0) - info.getNumber(2)));
		
		//add usage history
		//if(messages.size() > 10)
		tableSlot.setHeight(Math.min(24 * messages.size(), 250) + "px" );
		tableSlot.clear();
		tableSlot.add(new MessageListToGrid(messages));
		GWT.log("Height: " + tableSlot.getOffsetHeight() + " Num of Messages: " + messages.size());
	}
	
	private void disableTextFields()
	{
		description.setEnabled(false);
		max.setEnabled(false);
		daily.setEnabled(false);
		lastDate.setEnabled(false);
		remaining.setEnabled(false);
		used.setEnabled(false);
	}
	
	private void clearTextFields()
	{
		description.setValue(null);
		max.setValue(null);
		daily.setValue(null);
		lastDate.setValue(null);
		remaining.setValue(null);
		used.setValue(null);
		tableSlot.clear();
		tableSlot.add(new Label("Fetching info, please wait ..."));
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		fetchInfo.setEnabled(false);
		status.setText("Fetching message controller information, please wait ...");
		messagingService.getControllerDetails(type.getValue(type.getSelectedIndex()), infoCallBack);
	}

}
