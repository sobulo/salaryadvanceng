package com.fertiletech.addosser.admin.app.content;

import java.util.Date;
import java.util.List;

import com.fertiletech.addosser.admin.ConsumerLeadHandler;
import com.fertiletech.addosser.admin.RowRefresher;
import com.fertiletech.addosser.admin.WebAppHelper;
import com.fertiletech.addosser.admin.app.ContentWidget;
import com.fertiletech.addosser.admin.app.HelpPageGenerator;
import com.fertiletech.addosser.client.utils.PanelUtilities;
import com.fertiletech.addosser.client.utils.SearchDateBox;
import com.fertiletech.addosser.shared.table.TableMessage;
import com.fertiletech.addosser.table.ShowcaseTable;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.Widget;

public class ApplicationBlotterCW extends ContentWidget{
	

	public ApplicationBlotterCW() {
		super("By Date", "Loans requested within a given date range");
	}

	@Override
	public Widget onInitialize() {
		DockLayoutPanel container = new DockLayoutPanel(Unit.PX);
		final SearchDateBox searchBox = new SearchDateBox();
		final ShowcaseTable searchResults = new ShowcaseTable();
		searchResults.addValueChangeHandler(new ConsumerLeadHandler(new RowRefresher() {
			
			@Override
			public void refresh(TableMessage m) {
				searchResults.refresh(m);	
				//searchResults.refreshAll();
			}
		}));
		searchBox.addClickHandler(new ClickHandler() {
			
			private AsyncCallback<List<TableMessage>> searchCallback = new AsyncCallback<List<TableMessage>>() {

				@Override
				public void onFailure(Throwable caught) {
					PanelUtilities.errorBox.show("Error retrieving search results. <br/> " + caught.getMessage());
					searchBox.setEnabled(true);
				}

				@Override
				public void onSuccess(List<TableMessage> result) {
					searchResults.showTable(result);
					searchBox.setEnabled(true);
				}
			};

			@Override
			public void onClick(ClickEvent event) {
				Date[] params = searchBox.getSearchDates();
				if(params == null) return;
				searchBox.setEnabled(false);
				WebAppHelper.getLoanMktService().getSaleLeads(params[0], params[1], searchCallback );
			}
		});
		
		container.addNorth(searchBox, 50);
		container.add(searchResults);
		return container;
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
		GWT.runAsync(ApplicationBlotterCW.class, new RunAsyncCallback() {
			
			@Override
			public void onSuccess() {
				callback.onSuccess(onInitialize());
			}
			
			@Override
			public void onFailure(Throwable reason) {
				callback.onFailure(reason);
			}
		});
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_WELCOME_URL;
	}

}
