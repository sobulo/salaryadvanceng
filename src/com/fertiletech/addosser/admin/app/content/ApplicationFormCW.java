package com.fertiletech.addosser.admin.app.content;

import com.fertiletech.addosser.admin.app.ContentWidget;
import com.fertiletech.addosser.admin.app.HelpPageGenerator;
import com.fertiletech.addosser.admin.customerforms.LoanApplicationFormPanel;
import com.fertiletech.addosser.shared.oauth.CWArguments;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class ApplicationFormCW extends ContentWidget implements CWArguments{
	
	String savedId = null;

	LoanApplicationFormPanel appForm;
	public ApplicationFormCW() {
		super("Applicant Form", "Edit Application Form");
	}

	@Override
	public Widget onInitialize() {
		appForm = new LoanApplicationFormPanel();
		if(savedId != null)
			appForm.setLoanID(savedId);
		return appForm;
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
		GWT.runAsync(ApplicationFormCW.class, new RunAsyncCallback() {

			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}

			@Override
			public void onSuccess() {
				callback.onSuccess(onInitialize());
			}
		});
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_WELCOME_URL;
	}

	@Override
	public void setParameterValues(String args) {
		if(appForm == null)
			savedId = args;
		else
			appForm.setLoanID(args);
	}

}
