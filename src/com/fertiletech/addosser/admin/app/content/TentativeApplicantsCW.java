package com.fertiletech.addosser.admin.app.content;

import com.fertiletech.addosser.admin.NewLoanApplicantViewer;
import com.fertiletech.addosser.admin.app.ContentWidget;
import com.fertiletech.addosser.admin.app.HelpPageGenerator;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class TentativeApplicantsCW extends ContentWidget{

	public TentativeApplicantsCW() {
		super("Tentative Applicants", "View information entered so far by new applicants");
	}

	@Override
	public Widget onInitialize() {
		return new NewLoanApplicantViewer();
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
		GWT.runAsync(TentativeApplicantsCW.class, new RunAsyncCallback() {

			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}

			@Override
			public void onSuccess() {
				callback.onSuccess(onInitialize());
			}
		});
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_WELCOME_URL;
	}

}
