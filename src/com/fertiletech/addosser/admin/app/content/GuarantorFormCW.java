package com.fertiletech.addosser.admin.app.content;

import com.fertiletech.addosser.admin.app.ContentWidget;
import com.fertiletech.addosser.admin.app.HelpPageGenerator;
import com.fertiletech.addosser.admin.customerforms.GuarantorsFormPanel;
import com.fertiletech.addosser.shared.oauth.CWArguments;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class GuarantorFormCW extends ContentWidget implements CWArguments{
	public GuarantorFormCW() {
		this("First Guarantor");
	}

	public GuarantorFormCW(String name) {
		super(name, "View/Edit Guarantor Information");
	}	
	
	String saveId;
	GuarantorsFormPanel form;
	@Override
	public void setParameterValues(String args) {
		if(form == null)
			saveId = args;
		else
			form.setLoanID(saveId);
	}

	@Override
	public Widget onInitialize() {
		form = new GuarantorsFormPanel();
		if(saveId != null)
			form.setLoanID(saveId);
		return form;
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
		GWT.runAsync(GuarantorFormCW.class, new RunAsyncCallback() {

			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}

			@Override
			public void onSuccess() {
				callback.onSuccess(onInitialize());
			}
		});
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_WELCOME_URL;
	}

}
