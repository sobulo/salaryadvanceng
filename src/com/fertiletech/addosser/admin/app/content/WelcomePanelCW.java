package com.fertiletech.addosser.admin.app.content;

import com.fertiletech.addosser.admin.WelcomePanel;
import com.fertiletech.addosser.admin.app.ContentWidget;
import com.fertiletech.addosser.admin.app.HelpPageGenerator;
import com.fertiletech.addosser.client.MyRefreshCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;


public class WelcomePanelCW extends ContentWidget implements MyRefreshCallback{

	WelcomePanel display;
	public WelcomePanelCW() {
		super("Welcome", "Use this page to login and out of the portal");
		display = new WelcomePanel();
	}

	@Override
	public Widget onInitialize() {
		return display;
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
		callback.onSuccess(onInitialize());
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_WELCOME_URL;
	}
	
	public WelcomePanel getPanel()
	{
		return display;
	}

	@Override
	public void updateScreen() {
		display.updateScreen();
	}

}
