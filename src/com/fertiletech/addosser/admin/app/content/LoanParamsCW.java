package com.fertiletech.addosser.admin.app.content;

import com.fertiletech.addosser.admin.LoanMarketingParametersPanel;
import com.fertiletech.addosser.admin.app.ContentWidget;
import com.fertiletech.addosser.admin.app.HelpPageGenerator;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class LoanParamsCW extends ContentWidget{
	
	public LoanParamsCW() {
		super("Loan Params", "Parameters used by salaryadvanceng.com for loan applications");
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
		GWT.runAsync(LoanParamsCW.class, new RunAsyncCallback() {

			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}

			@Override
			public void onSuccess() {
				callback.onSuccess(onInitialize());
			}
		});
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_WELCOME_URL;
	}

	@Override
	public Widget onInitialize() {
		// TODO Auto-generated method stub
		return new LoanMarketingParametersPanel();
	}

}
