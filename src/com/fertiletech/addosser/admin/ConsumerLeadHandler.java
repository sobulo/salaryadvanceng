/**
 * 
 */
package com.fertiletech.addosser.admin;

import java.util.Date;

import com.fertiletech.addosser.admin.customerforms.ApplicationStatus;
import com.fertiletech.addosser.admin.util.text.RichTextToolbar;
import com.fertiletech.addosser.client.GUIConstants;
import com.fertiletech.addosser.client.utils.PanelUtilities;
import com.fertiletech.addosser.shared.DTOConstants;
import com.fertiletech.addosser.shared.FormHTMLDisplayBuilder;
import com.fertiletech.addosser.shared.exceptions.MissingEntitiesException;
import com.fertiletech.addosser.shared.table.TableMessage;
import com.fertiletech.addosser.shared.workstates.WorkflowStateInstance;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.ToggleButton;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class ConsumerLeadHandler implements ValueChangeHandler<TableMessage>{


	PopupPanel mainPopup;
	HTML summaryPanel;
	FlowPanel buttonPanel;
	VerticalPanel contentArea;
	SelectionButtonHandlers[] actionHandlersRefs;
	RowRefresher fresh;

	public ConsumerLeadHandler(RowRefresher r) {
		this.fresh = r;

        buttonPanel = new FlowPanel();
        summaryPanel = new HTML();
        
        //create Handlers
        actionHandlersRefs = new SelectionButtonHandlers[3];
        actionHandlersRefs[0] = new ViewRequestHandler();
        actionHandlersRefs[1] = new ViewCommentsHandler();
        actionHandlersRefs[2] = new AddCommentsHandler();
        //actionHandlersRefs[3] = new UpdateStatusHandler();
        //actionHandlersRefs[4] = new ViewDateHandler();
        
        Button viewButton = new Button("Application Details");
        viewButton.addClickHandler(actionHandlersRefs[0]);
        buttonPanel.add(viewButton);
        
        Button commentButton = new Button("Status History");
        commentButton.addClickHandler(actionHandlersRefs[1]);
        buttonPanel.add(commentButton);

        Button addCommentButton = new Button("Edit Status");
        addCommentButton.addClickHandler(actionHandlersRefs[2]);
        buttonPanel.add(addCommentButton);        
        
        /*
        Button appointmentButton = new Button("View Appointment");
        appointmentButton.addClickHandler(actionHandlersRefs[4]);
        buttonPanel.add(appointmentButton);                
        
        Button updateStatusButton = new Button("Update Application Status");
        updateStatusButton.addClickHandler(actionHandlersRefs[3]);
        buttonPanel.add(updateStatusButton);        
        */
        contentArea = new VerticalPanel();
        contentArea.add(summaryPanel);
        contentArea.add(buttonPanel);
        
        mainPopup = getNewPopup();
        mainPopup.add(contentArea);
	}
	
	private PopupPanel getNewPopup()
	{
        PopupPanel popupInit = new PopupPanel();
        popupInit.setGlassEnabled(true);
        popupInit.setAnimationEnabled(true);
        //popupInit.setStyleName("popupStyle");
        popupInit.setAutoHideEnabled(true);
        popupInit.setAutoHideOnHistoryEventsEnabled(true);
        return popupInit;
	}
	
	private void rowSelected(TableMessage m) {
		summaryPanel.setHTML(getSummaryHTML(m));
		
		for(int i = 0; i < actionHandlersRefs.length; i++)
			actionHandlersRefs[i].setMessage(m);
		
		mainPopup.center();
		mainPopup.show();
	}
	
	interface SelectionButtonHandlers extends ClickHandler
	{
		void fetchDataFromServer();
		String getDescription();
		void setMessage(TableMessage msg);
	}
	
	class ViewCommentsHandler extends ViewButtonHandler
	{

		@Override
		public void fetchDataFromServer() {
			WebAppHelper.LOAN_MKT_SERVICE.loadSaleLeadComments(selectedMsg.getMessageId(), fetchSubmittedFormCallback);
		}

		@Override
		public String getDescription() {
			return "comments";
		}
		
	}

	class ViewDateHandler extends ViewButtonHandler
	{

		@Override
		public void fetchDataFromServer() {
			viewArea.clear();
        	viewArea.add(new HTML("Decision tree classification - requires multiple operators"));
		}

		@Override
		public String getDescription() {
			return "Hot Tag";
		}
		
	}
	
	class ViewRequestHandler extends ViewButtonHandler
	{
		ApplicationStatus statusPanel;
		public ViewRequestHandler() {
			statusPanel = new ApplicationStatus(false);
			viewArea.clear();
			statusPanel.setSize("400px","400px");
			viewArea.add(statusPanel);
			
		}

		@Override
		public void fetchDataFromServer() {
        	//viewArea.add(statusPanel);
        	statusPanel.setLoanID(selectedMsg.getMessageId());
		}

		@Override
		public String getDescription() {
			return "Edit and download Addosser loan form";
		}
		
		protected void setupInitialDisplay()
		{
		}
		
	}
	
	class UpdateStatusHandler extends ViewButtonHandler
	{
		HTML statusMessage;
		ToggleButton confirmProspectButton, verifyDocumentsButton, approveLoanButton, fundLoanButton;
		Button moveToVerify, moveToDeny, moveToApprove, moveToPending, moveToFunded, nothingToDo;
		TextBox loanId;
		FlowPanel buttonContextPanel;
		final String NORMAL_BUTTON_STYLE;
		ToggleButton[] actionButtons;
		HandlerRegistration[] subButtonRegistrations;
		
	    final AsyncCallback<Integer> statusCallback = new AsyncCallback<Integer>() {

	        @Override
	        public void onSuccess(Integer result) {
	        	updateButtonContext(result);
	        	statusMessage.setText("Succesfully updated status");
	        }

	        @Override
	        public void onFailure(Throwable caught) {
	        	hide();
	        	PanelUtilities.errorBox.show("Unable to update application status. Error was: " + caught.getMessage());
	        }
	    };		
		
		public UpdateStatusHandler() {
			super();
			statusMessage = new HTML("Click on one of the buttons below, then confirm your selected action");
			VerticalPanel contentPanel = new VerticalPanel();
			buttonContextPanel = new FlowPanel();
			contentPanel.add(buttonContextPanel);
			contentPanel.add(statusMessage);
			viewArea.add(contentPanel);			

			confirmProspectButton = new ToggleButton("Prospect? Or not?", "Confirm/Deny as a Prospect");
			verifyDocumentsButton = new ToggleButton("Received All Documents?", "Confirm Documents Received");
			approveLoanButton = new ToggleButton("Approval Decided?", "Confirm approval status");
			fundLoanButton = new ToggleButton("Orbit Information?", "Confirm Orbit ID");   
			buttonPanel.add(confirmProspectButton);
			buttonPanel.add(verifyDocumentsButton);
			buttonPanel.add(approveLoanButton);
			buttonPanel.add(fundLoanButton);
			
			subButtonRegistrations = new HandlerRegistration[5]; //used in replacehandler, 1 for each moveTo.. button
			
			//used to achieve radio button effect via enableButton method
			actionButtons  = new ToggleButton[4];
			actionButtons[0] = confirmProspectButton;
			actionButtons[1] = verifyDocumentsButton;
			actionButtons[2] = approveLoanButton;
			actionButtons[3] = fundLoanButton;
			
			//these are added later on demand ... depending on the context?
			moveToVerify = new Button("Click here to confirm prospect");
			moveToDeny = new Button("Click here to deny application");
			moveToPending = new Button("Click here to indicate all documents received");
			moveToApprove = new Button("Click here to indicate loan approved!!");
			moveToFunded = new Button("Click here to save orbit ID");
			loanId = new TextBox();
			nothingToDo = new Button("Bye-Bye");
			nothingToDo.addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					hide();
				}
			});
			//store style name
			NORMAL_BUTTON_STYLE = moveToDeny.getStyleName();
			
			//specify context buttons to show
			confirmProspectButton.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
				
				@Override
				public void onValueChange(ValueChangeEvent<Boolean> event) {
					if(event.getValue() == true)
					{
						buttonContextPanel.add(moveToVerify);
						buttonContextPanel.add(moveToDeny);
					}
					else
						buttonContextPanel.clear();
				}
			});
			
			verifyDocumentsButton.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
				
				@Override
				public void onValueChange(ValueChangeEvent<Boolean> event) {
					if(event.getValue() == true)
					{
						buttonContextPanel.add(moveToPending);					
					}
					else
						buttonContextPanel.clear();
				}
			});
			
			approveLoanButton.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
				
				@Override
				public void onValueChange(ValueChangeEvent<Boolean> event) {
					if(event.getValue() == true)
					{
						buttonContextPanel.add(moveToApprove);
						buttonContextPanel.add(moveToDeny);
					}
					else
						buttonContextPanel.clear();
				}
			});
			
			fundLoanButton.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
				
				@Override
				public void onValueChange(ValueChangeEvent<Boolean> event) {
					if(event.getValue() == true)
					{
						buttonContextPanel.add(new Label("Orbit Loan Account#: "));
						buttonContextPanel.add(loanId);
						buttonContextPanel.add(moveToFunded);
					}
					else
						buttonContextPanel.clear();
				}
			});
		}
		
		@Override
		protected void setupInitialDisplay(){
			updateButtonContext(selectedMsg.getNumber(DTOConstants.TAB_IDENTIFIER_MSG_IDX).intValue());
		}
		
		private void enableButton(ToggleButton button)
		{
			//we use this to create a radio button effect. where only 1 can be selected at a time
			buttonContextPanel.clear();
			for (int i = 0; i < actionButtons.length; i++)
			{
				if(button == actionButtons[i])
					actionButtons[i].setEnabled(true);
				else
					actionButtons[i].setEnabled(false);
				actionButtons[i].setDown(false);				
			}
			//enable all subcontext buttons, won't be visible though since we've cleared context panel
			moveToVerify.setEnabled(true);
			moveToDeny.setEnabled(true);
			moveToPending.setEnabled(true);
			moveToApprove.setEnabled(true);
			moveToFunded.setEnabled(true);
		}
		
		private void updateButtonContext(int qID)
		{
			boolean isHot = false;
			moveToVerify.setStyleName(NORMAL_BUTTON_STYLE);
			moveToDeny.setStyleName(NORMAL_BUTTON_STYLE);
			
			//figure out what context and then setup buttons appropriately
			switch(qID)
			{
			case DTOConstants.HOT_IDX:
				isHot = true;
			case DTOConstants.COLD_IDX:
				/*if(isHot)
					moveToVerify.setStyleName("red");
				else
					moveToDeny.setStyleName("blue");*/
				enableButton(confirmProspectButton);
				break;
			case DTOConstants.VERIFY_IDX:
				enableButton(verifyDocumentsButton);
				break;
			case DTOConstants.APPROVE_IDX:
				enableButton(approveLoanButton);
				break;
			case DTOConstants.CLOSED_IDX:
				enableButton(fundLoanButton);
				break;
			default:
				PanelUtilities.infoBox.show("Terminal state. Nothing to do. Contact IT if you need to modify the application");
				break;
			}	
		}	
		
		@Override
		protected void replaceHandler()
		{
			int qID = selectedMsg.getNumber(DTOConstants.TAB_IDENTIFIER_MSG_IDX).intValue();
			
			if(qID < 0) //sanity check ... updatebuttoncontext method should prevent this from ever being true
				return;
			
			super.replaceHandler();
			
			for(int i = 0; i < subButtonRegistrations.length; i++)
			{
				if(subButtonRegistrations[i] != null)
				{
					subButtonRegistrations[i].removeHandler();
					subButtonRegistrations[i] = null;
				}
			}
			
			subButtonRegistrations[0] = moveToVerify.addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {					
					statusMessage.setHTML("<span style='color:green'>Moving application to verification pending queue. Please wait ...</span>");
					moveToVerify.setEnabled(false);
					WebAppHelper.LOAN_MKT_SERVICE.updateOpsQueue(selectedMsg.getMessageId(), DTOConstants.VERIFY_IDX, null, statusCallback);
				}
			});
			subButtonRegistrations[1] = moveToDeny.addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {					
					statusMessage.setHTML("<span style='color:orange'>Moving application to denied queue. Please wait ...</span>");
					moveToDeny.setEnabled(false);
					WebAppHelper.LOAN_MKT_SERVICE.updateOpsQueue(selectedMsg.getMessageId(), Integer.MIN_VALUE, null, statusCallback);
				}
			});
			subButtonRegistrations[2] = moveToPending.addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {					
					statusMessage.setHTML("<span style='color:green'>Moving application to pending approval queue. Please wait ...</span>");
					moveToPending.setEnabled(false);
					WebAppHelper.LOAN_MKT_SERVICE.updateOpsQueue(selectedMsg.getMessageId(), DTOConstants.APPROVE_IDX, null, statusCallback);
				}
			});
			subButtonRegistrations[3] = moveToApprove.addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {					
					statusMessage.setHTML("<span style='color:blue'>Moving application to verification pending queue. Please wait ...</span>");
					moveToApprove.setEnabled(false);
					WebAppHelper.LOAN_MKT_SERVICE.updateOpsQueue(selectedMsg.getMessageId(), DTOConstants.CLOSED_IDX, null, statusCallback);
				}
			});
			subButtonRegistrations[4] = moveToFunded.addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {					
					String[] loanIDArg = new String[1];
					loanIDArg[0] = loanId.getText();
					if(loanIDArg[0] == null || loanIDArg[0].trim().length() == 0)
					{
						statusMessage.setHTML("<span style='color:red'>You must specify a valid orbit loan account ID</span>");
						return;
					}
					statusMessage.setHTML("<span style='color:blue'>Moving application to funded state. Please wait ...</span>");
					moveToFunded.setEnabled(false);
					WebAppHelper.LOAN_MKT_SERVICE.updateOpsQueue(selectedMsg.getMessageId(), Integer.MAX_VALUE, loanIDArg, statusCallback);
				}
			});	
		}
		
		@Override
		public void fetchDataFromServer(){
			//TODO, there should be a cleaner way of achieving this
			if(selectedMsg.getNumber(DTOConstants.TAB_IDENTIFIER_MSG_IDX).intValue() < 0)
			{
				hide();
			}
		}		
	}
	
	class AddCommentsHandler extends ViewButtonHandler
	{
		RichTextArea richText;
		ListBox states;
		Button saveButton;
		HTML statusMessage;
		HandlerRegistration saveHandler;
		private final static int MIN_COMMENT_SIZE = 30;

	    // Create an asynchronous callback to handle the result.
	    final AsyncCallback<Void> saveCallback = new AsyncCallback<Void>() {

	        @Override
	        public void onSuccess(Void result) {
	        	//richText.setText(null); //clear
	        	selectedMsg.setText(DTOConstants.NOTE_IDX, states.getSelectedItemText());
	        	states.setEnabled(true);
	        	fresh.refresh(selectedMsg);
	        	hide();
	        	PanelUtilities.infoBox.show("Saved status succesfully for loan application belonging to: " + selectedMsg.getText(DTOConstants.FULL_NAME_IDX));
	        }

	        @Override
	        public void onFailure(Throwable caught) {
	        	hide();
	        	states.setEnabled(true);
	        	PanelUtilities.errorBox.show("Unable to save values. Error was: " + caught.getMessage());
	        }
	    };
	    
	    /**
		 * 
		 */
		public AddCommentsHandler() {
			super();
			statusMessage = new HTML();
			VerticalPanel contentPanel = new VerticalPanel();
			contentPanel.add(getRichTextArea());
			contentPanel.add(statusMessage);
			saveButton = new Button("Change Status");
			buttonPanel.add(saveButton);
			viewArea.clear();
			viewArea.add(contentPanel);			
		}
		
		@Override
		protected void setupInitialDisplay()
		{
			richText.setText(null);
			statusMessage.setHTML("Specify status along with comments " + selectedMsg.getText(DTOConstants.FULL_NAME_IDX) + "'s application. Then hit save button to move this application from current state");
		}
		
		private Widget getRichTextArea()
		{
			states = new ListBox();
			for(WorkflowStateInstance st: WorkflowStateInstance.values())
			{
				if(st.ordinal() <= WorkflowStateInstance.PRE_APPROVED_NO.ordinal())
					continue;
				states.addItem(st.getDisplayString(), st.toString());
			}
			richText = new RichTextArea();
		    RichTextToolbar tb = new RichTextToolbar(richText, "Enter comments below");
		    VerticalPanel p = new VerticalPanel();
		    p.add(tb);
		    p.add(states);
		    p.add(richText);
		    
		    richText.setHeight("10em");
		    richText.setWidth("99%");
		    tb.setWidth("100%");
		    p.setWidth("100%");		
		    return p;	
		}		
		
		@Override
		protected void replaceHandler()
		{
			super.replaceHandler();
			if(saveHandler != null)
				saveHandler.removeHandler();
			saveHandler = saveButton.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					String comment = richText.getText();
					if(comment == null || comment.length() < MIN_COMMENT_SIZE)
					{
						statusMessage.setHTML("<span style='color:red'>Too short. Please enter a more descriptive comment</span>");
						return;
					}
					states.setEnabled(false);
					WebAppHelper.LOAN_MKT_SERVICE.updateOpsQueue(selectedMsg.getMessageId(), states.getSelectedValue(), richText.getHTML(),  saveCallback);
				}
			});
		}
		
		@Override
		public void fetchDataFromServer(){
			AsyncCallback<String[]> callback = new AsyncCallback<String[]>() {
				
				@Override
				public void onSuccess(String[] result) {
					for(int index = 0; index < states.getItemCount(); index++)
						if(states.getValue(index).equals(result[0]))
							states.setSelectedIndex(index);
					richText.setHTML(result[1]);
				}
				
				@Override
				public void onFailure(Throwable caught) {
					PanelUtilities.errorBox.show("Unable to retrieve. Error was: " + caught.getMessage());
					
				}
			};
			WebAppHelper.LOAN_MKT_SERVICE.getSangDisplayStatus(selectedMsg.getMessageId(), callback );
		}
	}
	
	
	class ViewButtonHandler implements SelectionButtonHandlers
	{
		protected TableMessage selectedMsg;
		private PopupPanel viewPopup;
		private Button goBackButton;
		protected HorizontalPanel buttonPanel;
		private HandlerRegistration goBackRegistration;
		protected ScrollPanel viewArea;
		
		protected void hide()
		{
			viewPopup.hide();
		}

	    final AsyncCallback<String> fetchSubmittedFormCallback = new AsyncCallback<String>() 
	    {
	        @Override
	        public void onSuccess(String result) {
	        	viewArea.clear();
	        	if(result == null)
	        		viewArea.add(new HTML("Unable to retrieve application. Contact info@fertiletech.com"));
	        	else
	        		viewArea.add(new HTML(result));
	        }

	        @Override
	        public void onFailure(Throwable caught) {
	        	PanelUtilities.errorBox.show("Error displaying your application form: " + caught.getMessage());
	        }
	    };
	    
	    final AsyncCallback<Date> fetchAppointmentDateCallback = new AsyncCallback<Date>() 
	    {
	        @Override
	        public void onSuccess(Date result) {
	        	viewArea.clear();
	        	if(result == null)
	        		viewArea.add(new HTML("an appointment has not yet been booked"));
	        	else
	        		viewArea.add(new HTML("Appointment date is: " + result));
	        }

	        @Override
	        public void onFailure(Throwable caught) {
	        	if(caught instanceof MissingEntitiesException)
	        	{
	        		viewArea.clear();
	        		viewArea.add(new HTML("User has not yet booked an appointment and will not be able to do so until you have confirmed them as a prospect " +
	        				"If you've already moved this person as a prospect, please contact them to go ahead and book an appointment"));
	        	}
	        	PanelUtilities.errorBox.show("Error displaying your application form: " + caught.getMessage());
	        }
	    };			    
		
		public ViewButtonHandler() {
			viewPopup = getNewPopup();
			viewArea = new ScrollPanel();
			goBackButton = new Button("Go Back/Cancel");
			buttonPanel = new HorizontalPanel();
			buttonPanel.setSpacing(5);
			buttonPanel.add(goBackButton);
			VerticalPanel content = new VerticalPanel();
			content.add(viewArea);
			content.add(buttonPanel);
			viewPopup.add(content);
		}

		/* (non-Javadoc)
		 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
		 */
		@Override
		public void onClick(ClickEvent event) {
			GWT.log("Called click event for " + this + " value of msg: " + (selectedMsg == null));
			mainPopup.hide();
			setupInitialDisplay();
			replaceHandler();
			viewPopup.show();
			fetchDataFromServer();
		}
		
		protected void setupInitialDisplay()
		{
			viewArea.clear();
			viewArea.add(new HTML("Please wait, fetching " + getDescription() + " for " + selectedMsg.getText(DTOConstants.FULL_NAME_IDX)));			
		}
		
		public void fetchDataFromServer()
		{
			WebAppHelper.LOAN_MKT_SERVICE.loadFormDisplay(selectedMsg.getMessageId(), false, fetchSubmittedFormCallback);			
		}
		
		protected void replaceHandler()
		{
			if(goBackRegistration != null)
				goBackRegistration.removeHandler();
			//replace
			goBackButton.addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					viewPopup.hide();
					rowSelected(selectedMsg);
				}
			});			
		}
		
		public String getDescription()
		{
			return "loan application form";
		}

		/* (non-Javadoc)
		 * @see com.fertiletech.addosser.client.admin.SalesLeadRowHandler.SelectionButtonHandlers#setMessage(com.fertiletech.addosser.client.gui.util.table.TableMessage)
		 */
		@Override
		public void setMessage(TableMessage msg) {
			GWT.log("My referece: " + this);
			GWT.log("Received msg: " + (msg==null) + " and value of current msg: " + (this.selectedMsg==null));
			this.selectedMsg = msg;
			GWT.log("2 Received msg: " + (msg==null) + " and value of current msg: " + (this.selectedMsg==null));			
		}
	}
	
	private NumberFormat SANG_FORMAT = NumberFormat.getFormat("###");
	private NumberFormat MONEY_FORMAT = NumberFormat.getFormat("###,###");
	//apppend html naira code, do similar with state change value check
	private String getSummaryHTML(TableMessage m)
	{		
		FormHTMLDisplayBuilder applicantInfo = new FormHTMLDisplayBuilder();
		
		applicantInfo.formBegins();
		
		applicantInfo.appendTextBody("Name", m.getText(DTOConstants.FULL_NAME_IDX), true);
		
		applicantInfo.appendTextBody("Company", m.getText(DTOConstants.COMPANY_NAME_IDX), true);	
		
		applicantInfo.appendTextBody("Request Type", m.getText(DTOConstants.TYPE_IDX), true);
		
		applicantInfo.lineBegins().appendTextBody("Salary", "<span>&#8358;</span>" + MONEY_FORMAT.format(m.getNumber(DTOConstants.SALARY_IDX)) + " NGN").
			appendTextBody("Requested Loan", "<span>&#8358;</span>"+ MONEY_FORMAT.format(m.getNumber(DTOConstants.AMOUNT_IDX))+ " NGN").lineEnds();		
		
		applicantInfo.lineBegins().appendTextBody("Email", m.getText(DTOConstants.EMAIL_IDX)).
				appendTextBody("Phone #", m.getText(DTOConstants.PHONE_NUMBER_IDX)).lineEnds();
		
		applicantInfo.lineBegins().appendTextBody("State", m.getText(DTOConstants.STATE_IDX)).
		appendTextBody("App Status", m.getText(DTOConstants.NOTE_IDX)).lineEnds();
		
		applicantInfo.lineBegins().appendTextBody("SANG ID", SANG_FORMAT.format(m.getNumber(DTOConstants.TAB_IDENTIFIER_MSG_IDX))).
			appendTextBody("Application Date", String.valueOf(m.getDate(DTOConstants.DATE_CREATED_IDX))).lineEnds();
		
		applicantInfo.formEnds();
		return applicantInfo.toString();
	}

	@Override
	public void onValueChange(ValueChangeEvent<TableMessage> event) {
		rowSelected(event.getValue());
	}
}
