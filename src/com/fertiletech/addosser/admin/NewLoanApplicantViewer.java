/**
 * 
 */
package com.fertiletech.addosser.admin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import com.fertiletech.addosser.admin.customerforms.LoanApplicationFormPanel;
import com.fertiletech.addosser.client.utils.PanelUtilities;
import com.fertiletech.addosser.shared.FormConstants;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.ResizeComposite;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class NewLoanApplicantViewer extends ResizeComposite implements ChangeHandler{
	public LayoutPanel applicantForm;
	ListBox applicants;
	public final static HTML tempDisplay = new HTML("Please wait, loading applicant's form ...");
	LoanApplicationFormPanel formDisplay; 
	
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<HashMap<String, String>> fetchFormDataCallback = new AsyncCallback<HashMap<String, String>>() {

        @Override
        public void onSuccess(HashMap<String, String> result) {
        	applicants.setEnabled(true);
        	if(result.size() == 1 && result.containsKey(FormConstants.EMAIL_KEY))
        	{
        		applicantForm.clear();
        		applicantForm.add(new HTML("User " + result.get(FormConstants.EMAIL_KEY) + " has logged in to apply but is yet to save any data"));
        	}
        	else
        	{
        		applicantForm.clear();
        		formDisplay.setFormData(result); //TODO this will fail when we allow saving illegal input
        		applicantForm.add(formDisplay);
        	}

        }

        @Override
        public void onFailure(Throwable caught) {
        	PanelUtilities.errorBox.show("Unable to retrieve application form. Try refreshing your browser. " +
        			"Contact info@fertiletech.com if problems persist. <p> Error msg: <b>"+ caught.getMessage() + "</b></p>");
        }
    }; 
    
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<HashMap<String, String>> fetchApplicantsCallback = new AsyncCallback<HashMap<String, String>>() {

        @Override
        public void onSuccess(HashMap<String, String> result) {
        	if(result.size() == 0)
        	{
        		applicants.addItem("No new applications found");
        		applicants.setEnabled(false);
        		applicantForm.clear();
        		applicantForm.add(new HTML("No new applications found"));
        	}
        	else
        	{
        		ArrayList<String> temp = new ArrayList<String>();
        		temp.addAll(result.keySet());
        		Collections.sort(temp);
        		for(String val : temp)
        			applicants.addItem(result.get(val), val);
        		applicants.setSelectedIndex(0);
        		showForm(applicants.getValue(0));
        	}

        }

        @Override
        public void onFailure(Throwable caught) {
        	PanelUtilities.errorBox.show("Unable to retrieve applicant form. Try refreshing your browser. " +
        			"Contact info@fertiletech.com if problems persist. <p> Error msg: <b>"+ caught.getMessage() + "</b></p>");
        }
    };     
	
	public NewLoanApplicantViewer()
	{
		DockLayoutPanel contentArea = new DockLayoutPanel(Unit.EM);
		applicantForm = new LayoutPanel();
		applicantForm.add(new HTML("<b>Please wait, applicant list loading ...</b>"));
		applicants = new ListBox();
		applicants.setWidth("30%");
		contentArea.addNorth(applicants, 2);
		contentArea.add(applicantForm);
		formDisplay = new LoanApplicationFormPanel(false);
		applicants.addChangeHandler(this);
		initWidget(contentArea);
		WebAppHelper.LOAN_MKT_SERVICE.getNewLoanIDs(fetchApplicantsCallback);
	}
	
	public void showForm(String loanID)
	{
		applicants.setEnabled(false);
		applicantForm.clear();
		applicantForm.add(tempDisplay);
		WebAppHelper.LOAN_MKT_SERVICE.getStoredLoanApplication(loanID, fetchFormDataCallback);
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ChangeHandler#onChange(com.google.gwt.event.dom.client.ChangeEvent)
	 */
	@Override
	public void onChange(ChangeEvent event) {
		showForm(applicants.getValue(applicants.getSelectedIndex()));
	}
}
