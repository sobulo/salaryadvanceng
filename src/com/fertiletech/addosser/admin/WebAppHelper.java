/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fertiletech.addosser.admin;


import com.fertiletech.addosser.client.HyperlinkedPanel;
import com.fertiletech.addosser.client.LoanMktService;
import com.fertiletech.addosser.client.LoanMktServiceAsync;
import com.fertiletech.addosser.client.LoginService;
import com.fertiletech.addosser.client.LoginServiceAsync;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Segun Razaq Sobulo
 * 
 * IMPORTANT: only use package access or private access in this class. This is to prevent
 * unnecessary loading of certain gwt files in parts of the app that are public
 * 
 * TODO run gwt code splitting analytic tools to explicitly confirm which classes get loaded by public 
 * sections of the app
 */
public class WebAppHelper {

    private static int indexCounter = 0;
    private final static int WELCOME_IDX = indexCounter++;
    private final static int LOAN_PARAMS_IDX = indexCounter++;
    private final static int LOGO_IMAGE_IDX = indexCounter++;    
    private final static int SALES_LEAD_IDX = indexCounter++;
    private final static int NEW_LOAN_VIEWER_IDX = indexCounter++;
    private final static int RAW_DATA_IDX = indexCounter++;

    private static HyperlinkedPanel[] appPanels = new HyperlinkedPanel[indexCounter];
    
    final static LoginServiceAsync READ_SERVICE = GWT.create(LoginService.class);
    final static LoanMktServiceAsync LOAN_MKT_SERVICE = GWT.create(LoanMktService.class);

    public static LoanMktServiceAsync getLoanMktService()
    {
    	return LOAN_MKT_SERVICE;
    }
    
    public static LoginServiceAsync getLoginService()
    {
    	return READ_SERVICE;
    }
    
    static HyperlinkedPanel[] initializePanels() {
        GWT.log("loading static block");
        //welcome panel
        appPanels[WELCOME_IDX] = new HyperlinkedPanel() {

            private Hyperlink link;
            private WelcomePanel welcomePanel;

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Welcome", "welcome");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget(int ignoredState) {
                if (welcomePanel == null) {
                    welcomePanel = new WelcomePanel();
                }
                return welcomePanel;
            }

			@Override
			public void onSwappedOut() {
				// TODO Auto-generated method stub
				
			}
        };
        
        appPanels[LOAN_PARAMS_IDX] = new HyperlinkedPanel() 
        {
            private Hyperlink link;
            private Widget paramPanel = null;

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Loan Params", "params");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget(int ignoredState) {
                if (paramPanel == null) {
                    paramPanel = new LoanMarketingParametersPanel();
                }
                return paramPanel;
            }

			@Override
			public void onSwappedOut() {
				// TODO Auto-generated method stub
				
			}
        };           

        appPanels[LOGO_IMAGE_IDX] = new HyperlinkedPanel() 
        {
            private Hyperlink link;
            private Widget paramPanel = null;

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Company Logo", "logo");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget(int ignoredState) {
                if (paramPanel == null) {
                    paramPanel = new ImageUploadPanel();
                }
                return paramPanel;
            }

			@Override
			public void onSwappedOut() {
				// TODO Auto-generated method stub
				
			}
        };         
        
        appPanels[SALES_LEAD_IDX] = new HyperlinkedPanel() 
        {
            private Hyperlink link;
            private Widget paramPanel = null;

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Pre-Loan Requests", "sale-leads");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget(int ignoredState) {
                if (paramPanel == null) {
                    paramPanel = new HTML("DEPRECATED");
                }
                return paramPanel;
            }

			@Override
			public void onSwappedOut() {
				// TODO Auto-generated method stub
				
			}
        };
        
        appPanels[NEW_LOAN_VIEWER_IDX] = new HyperlinkedPanel() 
        {
            private Hyperlink link;
            private Widget panel = null;

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Tentative Applicants", "tentative");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget(int ignoredState) {
                if (panel == null) {
                    panel = new NewLoanApplicantViewer();
                }
                return panel;
            }

			@Override
			public void onSwappedOut() {
				// TODO Auto-generated method stub
				
			}
        };
        
        appPanels[RAW_DATA_IDX] = new HyperlinkedPanel() 
        {
            private Hyperlink link;
            private Widget panel = null;

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Raw Data", "csvdata");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget(int ignoredState) {
                if (panel == null) {
                    panel = new RawDataDump();
                }
                return panel;
            }

			@Override
			public void onSwappedOut() {
				// TODO Auto-generated method stub
			}
        };         
                
        return appPanels;
    }
    
    static Hyperlink[] getMenuItems() {     
        Hyperlink[] result = new Hyperlink[appPanels.length];
        
        //welcome menu item
        for(int i = 0; i < appPanels.length; i++)
        	result[i] = appPanels[i].getLink();
        return result;
    }

    static int getWelcomeScreenIndex() {
        return WELCOME_IDX;
    }
}
