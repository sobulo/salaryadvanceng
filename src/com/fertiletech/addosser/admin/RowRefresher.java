package com.fertiletech.addosser.admin;

import com.fertiletech.addosser.shared.table.TableMessage;

public interface RowRefresher {
	public void refresh(TableMessage m);
	//public TableMessage getSelectedMessage();

}
