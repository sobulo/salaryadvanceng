package com.fertiletech.addosser.admin;

import com.fertiletech.addosser.client.utils.PanelUtilities;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.Widget;

public class RawDataDump extends Composite{

	private static RawDataDumpUiBinder uiBinder = GWT
			.create(RawDataDumpUiBinder.class);

	interface RawDataDumpUiBinder extends UiBinder<Widget, RawDataDump> {
	}
	
	@UiField
	TextArea displayArea;
	
    final AsyncCallback<String> tableCallback = new AsyncCallback<String>() {

        @Override
        public void onSuccess(String result) {
        	displayArea.setText(result);
        	displayArea.setEnabled(true);
        }

        @Override
        public void onFailure(Throwable caught) {
        	PanelUtilities.errorBox.show("Unable to fetch report. Error was: " + caught.getMessage());
        }
    };	

	public RawDataDump() {
		initWidget(uiBinder.createAndBindUi(this));
		displayArea.setHeight("90%");
		displayArea.setWidth("100%");
		displayArea.setText("Loading, please wait...");
		displayArea.setEnabled(false);
		WebAppHelper.LOAN_MKT_SERVICE.getAllSalesLeadAsCSV(tableCallback);
	}
}
