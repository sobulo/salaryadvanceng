package com.fertiletech.addosser.admin;

import com.fertiletech.addosser.admin.app.Showcase;
import com.fertiletech.addosser.client.GUIConstants;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.google.gwt.user.client.ui.RootPanel;


/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class AdministratorEntryPoint implements EntryPoint {
	
	//public final int DEPREACATED_ADDOSSER_CALC = 4;
	public final int MINI_CALC = 1;
	public final int ADMIN_PORTAL = 2;
	public final int APPLICATION_PORTAL = 3;
	public final int NEW_APPLICATION_PORTAL = 4;
	public final int BASE_URL = 5;
	
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() 
	{
		Panel rootPanel = RootPanel.get(GUIConstants.ROOT_ID_ADMIN);
		
		if(rootPanel == null)
		{
			Window.alert("Code download failed, try refreshing the page." +
			" Please contact info@fertiletech.com if problem persists");
			return;
		}
		else
			rootPanel.getElement().setInnerHTML("");
		
		setupAdminArea();
	}
			
	public void setupAdminArea()
	{
		GWT.runAsync(new RunAsyncCallback() {
			
			@Override
			public void onSuccess() {
				new Showcase();
			}
			
			@Override
			public void onFailure(Throwable reason) {
				Window.alert("Code download failed, try refreshing the page." +
						" Please contact info@fertiletech.com if problem persists");
				
			}
		});		
	}	
}
