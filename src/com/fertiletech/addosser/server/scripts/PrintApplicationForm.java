/**
 * 
 */
package com.fertiletech.addosser.server.scripts;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fertiletech.addosser.server.loanmktdata.ConsumerLoanSalesLead;
import com.fertiletech.addosser.server.loanmktdata.EntityConstants;
import com.fertiletech.addosser.server.loanmktdata.GuarantorFormData;
import com.fertiletech.addosser.server.loanmktdata.LoanApplicationFormData;
import com.fertiletech.addosser.server.loanmktdata.LoanMktDAO;
import com.fertiletech.addosser.server.scripts.downloads.InetImageBlob;
import com.fertiletech.addosser.server.scripts.downloads.PDFGenerationHelper;
import com.fertiletech.addosser.shared.FormConstants;
import com.fertiletech.addosser.shared.FormHTMLDisplayBuilder;
import com.fertiletech.addosser.shared.table.TableMessage;
import com.fertiletech.addosser.shared.table.TableMessageHeader;
import com.fertiletech.addosser.shared.table.TableMessageHeader.TableMessageContent;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.NotFoundException;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.pdfjet.Box;
import com.pdfjet.Cell;
import com.pdfjet.CoreFont;
import com.pdfjet.Font;
import com.pdfjet.Image;
import com.pdfjet.ImageType;
import com.pdfjet.Letter;
import com.pdfjet.Line;
import com.pdfjet.PDF;
import com.pdfjet.Page;
import com.pdfjet.Point;
import com.pdfjet.RGB;
import com.pdfjet.Table;
import com.pdfjet.TextLine;

/**
 * @author Segun Razaq Sobulo
 * 
 */
public class PrintApplicationForm extends HttpServlet {
	static
	{
		LoanMktDAO.registerClassesWithObjectify();
	}
	private static final Logger log = Logger.getLogger(PrintApplicationForm.class
			.getName());
	private final static String disclaimerBegin = "Please note that fraudulent activity is punishable under Banks and Other Financial Institution Act (BOFIA 1991) " +
			"I hereby confirm applying for the above credit facility ("; 
	
	private final static String disclaimerEnds = ") and certify that all the information provided by me" +
			" above and attached thereto is true, correct and complete. I authorize you to make any enquiry you consider necessary and appropriate for the purpose of evaluating this application.";
	private final static String HEADER_MESSAGE = "<center><table border='0' align='left' cellspacing=20 width='80%'><tr><td><img src='http://www.addosser.com/addosser-doc-logo.jpg'/></td>" + 
											"<td valign='middle'><b>ADDOSSER MICROFINANCE LOAN APPLICATION FORM</b></td></tr></table><hr /></center>";
	
    private final static int RIGHT_MARGIN = 40;
    private final static int TOP_MARGIN = 40;
    private final static int ITEM_COL_WIDTH = 170;
    private final static int NOTES_COL_WIDTH = 225;
    private final static int AMOUNT_COL_WIDTH = 75;
    private final static int PADDING_SIZE = 5;
    private final static int SPACE_BTW_BOXES = 12;
    private final static int HEADER_FONT_SIZE = 12;
    private final static int REGULAR_FONT_SIZE = 10;
    private final static int SMALL_FONT_SIZE = 8;
    private final static float REQ_REP_WIDTH = 550;
 
    private final static int BOX_STUDENT_INFO_HEIGHT = 80;
    private final static int BOX_ADDRESS_INFO_HEIGHT = 50;
    private final static int BOX_COMMENT_INFO_HEIGHT = 80;
    private final static int BOX_OFFICIAL_INFO_HEIGHT = 45;
    private final static int BOX_DEFAULT_COMPANY_HEIGHT = 65;
     
    private final static String SESS_PREFIX = "fertiletech.";
	private final static String APP_FORM_OP = "appform";
	private final static String GUARANTOR_FORM_OP = "grtform";
	private final static String CHECKLIST_FORM_OP = "chkform";
	private final static String OP_TYPE = "type";


	/**
	 * web.xml contains entries to ensure only registered developers for the app
	 * can execute this script. Does not go through login logic
	 */

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws IOException {
		ServletOutputStream out = res.getOutputStream();
		
		String loanId = req.getParameter(FormConstants.LOAN_ID_PARAM);
		String type = req.getParameter(OP_TYPE);

		if (loanId == null || loanId.trim().length() == 0) {
			out.println("<b><font color='red'>No resource requested</font></b></body></html>");
			return;
		}
		Objectify ofy = ObjectifyService.begin();		
		HttpSession sess = req.getSession();
		if(APP_FORM_OP.equals(type))
		{
    		List<TableMessage> data = (List<TableMessage>) sess.getAttribute(getAppFormDataName(loanId));
	    	if(data == null || data.size() == 0)
	    	{
	    		res.setContentType("text/html");
	    		res.getOutputStream().println("<h1>Downloads</h1><br/><hr>");
	    		res.getOutputStream().println("<a href='" + getApplicationFormLink(loanId, req) + "'>Application Form</a>"); 
	    		return;    		
	    	}    	
			String fileName = "sang-application-" + ObjectifyService.factory().stringToKey(loanId).getParent().getId() + ".pdf";
	        res.setContentType("application/octet-stream");        
	        res.setHeader("Content-disposition", "attachment; filename=" + fileName);	        
	        writeAppFormToStream(ofy, out, data);	    	
			
		}
		else if(GUARANTOR_FORM_OP.equals(type))
		{
			Key<GuarantorFormData> fk = ofy.getFactory().stringToKey(loanId);
			Key<ConsumerLoanSalesLead> leadKey = fk.getParent();
			Key<GuarantorFormData> firstGk = GuarantorFormData.getFormKey(leadKey, true);
    		List<TableMessage> data = (List<TableMessage>) sess.getAttribute(getGuarantorTableName(loanId, fk.equals(firstGk)));
    		HashMap<String, String> map = (HashMap<String, String>) sess.getAttribute(getGuarantorDataName(loanId, fk.equals(firstGk)));
	    	if(data == null || data.size() == 0)
	    	{
	    		res.setContentType("text/html");
	    		res.getOutputStream().println("<h1>Downloads</h1><br/><hr>");
	    		res.getOutputStream().println("<a href='" + getGuarantorFormLink(loanId, req) + "'>Guarantor Form</a>"); 
	    		return;    		
	    	}    	
			String fileName = "sang-guarantor-" + ObjectifyService.factory().stringToKey(loanId).getParent().getId() + ".pdf";
	        res.setContentType("application/octet-stream");        
	        res.setHeader("Content-disposition", "attachment; filename=" + fileName);	        
	        writeGuarantorFormToStream(ofy, out, data, map);	    	
			
		}
		else if(CHECKLIST_FORM_OP.equals(type))
		{
    		List<TableMessage> data = (List<TableMessage>) sess.getAttribute(getCheckListTableName(loanId));
    		String title = (String) sess.getAttribute(getCheckListTitleName(loanId));
    		Double amount = (Double) sess.getAttribute(getCheckListAmountName(loanId));
	    	if(data == null || data.size() == 0)
	    	{
	    		res.setContentType("text/html");
	    		res.getOutputStream().println("<h1>Downloads</h1><br/><hr>");
	    		res.getOutputStream().println("<a href='" + getCheckListFormLink(loanId, req) + "'>Requirements Checklist</a>"); 
	    		return;    		
	    	}    	
			String fileName = "sang-guarantor-" + ObjectifyService.factory().stringToKey(loanId).getParent().getId() + ".pdf";
	        res.setContentType("application/octet-stream");        
	        res.setHeader("Content-disposition", "attachment; filename=" + fileName);	        
	        writeChecklistFormToStream(ofy, out, data, title, amount);	    	
			
		}		
		else
		{
			//deprecated so delete?
			Key<LoanApplicationFormData> formKey = ofy.getFactory().stringToKey(
					loanId);
			try {
				LoanApplicationFormData formData = LoanMktDAO.getLoanFormData(formKey, ofy);
				res.setContentType("text/html");
				out.println("<html><body>");
				//print bank logo
				out.println(HEADER_MESSAGE);
				
				FormHTMLDisplayBuilder builder = JSPUtilities.loadFormDisplay(formData, true);
				builder.sectionEnds();
				builder.headerBegins().appendTextBody("Requirments").headerEnds();
				
				//TODO make this more dynamic, in particular calculate lessee check, guarantor check and number of guarantors
				builder.lineBegins().appendTextBody("I have attached all required documentation as listed on www.salaryadvanceng.com/requirements").lineEnds();
				builder.sectionEnds();
				
				//disclaimer
				builder.lineBegins().appendTextBody(disclaimerBegin).valueBegins().
					appendTextBody(JSPUtilities.convert(Integer.valueOf(formData.getFormData().get(FormConstants.REQUESTED_LOAN_AMOUNT_KEY)))).valueEnds();
				builder.appendTextBody(disclaimerEnds).labelEnds().formEnds();
				
				//print the form
				out.println(builder.toString());
			} catch (NotFoundException ex) {
				log.warning("Bad Loan ID specified for print URL: " + formKey
						+ "\n" + ex.getMessage());
				out.println("<b><font color='red'>Requested resource not found</font>");			
				return;
			} catch (Exception ex) {
				log.severe("ERROR while trying to print: " + formKey + "\n"
						+ ex.getLocalizedMessage());
				out.println("<b><font color='red'>Server Error. Please try again. If problem persists, contact info@fertiletech.com</font></b>");
				return;
			} finally {
				out.println("</html></body>");
			}			
		}
	}
	
    private static void writeAppFormToStream(Objectify ofy, OutputStream out, List<TableMessage> formData)
    {    	    	
    	//init pdf file
    	try
    	{        	
	        PDF pdf = new PDF(out);
	        
	        Font f1 = new Font(pdf, CoreFont.HELVETICA_BOLD);
	        f1.setSize(SMALL_FONT_SIZE-1);
	        Font f2 = new Font(pdf, CoreFont.HELVETICA);
	        f2.setSize(SMALL_FONT_SIZE-1);
	        
	        //logo stream
			Key<InetImageBlob> logoKey = new Key(InetImageBlob.class, FormConstants.PDF_REPORT_LOGO);
			InetImageBlob logo = ofy.find(logoKey);
			if(logo == null)
			{
				throw new IllegalArgumentException("Your company hasn't been properly setup for report" +
	    				" generation or setup data has been corrupted. Please contact IT to investigate");    					
			}	        
	        BufferedInputStream bis =
                new BufferedInputStream(new ByteArrayInputStream(logo.getImage()));
    	
	        
	        //passport photostream
			Key<InetImageBlob> passportKey = new Key(InetImageBlob.class, FormConstants.PDF_NO_IMAGE);
			InetImageBlob passport = ofy.find(passportKey);
			if(passport == null)
			{
				throw new IllegalArgumentException("Your company hasn't been properly setup for report" +
	    				" generation or setup data has been corrupted. Please contact IT to investigate");    					
			}	        
	        BufferedInputStream bis2 =
                new BufferedInputStream(new ByteArrayInputStream(passport.getImage()));

	    	printAppForm(formData, FormConstants.getCompanyInfo(), bis, bis2, f1, f2, pdf);
	    	pdf.flush();
    	}
    	catch(Exception ex)
    	{
    		StackTraceElement[] exLocations = ex.getStackTrace();
    		String msg = "";
    		for(StackTraceElement  exLoc : exLocations)
    		{
    			String exMsg = exLoc.toString();
    			if(exMsg.startsWith("com.fertile"))
    				msg += exMsg + "\n";
    		}
    		log.severe(msg);    		
    		throw new RuntimeException(ex.fillInStackTrace());
    	}    	
    }
    
    private static void writeGuarantorFormToStream(Objectify ofy, OutputStream out, List<TableMessage> formData, HashMap<String, String> dataMap)
    {    	    	
    	//init pdf file
    	try
    	{        	
	        PDF pdf = new PDF(out);
	        
	        Font f1 = new Font(pdf, CoreFont.HELVETICA_BOLD);
	        f1.setSize(SMALL_FONT_SIZE-1);
	        Font f2 = new Font(pdf, CoreFont.HELVETICA);
	        f2.setSize(SMALL_FONT_SIZE-1);
	        
	        //logo stream
			Key<InetImageBlob> logoKey = new Key(InetImageBlob.class, FormConstants.PDF_REPORT_LOGO);
			InetImageBlob logo = ofy.find(logoKey);
			if(logo == null)
			{
				throw new IllegalArgumentException("Your company hasn't been properly setup for report" +
	    				" generation or setup data has been corrupted. Please contact IT to investigate");    					
			}	        
	        BufferedInputStream bis =
                new BufferedInputStream(new ByteArrayInputStream(logo.getImage()));
    	
	        
	        //passport photostream
			Key<InetImageBlob> passportKey = new Key(InetImageBlob.class, FormConstants.PDF_NO_IMAGE);
			InetImageBlob passport = ofy.find(passportKey);
			if(passport == null)
			{
				throw new IllegalArgumentException("Your company hasn't been properly setup for report" +
	    				" generation or setup data has been corrupted. Please contact IT to investigate");    					
			}	        
	        BufferedInputStream bis2 =
                new BufferedInputStream(new ByteArrayInputStream(passport.getImage()));

	    	printGuarantorForm(dataMap, formData, FormConstants.getCompanyInfo(), bis, bis2, f1, f2, pdf);
	    	pdf.flush();
    	}
    	catch(Exception ex)
    	{
    		StackTraceElement[] exLocations = ex.getStackTrace();
    		String msg = "";
    		for(StackTraceElement  exLoc : exLocations)
    		{
    			String exMsg = exLoc.toString();
    			if(exMsg.startsWith("com.fertile"))
    				msg += exMsg + "\n";
    		}
    		log.severe(msg);    		
    		throw new RuntimeException(ex.fillInStackTrace());
    	}    	
    }
    
    private static void writeChecklistFormToStream(Objectify ofy, OutputStream out, List<TableMessage> requirementsTable, String applicantEmail, double requestedAmount)
    {    	    	
    	//init pdf file
    	try
    	{        	
	        PDF pdf = new PDF(out);
	        
	        Font f1 = new Font(pdf, CoreFont.HELVETICA_BOLD);
	        f1.setSize(SMALL_FONT_SIZE-1);
	        Font f2 = new Font(pdf, CoreFont.HELVETICA);
	        f2.setSize(SMALL_FONT_SIZE-1);
	        
	        //logo stream
			Key<InetImageBlob> logoKey = new Key(InetImageBlob.class, FormConstants.PDF_REPORT_LOGO);
			InetImageBlob logo = ofy.find(logoKey);
			if(logo == null)
			{
				throw new IllegalArgumentException("Your company hasn't been properly setup for report" +
	    				" generation or setup data has been corrupted. Please contact IT to investigate");    					
			}	        
	        BufferedInputStream bis =
                new BufferedInputStream(new ByteArrayInputStream(logo.getImage()));
    		        
	    	printChecklistForm(requirementsTable, FormConstants.getCompanyInfo(), applicantEmail, requestedAmount, bis, f1, f2, pdf);
	    	pdf.flush();
    	}
    	catch(Exception ex)
    	{
    		StackTraceElement[] exLocations = ex.getStackTrace();
    		String msg = "";
    		for(StackTraceElement  exLoc : exLocations)
    		{
    			String exMsg = exLoc.toString();
    			if(exMsg.startsWith("com.fertile"))
    				msg += exMsg + "\n";
    		}
    		log.severe(msg);    		
    		throw new RuntimeException(ex.fillInStackTrace());
    	}    	
    }
    
        
    private static void printAppForm(List<TableMessage> formData, String[] schoolInfo, BufferedInputStream imageStream, BufferedInputStream passportPhotoStream,
    		Font f1, Font f2, PDF pdf) throws Exception
    {
    	//setup additional fonts
        Font f3 = new Font(pdf, CoreFont.HELVETICA_BOLD);
        f3.setSize(HEADER_FONT_SIZE);
        Font f4 = new Font(pdf, CoreFont.HELVETICA_OBLIQUE);
        f4.setSize(SMALL_FONT_SIZE); 

	    //new page for student report card		    
	    Page page = new Page(pdf, Letter.PORTRAIT);
        
        //school logo
        Image logo = new Image(pdf, imageStream, ImageType.JPEG);
        
        Image passport = new Image(pdf, passportPhotoStream, ImageType.JPEG);
        
        final int BOX_COMPANY_INFO_HEIGHT = (int) Math.max(logo.getHeight() + (2 * PADDING_SIZE), BOX_DEFAULT_COMPANY_HEIGHT);
        int studentBoxY = TOP_MARGIN + BOX_COMPANY_INFO_HEIGHT + SPACE_BTW_BOXES;
        int tableY = studentBoxY + BOX_STUDENT_INFO_HEIGHT + SPACE_BTW_BOXES;
        
        int textStart = RIGHT_MARGIN + PADDING_SIZE;
        double headerStart = textStart + logo.getWidth() + PADDING_SIZE;

	    log.warning("Table created succesfully");
	    TextLine text = new TextLine(f3, schoolInfo[FormConstants.COMPANY_INFO_NAME_IDX]);
	    int textY = TOP_MARGIN + PADDING_SIZE + HEADER_FONT_SIZE;
	    text.setPosition(headerStart, textY);
	    
	    TextLine text1 = new TextLine(f4, schoolInfo[FormConstants.COMPANY_INFO_ADDR_IDX] );
	    int text1Y = textY + PADDING_SIZE + SMALL_FONT_SIZE;
	    text1.setPosition(headerStart, text1Y);
	    
	    TextLine text2 = new TextLine(f4, "Tel: " + schoolInfo[FormConstants.COMPANY_INFO_NUMS_IDX]);
	    int text2Y = text1Y + PADDING_SIZE + SMALL_FONT_SIZE;
	    text2.setPosition(headerStart, text2Y);
	    
	    TextLine text2b = new TextLine(f4, "Web: " + schoolInfo[FormConstants.COMPANY_INFO_WEB_IDX] + "   Email: " + schoolInfo[FormConstants.COMPANY_INFO_EMAIL_IDX]);
	    int text2bY = text2Y + PADDING_SIZE + SMALL_FONT_SIZE;
	    text2b.setPosition(headerStart, text2bY);		    
    	    
	    text.drawOn(page);
	    text1.drawOn(page);
	    text2.drawOn(page);
	    text2b.drawOn(page, true);
	    log.warning("company info generated succesfully");        
	    //box borders
	    double maxWidth = 550;
	    //setup student info
	    textY = studentBoxY + PADDING_SIZE + REGULAR_FONT_SIZE;
	    
	    

	    Box schoolInfoBox = new Box(RIGHT_MARGIN, TOP_MARGIN, maxWidth, BOX_COMPANY_INFO_HEIGHT);
	    //logo.setPosition(textStart, TOP_MARGIN + (BOX_COMPANY_INFO_HEIGHT/2 - logo.getHeight()/2));
	    //passport.setPosition(textStart, TOP_MARGIN + (BOX_COMPANY_INFO_HEIGHT/2 - passport.getHeight()/2));
	    passport.setPosition(RIGHT_MARGIN + maxWidth - logo.getWidth() - PADDING_SIZE, TOP_MARGIN + (BOX_COMPANY_INFO_HEIGHT/2 - logo.getHeight()/2));
	    logo.placeIn(schoolInfoBox);
	    logo.setPosition(PADDING_SIZE, PADDING_SIZE * 2);
	    logo.drawOn(page);
	    passport.placeIn(schoolInfoBox);
	    passport.setPosition(maxWidth - passport.getWidth() - PADDING_SIZE * 2, PADDING_SIZE * 2);
	    passport.drawOn(page);
	    Line line = new Line(RIGHT_MARGIN, TOP_MARGIN + BOX_COMPANY_INFO_HEIGHT, RIGHT_MARGIN + maxWidth, TOP_MARGIN + BOX_COMPANY_INFO_HEIGHT);
	    line.setColor(RGB.OLD_GLORY_BLUE);
	    line.setWidth(3);
	    line.drawOn(page);
	    
	    
	    boolean first = true;
	    Table table = null;
	    Point tableEnd = null;
	    textY = textY + SPACE_BTW_BOXES + PADDING_SIZE;

    	//convert bill desc itemized data to format experted by print report fn
    	log.warning(formData.toString());

	    //form table settings
    	List<List<Cell>> itemizedBillDesc = PDFGenerationHelper.convertStackedTableMessageToPDFTable(formData, f1, f2);        	 		    
	    table = new Table(f1, f2);
	    table.setData(itemizedBillDesc, Table.DATA_HAS_0_HEADER_ROWS);
	    table.setLineWidth(0.2);
	    table.setPosition(RIGHT_MARGIN, studentBoxY + PADDING_SIZE);
	    table.setCellPadding(PADDING_SIZE);
	    table.rightAlignNumbers();
	    table.setColumnWidth(0, maxWidth * 0.33);
	    table.setColumnWidth(1, maxWidth * 0.33);
	    table.setColumnWidth(2, maxWidth * 0.34);
	    
	    //print payments table for this deposit
	    while(true)
	    {
		    tableEnd = table.drawOn(page);
		    if(!table.hasMoreData())
		    {
		    	textY = (int) tableEnd.getY();
		    	break;
		    }
		    else
		    {
		    	page = new Page(pdf, Letter.PORTRAIT);
		    	textY = TOP_MARGIN;
		    }
	    }
	    
	    
    	
	    //form table settings
	    HashSet<Integer> ignore = new HashSet<Integer>();
    	itemizedBillDesc = PDFGenerationHelper.convertTableMessageToPDFTable(getExistingLoanTable(), f1, f2, ignore);        	 		    
	    table = new Table(f1, f2);
	    table.setData(itemizedBillDesc, Table.DATA_HAS_1_HEADER_ROWS);
	    table.setLineWidth(0.2);
	    table.setPosition(RIGHT_MARGIN, tableEnd.getY());
	    table.setCellPadding(PADDING_SIZE);
	    table.rightAlignNumbers();
	    table.setColumnWidth(0, maxWidth * 0.25);
	    table.setColumnWidth(1, maxWidth * 0.25);
	    table.setColumnWidth(2, maxWidth * 0.25);
	    table.setColumnWidth(3, maxWidth * 0.25);
	    
	    //print payments table for this deposit
	    while(true)
	    {
		    tableEnd = table.drawOn(page);
		    if(!table.hasMoreData())
		    {
		    	textY = (int) tableEnd.getY();
		    	break;
		    }
		    else
		    {
		    	page = new Page(pdf, Letter.PORTRAIT);
		    	textY = TOP_MARGIN;
		    }
	    } 
	    
        //setup official box
        double officialBoxY = tableEnd.getY() + SPACE_BTW_BOXES;
        Box officialBox = new Box(RIGHT_MARGIN, officialBoxY , maxWidth, BOX_OFFICIAL_INFO_HEIGHT);
        TextLine textd = new TextLine(f2, "I hereby Authorize Addosser Microfinance Bank Limited to verify the information provided on this form as to my credit and employment history.");
        double textdY = officialBoxY + PADDING_SIZE+ SMALL_FONT_SIZE;
        textd.setPosition(textStart, textdY);
        textd.drawOn(page);
        
        TextLine text7 = new TextLine(f2, "Mandatory Signature:                                                                                    ");
        double text7Y = textdY + PADDING_SIZE * 3 + REGULAR_FONT_SIZE;
        text7.setPosition(textStart, text7Y);
        text7.setUnderline(true);
        TextLine text8 = new TextLine(f2, "Confirm Signature:                                                                       Date: " 
        			+ EntityConstants.DATE_FORMAT.format(new Date()));
        text8.setUnderline(true);
        text8.setPosition(textStart + maxWidth/2, text7Y);
        text7.drawOn(page);
        text8.drawOn(page);
        officialBox.drawOn(page);       
        log.warning("deposit statements generated succesfully");
    }

    private static void printGuarantorForm(HashMap<String, String> dataObj, List<TableMessage> formData, String[] schoolInfo, BufferedInputStream imageStream, BufferedInputStream passportPhotoStream,
    		Font f1, Font f2, PDF pdf) throws Exception
    {
    	//setup additional fonts
        Font f3 = new Font(pdf, CoreFont.HELVETICA_BOLD);
        f3.setSize(HEADER_FONT_SIZE);
        Font f4 = new Font(pdf, CoreFont.HELVETICA_OBLIQUE);
        f4.setSize(SMALL_FONT_SIZE); 

	    //new page for student report card		    
	    Page page = new Page(pdf, Letter.PORTRAIT);
        
        //school logo
        Image logo = new Image(pdf, imageStream, ImageType.JPEG);
        
        Image passport = new Image(pdf, passportPhotoStream, ImageType.JPEG);
        
        final int BOX_COMPANY_INFO_HEIGHT = (int) Math.max(logo.getHeight() + (2 * PADDING_SIZE), BOX_DEFAULT_COMPANY_HEIGHT);
        int studentBoxY = TOP_MARGIN + BOX_COMPANY_INFO_HEIGHT + SPACE_BTW_BOXES;
        int tableY = studentBoxY + BOX_STUDENT_INFO_HEIGHT + SPACE_BTW_BOXES;
        
        int textStart = RIGHT_MARGIN + PADDING_SIZE;
        double headerStart = textStart + logo.getWidth() + PADDING_SIZE;

	    log.warning("Table created succesfully");
	    TextLine text = new TextLine(f3, schoolInfo[FormConstants.COMPANY_INFO_NAME_IDX]);
	    int textY = TOP_MARGIN + PADDING_SIZE + HEADER_FONT_SIZE;
	    text.setPosition(headerStart, textY);
	    
	    TextLine text1 = new TextLine(f4, schoolInfo[FormConstants.COMPANY_INFO_ADDR_IDX] );
	    int text1Y = textY + PADDING_SIZE + SMALL_FONT_SIZE;
	    text1.setPosition(headerStart, text1Y);
	    
	    TextLine text2 = new TextLine(f4, "Tel: " + schoolInfo[FormConstants.COMPANY_INFO_NUMS_IDX]);
	    int text2Y = text1Y + PADDING_SIZE + SMALL_FONT_SIZE;
	    text2.setPosition(headerStart, text2Y);
	    
	    TextLine text2b = new TextLine(f4, "Web: " + schoolInfo[FormConstants.COMPANY_INFO_WEB_IDX] + "   Email: " + schoolInfo[FormConstants.COMPANY_INFO_EMAIL_IDX]);
	    int text2bY = text2Y + PADDING_SIZE + SMALL_FONT_SIZE;
	    text2b.setPosition(headerStart, text2bY);		    
    	    
	    text.drawOn(page);
	    text1.drawOn(page);
	    text2.drawOn(page);
	    text2b.drawOn(page, true);
	    log.warning("company info generated succesfully");        
	    //box borders
	    double maxWidth = 550;
	    //setup student info
	    textY = studentBoxY + PADDING_SIZE + REGULAR_FONT_SIZE;
	    
	    

	    Box schoolInfoBox = new Box(RIGHT_MARGIN, TOP_MARGIN, maxWidth, BOX_COMPANY_INFO_HEIGHT);
	    //logo.setPosition(textStart, TOP_MARGIN + (BOX_COMPANY_INFO_HEIGHT/2 - logo.getHeight()/2));
	    //passport.setPosition(textStart, TOP_MARGIN + (BOX_COMPANY_INFO_HEIGHT/2 - passport.getHeight()/2));
	    passport.setPosition(RIGHT_MARGIN + maxWidth - logo.getWidth() - PADDING_SIZE, TOP_MARGIN + (BOX_COMPANY_INFO_HEIGHT/2 - logo.getHeight()/2));
	    logo.placeIn(schoolInfoBox);
	    logo.setPosition(PADDING_SIZE, PADDING_SIZE * 2);
	    logo.drawOn(page);
	    passport.placeIn(schoolInfoBox);
	    passport.setPosition(maxWidth - passport.getWidth() - PADDING_SIZE * 2, PADDING_SIZE * 2);
	    passport.drawOn(page);
	    Line line = new Line(RIGHT_MARGIN, TOP_MARGIN + BOX_COMPANY_INFO_HEIGHT, RIGHT_MARGIN + maxWidth, TOP_MARGIN + BOX_COMPANY_INFO_HEIGHT);
	    line.setColor(RGB.OLD_GLORY_BLUE);
	    line.setWidth(3);
	    line.drawOn(page);
	    
	    
	    Table table = null;
	    Point tableEnd = null;
	    textY = textY + SPACE_BTW_BOXES + PADDING_SIZE;

    	//convert bill desc itemized data to format experted by print report fn
    	log.warning(formData.toString());

	    //form table settings
    	List<List<Cell>> itemizedBillDesc = PDFGenerationHelper.convertTableMessageToPDFTable(formData, f1, f2, f2, new HashSet<Integer>(), true);      	 		    
	    table = new Table(f1, f2);
	    table.setData(itemizedBillDesc, Table.DATA_HAS_1_HEADER_ROWS);
	    table.setLineWidth(0.2);
	    table.setPosition(RIGHT_MARGIN, studentBoxY + PADDING_SIZE);
	    table.setCellPadding(PADDING_SIZE);
	    table.setColumnWidth(0, maxWidth);
	    
	    //print payments table for this deposit
	    while(true)
	    {
		    tableEnd = table.drawOn(page);
		    if(!table.hasMoreData())
		    {
		    	textY = (int) tableEnd.getY();
		    	break;
		    }
		    else
		    {
		    	page = new Page(pdf, Letter.PORTRAIT);
		    	textY = TOP_MARGIN;
		    }
	    }
	    double lineY = tableEnd.getY() + SPACE_BTW_BOXES + SPACE_BTW_BOXES;	    
	    //
	    TextLine firstParagraph = new TextLine(f2, "I ");
	    double paraOffset = textStart + f2.stringWidth("I ") + PADDING_SIZE;
	    firstParagraph.setPosition(textStart, lineY);
	    firstParagraph.drawOn(page);
	    firstParagraph.setText(" " + dataObj.get(FormConstants.APPLICANT_SURNAME_KEY) + ", " +  dataObj.get(FormConstants.APPLICANT_OTHERNAME_KEY) + " ");
	    firstParagraph.setPosition(paraOffset, lineY);
	    firstParagraph.setUnderline(true);
	    firstParagraph.drawOn(page);
	    firstParagraph.setUnderline(false);
	    paraOffset = paraOffset + f2.stringWidth(firstParagraph.getText()) + PADDING_SIZE;
	    if(paraOffset >= maxWidth - 10)
	    {
	    	paraOffset = textStart;
	    	lineY += PADDING_SIZE + SMALL_FONT_SIZE;
	    }
	    List<TextLine> firstParagraphLines = PDFGenerationHelper.getPrintLines(PrintHelper.GUARANTOR_PARAGRAPH1, f2, maxWidth - paraOffset - 5, maxWidth - 20);
	    for(TextLine t : firstParagraphLines)
	    {
	    	t.setPosition(paraOffset, lineY);
	    	t.drawOn(page);
	    	paraOffset = textStart;
	    	lineY = lineY + PADDING_SIZE + SMALL_FONT_SIZE;
	    }
	    lineY += PADDING_SIZE +SPACE_BTW_BOXES;
	    
	    //2nd paragraph
	    firstParagraph = new TextLine(f1, "The Guarantor covenants thus: ");
	    firstParagraph.setPosition(textStart, lineY);
	    firstParagraph.drawOn(page);
	    lineY += PADDING_SIZE + SMALL_FONT_SIZE;
	    firstParagraph = new TextLine(f2, "I ");
	    paraOffset = textStart + f2.stringWidth("I ") + PADDING_SIZE;
	    firstParagraph.setPosition(textStart, lineY);
	    firstParagraph.drawOn(page);
	    firstParagraph.setText("   " + dataObj.get(FormConstants.APPLICANT_SURNAME_KEY) + ", " +  dataObj.get(FormConstants.APPLICANT_OTHERNAME_KEY) + "   ");
	    firstParagraph.setPosition(paraOffset, lineY);
	    firstParagraph.setUnderline(true);
	    firstParagraph.drawOn(page);
	    firstParagraph.setUnderline(false);
	    paraOffset = paraOffset + f2.stringWidth(firstParagraph.getText()) + PADDING_SIZE;
	    if(paraOffset >= maxWidth - 10)
	    {
	    	paraOffset = textStart;
	    	lineY += PADDING_SIZE + SMALL_FONT_SIZE;
	    }
	    firstParagraphLines = PDFGenerationHelper.getPrintLines(PrintHelper.GUARANTOR_PARAGRAPH2, f2, maxWidth - paraOffset - 5, maxWidth - 20);
	    for(TextLine t : firstParagraphLines)
	    {
	    	t.setPosition(paraOffset, lineY);
	    	t.drawOn(page);
	    	paraOffset = textStart;
	    	lineY = lineY + PADDING_SIZE + SMALL_FONT_SIZE;
	    }
	    lineY += PADDING_SIZE;

	    
	    //remaining paragraphs
	    for(String paragraph : PrintHelper.GUARANTOR_PARAGRAPHS)
	    {
	    	List<TextLine> paragraphLines = PDFGenerationHelper.getPrintLines(paragraph, f2, maxWidth - 20, maxWidth - 20);
	    	for(TextLine t : paragraphLines)
	    	{
	    		t.setPosition(textStart, lineY);
	    		t.drawOn(page);
	    		lineY = lineY + PADDING_SIZE + SMALL_FONT_SIZE;
	    	}
	    	lineY = lineY + PADDING_SIZE;
	    }

	    
        //signature section
        TextLine textgHeader = new TextLine(f1, "Guarantor");
        textgHeader.setUnderline(true);
        double textgY = lineY + PADDING_SIZE+ SMALL_FONT_SIZE;
        textgHeader.setPosition(textStart, textgY);
        textgHeader.drawOn(page);
        
        //witness header
        TextLine textWHeader = new TextLine(f1, "In the presence of");
        textWHeader.setUnderline(true);
        double textWX = maxWidth/2 + PADDING_SIZE*2;
        textWHeader.setPosition(textWX, textgY);
        textWHeader.drawOn(page);        

        //guarantor name
        String[] guarantorLabels = {"Name: ", "Residential Address: ", " ", " ", "Phone: ", "Signature: "};
        String[] guarantorValues = {dataObj.get(FormConstants.APPLICANT_SURNAME_KEY) + ", " + dataObj.get(FormConstants.APPLICANT_OTHERNAME_KEY), 
        		                    " ", " ", " ", dataObj.get(FormConstants.PRIMARY_PHONE_KEY), " "};
        String[] witnessLabels = {"Name of witness: ", "Employer's Address: "," ", " ", " Phone: ", "Signature: "};
        for(int i = 0; i < guarantorLabels.length; i++)
        {
            TextLine textg = new TextLine(f2, guarantorLabels[i]);
            textgY = textgY + PADDING_SIZE + SMALL_FONT_SIZE + (i/(guarantorLabels.length-1) * PADDING_SIZE * 3);
            textg.setPosition(textStart, textgY);
            textg.drawOn(page);
            double offset = f2.stringWidth(guarantorLabels[i]);
            textg.setText(PDFGenerationHelper.padValue(f2, guarantorValues[i], maxWidth/2 - PADDING_SIZE*10 - offset));
            textg.setPosition(textStart + offset, textgY);
            textg.setUnderline(true);
            textg.drawOn(page);
            if(i == 1)
            {
            	List<TextLine> addressLines = PDFGenerationHelper.getPrintLines(dataObj.get(FormConstants.ADDRESS_KEY), f2, maxWidth/2 - PADDING_SIZE*10 - offset, maxWidth/2 - PADDING_SIZE*10);
            	double addressX = textStart + offset;
            	double addressY = textgY;
                int count = 0;
            	for(TextLine a : addressLines)
            	{
            		count++;
            		a.setPosition(addressX, addressY);
            		a.drawOn(page);
            		if(count == 3)
            			break;
            		addressX = textStart;
            		addressY = addressY + PADDING_SIZE + SMALL_FONT_SIZE;
            	}
            }
            
            //witness name
            TextLine textW = new TextLine(f2, witnessLabels[i]);
            textW.setPosition(textWX, textgY);
            textW.drawOn(page);
            offset = f2.stringWidth(witnessLabels[i]);
            textW.setText(PDFGenerationHelper.padValue(f2, " ", maxWidth/2 - PADDING_SIZE*4 - offset));
            offset = f2.stringWidth(witnessLabels[i]);
            textW.setUnderline(true);
            textW.setPosition(textWX + offset, textgY);
            textW.drawOn(page);
        }
        
        log.warning("deposit statements generated succesfully");
    }
    
    private static void printChecklistForm(List<TableMessage> requirementsTable, String[] schoolInfo, String applicantEmail, Double amount, BufferedInputStream imageStream,
    		Font f1, Font f2, PDF pdf) throws Exception
    {
    	//setup additional fonts
        Font f3 = new Font(pdf, CoreFont.HELVETICA_BOLD);
        f3.setSize(HEADER_FONT_SIZE);
        Font f4 = new Font(pdf, CoreFont.HELVETICA_OBLIQUE);
        f4.setSize(SMALL_FONT_SIZE); 

	    //new page for student report card		    
	    Page page = new Page(pdf, Letter.PORTRAIT);
        
        //school logo
        Image logo = new Image(pdf, imageStream, ImageType.JPEG);
        
        final int BOX_COMPANY_INFO_HEIGHT = (int) Math.max(logo.getHeight() + (2 * PADDING_SIZE), BOX_DEFAULT_COMPANY_HEIGHT);
        int studentBoxY = TOP_MARGIN + BOX_COMPANY_INFO_HEIGHT + SPACE_BTW_BOXES;
        
        int textStart = RIGHT_MARGIN + PADDING_SIZE;
        double headerStart = textStart + logo.getWidth() + PADDING_SIZE;

	    log.warning("Table created succesfully");
	    TextLine text = new TextLine(f3, schoolInfo[FormConstants.COMPANY_INFO_NAME_IDX]);
	    int textY = TOP_MARGIN + PADDING_SIZE + HEADER_FONT_SIZE;
	    text.setPosition(headerStart, textY);
	    
	    TextLine text1 = new TextLine(f4, schoolInfo[FormConstants.COMPANY_INFO_ADDR_IDX] );
	    int text1Y = textY + PADDING_SIZE + SMALL_FONT_SIZE;
	    text1.setPosition(headerStart, text1Y);
	    
	    TextLine text2 = new TextLine(f4, "Tel: " + schoolInfo[FormConstants.COMPANY_INFO_NUMS_IDX]);
	    int text2Y = text1Y + PADDING_SIZE + SMALL_FONT_SIZE;
	    text2.setPosition(headerStart, text2Y);
	    
	    TextLine text2b = new TextLine(f4, "Web: " + schoolInfo[FormConstants.COMPANY_INFO_WEB_IDX] + "   Email: " + schoolInfo[FormConstants.COMPANY_INFO_EMAIL_IDX]);
	    int text2bY = text2Y + PADDING_SIZE + SMALL_FONT_SIZE;
	    text2b.setPosition(headerStart, text2bY);		    
    	    
	    text.drawOn(page);
	    text1.drawOn(page);
	    text2.drawOn(page);
	    text2b.drawOn(page, true);
	    log.warning("company info generated succesfully");        
	    //box borders
	    double maxWidth = 550;
	    //setup student info
	    textY = studentBoxY + PADDING_SIZE + REGULAR_FONT_SIZE;
	    

	    Box schoolInfoBox = new Box(RIGHT_MARGIN, TOP_MARGIN, maxWidth, BOX_COMPANY_INFO_HEIGHT);
	    //logo.setPosition(textStart, TOP_MARGIN + (BOX_COMPANY_INFO_HEIGHT/2 - logo.getHeight()/2));
	    //passport.setPosition(textStart, TOP_MARGIN + (BOX_COMPANY_INFO_HEIGHT/2 - passport.getHeight()/2));
	    logo.placeIn(schoolInfoBox);
	    logo.setPosition(PADDING_SIZE, PADDING_SIZE * 2);
	    logo.drawOn(page);
	    //passport.drawOn(page);
	    //pdf.flush();
	    //schoolInfoBox.drawOn(page);	    
	    Line line = new Line(RIGHT_MARGIN, TOP_MARGIN + BOX_COMPANY_INFO_HEIGHT, RIGHT_MARGIN + maxWidth, TOP_MARGIN + BOX_COMPANY_INFO_HEIGHT);
	    line.setColor(RGB.OLD_GLORY_BLUE);
	    line.setWidth(3);
	    line.drawOn(page);
	    
	    Table table = null;
	    Point tableEnd = null;
	    textY = textY + SPACE_BTW_BOXES + PADDING_SIZE;

	    List<TextLine> lines = PDFGenerationHelper.getPrintLines(PrintHelper.getRequirementParagraph(amount, applicantEmail), f2, maxWidth - 20, maxWidth - 20);
	    for(TextLine l : lines)
	    {
	    	l.setPosition(textStart, textY);
	    	textY += SMALL_FONT_SIZE + PADDING_SIZE;
	    	l.drawOn(page);
	    }
	    textY = textY + SPACE_BTW_BOXES + PADDING_SIZE;
	    
    	//convert bill desc itemized data to format experted by print report fn
    	
	    //form table settings
	    HashSet<Integer> opts = new HashSet<Integer>();
	    opts.add(-1);
    	List<List<Cell>> itemizedBillDesc = PDFGenerationHelper.convertTableMessageToPDFTable(requirementsTable, f1, f2, f2, opts, true);      	 		    
	    table = new Table(f1, f2);
	    table.setData(itemizedBillDesc, Table.DATA_HAS_1_HEADER_ROWS);
	    table.setLineWidth(0.2);
	    table.setPosition(RIGHT_MARGIN, textY);
	    table.setCellPadding(PADDING_SIZE);
	    table.autoAdjustColumnWidths();
	    
	    //print payments table for this deposit
	    while(true)
	    {
		    tableEnd = table.drawOn(page);
		    if(!table.hasMoreData())
		    {
		    	textY = (int) tableEnd.getY();
		    	break;
		    }
		    else
		    {
		    	page = new Page(pdf, Letter.PORTRAIT);
		    	textY = TOP_MARGIN;
		    }
	    }
	    double lineY = tableEnd.getY() + SPACE_BTW_BOXES + SPACE_BTW_BOXES;
	    TextLine optionsLine = new TextLine(f1, PrintHelper.PICKUP_INTRO);
	    optionsLine.setPosition(textStart, lineY);
	    optionsLine.setUnderline(true);
	    optionsLine.drawOn(page);
	    optionsLine.setUnderline(false);
	    optionsLine.setFont(f2);
	    lineY = lineY + SMALL_FONT_SIZE + PADDING_SIZE;
	    for(String s : PrintHelper.PICKUP_OPTIONS)
	    {
	    	optionsLine.setText(s);
	    	optionsLine.setPosition(textStart, lineY);
	    	optionsLine.drawOn(page);
	    	lineY += SMALL_FONT_SIZE + PADDING_SIZE;
	    }
        
        log.warning("deposit statements generated succesfully");
    }    
    
    
	public static String getApplicationFormLink(String loanID, HttpServletRequest req)
	{
		Key<LoanApplicationFormData> formKey = ObjectifyService.factory().stringToKey(loanID);
		Objectify ofy = ObjectifyService.begin();
		Map<Key<Object>, Object> objMap = ofy.get(formKey, formKey.getParent());
		LoanApplicationFormData loanObj = (LoanApplicationFormData) objMap.get(formKey);
		ConsumerLoanSalesLead lead = (ConsumerLoanSalesLead) objMap.get(formKey.getParent());
		String appId = lead.getLeadID() == null ? "" : String.valueOf(lead.getLeadID());
		List<TableMessage> loanDataTable = PrintHelper.convertLoanToPDFInput(loanObj, appId);
        HttpSession sess = req.getSession();
        sess.setAttribute(getAppFormDataName(loanID), loanDataTable);
        return "http://" + req.getHeader("Host") + "/print/form?" + FormConstants.LOAN_ID_PARAM + "=" + loanID + "&" + OP_TYPE + "=" + APP_FORM_OP;
	}

	public static String getGuarantorFormLink(String loanID, HttpServletRequest req)
	{
		Objectify ofy = ObjectifyService.begin();
		Key<GuarantorFormData> formKey = ObjectifyService.factory().stringToKey(loanID);
		log.log(Level.WARNING, "Form Key: " + formKey.toString());
		Key<ConsumerLoanSalesLead> pk = formKey.getParent();
		log.log(Level.WARNING, "Parent Key" + pk.toString());
		Key<GuarantorFormData> firstKey = GuarantorFormData.getFormKey(pk, true);
		log.log(Level.WARNING, "First Key: " + firstKey.toString());
		GuarantorFormData loanObj = ofy.get(firstKey);
		List<TableMessage> loanDataTable = PrintHelper.convertGuarantorToPDFInput(ofy, loanObj);
        HttpSession sess = req.getSession();
        sess.setAttribute(getGuarantorTableName(loanID,firstKey.equals(formKey)), loanDataTable);
        sess.setAttribute(getGuarantorDataName(loanID,firstKey.equals(formKey)), loanObj.getFormData());

        return "http://" + req.getHeader("Host") + "/print/form?" + FormConstants.LOAN_ID_PARAM + "=" + loanID + "&" + OP_TYPE + "=" + GUARANTOR_FORM_OP;
	}
	
	public static String getCheckListFormLink(String loanID, HttpServletRequest req)
	{
		Objectify ofy = ObjectifyService.begin();
		Key<ConsumerLoanSalesLead> leadKey = ObjectifyService.factory().stringToKey(loanID);
		ConsumerLoanSalesLead loanObj = ofy.get(leadKey);
		List<TableMessage> loanDataTable = PrintHelper.getRequirements(loanObj.getRequestedAmount());
        HttpSession sess = req.getSession();
        sess.setAttribute(getCheckListTableName(loanID), loanDataTable);
        sess.setAttribute(getCheckListAmountName(loanID), new Double(loanObj.getRequestedAmount()));
        sess.setAttribute(getCheckListTitleName(loanID), leadKey.getParent().getName());
        return "http://" + req.getHeader("Host") + "/print/form?" + FormConstants.LOAN_ID_PARAM + "=" + loanID + "&" + OP_TYPE + "=" + CHECKLIST_FORM_OP;
	}	
	
    private static void printLine(String line, int xPos, int yPos, Font font, Page page) throws Exception
    {
    	printLine(line, xPos, yPos, font, page, null);
    }
    
    private final static int NUM_OF_EXIS_LOAN_ROWS = 3;
    public static List<TableMessage> getExistingLoanTable()
    {
    	List<TableMessage> result = new ArrayList<TableMessage>();
    	TableMessageHeader h = new TableMessageHeader(4);
    	h.setText(0, "Name of Bank or Institution", TableMessageContent.TEXT);
    	h.setText(1, "Principal Amount", TableMessageContent.NUMBER);
    	h.setText(2, "Outstanding Balance", TableMessageContent.NUMBER);
    	h.setText(3, "Monthly Repayment", TableMessageContent.NUMBER);
    	result.add(h);
    	for(int i = 0; i < NUM_OF_EXIS_LOAN_ROWS; i++)
    	{
	    	TableMessage row = new TableMessage(1, 3, 0);
	    	row.setText(0, null);
	    	row.setNumber(0, null);
	    	row.setNumber(1, null);
	    	row.setNumber(2, null);
	    	result.add(row);
    	}
    	return result;
    }   
    
    private static void printLine(String line, int xPos, int yPos, Font font, Page page, double[] color) throws Exception
    {
    	if(line == null)
    		line = "{BLANK}";
	    
    	TextLine text = new TextLine(font, line);
	    text.setPosition(xPos, yPos);
	    
	    if(color != null)
	    	text.setColor(color);
	    
	    text.drawOn(page);
    }
	
	public static String getAppFormDataName(String id)
	{
		return SESS_PREFIX + id + "formdata";
	}
	
	public static String getGuarantorDataName(String id, boolean isFirst)
	{
		return SESS_PREFIX + id + "guardata" + isFirst;
	}
	
	public static String getGuarantorTableName(String id, boolean isFirst)
	{
		return SESS_PREFIX + id + "guardtab" + isFirst;
	}
	
	public static String getCheckListTitleName(String id)
	{
		return SESS_PREFIX + id + "checktitle";
	}
	
	public static String getCheckListTableName(String id)
	{
		return SESS_PREFIX + id + "checktable";
	}
	
	public static String getCheckListAmountName(String id)
	{
		return SESS_PREFIX + id + "checkamount";
	}	
}