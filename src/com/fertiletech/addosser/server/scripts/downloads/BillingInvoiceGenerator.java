/**
 * 
 */
package com.fertiletech.addosser.server.scripts.downloads;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fertiletech.addosser.server.loanmktdata.EntityConstants;
import com.fertiletech.addosser.shared.FormConstants;
import com.fertiletech.addosser.shared.exceptions.MissingEntitiesException;
import com.fertiletech.addosser.shared.table.TableMessage;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.pdfjet.Box;
import com.pdfjet.Cell;
import com.pdfjet.CoreFont;
import com.pdfjet.Font;
import com.pdfjet.Image;
import com.pdfjet.ImageType;
import com.pdfjet.Letter;
import com.pdfjet.PDF;
import com.pdfjet.Page;
import com.pdfjet.Point;
import com.pdfjet.RGB;
import com.pdfjet.Table;
import com.pdfjet.TextLine;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class BillingInvoiceGenerator extends HttpServlet
{
    private final static int RIGHT_MARGIN = 40;
    private final static int TOP_MARGIN = 40;
    private final static int ITEM_COL_WIDTH = 170;
    private final static int NOTES_COL_WIDTH = 225;
    private final static int AMOUNT_COL_WIDTH = 75;
    private final static int PADDING_SIZE = 5;
    private final static int SPACE_BTW_BOXES = 12;
    private final static int HEADER_FONT_SIZE = 12;
    private final static int REGULAR_FONT_SIZE = 10;
    private final static int SMALL_FONT_SIZE = 8;
    private final static float REQ_REP_WIDTH = 550;
 
    private final static int BOX_STUDENT_INFO_HEIGHT = 80;
    private final static int BOX_ADDRESS_INFO_HEIGHT = 50;
    private final static int BOX_COMMENT_INFO_HEIGHT = 80;
    private final static int BOX_OFFICIAL_INFO_HEIGHT = 30;
    private final static int BOX_DEFAULT_COMPANY_HEIGHT = 60;
     
    private final static String SESS_PREFIX = "fertiletech.";
    
    private static final Logger log = Logger.getLogger(BillingInvoiceGenerator.class.getName());
    
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException
    {
		/*PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
        try 
        {
			LoginPortal.verifyRole(allowedRoles, req);
		} catch (LoginValidationException e) 
		{
			res.getOutputStream().println("<html><body><b>Illegal Access: " + e.getMessage() +
					"</b></body></html>");
			return;
		}*/
		
    	Objectify ofy = ObjectifyService.begin();
    	
    	String id = null; //req.getParameter(TaskConstants.BILL_DESC_KEY_PARAM);
    	
    	if(id == null)
    	{
    		res.setContentType("text/html");
    		res.getOutputStream().println("<b><font color='red'>Unable to process request. Please ensure this page is accessed via a link" +
    				" recently generated from the report cards panel. If problem persists contact IT</font></b>");
    		return;
    	}
    	
    	HttpSession sess = req.getSession();
    	String type = req.getParameter("type");
		String fileName = EntityConstants.DATE_FORMAT.format(new Date(Long.valueOf(id)));
		fileName = fileName.replace(" ", "-");
		String[] companyInfo = (String[]) sess.getAttribute(getSchoolInfoSessionName(id));

    	if(type.equals("depstat"))
    	{
    		TableMessage[] depInfo = (TableMessage[]) sess.getAttribute(getDepositDescSessionName(id));
    		List<TableMessage>[] payments = (List<TableMessage>[]) sess.getAttribute(getDepositListSessionName(id));
    		TableMessage addressee = (TableMessage) sess.getAttribute(getDepositOwnerName(id));
    		Date[] queryDates = (Date[]) sess.getAttribute(getDepositQueryDatesName(id));
	    	if(depInfo == null || payments == null || payments.length != depInfo.length)
	    	{
	    		res.setContentType("text/html");
	    		res.getOutputStream().println("<b><font color='red'>Unable to retrieve deposit session data. Please ensure this page is accessed via a link" +
	    				" recently generated from the deposit statements panel. If problem persists contact IT</font></b>" + (depInfo==null?depInfo:depInfo.length) +" , "+(payments==null?payments:payments.length)); 
	    		return;    		
	    	}    	
			fileName = fileName + "-deposit-statement.pdf";
	        res.setContentType("application/octet-stream");        
	        res.setHeader("Content-disposition", "attachment; filename=" + fileName);	        
	        OutputStream out = res.getOutputStream();
	        writeDepositStatementToStream(ofy, out, queryDates[0], queryDates[1], addressee, depInfo, payments, companyInfo);	    	
    	}
    	else
    	{
    		res.setContentType("text/html");
    		res.getOutputStream().println("<b><font color='red'>Unrecognized op type. Are you sure you have the right url? If problem persists contact IT</font></b>"); 
    	}
        /*out.flush();
        log.warning("flushing done");
        out.close();
        log.warning("close done");*/
    }

    private static void writeDepositStatementToStream(Objectify ofy, OutputStream out, Date startDate, Date endDate, TableMessage addressee, TableMessage[] depositInfo, List<TableMessage>[] payments, String[] schoolInfo)
    {    	    	
    	//init pdf file
    	try
    	{        	
	        PDF pdf = new PDF(out);
	        
	        Font f1 = new Font(pdf, CoreFont.HELVETICA_BOLD);
	        f1.setSize(SMALL_FONT_SIZE-1);
	        Font f2 = new Font(pdf, CoreFont.HELVETICA);
	        f2.setSize(SMALL_FONT_SIZE-1);
	        
			Key<InetImageBlob> logoKey = new Key(InetImageBlob.class, FormConstants.PDF_REPORT_LOGO);
			InetImageBlob logo = ofy.find(logoKey);
			if(logo == null)
			{
				throw new IllegalArgumentException("Your company hasn't been properly setup for report" +
	    				" generation or setup data has been corrupted. Please contact IT to investigate");    					
			}	        
	        BufferedInputStream bis =
                new BufferedInputStream(new ByteArrayInputStream(logo.getImage()));
    	
	    	printDepositStatements(startDate, endDate, addressee, payments, depositInfo, schoolInfo, bis, f1, f2, pdf);
	    	pdf.flush();
    	}
    	catch(Exception ex)
    	{
    		StackTraceElement[] exLocations = ex.getStackTrace();
    		String msg = "";
    		for(StackTraceElement  exLoc : exLocations)
    		{
    			String exMsg = exLoc.toString();
    			if(exMsg.startsWith("com.fertile"))
    				msg += exMsg + "\n";
    		}
    		log.severe(msg);    		
    		throw new RuntimeException(ex.fillInStackTrace());
    	}    	
    }
    
    
    
    
    private static void printDepositStatements(Date startDate, Date endDate, TableMessage addressee, List<TableMessage>[] payments,
    		TableMessage[] deposits, String[] schoolInfo, BufferedInputStream imageStream,
    		Font f1, Font f2, PDF pdf) throws Exception
    {
    	//setup additional fonts
        Font f3 = new Font(pdf, CoreFont.HELVETICA_BOLD);
        f3.setSize(HEADER_FONT_SIZE);
        Font f4 = new Font(pdf, CoreFont.HELVETICA_OBLIQUE);
        f4.setSize(SMALL_FONT_SIZE); 

	    //new page for student report card		    
	    Page page = new Page(pdf, Letter.PORTRAIT);
        
        //school logo
        Image logo = new Image(pdf, imageStream, ImageType.JPEG);
        logo.scaleBy(0.85);
        
        final int BOX_COMPANY_INFO_HEIGHT = (int) Math.max(logo.getHeight() + (2 * PADDING_SIZE), BOX_DEFAULT_COMPANY_HEIGHT);
        int studentBoxY = TOP_MARGIN + BOX_COMPANY_INFO_HEIGHT + SPACE_BTW_BOXES;
        int tableY = studentBoxY + BOX_STUDENT_INFO_HEIGHT + SPACE_BTW_BOXES;
        
        int textStart = RIGHT_MARGIN + PADDING_SIZE;

	    log.warning("Table created succesfully");
	    TextLine text = null; //new TextLine(f3, schoolInfo[DTOConstants.COMPANY_INFO_NAME_IDX]);
	    int textY = TOP_MARGIN + PADDING_SIZE + HEADER_FONT_SIZE;
	    text.setPosition(textStart, textY);
	    
	    TextLine text1 = null; //new TextLine(f4, schoolInfo[DTOConstants.COMPANY_INFO_ADDR_IDX] );
	    int text1Y = textY + PADDING_SIZE + SMALL_FONT_SIZE;
	    text1.setPosition(textStart, text1Y);
	    
	    TextLine text2 = null; //new TextLine(f4, "Tel: " + schoolInfo[DTOConstants.COMPANY_INFO_NUMS_IDX]);
	    int text2Y = text1Y + PADDING_SIZE + SMALL_FONT_SIZE;
	    text2.setPosition(textStart, text2Y);
	    
	    TextLine text2b = null; //new TextLine(f4, "Web: " + schoolInfo[DTOConstants.COMPANY_INFO_WEB_IDX] + "   Email: " + schoolInfo[DTOConstants.COMPANY_INFO_EMAIL_IDX]);
	    int text2bY = text2Y + PADDING_SIZE + SMALL_FONT_SIZE;
	    text2b.setPosition(textStart, text2bY);		    
    	    
	    text.drawOn(page);
	    text1.drawOn(page);
	    text2.drawOn(page);
	    text2b.drawOn(page, true);
	    log.warning("company info generated succesfully");        
	    //box borders
	    double maxWidth = 550;
	    //setup student info
	    textY = studentBoxY + PADDING_SIZE + REGULAR_FONT_SIZE;
	    //printLine("NAME: " + addressee.getText(DTOConstants.TNT_FNAME_IDX) + " " + addressee.getText(DTOConstants.TNT_LNAME_IDX), textStart, textY, f2, page);
	    //printLine("COMPANY: " + addressee.getText(DTOConstants.TNT_COMPANY_IDX), textStart + (int) maxWidth/2, textY, f2, page);

	    
	    if(true)//addressee.getText(DTOConstants.TNT_APT_ID_IDX) != null && addressee.getText(DTOConstants.TNT_APT_ID_IDX).trim().length() > 0)
	    {
		    textY = textY + PADDING_SIZE + REGULAR_FONT_SIZE;
		    //printLine("BUILDING:  " + addressee.getText(DTOConstants.TNT_BUILDING_IDX),
		    //		textStart, textY, f2, page);	    	
		    //printLine("APT: " + addressee.getText(DTOConstants.TNT_APT_IDX),
		    //		textStart + (int) maxWidth/2, textY, f2, page);	    	
	    }
	    else
	    {
		    textY = textY + PADDING_SIZE + REGULAR_FONT_SIZE;
		    //printLine("ADDRESS: " + addressee.getText(DTOConstants.TNT_ADDRESS_IDX), textStart, textY, f2, page);
	    }

	    textY = textY + PADDING_SIZE + REGULAR_FONT_SIZE;
	    //printLine("TEL: " + addressee.getText(DTOConstants.TNT_PRIMARY_PHONE_IDX), textStart, textY, f2, page);
	    //printLine("EMAIL: " + addressee.getText(DTOConstants.TNT_PRIMARY_EMAIL_IDX) , textStart + (int) maxWidth/2, textY, f2, page);
	    
	    
	    textY = textY + PADDING_SIZE + REGULAR_FONT_SIZE;
	    printLine("STATEMENT PERIOD: " + EntityConstants.DATE_FORMAT.format(startDate) + " to " + EntityConstants.DATE_FORMAT.format(endDate), textStart, textY, f2, page);	    
	    printLine("NO. OF ACCOUNTS INCLUDED: " + deposits.length, textStart + (int) maxWidth/2, textY, f2, page);	    

	    log.warning("tenant info generated ok");
	    
	    

	    Box schoolInfoBox = new Box(RIGHT_MARGIN, TOP_MARGIN, maxWidth, BOX_COMPANY_INFO_HEIGHT);
	    logo.setPosition(RIGHT_MARGIN + maxWidth - logo.getWidth() - PADDING_SIZE, TOP_MARGIN + (BOX_COMPANY_INFO_HEIGHT/2 - logo.getHeight()/2));
	    logo.drawOn(page);
	    schoolInfoBox.drawOn(page);
	    Box studentBox = new Box(RIGHT_MARGIN, studentBoxY , maxWidth, BOX_STUDENT_INFO_HEIGHT);
	    studentBox.drawOn(page);
	    
	    
	    boolean first = true;
	    Table table = null;
	    Point tableEnd = null;
	    textY = textY + SPACE_BTW_BOXES + PADDING_SIZE;
        for(int i = 0; i < deposits.length; i++)
        { 		     	
        	//convert bill desc itemized data to format experted by print report fn
        	List<TableMessage> payList = payments[i];
        	log.warning(payList.toString());

        	HashSet<Integer> ignore = new HashSet<Integer>();
        	List<List<Cell>> itemizedBillDesc = PDFGenerationHelper.convertTableMessageToPDFTable(payList, f1, f2, ignore);        	
        	
		    //print deposit header
		    textY = textY + PADDING_SIZE + REGULAR_FONT_SIZE + PADDING_SIZE;
		    StringBuilder depositHeader = new StringBuilder("[Account: ").append(deposits[i].getText(0)).append(" / ").append(deposits[i].getText(1));
		    depositHeader.append("]    [Balance: ").append(EntityConstants.NUMBER_FORMAT.format(deposits[i].getNumber(0))).
		    		append("]    [Last Modified: ").append(EntityConstants.DATE_FORMAT.format(deposits[i].getDate(0)) + "]");
		    double[] headerPositiveColr = {8/255.0, 159/255.0, 82/255.0};
		    double[] headerNegativeColr = {192/255.0, 50/255.0, 46/255.0};
		    double[] lineColor = deposits[i].getNumber(0) > 0? headerPositiveColr : headerNegativeColr ;
		    printLine(depositHeader.toString(), textStart, textY, f1, page, lineColor);
		    
		    //table aettings
		    table = new Table(f1, f2);
		    table.setData(itemizedBillDesc, Table.DATA_HAS_1_HEADER_ROWS);
		    table.setLineWidth(0.2);
		    table.setPosition(RIGHT_MARGIN, textY + (PADDING_SIZE));
		    table.setCellPadding(PADDING_SIZE);
		    table.setColumnWidth(0, maxWidth * 0.05);
		    table.setColumnWidth(1, maxWidth * 0.5);
		    table.setColumnWidth(2, maxWidth * 0.05);
		    table.setColumnWidth(3, maxWidth * 0.11);
		    table.setColumnWidth(4, maxWidth * 0.11);
		    table.setColumnWidth(5, maxWidth * 0.09);
		    table.setColumnWidth(6, maxWidth * 0.09);
		    table.rightAlignNumbers();
		    
		    //print payments table for this deposit
		    while(true)
		    {
			    tableEnd = table.drawOn(page);
			    if(!table.hasMoreData())
			    {
			    	textY = (int) tableEnd.getY();
			    	break;
			    }
			    else
			    {
			    	page = new Page(pdf, Letter.PORTRAIT);
			    	textY = TOP_MARGIN;
			    }
		    }    		    
        }
        //setup official box
        double officialBoxY = tableEnd.getY() + SPACE_BTW_BOXES;
        Box officialBox = new Box(RIGHT_MARGIN, officialBoxY , maxWidth, BOX_OFFICIAL_INFO_HEIGHT);
        TextLine text7 = new TextLine(f2, "Official Name:                                                                                    ");
        double text7Y = officialBoxY + PADDING_SIZE * 2+ REGULAR_FONT_SIZE;
        text7.setPosition(textStart, text7Y);
        text7.setUnderline(true);
        TextLine text8 = new TextLine(f2, "Official Signature:                                                                           Date: " 
        			+ EntityConstants.DATE_FORMAT.format(new Date()));
        text8.setUnderline(true);
        text8.setPosition(textStart + maxWidth/2, text7Y);
        text7.drawOn(page);
        text8.drawOn(page);
        officialBox.drawOn(page);       
        log.warning("deposit statements generated succesfully");
    }
    
    
    private static void printLine(String line, int xPos, int yPos, Font font, Page page) throws Exception
    {
    	printLine(line, xPos, yPos, font, page, null);
    }
    
    private static void printLine(String line, int xPos, int yPos, Font font, Page page, double[] color) throws Exception
    {
    	if(line == null)
    		line = "{BLANK}";
	    TextLine text = new TextLine(font, line);
	    text.setPosition(xPos, yPos);
	    if(color != null)
	    	text.setColor(color);
	    text.drawOn(page);
    }
    
    public static HashMap<String, double[]> getCurrencyColors()
    {
    	HashMap<String, double[]> result = new HashMap<String, double[]>();
    	result.put("NGN", RGB.OLD_GLORY_RED);
    	result.put("USD", RGB.OLD_GLORY_BLUE);
    	result.put("GBP", RGB.GREEN);
    	result.put("EUR", RGB.MAGENTA);
    	return result;
    }
    
    
    public static HashMap<String, List<TableMessage>> getItemizedBillDescriptions(Collection billList)
    {
        HashMap<String, List<TableMessage>> descriptions = new HashMap<String, List<TableMessage>>();
        /*for(InetBill b  : billList)
        	descriptions.put(b.getKey().getString(), AccountManagerImpl.getBillDescriptionInfo(b));*/
        return descriptions;
    }
    

	
	public static String getDepositStatementLink(String depOwner, String[] depKeyStrs, Date startDate, Date endDate,
			HttpServletRequest req)
		throws MissingEntitiesException
	{	
		return "";
/*        Objectify ofy = ObjectifyService.begin();
        Key<? extends Contact> addresseeKey = ofy.getFactory().stringToKey(depOwner);
        Contact adresseeObj = ofy.get(addresseeKey);
        List<Key<InetResidentAccount>> depKeys = new ArrayList<Key<InetResidentAccount>>(depKeyStrs.length);
        for(String bkStr : depKeyStrs)
        {
        	Key<InetResidentAccount> bk = ofy.getFactory().stringToKey(bkStr);
            log.warning("Print Deposit Requested by " + LoginHelper.getLoggedInUser(req) + " for " + startDate + " to " + endDate + " and " + bk);
        	depKeys.add(bk);
        }
        Collection<InetResidentAccount> depList = ReadDAO.getEntities(depKeys, ofy, true).values();
        HashSet<Key<InetBill>> invoiceList = new HashSet<Key<InetBill>>();
        HashMap<Key<InetResidentAccount>, List<LedgerEntry>> paymentMap = new HashMap<Key<InetResidentAccount>, List<LedgerEntry>>();
        //first pass, fetch payments and invoices
        for(InetResidentAccount dep : depList)
        {
        	List<LedgerEntry> payments = InetDAO4Accounts.getLedgerEntries(dep.getKey(), startDate, endDate, ofy);
        	Collections.sort(payments, dep);
        	paymentMap.put(dep.getKey(), payments);
        	for(LedgerEntry pay : payments)
        	{
        		if(pay instanceof InetWithdrawal)
            		invoiceList.add(((InetWithdrawal) pay).getAllocatedBill());        			
        	}
        }
        Map<Key<InetBill>, InetBill> invoiceMap = ReadDAO.getEntities(invoiceList, ofy, true);
        List<TableMessage>[] tablesToPrint = new List[depList.size()];
        TableMessage[] depositInfo = new TableMessage[depList.size()];
        
        Apartment a = null;
        if(adresseeObj instanceof Tenant)
        	a = ofy.get(((Tenant) adresseeObj).getApartmentUnit());
        TableMessage addressee = TenantDAOManagerImpl.getContactDTO(adresseeObj, a);
        
        int i = 0;
        for(InetResidentAccount dep : depList)
        {
        	TableMessage m = new TableMessage(2,2,2);
        	m.setText(0, dep.getKey().getName());
        	Key<Tenant> tk = dep.getResident();
        	m.setText(1, tk.getParent().getName() + "/" + tk.getParent().getParent().getName());
        	m.setNumber(0, dep.getCurrentAmount());
        	m.setNumber(1, dep.getNumberOfTxns());
        	m.setDate(0, dep.getLastModifiedDate());
        	m.setDate(1, dep.getCreateDate());
        	m.setMessageId(dep.getKey().getString());
        	depositInfo[i++] = m;        	
        }
        
        Arrays.sort(depositInfo, new Comparator<TableMessage>() {

        	int[] compareFields = {1, 0};
			@Override
			public int compare(TableMessage o1, TableMessage o2) {
				for(int i = 0; i < compareFields.length; i++)
				{
					int compareIdx = i;
					int compare = o1.getText(compareIdx).compareToIgnoreCase(o1.getText(compareIdx));
					if(compare == 0) continue;
					return compare;
				}
				return 0;
			}
		});                
        
        for(i = 0; i < depositInfo.length; i++)
        {
        	Key<InetResidentAccount> raKey = ObjectifyService.factory().stringToKey(depositInfo[i].getMessageId());
        	//build deductions table
        	List<LedgerEntry> payments = paymentMap.get(raKey);
        	tablesToPrint[i] = DTOUtils.getLedgerDTO(payments, depositInfo[i].getMessageId(), InetDAO4Accounts.getCompanyAcctsMap(ofy));        	
        }

        HttpSession sess = req.getSession();
        String id = String.valueOf(new Date().getTime());
        sess.setAttribute(getDepositListSessionName(id), tablesToPrint);
        sess.setAttribute(getDepositDescSessionName(id), depositInfo);
        sess.setAttribute(getSchoolInfoSessionName(id), DTOConstants.getCompanyInfo());
        sess.setAttribute(getDepositOwnerName(id), addressee);
        Date[] qd = {startDate, endDate};
        sess.setAttribute(getDepositQueryDatesName(id), qd);
        return "<b>DOWNLOAD: </b><a href='" + "http://" + req.getHeader("Host") + "/pdf/billing?" + 
        		TaskConstants.BILL_DESC_KEY_PARAM + "=" + id + "&type=depstat'>Download statements for " + 
        		depList.size() + " resident account(s)</a>";*/         
	}
	
	

	public static String getDepositListSessionName(String id)
	{
		return SESS_PREFIX + id + "deplist";
	}
	
	public static String getDepositDescSessionName(String id)
	{
		return SESS_PREFIX + id + "depdesc";
	}
	
	public static String getDepositOwnerName(String id)
	{
		return SESS_PREFIX + id + "depownr";
	}
	
	public static String getDepositQueryDatesName(String id)
	{
		return SESS_PREFIX + id + "depdates";
	}	
	
	
	public static String getSchoolInfoSessionName(String id)
	{
		return SESS_PREFIX + id + "compinv";
	}	
}
