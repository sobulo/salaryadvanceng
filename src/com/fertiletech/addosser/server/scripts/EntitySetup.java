/**
 * 
 */
package com.fertiletech.addosser.server.scripts;


import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Logger;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fertiletech.addosser.server.loanmktdata.ApplicationParameters;
import com.fertiletech.addosser.server.loanmktdata.ConsumerLoanSalesLead;
import com.fertiletech.addosser.server.loanmktdata.EntityConstants;
import com.fertiletech.addosser.server.loanmktdata.LoanApplicationFormData;
import com.fertiletech.addosser.server.loanmktdata.LoanMktDAO;
import com.fertiletech.addosser.server.login.LoginHelper;
import com.fertiletech.addosser.server.messaging.MessagingDAO;
import com.fertiletech.addosser.server.messaging.SendGridController;
import com.fertiletech.addosser.server.tasks.TaskQueueHelper;
import com.fertiletech.addosser.shared.DTOConstants;
import com.fertiletech.addosser.shared.exceptions.DuplicateEntitiesException;
import com.fertiletech.addosser.shared.workstates.WorkflowStateInstance;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.Query;



/**
 * @author Segun Razaq Sobulo
 *
 */
public class EntitySetup extends HttpServlet{
	private static final Logger log = Logger.getLogger(EntitySetup.class.getName());
	
	static{
		LoanMktDAO.registerClassesWithObjectify();
	}
	/**
	 * web.xml contains entries to ensure only registered developers for the app can execute
	 * this script. Does not go through login logic 
	 */
	
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException
	{
		ServletOutputStream out = res.getOutputStream();
		res.setContentType("text/html");
		try
		{
			String type = req.getParameter("type");
			if(type == null)
			{
				out.println("<b><font color='red'>Please specify a type</font></b>");
				return;
			}
			out.println("<b>setup starting</b><br/>");
			if(type.equals("params"))
			{
				String result = createParameter();
				out.println(result);
				log.warning(result + " triggered by " + LoginHelper.getLoggedInUser(req));
			}
			else if(type.equals("email"))
			{
				String fromAddress = "hello@salaryadvanceng.com";
				String bccAddress = "customer-care@salaryadvanceng.com";
		    	SendGridController controller = MessagingDAO.createEmailController(MessagingDAO.PUBLIC_EMAILER, fromAddress, bccAddress, 1000, 1000, 10000);
		    	log.warning("created: " + controller.getKey());
		    	controller = MessagingDAO.createEmailController(MessagingDAO.SYSTEM_EMAILER, fromAddress, bccAddress, 10000, 10000, 100000);
		    	log.warning("created: " + controller.getKey());		    	
			}
			else if(type.equals("address"))
			{
				Date start = new Date(116, 9, 1);
				Date end = new Date();
				List<Key<ConsumerLoanSalesLead>> leadKeys = LoanMktDAO.getSalesLeadsByDate(start, end).listKeys();
				for(Key<ConsumerLoanSalesLead> key : leadKeys)
					TaskQueueHelper.scheduleAddressUpdate(key.getString());
				out.println("Scheduled: " + leadKeys.size() +" tasks<br/><hr/>");
				out.flush();
			}
			else if(type.equals("geo"))
			{
				Date start = new Date(116, 9, 1);
				Date end = new Date();
				List<Key<ConsumerLoanSalesLead>> leadKeys = LoanMktDAO.getSalesLeadsByDate(start, end).listKeys();
				for(Key<ConsumerLoanSalesLead> key : leadKeys)
					TaskQueueHelper.scheduleGeoCoding(key.getString());
				out.println("Scheduled: " + leadKeys.size() +" tasks<br/><hr/>");
				out.flush();
			}
			else if(type.equals("valentine") || type.equals("vt"))
			{
				out.println("Commencing --> " + type + "br/>");
				Calendar cal = Calendar.getInstance();
				 Date start = new GregorianCalendar(2018, 0, 1).getTime();
				 Date end = new Date();
			
				Iterable<ConsumerLoanSalesLead> salesLeads = LoanMktDAO.getSalesLeadsByDate(start, end);
				

				final String[] TESTIMONIALS = {"http://info.salaryadvanceng.com/_/rsrc/1373635766843/testimonials/eb.png",
						"http://info.salaryadvanceng.com/_/rsrc/1373635806220/testimonials/og.png",
						"http://info.salaryadvanceng.com/_/rsrc/1375093001385/testimonials/david.png",
						"http://info.salaryadvanceng.com/_/rsrc/1375095488950/testimonials/test.png",
						"http://info.salaryadvanceng.com/_/rsrc/1373640871822/testimonials/la.png",
						"http://info.salaryadvanceng.com/_/rsrc/1375093111592/testimonials/toyin.png"};
				final Random RANDMONIAL = new Random();
				int count = 0;
				int leadCount = 0;
				for(ConsumerLoanSalesLead lead : salesLeads)
				{
					try
					{
					leadCount++;
					String recipientList = lead.getEmail();
					
					if(type.equals("vt"))
					{
						if(!recipientList.equalsIgnoreCase("segun.sobulo@gmail.com"))
							continue;
							
					}
					String imageDisplay = TESTIMONIALS[RANDMONIAL.nextInt(TESTIMONIALS.length)];
					String text = "Dear " + lead.getFullName() +", "
							+ ""
							+ "Did you know that you can now get an instant loan quote when you apply for a loan at www.salaryadvanceng.com. Simply login to complete your loan application on our portal which you started "
							+ DateFormat.getDateInstance().format(lead.getDateCreated()) 
							+ " We look forward to serving you with our competitive interest rate and access to a loan facility which is accessible within 24 hours of completing your documentation. "
							+ " To complete your application, visit www.salaryadvanceng.com. "
							+ ""
							+ " We truly do care about giving you the financial flexibility you want in a prompt fashion. To contact us for further  enquiries (e.g issues with login, requirements etc), you can call 09037414749 or email hello@salaryadvanceng.com."
							+ " "
							+ " Get your loan today " + lead.getFullName() 
							+ ""
							+ " Best Regards, "
							+ ""
							+ " SALARY ADVANCE NG.";
					String html = "<img style='float:left; margin: 20px' src='"+imageDisplay+"'/>" 
							+ "Dear " + lead.getFullName() +", "
							+ "<p>Did you know you can now get an instant loan quote when you apply for a loan at <a href='http://www.salaryadvanceng.com'>www.salaryadvanceng.com</a>. Simply login to complete your loan application on our portal which you started "
							+ DateFormat.getDateInstance().format(lead.getDateCreated()) 
							+ " </p><p>We look forward to serving you with our competitive interest rate and access to a loan facility which is accessible within 24 hours of submitting your documentation. "
							+ "</p><p>We truly do care about giving you the financial flexibility you want in a prompt fashion. To contact us for further  enquiries (e.g issues with login, requirements etc), you can call 09037414749 or email hello@salaryadvanceng.com."
							+ "</p><p>Get your loan today " + lead.getFullName() + ". <a href='http://www.salaryadvanceng.com/#apply/" + lead.getKey().getString()
							+ "'>Click here to continue your application</a>" + (lead.getMonthlySalary()==null?" and complete your employment history to get an instant loan quote":" and to view your loan quote")
							+ "</p><p>Best Regards, "
							+ "<br/>SALARY ADVANCE NG.</p>";
							
					String[] messageContent = {text, html};
					MessagingDAO.scheduleSendEmail(recipientList, messageContent, "Find out how much you can borrow. Get a free instant loan quote now", false);
					count++;
					if(type.equals("vt"))
					{
						out.println("<p><b>message sent: " + count + "</b><br/>");
						out.println("<b>Text:</b><br/>" + messageContent[0]);
						out.println("<br/><hr/><b>HTML:</b><br/>" + messageContent[1] + "<br/><hr/></p>");
					}
					}catch(Exception tx)
					{
						out.print("<p>Exception sending mail for: " + lead.getEmail() + "<br/> Sent: " + count +
								" <br/>Attempted: " + leadCount + "<br/> Message: " + tx.getMessage() +"</p>");
					}
				}
				out.println("Total leads: " + leadCount + " Total mails: " + count);
	
			}
			out.println("<b>setup completed</b><br/>");
		}
		catch(Exception ex)
		{
			out.println("An error occured when running tasks: " + ex.getMessage());
			out.flush();
		}
	}
	
	
	/*private static void saveForms(Objectify ofy, ArrayList<Key<LoanApplicationFormData>> batchedKeys, 
			HashMap<Key<LoanApplicationFormData>, Date> formDates)
	{
		Map<Key<LoanApplicationFormData>, LoanApplicationFormData> batchedForms = ofy.get(batchedKeys);
		//ArrayList<LoanApplicationFormData> saveForms = new ArrayList<>(); 
		for(Key<LoanApplicationFormData> updateKey : batchedForms.keySet())
		{
			LoanApplicationFormData form = batchedForms.get(updateKey);
			form.stampTime(formDates.get(updateKey));
		}
		ofy.put(batchedForms.values());
	}*/
	private static String createParameter() throws DuplicateEntitiesException
	{
		HashMap<String, String> params = new HashMap<String, String>();
		params.put(DTOConstants.MAX_PRINCIPAL_KEY, "1000000");
		params.put(DTOConstants.MIN_PRINCIPAL_KEY, "100000");
		params.put(DTOConstants.MAX_TENOR_KEY, "6");
		params.put(DTOConstants.MIN_TENOR_KEY, "1");
		params.put(DTOConstants.INTEREST_KEY, "0.07");
		ApplicationParameters paramObj = LoanMktDAO.createApplicationParameters(EntityConstants.MARKETING_PARAM_ID, params);
		return "Created: " + paramObj.getKey();
	}
}
