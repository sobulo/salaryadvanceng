/**
 * 
 */
package com.fertiletech.addosser.server.scripts;

import java.text.DecimalFormat;
import java.util.HashMap;

import com.fertiletech.addosser.server.loanmktdata.LoanApplicationFormData;
import com.fertiletech.addosser.shared.FormConstants;
import com.fertiletech.addosser.shared.FormHTMLDisplayBuilder;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class JSPUtilities {


	  private static final String[] tensNames = {
	    "",
	    " ten",
	    " twenty",
	    " thirty",
	    " forty",
	    " fifty",
	    " sixty",
	    " seventy",
	    " eighty",
	    " ninety"
	  };

	  private static final String[] numNames = {
	    "",
	    " one",
	    " two",
	    " three",
	    " four",
	    " five",
	    " six",
	    " seven",
	    " eight",
	    " nine",
	    " ten",
	    " eleven",
	    " twelve",
	    " thirteen",
	    " fourteen",
	    " fifteen",
	    " sixteen",
	    " seventeen",
	    " eighteen",
	    " nineteen"
	  };

	  private static String convertLessThanOneThousand(int number) {
	    String soFar;

	    if (number % 100 < 20){
	      soFar = numNames[number % 100];
	      number /= 100;
	    }
	    else {
	      soFar = numNames[number % 10];
	      number /= 10;

	      soFar = tensNames[number % 10] + soFar;
	      number /= 10;
	    }
	    if (number == 0) return soFar;
	    return numNames[number] + " hundred" + soFar;
	  }


	  public static String convert(long number) {
	    // 0 to 999 999 999 999
	    if (number == 0) { return "zero"; }

	    String snumber = Long.toString(number);

	    // pad with "0"
	    String mask = "000000000000";
	    DecimalFormat df = new DecimalFormat(mask);
	    snumber = df.format(number);

	    // XXXnnnnnnnnn 
	    int billions = Integer.parseInt(snumber.substring(0,3));
	    // nnnXXXnnnnnn
	    int millions  = Integer.parseInt(snumber.substring(3,6)); 
	    // nnnnnnXXXnnn
	    int hundredThousands = Integer.parseInt(snumber.substring(6,9)); 
	    // nnnnnnnnnXXX
	    int thousands = Integer.parseInt(snumber.substring(9,12));    

	    String tradBillions;
	    switch (billions) {
	    case 0:
	      tradBillions = "";
	      break;
	    case 1 :
	      tradBillions = convertLessThanOneThousand(billions) 
	      + " billion ";
	      break;
	    default :
	      tradBillions = convertLessThanOneThousand(billions) 
	      + " billion ";
	    }
	    String result =  tradBillions;

	    String tradMillions;
	    switch (millions) {
	    case 0:
	      tradMillions = "";
	      break;
	    case 1 :
	      tradMillions = convertLessThanOneThousand(millions) 
	      + " million ";
	      break;
	    default :
	      tradMillions = convertLessThanOneThousand(millions) 
	      + " million ";
	    }
	    result =  result + tradMillions;

	    String tradHundredThousands;
	    switch (hundredThousands) {
	    case 0:
	      tradHundredThousands = "";
	      break;
	    case 1 :
	      tradHundredThousands = "one thousand ";
	      break;
	    default :
	      tradHundredThousands = convertLessThanOneThousand(hundredThousands) 
	      + " thousand ";
	    }
	    result =  result + tradHundredThousands;

	    String tradThousand;
	    tradThousand = convertLessThanOneThousand(thousands);
	    result =  result + tradThousand;

	    // remove extra spaces!
	    return result.replaceAll("^\\s+", "").replaceAll("\\b\\s{2,}\\b", " ");
	  }
	  
		public static FormHTMLDisplayBuilder loadFormDisplay(LoanApplicationFormData formDataObj, boolean useDark) {		
			//check form ready for display only
			if(!formDataObj.isSubmmited())
				throw new RuntimeException("attempting to display an unsubmitted application");
			
			
			//build form
			FormHTMLDisplayBuilder display = new FormHTMLDisplayBuilder(useDark).formBegins();
			HashMap<String, String> formData = formDataObj.getFormData();
			//personal background info
			display.headerBegins().appendTextBody("Personal Information").headerEnds();
			display.appendTextBody("Name of Applicant", formData.get(FormConstants.APPLICANT_SURNAME_KEY) + ", " + 
					formData.get(FormConstants.APPLICANT_OTHERNAME_KEY), true);
			
			display.lineBegins();
			display.appendTextBody("SEX", formData.get(FormConstants.SEX_KEY));
			display.appendTextBody("Marital Status", formData.get(FormConstants.MARITAL_STATUS_KEY), false);
			display.appendTextBody("Date of Birth", formData.get(FormConstants.DATE_OF_BIRTH_KEY), false);
			display.lineEnds();
			
			display.lineBegins();
			display.appendTextBody("Personal email", formData.get(FormConstants.EMAIL_KEY), false);
			display.appendTextBody("Phone No", formData.get(FormConstants.PRIMARY_PHONE_KEY), false);
			display.appendTextBody("Alternate No", formData.get(FormConstants.SECONDARY_PHONE_KEY), false);
			display.lineEnds();
			
			display.appendTextBody("Residential Address", formData.get(FormConstants.ADDRESS_KEY), true);
			display.appendTextBody("Nearest Bus stop/Area", formData.get(FormConstants.BUS_STOP_KEY), true);
			display.sectionEnds();
			
			//employment information
			display.headerBegins().appendTextBody("Employment Information").headerEnds();
			display.lineBegins().appendTextBody("Employer", formData.get(FormConstants.EMPLOYER_KEY)).
				appendTextBody("Branch/Address", formData.get(FormConstants.BRANCH_KEY)).lineEnds();
			display.lineBegins().appendTextBody("Office Tel No", formData.get(FormConstants.OFFICE_PHONE_KEY)).
				appendTextBody("Personal Office Email", formData.get(FormConstants.OFFICE_EMAIL_KEY)).lineEnds();
			display.lineBegins().appendTextBody("Grade/Rank", formData.get(FormConstants.JOB_RANK_KEY)).
				appendTextBody("Length of Service with Current Employer", formData.get(FormConstants.HIRE_DURATION_KEY)).lineEnds();
			display.lineBegins().appendTextBody("Job Description:", formData.get(FormConstants.JOB_TITLE_KEY)).
				appendTextBody("Employee Number", formData.get(FormConstants.EMPLOYEE_ID_KEY)).
				appendTextBody("Dept/Unit", " ").lineEnds();
			display.lineBegins().appendTextBody("Monthly Salary:", formData.get(FormConstants.SALARY_KEY)).
				appendTextBody("Annual Salary", formData.get(FormConstants.ANNUAL_SALARY_KEY))
				.appendTextBody("Salary Pay Day:", formData.get(FormConstants.SALARY_DATE_KEY)).lineEnds();
			display.sectionEnds();
			
			//loan request summary
			String title = formData.get(FormConstants.LOAN_TYPE_KEY);
			display.headerBegins().appendTextBody(title + " - Loan Request Summary").headerEnds();
			display.lineBegins().appendTextBody("Amount in Figures:", formData.get(FormConstants.REQUESTED_LOAN_AMOUNT_KEY)).
			appendTextBody("Proposed Tenor", formData.get(FormConstants.TENOR_KEY)).
			appendTextBody("Purpose", formData.get(FormConstants.LOAN_PURPOSE_KEY)).lineEnds();
			display.appendTextBody("Total Existing Loans", formData.get(FormConstants.EXISTING_LOAN_KEY), true);
			
			return display;
		}

		public static FormHTMLDisplayBuilder loadGuarantorDisplay(LoanApplicationFormData formDataObj, boolean useDark) {		
			//check form ready for display only
			if(!formDataObj.isSubmmited())
				throw new RuntimeException("attempting to display an unsubmitted application");
			
			
			//build form
			FormHTMLDisplayBuilder display = new FormHTMLDisplayBuilder(useDark).formBegins();
			HashMap<String, String> formData = formDataObj.getFormData();
			//personal background info
			display.headerBegins().appendTextBody("Guarantor Information").headerEnds();
			display.appendTextBody("Name:", formData.get(FormConstants.APPLICANT_SURNAME_KEY) + ", " + 
					formData.get(FormConstants.APPLICANT_OTHERNAME_KEY), true);
						
			display.lineBegins();
			display.appendTextBody("Personal email", formData.get(FormConstants.EMAIL_KEY), false);
			display.appendTextBody("Phone No", formData.get(FormConstants.PRIMARY_PHONE_KEY), false);
			display.appendTextBody("Alternate No", formData.get(FormConstants.SECONDARY_PHONE_KEY), false);
			display.lineEnds();
			
			display.appendTextBody("Residential Address", formData.get(FormConstants.ADDRESS_KEY), true);
			display.sectionEnds();
			
			//employment information
			display.headerBegins().appendTextBody("Guarantor Employment Information").headerEnds();
			display.lineBegins().appendTextBody("Employer", formData.get(FormConstants.EMPLOYER_KEY)).
				appendTextBody("Branch/Address", formData.get(FormConstants.BRANCH_KEY)).lineEnds();
			display.lineBegins().appendTextBody("Office Tel No", formData.get(FormConstants.OFFICE_PHONE_KEY)).
				appendTextBody("Personal Office Email", formData.get(FormConstants.OFFICE_EMAIL_KEY)).lineEnds();
			display.lineBegins().appendTextBody("Position Held", formData.get(FormConstants.JOB_RANK_KEY)).lineEnds();
			display.lineBegins().appendTextBody("Monthly Salary:", formData.get(FormConstants.SALARY_KEY)).lineEnds();
			display.sectionEnds();
			
			
			return display;
		}
		
}
