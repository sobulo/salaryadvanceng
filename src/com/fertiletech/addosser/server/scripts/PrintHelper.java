package com.fertiletech.addosser.server.scripts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.fertiletech.addosser.server.loanmktdata.ConsumerLoanSalesLead;
import com.fertiletech.addosser.server.loanmktdata.EntityConstants;
import com.fertiletech.addosser.server.loanmktdata.LoanApplicationFormData;
import com.fertiletech.addosser.shared.FormConstants;
import com.fertiletech.addosser.shared.table.TableMessage;
import com.fertiletech.addosser.shared.table.TableMessageHeader;
import com.fertiletech.addosser.shared.table.TableMessageHeader.TableMessageContent;
import com.fertiletech.utils.ColumnAccessor;
import com.fertiletech.utils.DateColumnAccessor;
import com.fertiletech.utils.ExcelManager;
import com.fertiletech.utils.NumberColumnAccessor;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;

public class PrintHelper {
	
	//fields for pdf file generation
    private final static HashMap<String, String> pdfFieldsMap = new HashMap<String, String>();
    private final static HashSet<String> formatNumber = new HashSet<String>();
    private final static String SANG_ID = "sangid";
    private final static String BORROWER = "borrower";
    static
    {    	
        pdfFieldsMap.put(FormConstants.APPLICANT_SURNAME_KEY, "Surname");
        pdfFieldsMap.put(FormConstants.APPLICANT_OTHERNAME_KEY, "Other Names");
        pdfFieldsMap.put(FormConstants.PRIMARY_PHONE_KEY, "Phone Number");
        pdfFieldsMap.put(FormConstants.ADDRESS_KEY, "Residential Address");
        pdfFieldsMap.put(FormConstants.EMAIL_KEY, "Personal Email Address");
        pdfFieldsMap.put(FormConstants.BUS_STOP_KEY, "Bus stop/Area");
        pdfFieldsMap.put(FormConstants.EMPLOYER_KEY, "Employer");
        pdfFieldsMap.put(FormConstants.BRANCH_KEY, "Branch/Address");
        pdfFieldsMap.put(FormConstants.SALARY_KEY, "Monthly Salary");
        pdfFieldsMap.put(FormConstants.ANNUAL_SALARY_KEY, "Annual Salary");
        pdfFieldsMap.put(FormConstants.REQUESTED_LOAN_AMOUNT_KEY, "Requested Loan Amount");
        pdfFieldsMap.put(FormConstants.EXISTING_LOAN_KEY, "Total of any Existing Loans");
        pdfFieldsMap.put(FormConstants.DATE_OF_BIRTH_KEY, "Date of Birth");
        pdfFieldsMap.put(FormConstants.SECONDARY_PHONE_KEY, "Secondary Phone");
        pdfFieldsMap.put(FormConstants.OFFICE_PHONE_KEY, "Office Phone");
        pdfFieldsMap.put(FormConstants.OFFICE_EMAIL_KEY, "Office Email");
        pdfFieldsMap.put(FormConstants.MARITAL_STATUS_KEY, "Marital Status");
        pdfFieldsMap.put(FormConstants.JOB_RANK_KEY, "Grade/Level");
        pdfFieldsMap.put(FormConstants.JOB_TITLE_KEY, "Occupation/Job Title");
        pdfFieldsMap.put(FormConstants.HIRE_DURATION_KEY, "Hire Duration");
        pdfFieldsMap.put(FormConstants.EMPLOYEE_ID_KEY, "Staff ID");
        pdfFieldsMap.put(FormConstants.SALARY_DATE_KEY, "Salary Date");
        pdfFieldsMap.put(FormConstants.TENOR_KEY, "Tenor");
        pdfFieldsMap.put(FormConstants.SEX_KEY, "Gender");
        pdfFieldsMap.put(FormConstants.LOAN_PURPOSE_KEY, "Loan Purpose");
        pdfFieldsMap.put(FormConstants.IDENTIFICATION_KEY, "Identification Type");
        pdfFieldsMap.put(FormConstants.NATIONALITY_KEY, "Nationality");
        pdfFieldsMap.put(FormConstants.ORIGIN_STATE_KEY, "State of Origin");
        pdfFieldsMap.put(FormConstants.LOCAL_GOVT_KEY, "Local Govt. Area"); 
        pdfFieldsMap.put(FormConstants.MOTHERS_MAIDEN_KEY, "Mother's Maiden Name"); 
        pdfFieldsMap.put(FormConstants.EMPLOYMENT_DATE_KEY, "Employment Date");
        pdfFieldsMap.put(FormConstants.TAX_ID_KEY, "Tax ID"); 
        pdfFieldsMap.put(FormConstants.CONFIRMATION_DATE_KEY, "Confirmation Date"); 
        pdfFieldsMap.put(FormConstants.DEPT_KEY, "Department");
        pdfFieldsMap.put(FormConstants.ACCT_NO_KEY, "Account No."); 
        pdfFieldsMap.put(FormConstants.ACCT_TYPE_KEY, "Account Type"); 
        pdfFieldsMap.put(FormConstants.BANK_NAME_KEY, "Bank Name(Salary Acct)");
        pdfFieldsMap.put(FormConstants.KIN_ADDRESS_KEY, "Address"); 
        pdfFieldsMap.put(FormConstants.KIN_EMAIL_KEY, "Email");
        pdfFieldsMap.put(FormConstants.KIN_OTHER_NAME_KEY, "First Name"); 
        pdfFieldsMap.put(FormConstants.KIN_SURNNAME_KEY, "Surname"); 
        pdfFieldsMap.put(FormConstants.KIN_PHONE_KEY, "Phone No.");
        pdfFieldsMap.put(FormConstants.KIN_RELATION_KEY, "Relationship");
        pdfFieldsMap.put(FormConstants.MONTHLY_REPAY_KEY, "Monthly Repayment");
        pdfFieldsMap.put(SANG_ID, "Application ID");
        
        formatNumber.add(FormConstants.SALARY_KEY);
        formatNumber.add(FormConstants.ANNUAL_SALARY_KEY);
    }
    
	public static TableMessageHeader getFormPDFTableHeader(String title, int colSpan)
	{
		TableMessageHeader header = new TableMessageHeader(1);
		header.setText(0, title.toUpperCase(), TableMessageContent.TEXT);
		header.setCaption(String.valueOf(colSpan));
		return header;
	}
	
	public static TableMessage getFormPDFRow(HashMap<String, String> formData, List<String> keys, List<Integer> colSpans)
	{
		if(keys.size() != colSpans.size())
			throw new RuntimeException("Size mistmatch for list starting with: " + pdfFieldsMap.get(keys.get(0)));
		TableMessage row = new TableMessage(keys.size(), keys.size(), 0);
		for(int i = 0; i < keys.size(); i++)
		{
			String key = keys.get(i);
			String val = formData.get(key);
			if(formatNumber.contains(key))
				val = formatNumber(val);
			row.setText(i, pdfFieldsMap.get(key) + ":  " + (val==null?"":val));
			row.setNumber(i, colSpans.get(i));
		}
		return row;
	}

	public static TableMessage getFormPDFRow(String label, String val)
	{
		TableMessage row = new TableMessage(1, 0, 0);
		row.setText(0, label + val);
		return row;
	}
	
	public static String formatNumber(String val)
	{
		if(val == null)
			return "";
		
		if(val.length() > 0)
			val = EntityConstants.NUMBER_FORMAT.format(Double.valueOf(val));
		
		return val;
	}
	
	public static List<TableMessage> convertLoanToPDFInput(LoanApplicationFormData form, String id)
	{
		HashMap<String, String> formData = form.getFormData();
		formData.put(SANG_ID, id);
		List<TableMessage> result = new ArrayList<TableMessage>();
		
		result.add(getFormPDFTableHeader("Loan Application Form", 3));
		List<String> dataRow = Arrays.asList(SANG_ID);
		List<Integer> colSpans = Arrays.asList(3);
		TableMessage brandedRow = getFormPDFRow(formData, dataRow, colSpans);
		//String brand = brandedRow.getText(0).trim().equals(pdfFieldsMap.get(SANG_ID)) ? "" : " -- via www.salaryadvanceng.com";
		brandedRow.setText(0, brandedRow.getText(0)); //+ brand);
		result.add(brandedRow);
		
		//personal info		
		result.add(getFormPDFTableHeader("Applicant Information", 3));
		
		dataRow = Arrays.asList(FormConstants.APPLICANT_SURNAME_KEY, FormConstants.APPLICANT_OTHERNAME_KEY);
		colSpans = Arrays.asList(1, 2);
		result.add(getFormPDFRow(formData, dataRow, colSpans));
		
		dataRow = Arrays.asList(FormConstants.DATE_OF_BIRTH_KEY, FormConstants.SEX_KEY, FormConstants.PRIMARY_PHONE_KEY);
		colSpans = Arrays.asList(1, 1, 1);
		result.add(getFormPDFRow(formData, dataRow, colSpans));
		
		dataRow = Arrays.asList(FormConstants.ADDRESS_KEY);
		colSpans = Arrays.asList(3);
		result.add(getFormPDFRow(formData, dataRow, colSpans));
		
		dataRow = Arrays.asList(FormConstants.SECONDARY_PHONE_KEY, FormConstants.IDENTIFICATION_KEY, FormConstants.MARITAL_STATUS_KEY);
		colSpans = Arrays.asList(1, 1, 1);
		result.add(getFormPDFRow(formData, dataRow, colSpans));
		
		dataRow = Arrays.asList(FormConstants.NATIONALITY_KEY, FormConstants.ORIGIN_STATE_KEY, FormConstants.LOCAL_GOVT_KEY);
		colSpans = Arrays.asList(1, 1, 1);
		result.add(getFormPDFRow(formData, dataRow, colSpans));

		
		dataRow = Arrays.asList(FormConstants.EMAIL_KEY);
		colSpans = Arrays.asList(3);
		result.add(getFormPDFRow(formData, dataRow, colSpans));

		dataRow = Arrays.asList(FormConstants.JOB_TITLE_KEY, FormConstants.MOTHERS_MAIDEN_KEY);
		colSpans = Arrays.asList(1, 2);
		result.add(getFormPDFRow(formData, dataRow, colSpans));		
		
		//employment info
		result.add(getFormPDFTableHeader("Employment Information", 3));
		
		dataRow = Arrays.asList(FormConstants.EMPLOYER_KEY);
		colSpans = Arrays.asList(3);
		result.add(getFormPDFRow(formData, dataRow, colSpans));
		
		dataRow = Arrays.asList(FormConstants.BRANCH_KEY, FormConstants.HIRE_DURATION_KEY);
		colSpans = Arrays.asList(2, 1);
		result.add(getFormPDFRow(formData, dataRow, colSpans));		
		
		dataRow = Arrays.asList(FormConstants.OFFICE_PHONE_KEY, FormConstants.EMPLOYEE_ID_KEY, FormConstants.TAX_ID_KEY);
		colSpans = Arrays.asList(1, 1, 1);
		result.add(getFormPDFRow(formData, dataRow, colSpans));
		
		dataRow = Arrays.asList(FormConstants.EMPLOYMENT_DATE_KEY, FormConstants.CONFIRMATION_DATE_KEY, FormConstants.DEPT_KEY);
		colSpans = Arrays.asList(1, 1, 1);
		result.add(getFormPDFRow(formData, dataRow, colSpans));

		dataRow = Arrays.asList(FormConstants.JOB_RANK_KEY, FormConstants.ANNUAL_SALARY_KEY, FormConstants.SALARY_KEY);
		colSpans = Arrays.asList(1, 1, 1);
		result.add(getFormPDFRow(formData, dataRow, colSpans));		
		
		dataRow = Arrays.asList(FormConstants.OFFICE_EMAIL_KEY);
		colSpans = Arrays.asList(3);
		result.add(getFormPDFRow(formData, dataRow, colSpans));		
		
		dataRow = Arrays.asList(FormConstants.BANK_NAME_KEY, FormConstants.ACCT_TYPE_KEY);
		colSpans = Arrays.asList(2, 1);
		result.add(getFormPDFRow(formData, dataRow, colSpans));		
		
		dataRow = Arrays.asList(FormConstants.ACCT_NO_KEY, FormConstants.SALARY_DATE_KEY);
		colSpans = Arrays.asList(2, 1);
		result.add(getFormPDFRow(formData, dataRow, colSpans));		
		
		//next of kin information
		result.add(getFormPDFTableHeader("NEXT OF KIN", 3));
		
		dataRow = Arrays.asList(FormConstants.KIN_SURNNAME_KEY, FormConstants.KIN_OTHER_NAME_KEY, FormConstants.KIN_RELATION_KEY);
		colSpans = Arrays.asList(1,1,1);
		result.add(getFormPDFRow(formData, dataRow, colSpans));		
		
		dataRow = Arrays.asList(FormConstants.KIN_ADDRESS_KEY);
		colSpans = Arrays.asList(3);
		result.add(getFormPDFRow(formData, dataRow, colSpans));		
		
		dataRow = Arrays.asList(FormConstants.KIN_EMAIL_KEY, FormConstants.KIN_PHONE_KEY);
		colSpans = Arrays.asList(2, 1);
		result.add(getFormPDFRow(formData, dataRow, colSpans));		
		
		//loan information
		result.add(getFormPDFTableHeader("LOAN DETAILS", 3));
		
		dataRow = Arrays.asList(FormConstants.REQUESTED_LOAN_AMOUNT_KEY, FormConstants.TENOR_KEY, FormConstants.MONTHLY_REPAY_KEY);
		colSpans = Arrays.asList(1,1,1);
		result.add(getFormPDFRow(formData, dataRow, colSpans));
		
		//outstanding obligations ... see caller for where obligation rows are added, done because requires 4 cols unlike 3 for above
		//idealy obligations header should go with what builds its rows but easier to achieve via header functionality here
		result.add(getFormPDFTableHeader("OTHER LOANS, DEBTS, OR OBLIGATIONS (TOTAL: " + formatNumber(formData.get(FormConstants.EXISTING_LOAN_KEY)) + " Naira)", 3));
		
		return result;
	}
	
	public static List<TableMessage> convertGuarantorToPDFInput(Objectify ofy, LoanApplicationFormData form)
	{
		HashMap<String, String> formData = form.getFormData();
		Key<ConsumerLoanSalesLead> leadKey= form.getParentKey();
		formData.put(SANG_ID, String.valueOf(leadKey.getId()));
		List<TableMessage> result = new ArrayList<TableMessage>();
		LoanApplicationFormData applicant = ofy.get(LoanApplicationFormData.getFormKey(leadKey));
		TableMessageHeader h = new TableMessageHeader(1);
		h.setText(0, "PERSONAL GUARANTEE", TableMessageContent.TEXT);
		result.add(h);
		result.add(getFormPDFRow("Effective Date: ", EntityConstants.DATE_FORMAT.format(new Date())));
		result.add(getFormPDFRow("Given By: ", formData.get(FormConstants.APPLICANT_SURNAME_KEY) + ", " + formData.get(FormConstants.APPLICANT_OTHERNAME_KEY)));		
        result.add(getFormPDFRow("Employer's Name: ", formData.get(FormConstants.EMPLOYER_KEY)));
        result.add(getFormPDFRow("Position Held: ", formData.get(FormConstants.JOB_RANK_KEY)));
        result.add(getFormPDFRow("Monthly Income: ", formData.get(FormConstants.SALARY_KEY)));
        result.add(getFormPDFRow("Employer's Address: ", formData.get(FormConstants.ADDRESS_KEY)));
        result.add(getFormPDFRow("Office Email Address: ", formData.get(FormConstants.OFFICE_EMAIL_KEY)));
        result.add(getFormPDFRow("In Favor Of (borrower): ", applicant.getFormData().get(FormConstants.APPLICANT_SURNAME_KEY) + ", " + applicant.getFormData().get(FormConstants.APPLICANT_OTHERNAME_KEY)));
        result.add(getFormPDFRow("Given To: ", "Addosser Microfinance Bank Limited('The Bank'), 32 Lewis Street, Lagos Island, Lagos"));
		return result;
	}
	
	public final static String GUARANTOR_PARAGRAPH1 = " hereby unconditionally and irrevocably guarantee to Addosser Microfinance Bank Limited, granting facility in the sum of ________________________________________________." +
			" (Principal plus Interest) the due and punctual performance and observance by the Borrower of his obligations under the loan agreement.";
	
	public final static String GUARANTOR_PARAGRAPH2 = " hereby guarantees absolutely and unconditionally the prompt payment when due, and at any time thereafter, of all " +
			"indebtedness and obligations of every kind and nature of the borrower to the bank, absolute or contingent, due or to become due, now or hereafter existing" +
			" (the �Indebtedness�), and in addition, the undersigned agrees to pay all expenses, including attorneys� fees and legal expenses, paid or incurred by the " +
			"bank to collect the Indebtedness, or any part thereof, and to enforce this guaranty.";
	
	public final static String GUARANTOR_PARAGRAPH3 = "The liability under this guaranty shall in no way be affected, by any compromise, " +
			"waiver, settlement, change or modification to the loan agreement and I understand that such amendment and failure to be" +
			" notified of same, shall not release me from my liability under this Guarantee. I hereby waive any presentment, protest" +
			" and notice of dishonor or default, notice of acceptance of the guaranty, notice of extensions of credit or other " +
			"actions taken in reliance hereon, and all demands and notices of any kind in connection with this guaranty or the " +
			"Indebtedness.";
	public final static String GUARANTOR_PARAGRAPH4 = "In the event that the borrower fails to make good on any of his outstanding " +
			"debt obligations, Addosser Microfinance Bank Limited shall have recourse to me to settle the outstanding debt" +
			" (principal and accrued interest) in a prompt manner. There shall be no obligation on the part of the bank at any time" +
			" to resort first for payment to the borrower and the bank shall have the right to enforce this guaranty irrespective of" +
			" whether or not proceedings or steps are being taken against any party primarily or secondarily liable on the " +
			"Indebtedness.";
	public final static String GUARANTOR_PARAGRAPH5 = "I have carefully reviewed this contract and agree to and accept its terms and conditions.";
	
	public final static String GUARANTOR_PARAGRAPH6 = "I am executing this Agreement as of the Effective Date first written above.";
	
	public final static String[] GUARANTOR_PARAGRAPHS  = {GUARANTOR_PARAGRAPH3, GUARANTOR_PARAGRAPH4, GUARANTOR_PARAGRAPH5, GUARANTOR_PARAGRAPH6};
	
	//requirements
	private final static String REQUIREMENTS_PARAGRAPH_PARTA = "Upon receipt (and verification) of the documents listed below the bank will process your loan request of ";
	private final static String REQUIREMENTS_PARAGRAPH_PARTB = " Naira. You will be notified of the bank's decision shortly afterwards via the email address you provided: ";
	private final static String REQUIREMENTS_PARAGRAPH_PARTC = ". The SalaryAdvanceNG.com approval process is intentionally designed to be quick and convenient in order to ensure you receive money within 1 business day." +
			" Call aloan officer immediately if you need further assitance with your application:" +
			" 0806-279-8100 (Jade), 0806-678-7187 (Hajarat), 0803-560-3519 (Mayowa).";
	public final static String PICKUP_INTRO = "Submit your documents through any of the mediums listed below:";
	public final static String[] PICKUP_OPTIONS = 
		{"A1. Scan, then email your documents to customerservices@addosser.com **", 
		"B1. Visit Addosser Microfinance Bank's head office located at 32 Lewis Street, Lagos Island, Lagos ",
		 "B2. Or visit www.addosser.com to find an Addosser branch nearby to drop off your documents", "B3. Or request document pickup from your workplace (available for select locations)",
		 "**.  When sending in documents via email, please note that one of B1, B2, or B3 is still required for submitting post-dated cheques. Email option allows for faster loan approvals"};
	private final static String REQUIREMENT0 = "One signed and duly completed loan application form";
	private final static String REQUIREMENT1 = "Last six (6) months� bank statement showing last six (6) salary entries (endorsed by the bank)";
	private final static String REQUIREMENT2 = "Photocopy of your employment letter or Letter of confirmation or Recent promotion letter/ salary increase";
	private final static String REQUIREMENT3 = "NUBAN-compliant postdated cheques from salary account (one for each monthly repayment)";
	private final static String REQUIREMENT4 = "One(1) Recent Passport photograph";
	private final static String REQUIREMENT5 = "Photocopy of your staff identification card";
	private final static String REQUIREMENT6 = "signed and duly completed guarantor�s form (Guarantor MUST be a colleague at work with monthly income within or above your salary grade)";
	private final static String REQUIREMENT7 = "Guarantor�s Passport Photograph and Photocopy of staff identification card";
	private final static String REQUIREMENT8 = "Guarantor�s Post-dated cheque (for loan defaults)";
	private final static String[] MANDATORY_REQUIREMENTS = {REQUIREMENT0, REQUIREMENT1, REQUIREMENT2, REQUIREMENT3, REQUIREMENT4, REQUIREMENT5};
	private final static double TWO_GUARANTORS_THRESHOLD = 749999.5;
	private final static double GUARANTOR_CHEQUE_THRESHOLD = 249999.5;
	
	public static List<TableMessage> getRequirements(double loanAmount)
	{
		List<TableMessage> result = new ArrayList<TableMessage>();
		TableMessageHeader header = new TableMessageHeader(2);
		header.setText(0, "#", TableMessageContent.NUMBER);
		header.setText(1, "Required Documents", TableMessageContent.TEXT);
		result.add(header);
		
		int count = 1;
		for(String req : MANDATORY_REQUIREMENTS)
			addRequirement(result, req, count++);
		
		String prefix = "One ";
		if(loanAmount > TWO_GUARANTORS_THRESHOLD)
			prefix = "Two ";
		addRequirement(result, prefix + REQUIREMENT6, count++);
		addRequirement(result, REQUIREMENT7, count++);
		if(loanAmount > GUARANTOR_CHEQUE_THRESHOLD)
			addRequirement(result, REQUIREMENT8, count++);;
		
		return result;
	}
	
	public static String getRequirementParagraph(double loanAmount, String email)
	{
		return REQUIREMENTS_PARAGRAPH_PARTA + EntityConstants.NUMBER_FORMAT.format(loanAmount) + REQUIREMENTS_PARAGRAPH_PARTB + email + REQUIREMENTS_PARAGRAPH_PARTC;
	}
	
	private static void addRequirement(List<TableMessage> buffer, String val, int count)
	{
		TableMessage m = new TableMessage(1, 1, 0);
		m.setNumber(0, count);
		m.setText(0, val);
		buffer.add(m);
	}
}
