/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fertiletech.addosser.server.scripts;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;

import com.fertiletech.addosser.server.login.LoginHelper;
import com.fertiletech.addosser.server.orbitdata.OrbitConstants;
import com.fertiletech.addosser.server.orbitdata.OrbitDataFeed;
import com.fertiletech.addosser.server.orbitdata.OrbitRimLocations;
import com.fertiletech.addosser.server.orbitdata.OrbitStruct;
import com.fertiletech.addosser.server.tasks.TaskQueueHelper;
import com.fertiletech.addosser.shared.utils.GeneralFuncs;
import com.fertiletech.utils.ColumnAccessor;
import com.fertiletech.utils.EmptyColumnValueException;
import com.fertiletech.utils.ExcelManager;
import com.fertiletech.utils.InvalidColumnValueException;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

import cern.colt.Arrays;

public class BulkOrbitUpload extends HttpServlet {
    //TODO review classes of j9educationaction package and ensure no instance
    //of services being created. instead create static versions of such fns
    private static final Logger log =
            Logger.getLogger(BulkOrbitUpload.class.getName());
    private final static String STUD_UPLD_SESSION_NAME = "studentUploadData";
    private final static String CLASS_KEY_SESSION_NAME = "classkey";
    private final static String LOAD_PARAM_NAME = "loadData";
    private final static String GOOD_DATA_PREFIX = "<i></i>";


    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException 
    {
		//step two .. save the fetched sheet into an orbit object
        res.setContentType("text/html");
        ServletOutputStream out = res.getOutputStream();
        if (req.getParameter(LOAD_PARAM_NAME) != null)
        {
        	String userEmail = LoginHelper.getLoggedInUser(req);
            Object upldSessObj = req.getSession().getAttribute(STUD_UPLD_SESSION_NAME);
            Object clsSessObj = req.getSession().getAttribute(CLASS_KEY_SESSION_NAME);
            if (upldSessObj != null && clsSessObj != null) 
            {
                //retrive data from session object
                ArrayList<OrbitStruct> orbitDataStruct = (ArrayList<OrbitStruct>) upldSessObj;
                String feedName = (String) clsSessObj;

                String okMsg = "<b>Name,Company,Address,Amount,Tenure,OrbitId</b><br/>";
                ArrayList<String> orbitIDs = new ArrayList<String>(orbitDataStruct.size());


  
                
                String[] colNames= {OrbitConstants.NAME_HEADER,OrbitConstants.COMPANY_HEADER,
                        OrbitConstants.ADDRESS_HEADER, //2
                        OrbitConstants.ADDRESS_HEADER2, //3
                        OrbitConstants.ORBIT_HEADER, //4
                        OrbitConstants.TENOR_HEADER, //5
                        OrbitConstants.AMOUNT_HEADER, //6
                        OrbitConstants.RIM_HEADER, //7
                        OrbitConstants.DATE_CGRANTED_HEADER, //8
                        OrbitConstants.DATE_MATURITY_HEADER, //9
                        OrbitConstants.STATUS_HEADER, //10
                        OrbitConstants.PRODUCT_HEADER, //11
                        OrbitConstants.PHONE_HEADER, //12
                        OrbitConstants.EMAIL_HEADER,  //13
                        OrbitConstants.SECTOR_HEADER}; //14
                int numRows = orbitDataStruct.size();
                OrbitDataFeed orbitFeed = new OrbitDataFeed(feedName, colNames, numRows, userEmail);
                //initialize orbitDataArr
                //first pass lets see if we can fetch any logins
                String [] rims = new String[numRows];
                String [] addresses = new String[numRows];
                boolean doRim = false;
                for(int i = 0; i < numRows; i++)
                {
                	OrbitStruct rowData = orbitDataStruct.get(i);
                	orbitFeed.setrowValue(colNames[0], i, rowData.name);;
                	orbitFeed.setrowValue(colNames[1], i, rowData.company);
                	orbitFeed.setrowValue(colNames[2], i, rowData.address);
                	orbitFeed.setrowValue(colNames[3], i, rowData.address2);
                	orbitFeed.setrowValue(colNames[4], i, rowData.orbitId); 
                	orbitFeed.setrowValue(colNames[5], i, rowData.tenure);
          
                	orbitFeed.setrowValue(colNames[6], i, rowData.amount);;
                	orbitFeed.setrowValue(colNames[7], i, rowData.rim);
                	orbitFeed.setrowValue(colNames[8], i, rowData.dateGranted);
                	orbitFeed.setrowValue(colNames[9], i, rowData.maturityDate);
                	orbitFeed.setrowValue(colNames[10], i, rowData.status); 
                	orbitFeed.setrowValue(colNames[11], i, rowData.product);
                	
                	orbitFeed.setrowValue(colNames[12], i, rowData.phoneNumber);;
                	orbitFeed.setrowValue(colNames[13], i, rowData.email);
                	orbitFeed.setrowValue(colNames[14], i, rowData.sector);
              	
                	if(!(rowData.address + rowData.address2).trim().equals(""))
                	{	
	                	rims[i] = rowData.rim;
	                	String address = rowData.address;
	                	String suffix= "";
	                	String vi = ", Victoria Island";
	                	if(rowData.address2.equalsIgnoreCase("V/I"))
	                		suffix = vi;
	                	else if(rowData.address2.equalsIgnoreCase("V/I."))
	                		suffix = vi;
	                	else if(rowData.address2.equalsIgnoreCase("V.I."))
	                		suffix = vi;
	                	else if(rowData.address2.equalsIgnoreCase("V.I"))
	                		suffix = vi;
	                	else if(rowData.address2.equalsIgnoreCase("Victoria."))
	                		suffix = vi;
	                	else if(rowData.address2.equalsIgnoreCase("Victoria"))
	                		suffix = vi;
	                	else if(rowData.address2.equalsIgnoreCase("VI"))
	                		suffix = vi;
	                	else if(rowData.address2.length() > 2)
	                		suffix = ", " + rowData.address2;
	                	
	                		
	                	addresses[i] =  address + suffix + ". Lagos, Nigeria.";
	                	doRim = true;
                	}
                	else
                		rims[i] = null; //sanity check. default is null
                	
                	okMsg += rowData.name + "," + rowData.company + "," + rowData.address + "," +
                            rowData.amount + "," + rowData.tenure + "," + rowData.orbitId + "<br/>";
                }
                
                StringBuilder badMsg = new StringBuilder();
                
          
                
              //handle no addresses
                Objectify ofy;
                if(doRim)
                {
                	ofy = ObjectifyService.begin();
	                OrbitRimLocations rimLocations = ofy.find(OrbitRimLocations.getKey());
	                if(rimLocations == null)
	                {
	                	createRimMap(ofy, addresses, rims);
	                	out.println("Created rim map: " + rims.length + " entries, geo coding has commenced, address locations should be available on the map within an hour</br> ");
	                }
	                else
	                {
	                	String added = Arrays.toString(rimLocations.addNewRims(rims, addresses));
	                	ofy.put(rimLocations);
	                	out.println("rim map additions: " + added + 
	                			" entries, geo coding has commenced, address locations should be available on the map within an hour</br> ");
	                }
                }
                	
                ofy = ObjectifyService.beginTransaction();
            	try
            	{
            		ofy.put(orbitFeed);
            		if(doRim)
            			TaskQueueHelper.scheduleUpdateOrbitRim();
        			ofy.getTxn().commit();
        		} 
            	catch(Exception e)
            	{
            		badMsg.append("<li> Error saving feed: ").append(feedName).append("</li>");
            	}
            	finally {
        			if (ofy.getTxn().isActive())
        				ofy.getTxn().rollback();
        		}

                if(badMsg.length() == 0)
                {
                	out.println("Succesully loaded: " + orbitDataStruct.size() 
                	+ " rows from Orbit into S.A.NG. Displaying 6 of " + colNames.length + 
                	" columns uploaded below");
                	out.println("<p>" + okMsg + "</p>");
                }
                else
                	out.println("<p>" + badMsg + "</p>");
                	

                //set session data to null as we've saved the data
                req.setAttribute(STUD_UPLD_SESSION_NAME, null);
                req.setAttribute(CLASS_KEY_SESSION_NAME, null);
            }
            else
            {
                out.println("<b>unable to retrieve session info. Try " +
                        "enabling cookies in your browser.</b>");
            }
        }
        else
        {
        	//Step 1 fetch the spreadsheet
            ServletFileUpload upload = new ServletFileUpload();
            try {
                FileItemIterator iterator = upload.getItemIterator(req);
                String feedName = "";
                while (iterator.hasNext()) {
                    FileItemStream item = iterator.next();
                    InputStream stream = item.openStream();
                    if (item.isFormField() && item.getFieldName().equals(CLASS_KEY_SESSION_NAME)) {
                    	feedName = Streams.asString(stream);;
                        req.getSession().setAttribute(CLASS_KEY_SESSION_NAME, feedName);
                    } else {
                        log.warning("Bulk orbit upload attempt: " + item.getFieldName() + ", name = " + item.getName());
                        ColumnAccessor[] headerAccessors = OrbitConstants.ORBIT_UPLOAD_ACCESSORS;
                        String[] expectedHeaders = ColumnAccessor.getAccessorNames(headerAccessors);
                        //TODO, return error page if count greater than 200 rows
                        ExcelManager sheetManager = new ExcelManager(stream);
                        //confirm headers in ssheet match what we expect
                        String[] sheetHeaders;
                        try {
                            sheetHeaders = sheetManager.getHeaders();

                            //expectedheaders are uppercase so convert sheet prior to compare
                            for (int s = 0; s < sheetHeaders.length; s++) {
                                sheetHeaders[s] = sheetHeaders[s].toUpperCase();
                            }
                        } catch (InvalidColumnValueException ex) {
                        	log.severe(ex.getMessage());
                            out.println("<b>Unable to read excel sheet headers</b><br/> Error was: " 
                            		+ ex.getMessage());
                            return;
                        }
                        ArrayList<String> sheetOnly = new ArrayList<String>();
                        ArrayList<String> intersect = new ArrayList<String>();
                        ArrayList<String> expectedOnly = new ArrayList<String>();
                        GeneralFuncs.arrayDiff(expectedHeaders, sheetHeaders, intersect, expectedOnly, sheetOnly);
                        //handle errors in column headers
                        if (expectedOnly.size() > 0 || sheetOnly.size() > 0) {
                            out.println("<b>Uploaded sheet should contain only these headers:</b>");
                            printHtmlList(expectedHeaders, out);
                            if (expectedOnly.size() > 0) {
                                out.println("<b>These headers are missing from ssheet:</b>");
                                printHtmlList(expectedOnly, out);
                            }
                            if (sheetOnly.size() > 0) {
                                out.println("<b>These headers are not expected but were found in ssheet</b>");
                                printHtmlList(sheetOnly, out);
                            }
                            return;
                        }

                        sheetManager.initializeAccessorList(headerAccessors);
                        //todo, replace these with string builders
                        String goodDataHTML = "";
                        String badDataHTML = "";
                        String tempVal = "";
                        String[] htmlHeaders = new String[headerAccessors.length];
                        ArrayList<OrbitStruct> sessionData = new ArrayList<OrbitStruct>();
                

                        for (int i = 0; i < headerAccessors.length; i++) {
                            htmlHeaders[i] = headerAccessors[i].getHeaderName();
                        }

                        int numRows = sheetManager.totalRows();
                        for (int i = 1; i < numRows; i++) 
                        {
                        	try
                        	{
	                            String row = "<tr>";
	                            OrbitStruct tempStudData = new OrbitStruct();
	                            for (int j = 0; j < headerAccessors.length; j++) {
	                                tempVal = getValue(i, headerAccessors[j], sheetManager, tempStudData);
	                                row += "<td>" + tempVal + "</td>";
	                            }
	                            	                            
	                            if(tempStudData.isValid)
	                            {
	                                //append to display result
	                                goodDataHTML += row;
	                                sessionData.add(tempStudData);
	                            }
	                            else 
	                            {
	                                badDataHTML += (row + "</tr>");
	                            }
                        	}
                        	catch(EmptyColumnValueException ex) {} //logic to help skip blank lines
                        }

                        String htmlTableStart = "<TABLE border='1' class='themePaddedBorder'>";
                        String htmlHeader = "";
                        for (String s : htmlHeaders) {
                            htmlHeader += "<TH>" + s + "</TH>";
                        }
                        String htmlTableEnd = "</TABLE>";

                        if (goodDataHTML.length() > 0) 
                        {
                            out.println(GOOD_DATA_PREFIX);
                            goodDataHTML = "<b>Below shows data that passed prelim checks. Hit save to store the data for: " + 
                            feedName + "</b>" + htmlTableStart + 
                            "<TR>" + htmlHeader + "</TR>" + goodDataHTML + htmlTableEnd;
                        }

                        if (badDataHTML.length() > 0) 
                        {
                            out.println("<b>Below shows records with errors</b>");
                            out.println(htmlTableStart);
                            out.println("<TR>");
                            out.println(htmlHeader);
                            out.println("</TR>");
                            out.print(badDataHTML);
                            out.println(htmlTableEnd);
                        }

                        out.println(goodDataHTML);

                        req.getSession().setAttribute(STUD_UPLD_SESSION_NAME,
                                sessionData);
                    }
                }
            } catch (FileUploadException ex) {
                out.println("<b>File upload failed:</b>" + ex.getMessage());
            }
            catch (InvalidColumnValueException ex) {
                log.severe(ex.getMessage());
                out.println("<b>Unable to read excel sheet</b><br/> Error was: " + ex.getMessage());
            }
        }
    }

    private String getValue(int row, ColumnAccessor accessor, ExcelManager sheetManager, OrbitStruct studData) throws InvalidColumnValueException {
        Object temp;
        String val;
        String headerName = accessor.getHeaderName();
        try {

            temp = sheetManager.getData(row, accessor);

            //handle special case date, non-string input
            if(headerName.equals(OrbitConstants.DATE_MATURITY_HEADER) || 
            		headerName.equals(OrbitConstants.DATE_CGRANTED_HEADER))
            {
            	try
            	{            	
	            	temp = sheetManager.getData(row, accessor);
	                if(temp == null)
	                    val = "";
	                else
	                	val = OrbitConstants.DATE_FORMATTER.format((Date) temp);            
            	}                
            	catch(Exception e)
            	{
            		val = "";
            	} 
            	if(headerName.equals(OrbitConstants.DATE_MATURITY_HEADER))
            		studData.maturityDate = val;
            	else 
            		studData.dateGranted = val;
                return val;
            } 
            
            //handle case of number
            if(headerName.equals(OrbitConstants.AMOUNT_HEADER))
            {
            	try
            	{            	
	            	temp = sheetManager.getData(row, accessor);
	                if(temp == null)
	                    val = "";
	                else
	                	val = OrbitConstants.NUMBER_FORMATTER.format((Double) temp);            
            	}                
            	catch(Exception e)
            	{
            		val = "";
            	} 
            	studData.amount = val;
                return val;
            }             
            
            
            //handle other fields
            if(temp == null)
                val = "";
            else
            {
                val = ((String) temp);
                if(val.equals("[NULL]"))
                	val = "";
            }

            if(headerName.equals(OrbitConstants.NAME_HEADER))
            	studData.name = val;
            else if(headerName.equals(OrbitConstants.COMPANY_HEADER))
            	studData.company = val;
            else if(headerName.equals(OrbitConstants.ADDRESS_HEADER))
            	studData.address = val;
            else if(headerName.equals(OrbitConstants.ADDRESS_HEADER2))
            	studData.address2 = val;               

            else if(headerName.equals(OrbitConstants. ORBIT_HEADER))
            	studData.orbitId = val;
            else if(headerName.equals(OrbitConstants.TENOR_HEADER))
            	studData.tenure = val;
            else if(headerName.equals(OrbitConstants. RIM_HEADER))
            	studData.rim = val;            
  
            
            else if(headerName.equals(OrbitConstants.STATUS_HEADER))
            	studData.status = val;
            else if(headerName.equals(OrbitConstants.PRODUCT_HEADER))
            	studData.product = val;
            else if(headerName.equals(OrbitConstants.PHONE_HEADER))
            	studData.phoneNumber = val;               

            else if(headerName.equals(OrbitConstants. EMAIL_HEADER))
            	studData.email = val;
            else if(headerName.equals(OrbitConstants.SECTOR_HEADER))
            	studData.sector = val;
         
        }
        catch(EmptyColumnValueException ex)
        {
        	if(sheetManager.isBlankRow(row))
        		throw ex;
        	val = ex.getMessage();
        	log.warning(ex.getMessage());
        	studData.isValid = false;
        }        
        catch (InvalidColumnValueException ex) {
            val = ex.getMessage();
            studData.isValid = false;
        }
        return val;
    }

    private void printHtmlList(String[] list, ServletOutputStream out) throws IOException {
        out.println("<ul>");
        for (String s : list) {
            out.println("<li>" + s + "</li>");
        }
        out.println("</ul>");
    }

    public void printHtmlList(List<String> list, ServletOutputStream out) throws IOException
    {
        out.println("<ul>");
        for (String s : list) {
            out.println("<li>" + s + "</li>");
        }
        out.println("</ul>");
    }
    
    public static Key<OrbitRimLocations> createRimMap(Objectify ofy, String[] addys, String[] rims)
    {
    	String[] temp = {};
    	OrbitRimLocations rimLocations = new OrbitRimLocations(rims, addys);
    	Key<OrbitRimLocations> result = ofy.put(rimLocations);
    	return result;
    }
}



