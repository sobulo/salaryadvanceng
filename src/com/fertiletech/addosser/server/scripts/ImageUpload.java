/**
 * 
 */
package com.fertiletech.addosser.server.scripts;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;

import com.fertiletech.addosser.server.loanmktdata.LoanMktDAO;
import com.fertiletech.addosser.server.scripts.downloads.InetImageBlob;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class ImageUpload extends HttpServlet {
	static
	{
		LoanMktDAO.registerClassesWithObjectify();
	}	
	private static final Logger log = Logger.getLogger(ImageUpload.class
			.getName());

	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		try {

			log.warning("received request");
			/*PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_SUPER};
	        try 
	        {
				LoginPortal.verifyRole(allowedRoles, req);
			} catch (LoginValidationException e) 
			{
				res.getOutputStream().println("<html><body><b>Illegal Access: " + e.getMessage() +
				"</b></body></html>");
				return;
			}*/			
			ServletFileUpload upload = new ServletFileUpload();
			res.setContentType("text/plain");

			FileItemIterator iterator = upload.getItemIterator(req);
			String blobName = null;
			while (iterator.hasNext()) {
				FileItemStream item = iterator.next();
				InputStream stream = item.openStream();

				if (item.isFormField()) {
					log.warning("Got a form field: " + item.getFieldName() + " with yet another name: " + item.getName());
					blobName = Streams.asString(stream);
					log.warning("Value is: " + blobName);
				} else {
					log.warning("Got an uploaded file: " + item.getFieldName()
							+ ", name = " + item.getName());

					// You now have the filename (item.getName() and the
					// contents (which you can read from stream). Here we just
					// print them back out to the servlet output stream, but you
					// will probably want to do something more interesting (for
					// example, wrap them in a Blob and commit them to the
					// datastore).
					int len = 0;
					byte[] buffer = new byte[32768];
					int totalBytesRead = 0;
					while ((len = stream.read(buffer, len, buffer.length)) != -1) {
						totalBytesRead += len;
						if (totalBytesRead >= buffer.length)
							break;
						// res.getOutputStream().write(buffer, 0, len);
						log.warning("wrote " + len + " bytes for school report card image");
					}

					if (totalBytesRead < buffer.length) {
						byte[] imgData = Arrays.copyOf(buffer, totalBytesRead);
						InetImageBlob image = new InetImageBlob(blobName);
						image.setImage(imgData);
						Objectify ofy = ObjectifyService.beginTransaction();
						try
						{
							Key<InetImageBlob> dsKey = ofy.put(image);
							log.warning("Saved image for " + dsKey);
							ofy.getTxn().commit();
						}
						finally
						{
							if(ofy.getTxn().isActive())
								ofy.getTxn().rollback();
						}
						res.getOutputStream().println("Saved file successfully");
					}
					else
						res.getOutputStream().println("File is too large");
				}
			}
		} catch (Exception ex) {
			throw new ServletException(ex);
		}
	}

}
