/**
 * 
 */
package com.fertiletech.addosser.server.scripts;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Logger;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fertiletech.addosser.server.loanmktdata.ConsumerLoanSalesLead;
import com.fertiletech.addosser.server.loanmktdata.LoanMktDAO;
import com.fertiletech.addosser.server.messaging.MessagingDAO;
import com.fertiletech.addosser.shared.DTOConstants;
import com.fertiletech.addosser.shared.workstates.WorkflowStateInstance;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class IntraDayMail extends HttpServlet{
	private static final Logger log = Logger.getLogger(IntraDayMail.class.getName());
	
	static
	{
		LoanMktDAO.registerClassesWithObjectify();
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException
	{
		ServletOutputStream out = res.getOutputStream();
		res.setContentType("text/html");
		String recipientList = "info@fertiletech.com";
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, -7);
		String startTime = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM).format(cal.getTime());
		String endTime = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM).format(new Date());
		
		
		StringBuilder textMessage = new StringBuilder("Status report on loan applications, start date: " + startTime + " end date: " + endTime + " .\n\n");
		StringBuilder htmlMessage = new StringBuilder("Status report on loan applications, start time: " + startTime + " end time: " + endTime + "<br/><hr>");
		Iterable<ConsumerLoanSalesLead> salesLeads = LoanMktDAO.getSalesLeadsByDate(cal.getTime(), new Date());
		HashMap<WorkflowStateInstance, Integer> aggregates = new HashMap<WorkflowStateInstance, Integer>();
		for(WorkflowStateInstance st : WorkflowStateInstance.values())
			aggregates.put(st, 0);
		
		int stateCount = 0;
		for(ConsumerLoanSalesLead lead : salesLeads)
		{
			stateCount = aggregates.get(lead.getCurrentState());
			aggregates.put(lead.getCurrentState(), stateCount + 1);
		}
		
		textMessage.append("IT Name\tOps Name\tCounts\t");
		htmlMessage.append("<table border='1' style='border:1 px solid green'><tr><td>Status</td><td>Suggested Action</td><td>Counts</td></tr>");
		for(WorkflowStateInstance st : WorkflowStateInstance.values())
		{
			stateCount = aggregates.get(st);
			log.warning("State: " + st + " count: " + stateCount + " Ops queue: " + st.opsQueue());
			String action = st.opsQueue() == null || st.opsQueue() < 0 || st.opsQueue() >= DTOConstants.OPS_SUGGESTIONS.length? "Check" : DTOConstants.OPS_SUGGESTIONS[st.opsQueue()];
			log.warning("State action: " + action);
			textMessage.append("\t").append(st.toString()).append("\t").append(action).append("\t").append(stateCount).append("\n");
			htmlMessage.append("<tr><td>").append(st.toString()).append("</td><td>").append(action).append("</td><td>").append(stateCount).append("</td></tr>");			
		}
		String[] messageContent = {textMessage.toString(), htmlMessage.append("</table>").toString()};
		MessagingDAO.scheduleSendEmail(recipientList, messageContent, "S.A.NG Intra-Day Counts", false);
		out.println("<b>message sent</b><br/>");
	}
}
