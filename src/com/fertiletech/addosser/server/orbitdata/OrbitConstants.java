package com.fertiletech.addosser.server.orbitdata;

import java.text.DateFormat;
import java.text.NumberFormat;

import com.fertiletech.utils.ColumnAccessor;
import com.fertiletech.utils.DateColumnAccessor;
import com.fertiletech.utils.ExcelManager;
import com.fertiletech.utils.NumberColumnAccessor;

public class OrbitConstants {
	
    public final static DateFormat DATE_FORMATTER =
            DateColumnAccessor.getDateFormatter();
    public final static NumberFormat NUMBER_FORMATTER = NumberFormat.getInstance();



    public final static String NAME_HEADER = "Account Name".toUpperCase();
    public final static String COMPANY_HEADER = "Organisation".toUpperCase();
    public final static String ADDRESS_HEADER = "Address1".toUpperCase();
    public final static String ADDRESS_HEADER2 = "Address2".toUpperCase();
    public final static String ORBIT_HEADER = "Account Number".toUpperCase();
    public final static String TENOR_HEADER = "Term".toUpperCase();
    public final static String AMOUNT_HEADER = "Amount Granted".toUpperCase();
    
    
    public final static String RIM_HEADER = "RIM No".toUpperCase();
    public final static String DATE_CGRANTED_HEADER = "Date Granted".toUpperCase();
    public final static String DATE_MATURITY_HEADER = "Maturity Date".toUpperCase();
    public final static String STATUS_HEADER = "Status".toUpperCase();
    public final static String PRODUCT_HEADER = "Product".toUpperCase();
    public final static String PHONE_HEADER = "Phone Number".toUpperCase();
    public final static String EMAIL_HEADER = "Mail".toUpperCase(); 
    public final static String SECTOR_HEADER = "Sector".toUpperCase();    
 ;
    
	///fields for excel file uplaod
 	public final static ColumnAccessor[] ORBIT_UPLOAD_ACCESSORS =
         new ColumnAccessor[15];
 
 	static
    {
    	NUMBER_FORMATTER.setMaximumFractionDigits(2);
        ORBIT_UPLOAD_ACCESSORS[0] = new ExcelManager.DefaultAccessor(NAME_HEADER, true);
        ORBIT_UPLOAD_ACCESSORS[1] = new ExcelManager.DefaultAccessor(COMPANY_HEADER, true);
        ORBIT_UPLOAD_ACCESSORS[2] = new ExcelManager.DefaultAccessor(ADDRESS_HEADER, false);
        ORBIT_UPLOAD_ACCESSORS[3] = new ExcelManager.DefaultAccessor(ADDRESS_HEADER2, false);        
        ORBIT_UPLOAD_ACCESSORS[4] = new ExcelManager.DefaultAccessor(ORBIT_HEADER, true);
        ORBIT_UPLOAD_ACCESSORS[5] = new ExcelManager.DefaultAccessor(TENOR_HEADER, true);
        ORBIT_UPLOAD_ACCESSORS[6] = new NumberColumnAccessor(AMOUNT_HEADER, true);
        
        ORBIT_UPLOAD_ACCESSORS[7] = new ExcelManager.DefaultAccessor(RIM_HEADER, true);
        ORBIT_UPLOAD_ACCESSORS[8] = new DateColumnAccessor(DATE_CGRANTED_HEADER, true);
        ORBIT_UPLOAD_ACCESSORS[9] = new DateColumnAccessor(DATE_MATURITY_HEADER, true);        
        ORBIT_UPLOAD_ACCESSORS[10] = new ExcelManager.DefaultAccessor(STATUS_HEADER, true);
        ORBIT_UPLOAD_ACCESSORS[11] = new ExcelManager.DefaultAccessor(PRODUCT_HEADER, true);
        ORBIT_UPLOAD_ACCESSORS[12] = new ExcelManager.DefaultAccessor(PHONE_HEADER, false);
        ORBIT_UPLOAD_ACCESSORS[13] = new ExcelManager.DefaultAccessor(EMAIL_HEADER, false);        
        ORBIT_UPLOAD_ACCESSORS[14] = new ExcelManager.DefaultAccessor(SECTOR_HEADER, false);
    }
    
 	
    public final static int NAME_IDX = 0;
    public final static int COMPANY_IDX = 1;
    public final static int ACCT_NUM_IDX = 2;
    public final static int TENOR_IDX = 3;
    public final static int AMOUNT_IDX = 4;
    public final static int DATE_CGRANTED_IDX = 5;
    public final static int STATUS_IDX = 6;
    public final static int PRODUCT_IDX = 7;
    public final static int SECTOR_IDX = 8;
  
				

}
