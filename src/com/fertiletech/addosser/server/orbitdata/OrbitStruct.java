package com.fertiletech.addosser.server.orbitdata;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.fertiletech.addosser.shared.table.TableMessage;
import com.fertiletech.addosser.shared.table.TableMessageHeader;
import com.fertiletech.addosser.shared.table.TableMessageHeader.TableMessageContent;

public class OrbitStruct implements Serializable
{	
    public String name;
    public String company;
    public String address;    
    public String address2;
    public String amount;
    public String tenure;
    public String orbitId;
    public String rim;
    public String dateGranted;
    public String maturityDate;
    public String status;
    public String product;
    public String phoneNumber;
    public String email; 
    public String sector;  
    
    
    public boolean isValid = true;
    
    private static HashMap<String, OrbitStruct> getRimOrbitStructMap(OrbitStruct[] orbitFeedStruct)
    {
    	HashMap<String, OrbitStruct> rimMap = new HashMap<>();
    	for(OrbitStruct orb : orbitFeedStruct)
    		rimMap.put(orb.rim, orb);
    	return rimMap;
    }
    
    public static HashMap<String, OrbitStruct> getRimOrbitStructMap(HashMap<String, String[]> orbitFeed)
    {
    	return getRimOrbitStructMap(getFeedAsOrbitStruct(orbitFeed));
    }
    
    public static List<TableMessage> getFeedAsTable(HashMap<String, String[]> orbitFeed)
    {
    	return getFeedAsTable(getFeedAsOrbitStruct(orbitFeed));
    }
    
    private static List<TableMessage> getFeedAsTable(OrbitStruct[] orbitFeedStruct)
    {
        TableMessageHeader columnHeaders = new TableMessageHeader(9);
        columnHeaders.setText(OrbitConstants.NAME_IDX, OrbitConstants.NAME_HEADER, TableMessageContent.TEXT);
        columnHeaders.setText(OrbitConstants.COMPANY_IDX, OrbitConstants.COMPANY_HEADER, TableMessageContent.TEXT);
        columnHeaders.setText(OrbitConstants.ACCT_NUM_IDX, OrbitConstants.ORBIT_HEADER, TableMessageContent.TEXT);
        columnHeaders.setText(OrbitConstants.TENOR_IDX, OrbitConstants.TENOR_HEADER, TableMessageContent.TEXT);
        columnHeaders.setText(OrbitConstants.AMOUNT_IDX, OrbitConstants.AMOUNT_HEADER, TableMessageContent.TEXT);
        columnHeaders.setText(OrbitConstants.DATE_CGRANTED_IDX, OrbitConstants.DATE_CGRANTED_HEADER, TableMessageContent.TEXT);
        columnHeaders.setText(OrbitConstants.STATUS_IDX, OrbitConstants.STATUS_HEADER, TableMessageContent.TEXT);
        columnHeaders.setText(OrbitConstants.PRODUCT_IDX, OrbitConstants.PRODUCT_HEADER, TableMessageContent.TEXT);
        columnHeaders.setText(OrbitConstants.SECTOR_IDX, OrbitConstants.SECTOR_HEADER, TableMessageContent.TEXT);
        ArrayList<TableMessage> table = new ArrayList<>(orbitFeedStruct.length + 1);
        table.add(columnHeaders);
        for(OrbitStruct orb : orbitFeedStruct)
        {
        	TableMessage row = new TableMessage(9,0,0);
            row.setText(OrbitConstants.NAME_IDX, orb.name);
            row.setText(OrbitConstants.COMPANY_IDX, orb.company);
            row.setText(OrbitConstants.ACCT_NUM_IDX, orb.orbitId);
            row.setText(OrbitConstants.TENOR_IDX, orb.tenure);
            row.setText(OrbitConstants.AMOUNT_IDX, orb.amount);
            row.setText(OrbitConstants.DATE_CGRANTED_IDX, orb.dateGranted);
            row.setText(OrbitConstants.STATUS_IDX, orb.status);
            row.setText(OrbitConstants.PRODUCT_IDX, orb.product);
            row.setText(OrbitConstants.SECTOR_IDX, orb.sector);
        	table.add(row);
        }
    	return table;
    }
    
    public static OrbitStruct[] getFeedAsOrbitStruct(HashMap<String, String[]> orbitFeed)
    {
    	OrbitStruct[] rows = null;
    	for(String colName: orbitFeed.keySet())
    	{
    		String[] columnVals = orbitFeed.get(colName);
    		
    		//init rows
    		if(rows == null)
    		{
    			rows = new OrbitStruct[columnVals.length];
    			for(int i = 0; i < rows.length; i++)
    				rows[i] = new OrbitStruct();
    		}
    		
    		for(int i = 0; i < columnVals.length; i++)
    		{
    			OrbitStruct row = rows[i];
    			String val = columnVals[i];
    			if(colName.equals(OrbitConstants.NAME_HEADER))
    			{
    				row.name = val;
    			}
    			else if(colName.equals(OrbitConstants.COMPANY_HEADER))
    			{
    				row.company = val;
    			}
    			else if(colName.equals(OrbitConstants.ADDRESS_HEADER))
    			{
    				row.address =val;
    			}
    			else if(colName.equals(OrbitConstants.ADDRESS_HEADER2))
    			{
    				row.address2 = val;
    			}
    			else if(colName.equals(OrbitConstants.ORBIT_HEADER))
    			{
    				row.orbitId = val;
    			}
    			else if(colName.equals(OrbitConstants.TENOR_HEADER))
    			{
    				row.tenure = val;
    			}
    			else if(colName.equals(OrbitConstants.AMOUNT_HEADER))
    			{
    				row.amount = val;
    			}
    			else if(colName.equals(OrbitConstants.RIM_HEADER))
    			{
    				row.rim = val;
    			}
    			else if(colName.equals(OrbitConstants.DATE_CGRANTED_HEADER))
    			{
    				row.dateGranted = val;
    			}
    			else if(colName.equals(OrbitConstants.DATE_MATURITY_HEADER))
    			{
    				row.maturityDate = val;
    			}
    			else if(colName.equals(OrbitConstants.STATUS_HEADER))
    			{
    				row.status = val;
    			}
    			else if(colName.equals(OrbitConstants.PRODUCT_HEADER))
    			{
    				row.product = val;
    			}
    			else if(colName.equals(OrbitConstants.PHONE_HEADER))
    			{
    				row.phoneNumber = val;
    			}
    			else if(colName.equals(OrbitConstants.EMAIL_HEADER))
    			{
    				row.email = val;
    				
    			}
    			else if(colName.equals(OrbitConstants.SECTOR_HEADER))
    			{
    				row.sector = val;
    				
    			}
    			else
    				throw new IllegalStateException("Unrecognized column in data feed: " + colName);
    		}
    	}
    	return rows;
    }
}
