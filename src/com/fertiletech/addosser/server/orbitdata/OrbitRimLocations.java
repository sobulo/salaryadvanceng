package com.fertiletech.addosser.server.orbitdata;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.persistence.Id;

import com.google.appengine.api.datastore.GeoPt;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Serialized;
import com.googlecode.objectify.annotation.Unindexed;

@Unindexed
@Cached
public class OrbitRimLocations 
{
	@Id
	String myId;
	
	@Serialized
	ArrayList<String>rimIds;
	@Serialized
	ArrayList<Boolean>querySucceeded;
	@Serialized
	ArrayList<GeoPt> mapPoints;
	@Serialized
	ArrayList<String> address;
	
	Date createDate;

	
	final static String ONLY_KEY = "RIM_MAP";
	
	OrbitRimLocations()
	{
		this.myId = ONLY_KEY;
		createDate = new Date();
	}
	
	public int[] getRawDataCounts()
	{
		int[] result = new int [4];
		result[0] = rimIds.size();
		result[1] = querySucceeded.size();
		result[2] = mapPoints.size();
		result[3] = address.size();
		return result;
	}
	
	public int[] getQueryStatusCounts()
	{
		int[] result = new int [3];
		int nullCount = 0;
		int successCount = 0;
		int failCount = 0;
		for(Boolean q : querySucceeded)
		{
			if(q == null)
				nullCount++;
			else if(q)
				successCount++;
			else
				failCount++;
		}
		result[0] = nullCount;
		result[1] = failCount;
		result[2] = successCount;
		return result;
	}

	public OrbitRimLocations(String[] rimIds, String[] address)
	{
		this();
		this.address = new ArrayList<>(rimIds.length);
		this.rimIds = new ArrayList<>(rimIds.length);
		mapPoints = new ArrayList<>(rimIds.length);;
		querySucceeded = new ArrayList<>(rimIds.length);
		addNewRims(rimIds, address);

	}
	
	public static Key<OrbitRimLocations> getKey()
	{
		return new Key<OrbitRimLocations>(OrbitRimLocations.class, ONLY_KEY);
	}
	
	public HashMap<String, GeoPt> getRimMap() {
		HashMap<String, GeoPt>  rimMap = new HashMap<>();
		for(int i = 0; i < querySucceeded.size(); i++)
		{
			String rim = rimIds.get(i);
			String addy = address.get(i);
			Boolean queryDone = querySucceeded.get(i);
			if(queryDone == null)
			{
				throw new IllegalStateException("GeoCoding not yet attempted for RIM: " + 
						rim + " Address: " + addy);
			}
			if(querySucceeded.get(i)) //geo code successful
				rimMap.put(rim, mapPoints.get(i));
		}
		return rimMap;
	}
	
	public HashMap<String, String> getRimAddresses()
	{
		HashMap<String, String> result = new HashMap<>();
		for(int i = 0; i < rimIds.size(); i++)
			result.put(rimIds.get(i), address.get(i));
		return result;	
	}

	public void updateQueryState(int idx, boolean update)
	{
		if(update)
			querySucceeded.set(idx,true);
		else if(mapPoints.get(idx) == null) //don't overwrite
			querySucceeded.set(idx, false);
		
			
	}
	
	public void setGeoPt(int idx, GeoPt loc) {
		mapPoints.add(idx, loc);
	}
	
	public boolean hasUncompletedQuery()
	{
		for(Boolean attempt: querySucceeded)
			if(attempt == null)
				return true;
		return false;
	}
	
	public String getAddress(int idx)
	{
		return address.get(idx);
	}	
	
	public Integer getNextAvailableLocation()
	{
		for(int idx = 0; idx < querySucceeded.size(); idx++)
		{
			Boolean attempt = querySucceeded.get(idx);
			if(attempt == null)
				return idx;
		}
		return null; //geo code attempt has been made for all addresses
	}
	
	public int[] addNewRims(String[] rimIds, String[] address)
	{
		if(rimIds.length != address.length)
			throw new IllegalArgumentException("Rim Ids "  + rimIds.length +
					" and Addresses " + address.length + ""
							+ "are of differnt sizes");

		HashMap<String, GeoPt> rimMap = getRimMap();
		int addCount = 0;
		for(int i = 0; i < rimIds.length; i++)
		{
			if(rimIds[i] == null)
				continue;
			String rim = rimIds[i];
			String addy = address[i];
			if(rimMap.containsKey(rim))
				continue;
			this.rimIds.add(rim);
			this.mapPoints.add(null);
			this.address.add(addy);
			this.querySucceeded.add(null);
			addCount++;
		}
		return getRawDataCounts();
	}
}