package com.fertiletech.addosser.server.orbitdata;

import java.util.Date;
import java.util.HashMap;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Serialized;
import com.googlecode.objectify.annotation.Unindexed;

@Unindexed
@Cached
public class OrbitDataFeed 
{
	@Id
	String myId;
	
	@Serialized
	HashMap<String, String[]> orbitData;
	
	@Indexed
	Date createDate;
	
	String updateUser;
	
	OrbitDataFeed()
	{
		orbitData = new HashMap<String, String[]>();
		createDate = new Date();
	}
	
	OrbitDataFeed(String name)
	{
		this();
		this.myId = name;
	}

	public OrbitDataFeed(String name, String[] colNames, int numRows, String userEmail)
	{
		this(name);
		for (String col : colNames)
			orbitData.put(col, new String[numRows]); 
		this.updateUser = userEmail;
	}
	
	public static Key<OrbitDataFeed> getKey(String ID)
	{
		return new Key<OrbitDataFeed>(OrbitDataFeed.class, ID);
	}
	
	public HashMap<String, String[]> getorbitData() {
		return orbitData;
	}
	
	public String getUpdater()
	{
		return updateUser;
	}
	
	public Date getDateCreated()
	{
		return createDate;
	}

	public void setorbitData(HashMap<String, String[]> orbitData) {
		this.orbitData = orbitData;
	}
	
	public void setrowValue(String colName, int rowIdx, String value)
	{
		if(!orbitData.containsKey(colName))
			throw new IllegalArgumentException("This field is not stored in the orbit feed: " 
		+ colName);
		orbitData.get(colName)[rowIdx] = value;
	}

	public Key<OrbitDataFeed> getKey()
	{
		return new Key<OrbitDataFeed>(OrbitDataFeed.class, myId);
	}
}
