/* Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fertiletech.addosser.server.pub;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fertiletech.addosser.server.loanmktdata.ConsumerLoanSalesLead;
import com.fertiletech.addosser.server.loanmktdata.LoanMktDAO;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

// [START example]
@SuppressWarnings("serial")
public class LoanStatusServlet extends HttpServlet {

	private final static String ERR_KEY = "sangerror";
	// [START setup]
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
													// (this Servlet)
		req.setAttribute("page", "status"); // Tells base.jsp to include
											// form.jsp
		req.getRequestDispatcher("/base.jsp").forward(req, resp);
		req.setAttribute(ERR_KEY,"email must be identical to the one you specified in the loan request form");
	}
	// [END setup]

	private void saveOldformDate(HttpServletRequest req)
	{
		req.setAttribute("oldmail", req.getParameter("email"));
		req.setAttribute("oldid", req.getParameter("id"));
	}
	// [START formpost]
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		HttpSession session = req.getSession();
		// [END bookBuilder]

		// go get the lead
		boolean notFound = false;
		String email = req.getParameter("email").toLowerCase();
		String id = req.getParameter("id");
		Objectify ofy = ObjectifyService.begin();
		List<Key<ConsumerLoanSalesLead>> leadKeys = LoanMktDAO.getConsumerLoanLeadKeysViaEmail(ofy, email);
		Map<Key<ConsumerLoanSalesLead>, ConsumerLoanSalesLead> leadsMap = null;
		if (leadKeys != null && leadKeys.size() > 0)
			leadsMap = ofy.get(leadKeys);
		Collection<ConsumerLoanSalesLead> leads = null;
		if (leadsMap != null && leadsMap.size() > 0)
			leads = leadsMap.values();

		if (leads != null && id != null) {
			try {
				Long idVal = Long.valueOf(id);
				boolean found = false;
				for(ConsumerLoanSalesLead ld : leads) 
				{
					if(ld.getLeadID().equals(idVal))
					{
						found = true;
						Book bk = DatastoreDao.leadStatusToBook(ld);
						req.setAttribute("book", bk);
						req.setAttribute("sang", ld.getKey().getString());
					}
					if(!found)
					{
						req.setAttribute(ERR_KEY, id + " does not match any reuests for " + email);
						saveOldformDate(req);
						
					}
				}
				
														// wrote
			} catch (Exception e) {
				req.setAttribute(ERR_KEY, "Error parsing input values, hit back om your browser and recheck values entered");
				saveOldformDate(req);
			}
		}else
		{
			req.setAttribute(ERR_KEY, "No requests found for " + email + " and " + id);
			saveOldformDate(req);
		}
	      
	      req.setAttribute("page", "status");
	      req.getRequestDispatcher("/base.jsp").forward(req, resp);
	}
	// [END formpost]
}
// [END example]
