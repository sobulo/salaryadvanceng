/* Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fertiletech.addosser.server.pub;


import java.io.IOException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;

import com.fertiletech.addosser.shared.DTOConstants;

// [START example]
@SuppressWarnings("serial")
public class UpdateBookServlet extends HttpServlet {
  @Override
  public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
      IOException {
	    req.setAttribute("action", "Loan Request Form");          // Part of the Header in form.jsp
	    req.setAttribute("destination", "update");  // The urlPattern to invoke (this Servlet)
	    req.setAttribute("page", "form");           // Tells base.jsp to include form.jsp
	    req.getRequestDispatcher("/base.jsp").forward(req, resp);
  }

  @Override
  public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
      IOException {

    try {

      // [START bookBuilder]
      Book originalBook = CreateBookServlet.requestToBook(req);
      Book displayBook= CreateBookServlet.requestToBook(req);
      
      boolean success = DatastoreDao.populateFormErrors(displayBook);


      //hack go back buttom
      if(!success)
      {
    	  originalBook.setStyle("btn-danger");
    	  originalBook.setMessage("Edit Errors");
      }
      else    
      {
    	  originalBook.setStyle("btn-info");
    	  originalBook.setMessage("Edit Form");
      }
      req.setAttribute("dpbook", displayBook);
      req.setAttribute("book", originalBook);
      req.setAttribute("page", "review");
      req.getRequestDispatcher("/base.jsp").forward(req, resp);
    } catch (Exception e) {
      throw new ServletException("Error updating book", e);
    }
  }
}
// [END example]
