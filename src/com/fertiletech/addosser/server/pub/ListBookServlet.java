/* Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fertiletech.addosser.server.pub;



import java.io.IOException;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fertiletech.addosser.server.loanmktdata.EntityConstants;
import com.fertiletech.addosser.server.loanmktdata.LoanMktDAO;

// [START example]
@SuppressWarnings("serial")
public class ListBookServlet extends HttpServlet {

  @Override
  public void init() throws ServletException {
    BookDao dao = new DatastoreDao();;
    this.getServletContext().setAttribute("dao", dao);
    this.getServletContext().setAttribute(
        "isCloudStorageConfigured", false);
    LoanMktDAO.registerClassesWithObjectify();
  }

  @Override
  public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException,
      ServletException {
    BookDao dao = (BookDao) this.getServletContext().getAttribute("dao");
    List<Book> books = null;
    try {
      books = dao.listRecentBookings();

    } catch (Exception e) {
      throw new ServletException("Error listing books", e);
    }
    req.getSession().getServletContext().setAttribute("books", books);
    String salaryStr = req.getParameter("monthly-salary");
    if(salaryStr != null)
    	req.setAttribute("eligible", EntityConstants.getEligibleLoanAmount(salaryStr));   
    	
    req.setAttribute("page", "list");   
    req.getRequestDispatcher("/base.jsp").forward(req, resp);
  }
}
// [END example]
