/* Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fertiletech.addosser.server.pub;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fertiletech.addosser.server.tasks.TaskQueueHelper;



// [START example]
@SuppressWarnings("serial")
public class CreateBookServlet extends HttpServlet {

  // [START setup]
  @Override
  public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
      IOException {
    req.setAttribute("action", "Loan Request Form");          // Part of the Header in form.jsp
    req.setAttribute("destination", "create");  // The urlPattern to invoke (this Servlet)
    req.setAttribute("page", "form");           // Tells base.jsp to include form.jsp
    req.getRequestDispatcher("/base.jsp").forward(req, resp);
  }
  // [END setup]

  // [START formpost]
  @Override
  public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
      IOException {

    HttpSession session = req.getSession();


    // [START bookBuilder]
    Book book =requestToBook(req);
    // [END bookBuilder]

    BookDao dao = (BookDao) this.getServletContext().getAttribute("dao");
    try {
      String []  id= dao.createBook(book);
      session.setAttribute(Book.SANG_CREATE, id);
      String[] comments = {"Application for: " + book.toString(), " Response sent: " + id[1]};
      TaskQueueHelper.scheduleCreateComment(comments, id[0], book.getEmail());
      resp.sendRedirect("/read?id=" + id[0]);   // read what we just wrote
    } catch (Exception e) {
      throw new ServletException("Error creating book", e);
    }
  }
  
  public static Book requestToBook(HttpServletRequest req)
  {
	  Book bk = new Book.Builder()
		        .salary(req.getParameter("salary"))
		        .amount(req.getParameter("amount"))
		        .tenor(req.getParameter("tenor"))
		        .state(req.getParameter("state"))
		        .name(req.getParameter("name"))
		        .company(req.getParameter("company"))
		        .phone(req.getParameter("phone"))
		        .email(req.getParameter("email"))
		        .type(req.getParameter("type"))
		        .build(); 
	  bk.setAge(req.getParameter("age"));
	  bk.setHire(req.getParameter("hire"));
	  bk.setGender(req.getParameter("gender"));
	  
	  return bk;
  }
  // [END formpost]
}
// [END example]
