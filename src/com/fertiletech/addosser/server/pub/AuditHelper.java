package com.fertiletech.addosser.server.pub;

import java.text.DateFormat;
import java.util.List;

import com.fertiletech.addosser.server.loanmktdata.ConsumerLoanComment;
import com.fertiletech.addosser.server.loanmktdata.ConsumerLoanSalesLead;
import com.fertiletech.addosser.server.loanmktdata.LoanMktDAO;
import com.fertiletech.addosser.shared.FormHTMLDisplayBuilder;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;

public class AuditHelper {
	
	public final static String getAudutTrail(String leadID)
	{
		Key<ConsumerLoanSalesLead> key = ObjectifyService.factory().stringToKey(leadID);
		List<ConsumerLoanComment> commentList = LoanMktDAO.getLoanComments(ObjectifyService.begin(), key);
		FormHTMLDisplayBuilder result = new FormHTMLDisplayBuilder().formBegins();
		result.headerBegins().appendTextBody("Comments").headerEnds();
		DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
		DateFormat tf = DateFormat.getTimeInstance(DateFormat.MEDIUM);
		String[] bgStyles = {"#c1e3f6", "#dcf6c1", "#f9d4eb", "#f9f9d4"};
		int i = 0;
		for(ConsumerLoanComment c : commentList)
		{
			//beta testing mode ... show everything
			//if(respectShowPublic && !c.isShowPublic())
				//continue;
			int bgspot = i % bgStyles.length;
			i++;
			String clrStyle = "background-color:" + bgStyles[bgspot] +";";
			result.backgroundBegins(clrStyle);
			result.lineBegins().appendTextBody("User", c.getUser()).appendTextBody("Date", df.format(c.getDateUpdated()) + " " + tf.format(c.getDateUpdated())).lineEnds();
			result.lineBegins().appendTextBody(c.getComment().getValue()).lineEnds();
			result.sectionEnds();
			result.backGroundEnds();
		}
		return result.toString();
	}

}
