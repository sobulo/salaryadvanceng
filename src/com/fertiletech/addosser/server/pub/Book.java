/* Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Modified for the purpose of loan applications - sobulo@fertiletech.com
 */

package com.fertiletech.addosser.server.pub;

// [START example]
public class Book {
	// [START book]
	private String company;
	private String name;
	private String email;
	private String phone;
	private String salary;
	private String state;
	private String loanType;
	private String tenor;
	private String amount;
	private String ofykey;
	private String style;
	private String message;
	
	//additional fields requested by ops
	private String age;
	private String hire;
	private String gender;
	
	private Long id;
	
	public static String SANG_CREATE = "sang-create-message";
	
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getHire() {
		return hire;
	}

	public void setHire(String hire) {
		this.hire = hire;
	}

	// [END book]


	// [START constructor]
	// We use a Builder pattern here to simplify and standardize construction of
	// Book objects.
	private Book(Builder builder) {
		this.company = builder.company;
		this.name = builder.name;
		this.email = builder.email;
		this.phone = builder.phone;
		this.salary = builder.salary;
		this.state = builder.state;
		this.id = builder.id;
		this.loanType = builder.loanType;
		this.tenor = builder.tenor;
		this.amount = builder.amount;
		this.ofykey = builder.ofykey;

	}
	// [END constructor]

	// [START builder]
	public static class Builder {
		private String company;
		private String name;
		private String email;
		private String phone;
		private String salary;
		private String state;
		private Long id;
		private String loanType;
		private String tenor;
		private String amount;
		private String ofykey;
		

		public Builder company(String company) {
			this.company = company;
			return this;
		}

		public Builder name(String name) {
			this.name = name;
			return this;
		}

		public Builder email(String email) {
			this.email = email;
			return this;
		}

		public Builder phone(String phone) {
			this.phone = phone;
			return this;
		}

		public Builder salary(String salary) {
			this.salary = salary;
			return this;
		}

		public Builder state(String state) {
			this.state = state;
			return this;
		}
		
		public Builder ofykey(String ok) {
			this.ofykey = ok;
			return this;
		}

		public Builder type(String loanType) {
			this.loanType = loanType;
			return this;
		}
		
		public Builder amount(String a) {
			this.amount = a;
			return this;
		}

		public Builder tenor(String t) {
			this.tenor = t;
			return this;
		}

		public Builder id(Long id) {
			this.id = id;
			return this;
		}

		public Book build() {
			return new Book(this);
		}
	}
	// [END builder]

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	
	}

	public void setOfykey(String ofykey) {
		this.ofykey = ofykey;
	}
	
	public String getOfykey() {
		return ofykey;
	}


	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setType(String loanType) {
		this.loanType = loanType;
	}

	public String getType() {
		return loanType;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String number) {
		this.phone = number;
	}
	
	public void setTenor(String tenor) {
		this.tenor = tenor;
	}

	public String getTenor() {
		return tenor;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}
	

	public String getSalary() {
		return salary;
	}

	public void setSalary(String salary) {
		this.salary = salary;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	@Override
	public String toString() {
		return "Company: " + company + ", Name: " + name + ", Salary: " + salary + ", Requested for: " + email;
	}
}
// [END example]
