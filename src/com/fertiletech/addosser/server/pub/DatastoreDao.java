/* Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fertiletech.addosser.server.pub;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import com.fertiletech.addosser.server.loanmktdata.ConsumerLoanSalesLead;
import com.fertiletech.addosser.server.loanmktdata.LoanApplicantUniqueIdentifier;
import com.fertiletech.addosser.server.loanmktdata.LoanApplicationFormData;
import com.fertiletech.addosser.server.loanmktdata.LoanMktDAO;
import com.fertiletech.addosser.server.tasks.TaskQueueHelper;
import com.fertiletech.addosser.shared.DTOConstants;
import com.fertiletech.addosser.shared.FormConstants;
import com.fertiletech.addosser.shared.FormConstants.FormValidators;
import com.fertiletech.addosser.shared.workstates.WorkflowStateInstance;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

// [START example]
public class DatastoreDao implements BookDao {
    private static final Logger log =
            Logger.getLogger(DatastoreDao.class.getName());
	// [START constructor]

	public final static NumberFormat NUM_FMT = NumberFormat.getInstance();
	{
		NUM_FMT.setMaximumFractionDigits(2);
	}

	public DatastoreDao() {
	}
	// [END constructor]

	public static String parseNumber(String formatedStr) {
		return formatedStr; // no op, used to be below but string being parsed
							// is no longer formatted
		/*
		 * try { return NUM_FMT.parse(formatedStr).toString(); } catch
		 * (ParseException e) { return ""; }
		 */
	}

	private static String getFormFieldStyle(String msg, boolean success) {

		return "<span style='color:" + (success ? "#44ace9" : "red") + ";'>" + msg + "</span>";
	}

	public static boolean populateFormErrors(Book book) {
		HashMap<String, FormValidators[]> validatorMap = FormConstants.pubFieldsMap;

		FormValidators[] nameCheck = validatorMap.get(FormConstants.APPLICANT_SURNAME_KEY);
		FormValidators[] phoneCheck = validatorMap.get(FormConstants.PRIMARY_PHONE_KEY);
		FormValidators[] mailCheck = validatorMap.get(FormConstants.EMAIL_KEY);
		FormValidators[] employerCheck = validatorMap.get(FormConstants.EMPLOYER_KEY);
		FormValidators[] salaryCheck = validatorMap.get(FormConstants.SALARY_KEY);
		FormValidators[] loanCheck = validatorMap.get(FormConstants.REQUESTED_LOAN_AMOUNT_KEY);

		boolean okToSubmit = true;
		// checkName;
		String error = getFormErrors(nameCheck, book.getName(), " name check ");
		boolean success = error == null;
		book.setName(success ? getFormFieldStyle(book.getName(), true) : error);
		okToSubmit = okToSubmit && success;

		// checkPhone;
		error = getFormErrors(phoneCheck, book.getPhone(), " phone check ");
		success = error == null;
		book.setPhone(success ? getFormFieldStyle(book.getPhone(), true) : error);
		okToSubmit = okToSubmit && success;

		// checkMail;
		error = getFormErrors(mailCheck, book.getEmail(), " email check ");
		success = error == null;
		book.setEmail(success ? getFormFieldStyle(book.getEmail(), true) : error);
		okToSubmit = okToSubmit && success;

		// checkEmployer;
		error = getFormErrors(employerCheck, book.getCompany(), " employer check ");
		success = error == null;
		book.setCompany(success ? getFormFieldStyle(book.getCompany(), true) : error);
		okToSubmit = okToSubmit && success;

		// salaryCheck;
		error = getFormErrors(salaryCheck, book.getSalary(), " salary check ");
		success = error == null;
		String sal = null;
		if (success)
			sal = NUM_FMT.format(Double.valueOf(book.getSalary()));
		book.setSalary(success ? getFormFieldStyle(sal, true) : error);
		okToSubmit = okToSubmit && success;

		// loanCheck;
		error = getFormErrors(loanCheck, book.getAmount(), " loan check ");
		success = error == null;
		String amt = null;
		if (success)
			amt = NUM_FMT.format(Double.valueOf(book.getAmount()));
		book.setAmount(success ? getFormFieldStyle(amt, true) : error);
		okToSubmit = okToSubmit && success;

		// all check
		String msg = "See below for form validation errrors. Click button"
				+ " to revisit the form and rectify your mistakes";
		if (okToSubmit)
			msg = "Review the data below and then submit your request at the bpttom of the page."
					+ " or click the button below to edit your request";
		else
			book.setStyle("disabled");
		book.setMessage(getFormFieldStyle(msg, okToSubmit));

		// append url to product type
		// set product url
		String productUrl = "<a href='http://info.salaryadvanceng.com'>info.salaryadvanceng.com</a>";
		String[] URLs = DTOConstants.getLoanTypeUrl();

		for (int i = 0; i < DTOConstants.LOAN_TYPES.length; i++) {
			if (DTOConstants.LOAN_TYPES[i].equals(book.getType())) {
				productUrl = "<a target='_blank' href='" + URLs[i] + "'>" + URLs[i] + "</a>";
				break;
			}
		}
		String typeMsg = " <span>" + book.getType() + "</span> <span style='font-size: smaller;'> (product page: [ "
				+ productUrl + " ] )</span>";
		book.setType(getFormFieldStyle(typeMsg, true));

		return okToSubmit;

	}

	public static String getFormErrors(FormValidators[] checkList, String val, String description) {

		StringBuilder errors = new StringBuilder();
		for (FormValidators check : checkList)
			check.validate(description, val, errors);
		return errors.length() == 0 ? null : getFormFieldStyle(errors.toString(), false);
	}

	private HashMap<String, String> bookToForm(Book book) {
		String email = book.getEmail().toLowerCase();
		HashMap<String, String> appData = new HashMap<>();
		String[] fullName = book.getName().split(",");
		String lastName = fullName[0].trim();
		String firstName = fullName.length > 1 ? fullName[1].trim() : "";
		appData.put(FormConstants.EMAIL_KEY, email);
		appData.put(FormConstants.PRIMARY_PHONE_KEY, book.getPhone()); // 1
		appData.put(FormConstants.EMPLOYER_KEY, book.getCompany()); // 1
		appData.put(FormConstants.APPLICANT_SURNAME_KEY, lastName);
		appData.put(FormConstants.APPLICANT_OTHERNAME_KEY, firstName);
		appData.put(FormConstants.LOAN_TYPE_KEY, book.getType());
		appData.put(FormConstants.SALARY_KEY, parseNumber(book.getSalary()));
		appData.put(FormConstants.TENOR_KEY, book.getTenor());
		appData.put(FormConstants.REQUESTED_LOAN_AMOUNT_KEY, parseNumber(book.getAmount()));
		appData.put(FormConstants.STATE_KEY, book.getState());
		appData.put(FormConstants.AGE_KEY, book.getAge());
		appData.put(FormConstants.HIRE_DURATION_KEY, book.getHire());
		appData.put(FormConstants.SEX_KEY, book.getGender());
		return appData;

	}

	// [START create]
	@Override
	public String[] createBook(Book book) {

		HashMap<String, String> appData = bookToForm(book);
		Integer tenor = null;
		String tenorStr = appData.get(FormConstants.TENOR_KEY);

		if (tenorStr != null) {
			tenorStr = tenorStr.split(" ")[0];
			tenor = LoanMktDAO.getNumberValue(tenorStr).intValue();
		}

		Objectify ofy = ObjectifyService.beginTransaction();
		String email = appData.get(FormConstants.EMAIL_KEY).toLowerCase();

		LoanApplicantUniqueIdentifier applicantIdentifier = new LoanApplicantUniqueIdentifier(email);
		List<Key<ConsumerLoanSalesLead>> leadKeys = LoanMktDAO.getConsumerLoanLeadKeysViaEmail(ofy, email);

		ConsumerLoanSalesLead salesLead = null;
		String[] result = null;
		try {
			//log.warning("In 1");

			if (leadKeys.size() == 0) {
				//log.warning("In 2");
				salesLead = new ConsumerLoanSalesLead(email);
				salesLead.setDecisionFields(appData.get(FormConstants.PRIMARY_PHONE_KEY),
						appData.get(FormConstants.EMPLOYER_KEY), LoanMktDAO.getFullName(appData),
						appData.get(FormConstants.HIRE_DURATION_KEY), appData.get(FormConstants.LOAN_TYPE_KEY), tenor,
						LoanMktDAO.getNumberValue(appData.get(FormConstants.SALARY_KEY)),
						LoanMktDAO.getNumberValue(appData.get(FormConstants.REQUESTED_LOAN_AMOUNT_KEY)),
						appData.get(FormConstants.ADDRESS_KEY), appData.get(FormConstants.STATE_KEY));
				//log.warning("In 3");
				String[] comments = setLeadState(salesLead);
				ofy.put(applicantIdentifier, salesLead); // save lead first so
															// we can get its
															// auto-assigned key
				//log.warning("In 4");
				TaskQueueHelper.scheduleCreateComment(comments, salesLead.getKey().getString(), "sang-ai");
				//log.warning("In 5");
			} // below
			else {

				Key<ConsumerLoanSalesLead> existingLeadKey = leadKeys.get(0);
				salesLead = ofy.get(existingLeadKey);
				// touch file so it shows up in last modified
				WorkflowStateInstance temp = salesLead.getCurrentState();
				salesLead.setCurrentState(null);
				salesLead.setCurrentState(temp);
				salesLead.setAddress(salesLead.getAddress() == null ? "" : salesLead.getAddress() + " ");
				ofy.put(salesLead);

				String[] error = { existingLeadKey.getString(),
						"Follow up for an update on your loan request: " + email
								+ ". Email hello@salaryadvanceng.com to follow up on the status of your application",
						"color:blue" };
				result = error;

			}

			LoanApplicationFormData loanApp = new LoanApplicationFormData(salesLead.getKey(), appData);
			ofy.put(loanApp);
			ofy.getTxn().commit();
		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		//log.warning("out 1");

		if (result != null)
			return result;
		//log.warning("out 2");

		result = new String[3];
		result[0] = salesLead.getKey().getString();
		result[1] = "Loan request submitted. You may check the status at anytime using  your S.A.NG ID: "
				+ salesLead.getLeadID() + " and email address: " + email
				+ ".Alternatively you can make enquries regarding "
				+ "your request by sending an email to hello@salaryadvanceng.com, include your S.A.NG ID in the body of the email";
		result[2] = "color:green";
		log.warning("out 3");
		return result;

	}

	private String[] setLeadState(ConsumerLoanSalesLead salesLead) {
		ArrayList<String> comments = new ArrayList<String>(4);
		String[] commentHelper = new String[1];
		WorkflowStateInstance status = WorkflowStateInstance.checkForPreApprovalYes(salesLead, comments);
		salesLead.setCurrentState(status);
		commentHelper = comments.toArray(commentHelper);
		return commentHelper;
	}
	// [END create]

	// [START read]
	@Override
	public Book readBook(String bookId) {
		Objectify ofy = ObjectifyService.begin();
		Key<ConsumerLoanSalesLead> sk = ObjectifyService.factory().stringToKey(bookId);
		ConsumerLoanSalesLead lead = ofy.find(sk);
		if (lead == null)
			return null;
		return leadStatusToBook(lead);
	}

	public static Book leadStatusToBook(ConsumerLoanSalesLead salesLead) {
		NumberFormat fm = NumberFormat.getInstance();
		DateFormat df = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT);
		fm.setMaximumFractionDigits(2);
		Double amount = salesLead.getRequestedAmount();
		if (salesLead.getApprovedAmount() != null)
			amount = salesLead.getApprovedAmount();
		String tenor = "";
		if (salesLead.getRequestedTenor() != null)
			tenor = salesLead.getRequestedTenor() + "month(s)";
		if (salesLead.getApprovedTenor() != 0)
			tenor = salesLead.getApprovedTenor() + " month(s)";

		Double salary = salesLead.getMonthlySalary();
		return new Book.Builder().id(salesLead.getLeadID()).ofykey(salesLead.getCurrentState().toString())
				.salary(salary == null ? "" : fm.format(salary)).amount(amount == null ? "" : fm.format(amount))
				.tenor(tenor).state(salesLead.getState()).name(df.format(salesLead.getDateCreated()))
				.company(df.format(salesLead.getDateUpdated())).build();

	}

	// [END read]

	// [START listbooks]
	@Override
	public List<Book> listRecentBookings() {
		Objectify ofy = ObjectifyService.begin();
		List<ConsumerLoanSalesLead> leads = ofy.query(ConsumerLoanSalesLead.class).order("-dateCreated").limit(5)
				.list();
		ArrayList<Book> result = new ArrayList<>(leads.size());
		for (ConsumerLoanSalesLead ld : leads)
			result.add(leadStatusToBook(ld));
		return result;
	}
	// [END listbooks]

}
// [END example]
