/**
 * 
 */
package com.fertiletech.addosser.server;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import com.fertiletech.addosser.client.LoanMktService;
import com.fertiletech.addosser.server.loanmktdata.ApplicationParameters;
import com.fertiletech.addosser.server.loanmktdata.ConsumerLoanComment;
import com.fertiletech.addosser.server.loanmktdata.ConsumerLoanSalesLead;
import com.fertiletech.addosser.server.loanmktdata.CustomerAcknowledgementIndicator;
import com.fertiletech.addosser.server.loanmktdata.GuarantorFormData;
import com.fertiletech.addosser.server.loanmktdata.LoanApplicationFormData;
import com.fertiletech.addosser.server.loanmktdata.LoanMktDAO;
import com.fertiletech.addosser.server.login.LoginHelper;
import com.fertiletech.addosser.server.orbitdata.OrbitConstants;
import com.fertiletech.addosser.server.orbitdata.OrbitDataFeed;
import com.fertiletech.addosser.server.orbitdata.OrbitRimLocations;
import com.fertiletech.addosser.server.orbitdata.OrbitStruct;
import com.fertiletech.addosser.server.scripts.JSPUtilities;
import com.fertiletech.addosser.server.scripts.PrintApplicationForm;
import com.fertiletech.addosser.server.scripts.downloads.GenericExcelDownload;
import com.fertiletech.addosser.server.tasks.TaskQueueHelper;
import com.fertiletech.addosser.shared.DTOConstants;
import com.fertiletech.addosser.shared.FormConstants;
import com.fertiletech.addosser.shared.FormHTMLDisplayBuilder;
import com.fertiletech.addosser.shared.LoginRoles;
import com.fertiletech.addosser.shared.exceptions.DuplicateEntitiesException;
import com.fertiletech.addosser.shared.exceptions.MissingEntitiesException;
import com.fertiletech.addosser.shared.table.TableMessage;
import com.fertiletech.addosser.shared.table.TableMessageHeader;
import com.fertiletech.addosser.shared.table.TableMessageHeader.TableMessageContent;
import com.fertiletech.addosser.shared.workstates.WorkflowStateInstance;
import com.github.gwtbootstrap.client.ui.Lead;
import com.google.appengine.api.datastore.GeoPt;
import com.google.appengine.api.datastore.QueryResultIterable;
import com.google.appengine.api.datastore.Text;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.Query;

/**
 * @author Segun Razaq Sobulo
 *
 */
@SuppressWarnings("serial")
public class LoanMktServiceImpl extends RemoteServiceServlet implements LoanMktService
{
	static
	{
		LoanMktDAO.registerClassesWithObjectify();
	}
	
	private static final Logger log =
        Logger.getLogger(LoanMktServiceImpl.class.getName());

	@Override
	public String fetchGenericExcelLink(List<TableMessage> data,
			TableMessageHeader header) {
		return GenericExcelDownload.getGenericExcelDownloadLink(data, header, getThreadLocalRequest());
	}	

	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.client.LoanMktService#updateParameters(java.util.HashMap)
	 */
	@Override
	public void updateParameters(HashMap<String, String> parameters) throws MissingEntitiesException {
		Key<ApplicationParameters> paramKey = ServiceImplUtilities.getLoanAppConfigurationKey();
		LoanMktDAO.updateApplicationParameters(paramKey, parameters);
		ServiceImplUtilities.logUserUpdate(log, paramKey, LoginHelper.getLoggedInUser(getThreadLocalRequest()));
		
	}

	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.client.LoanMktService#getParameters()
	 */
	@Override
	public HashMap<String, String> getParameters() {
		return LoanMktDAO.getLoanMktParams();
	}

	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.client.LoanMktService#getSaleLeads(java.util.Date, java.util.Date)
	 */
	@Override
	public List<TableMessage>[] getSaleLeads(Date startDate, Date endDate, boolean useOpsQueue) {
		//add rows
		Query<ConsumerLoanSalesLead> saleLeads = LoanMktDAO.getSalesLeadsByDate(startDate, endDate);

		if(!useOpsQueue)
		{
			List<TableMessage> leadTable = LoanMktDAO.getConsumerLeadsAsTable(saleLeads);
			List<TableMessage>[] resultWrapper = new List[1];
			resultWrapper[0] = leadTable;
			return resultWrapper;
		}
		
		ArrayList<TableMessage>[] result = new ArrayList[DTOConstants.ALL_IDX + 1];
		for(int i = 0; i < result.length; i++)
		{
			result[i] = new ArrayList<TableMessage>();
			//build header
			TableMessageHeader header = new TableMessageHeader(9);
			header.setText(0, "Name", TableMessageContent.TEXT);
			header.setText(1, "Email", TableMessageContent.TEXT);
			header.setText(2, "Phone", TableMessageContent.TEXT);
			header.setText(3, "Company", TableMessageContent.TEXT);
			header.setText(4, "Type", TableMessageContent.TEXT);
			header.setText(5, "Salary", TableMessageContent.NUMBER);
			header.setText(6, "Principal", TableMessageContent.NUMBER);
			header.setText(7, "Date", TableMessageContent.DATE);
			header.setText(8, "Next Step?", TableMessageContent.TEXT);
			result[i].add(header);
		}
		
		LoanMktDAO.popuplateLeadsTable(saleLeads, result);
		return result;
	}
	
	@Override
	public List<TableMessage> getSaleLeads(Date startDate, Date endDate) {
		//add rows
		Query<ConsumerLoanSalesLead> saleLeads = LoanMktDAO.getSalesLeadsByDate(startDate, endDate);
			List<TableMessage> leadTable = LoanMktDAO.getConsumerLeadsAsTable(saleLeads);
			return leadTable;
	}

	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.client.LoanMktService#getStoredLoanApplication()
	 */
	@Override
	public HashMap<String, String> getStoredLoanApplication(String loanKeyStr) {
		return LoanMktDAO.getLoanFormData(loanKeyStr, ObjectifyService.begin()).getFormData();		
	}
	
	@Override
	public HashMap<String, String> getLoanApplicationWithLoanID(
			String loanKeyStr) {
		Key<ConsumerLoanSalesLead> leadKey = ObjectifyService.factory().stringToKey(loanKeyStr);
		HashMap<String, String> result = LoanMktDAO.getLoanFormData(ObjectifyService.begin(), leadKey).getFormData();
		result.put(FormConstants.LOAN_ID_PARAM, loanKeyStr);
		result.put(FormConstants.LOAN_INT_ID_PARAM, String.valueOf(leadKey.getId()));
		return result;
	}	
	
	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.client.LoanMktService#storeLoanApplicationData(java.util.HashMap)
	 */
	@Override
	public void storeLoanApplicationData(HashMap<String, String> appData, String loanKeyStr, boolean submit) {
		Objectify ofy = ObjectifyService.begin();
		
		//actual
		String formEmail = appData.get(FormConstants.EMAIL_KEY);
		
		//expected
		//List<ConsumerLoanSalesLead> salesList = LoanMktDAO.getConsumerLoanLeadViaEmail(ofy, formEmail);
		String userEmail = null;
		//TODO remove commented out code
		/*if(LoginHelper.obtainedToken())
			userEmail = LoginHelper.getLoggedInUser(this.getThreadLocalRequest());
		else if(formEmail == null || formEmail.trim().length() == 0)
			throw new RuntimeException("Invalid Arguments. Can't create loan/form data");*/


		userEmail = LoginHelper.getLoggedInUser(this.getThreadLocalRequest());

		if(formEmail == null || formEmail.trim().length() == 0)
			throw new RuntimeException("Invalid Arguments. Can't create loan/form data");
		
		//int numOfAppsOnFile = salesList.size();
		
		if(userEmail != null && !userEmail.equals(formEmail))
		{
			LoginRoles role = LoginHelper.getRole(getThreadLocalRequest());
			if(role.equals(LoginRoles.ROLE_PUBLIC))
				throw( new RuntimeException("Security alert, email mismatch: [" + userEmail + "] vs [" + formEmail + "]"));
		}
		
		Key<ConsumerLoanSalesLead> formSalesLeadKey = ofy.getFactory().stringToKey(loanKeyStr).getParent();						
		LoanMktDAO.updateLoanLeadAndFormData(appData, formSalesLeadKey, submit, formEmail);
		
		//store map location
		//LoanMktDAO.saveMapAttachment(formSalesLeadKey, getThreadLocalRequest());
		//TaskQueueHelper
	}
	
	public String[] saveLoanApplicationData(HashMap<String, String> appData, String loanKeyStr, boolean submit) 
		throws MissingEntitiesException
	{
		Objectify ofy = ObjectifyService.begin();
		
		//actual
		String formEmail = appData.get(FormConstants.EMAIL_KEY);
		
		String userEmail = null;
		LoginRoles role = null;
		
		if(formEmail == null || formEmail.trim().length() == 0)
			throw new MissingEntitiesException("Invalid Arguments. Can't create loan/form data");
		
		//TODO remove commented out code
		/*if(LoginHelper.obtainedToken())
		{
			userEmail = LoginHelper.getLoggedInUser(this.getThreadLocalRequest());
			role = LoginHelper.getRole(getThreadLocalRequest());
			if(role.equals(LoginRoles.ROLE_PUBLIC) && !userEmail.equals(formEmail))
				throw( new MissingEntitiesException("Email mismatch: signed in as (" + userEmail + ") [VS.] form email(" + formEmail + ")"));		
		}*/

		userEmail = LoginHelper.getLoggedInUser(this.getThreadLocalRequest());
		if(userEmail != null && !userEmail.equals(formEmail))
		{
			role = LoginHelper.getRole(getThreadLocalRequest());
			if(role.equals(LoginRoles.ROLE_PUBLIC))
				throw( new RuntimeException("Security alert, email mismatch: [" + userEmail + "] vs [" + formEmail + "]"));
		}
		
		
		Key<ConsumerLoanSalesLead> formSalesLeadKey = ObjectifyService.factory().stringToKey(loanKeyStr).getParent();
		LoanApplicationFormData formData = LoanMktDAO.updateLoanLeadAndFormData(appData, formSalesLeadKey, submit, formEmail);
		String[] result = new String[2];
		result[DTOConstants.LOAN_IDX] = formData.getKey().getString();
		result[DTOConstants.LOAN_ID_IDX] = String.valueOf(formSalesLeadKey.getId());
		
		//map location
		//LoanMktDAO.saveMapAttachment(formSalesLeadKey, getThreadLocalRequest());
		return result;
	}	

	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.client.LoanMktService#getStoredGuarantorApplication()
	 */
	@Override
	public HashMap<String, String> getStoredGuarantorApplication(String gurantorKeyStr) {
		return LoanMktDAO.getLoanFormData(gurantorKeyStr, ObjectifyService.begin()).getFormData();		
	}

	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.client.LoanMktService#storeGurantorApplicationData(java.util.HashMap, boolean)
	 */
	@Override
	public String[] storeGurantorApplicationData(HashMap<String, String> appData,
			String guarantorID, boolean submmitted) {
		Objectify ofy = ObjectifyService.begin();
		Key<GuarantorFormData> gk = ObjectifyService.factory().stringToKey(guarantorID);
		String userEmail = LoginHelper.getLoggedInUser(this.getThreadLocalRequest());
		if(userEmail == null)
			userEmail = "update";
		LoanApplicationFormData formData = LoanMktDAO.updateGuarantorFormData(appData, gk, submmitted, userEmail);
		String[] result = new String[2];
		result[DTOConstants.LOAN_IDX] = formData.getKey().getString();
		Key<ConsumerLoanSalesLead> pk = gk.getParent();
		result[DTOConstants.LOAN_ID_IDX] = String.valueOf(gk.equals(GuarantorFormData.getFormKey(pk, true)));
		return result;
	}

	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.client.LoanMktService#loadFormDisplay(java.lang.String, com.fertiletech.addosser.shared.FormConstants.FormTypes)
	 */
	@Override
	public String loadFormDisplay(String sentFormID, boolean isFormId) {
		LoanApplicationFormData formDataObj = null;
		if(isFormId)
		{
			log.warning("Form object detected");
			formDataObj = LoanMktDAO.getLoanFormData(sentFormID, ObjectifyService.begin());
		}
		else
		{
			log.warning("lead key detected");
			Key<ConsumerLoanSalesLead> leadKey = ObjectifyService.factory().stringToKey(sentFormID);
			formDataObj = LoanMktDAO.getLoanFormData(ObjectifyService.begin(), leadKey);
		}

		if(formDataObj instanceof GuarantorFormData)
			return JSPUtilities.loadGuarantorDisplay(formDataObj, false).formEnds().toString();
		else
			return JSPUtilities.loadFormDisplay(formDataObj, false).formEnds().toString();
	}



	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.client.LoanMktService#getLoanID()
	 */
	@Override
	public String[] getLoanID() throws DuplicateEntitiesException {
		return LoanMktDAO.getOrCreateLoanID(LoginHelper.getLoggedInUser(this.getThreadLocalRequest()));
	}

	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.client.LoanMktService#loadSaleLeadComments(java.lang.String)
	 */
	@Override
	public String loadSaleLeadComments(String leadID) {

		return loadSaleLeadComments(leadID, false);
	}
	
	private String loadSaleLeadComments(String leadID, boolean respectShowPublic)
	{
		Key<ConsumerLoanSalesLead> key = ObjectifyService.factory().stringToKey(leadID);
		List<ConsumerLoanComment> commentList = LoanMktDAO.getLoanComments(ObjectifyService.begin(), key);
		FormHTMLDisplayBuilder result = new FormHTMLDisplayBuilder().formBegins();
		result.headerBegins().appendTextBody("Comments").headerEnds();
		for(ConsumerLoanComment c : commentList)
		{
			if(respectShowPublic && !c.isShowPublic())
				continue;
			DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
			DateFormat tf = DateFormat.getTimeInstance(DateFormat.MEDIUM);
			result.lineBegins().appendTextBody("User", c.getUser()).appendTextBody("Date", df.format(c.getDateUpdated()) + " " + tf.format(c.getDateUpdated())).lineEnds();
			result.lineBegins().appendTextBody(c.getComment().getValue()).lineEnds();
			result.sectionEnds();
		}
		return result.toString();
	}

	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.client.LoanMktService#saveLoanComment(java.lang.String, java.lang.String)
	 */
	@Override
	public void saveLoanComment(String loanKeyStr, String text, boolean showPublic) {
		Key<ConsumerLoanSalesLead> leadKey = ObjectifyService.factory().stringToKey(loanKeyStr);
		LoanMktDAO.createComment(new Text(text), showPublic, leadKey, LoginHelper.getLoggedInUser(getThreadLocalRequest()));
	}

	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.client.LoanMktService#updateOpsQueue(int)
	 */
	@Override
	public int updateOpsQueue(String loanKeyStr, int queueID, String[] args) {
		//Change loan state based on the ops queue it is in
		ConsumerLoanSalesLead salesLead = null;
		WorkflowStateInstance state = null;
		Key<ConsumerLoanSalesLead> leadKey = ObjectifyService.factory().stringToKey(loanKeyStr);
		
		if(queueID == Integer.MAX_VALUE)
			state = WorkflowStateInstance.LOAN_FUNDED;
		else if(queueID == Integer.MIN_VALUE)
			state = WorkflowStateInstance.LOAN_APPROVED_NO;
		else
		{
			for(WorkflowStateInstance st : WorkflowStateInstance.values())
			{	
				if(st.opsQueue() == null || st.opsQueue() < 0)
					continue;
					
				if(state != null && (st.opsQueue() == queueID))
					throw new RuntimeException("Unable to update loan application state, multiple queues map to the state. Queue ID: " + queueID + " State: " + st + " Prior State: " + state);
				else if(st.opsQueue() == queueID)
					state = st;
			}
		}

		
		if(state == null)
			throw new RuntimeException("Unable to map queue to any state. Queue ID: " + queueID);
		else
			salesLead = LoanMktDAO.changeLoanState(leadKey, state, LoginHelper.getLoggedInUser(getThreadLocalRequest()), args);
		
		return salesLead.getCurrentState().opsQueue();
	}

	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.client.LoanMktService#customerAck()
	 */
	@Override
	public boolean getCustomerAck(String leadKeyStr) {
		Objectify ofy = ObjectifyService.begin();
		Key<ConsumerLoanSalesLead> leadKey = ofy.getFactory().stringToKey(leadKeyStr);
		Key<CustomerAcknowledgementIndicator> ackKey = CustomerAcknowledgementIndicator.getAckKey(leadKey);
		CustomerAcknowledgementIndicator ack = ofy.find(ackKey);
		if(ack == null)
			return false;
		else
			return ack.isSubmmited();
	}

	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.client.LoanMktService#setCustomerAck(boolean)
	 */
	@Override
	public void setCustomerAck(String leadKeyStr) {
		Key<ConsumerLoanSalesLead> loanKey = ObjectifyService.factory().stringToKey(leadKeyStr);
		LoanMktDAO.createAck(loanKey);
		//map location
		//LoanMktDAO.saveMapAttachment(loanKey, getThreadLocalRequest());
	}

	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.client.LoanMktService#getAppointmentDate()
	 */
	@Override
	public Date getAppointmentDate(String leadID) throws MissingEntitiesException {
		Objectify ofy = ObjectifyService.begin();
		Key<ConsumerLoanSalesLead> leadKey = ofy.getFactory().stringToKey(leadID);
		Key<CustomerAcknowledgementIndicator> ackiKey = CustomerAcknowledgementIndicator.getAckKey(leadKey);
		CustomerAcknowledgementIndicator acki = ofy.find(ackiKey);
		if(acki == null)
			throw new MissingEntitiesException("You must first indicate your documents are ready before you can book an appointment");
		return acki.getAppointMentTime();
	}

	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.client.LoanMktService#setAppointmentDate(java.util.Date)
	 */
	@Override
	public void setAppointmentDate(String loanStr, Date meetingDate) throws MissingEntitiesException {
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			Key<ConsumerLoanSalesLead> leadKey = ofy.getFactory().stringToKey(loanStr);
			Key<CustomerAcknowledgementIndicator> ackiKey = CustomerAcknowledgementIndicator.getAckKey(leadKey);
			CustomerAcknowledgementIndicator acki = ofy.find(ackiKey);
			if(acki == null)
				throw new MissingEntitiesException("You must first indicate your documents are ready before you can book an appointment");
			acki.setAppointMentTime(meetingDate);
			ofy.put(acki);
			String[] comments = {"Appointment booked for " + meetingDate};
			TaskQueueHelper.scheduleCreateComment(comments, loanStr, LoginHelper.getLoggedInUser(getThreadLocalRequest()));
			ofy.getTxn().commit();
			//LoanMktDAO.saveMapAttachment(leadKey, getThreadLocalRequest());
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
	}

	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.client.LoanMktService#okToPrint()
	 */
	@Override
	public Boolean okToPrint(String loanID) {
		Objectify ofy = ObjectifyService.begin();
		Key<ConsumerLoanSalesLead> leadKey = ofy.getFactory().stringToKey(loanID);
		ConsumerLoanSalesLead salesLead = ofy.get(leadKey);
		Key<CustomerAcknowledgementIndicator> ackKey = CustomerAcknowledgementIndicator.getAckKey(leadKey);
		CustomerAcknowledgementIndicator ack = ObjectifyService.begin().find(ackKey);
		if(ack == null || !ack.isSubmmited())
			return null;
		if( salesLead.getCurrentState().ordinal() < WorkflowStateInstance.VERIFICATION.ordinal())
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.client.LoanMktService#getNewLoanIDs()
	 */
	@Override
	public HashMap<String, String> getNewLoanIDs() {
		HashMap<String, String> result = new HashMap<String, String>();
		QueryResultIterable<Key<LoanApplicationFormData>> loanKeys = LoanMktDAO.getNewLoanKeys();
		for(Key<LoanApplicationFormData> key : loanKeys)
			result.put(ObjectifyService.factory().keyToString(key), key.getParent().getParent().getName());
		return result;
	}

	@Override
	public String getAllSalesLeadAsCSV() {
		Set<Key<ConsumerLoanSalesLead>> salesLeadKeys = LoanMktDAO.getAllLoanApps();
		Map<Key<ConsumerLoanSalesLead>, ConsumerLoanSalesLead> leads = ObjectifyService.begin().get(salesLeadKeys);
		StringBuilder result = new StringBuilder(LoanMktDAO.getCSVHeader());
		for(ConsumerLoanSalesLead sl : leads.values())
			LoanMktDAO.addLeadAsCSVLine(result, sl);
		return result.toString();
	}

	@Override
	public String[] startLoanApplication(HashMap<String, String> appData)
			throws DuplicateEntitiesException {
		String email = appData.get(FormConstants.EMAIL_KEY);
		ConsumerLoanSalesLead salesLead= new ConsumerLoanSalesLead(email);
		LoanApplicationFormData data = LoanMktDAO.createLoanLeadAndFormData(salesLead, appData);
		String[] result = new String[2];
		result[0] = data.getParentKey().getString();
		result[1] = data.getKey().getString();
		//LoanMktDAO.saveMapAttachment(salesLead.getKey(), getThreadLocalRequest());
		return result;
	}

	@Override
	public TableMessage getLoanState(String loanID) {
		Key<ConsumerLoanSalesLead> leadKey = ObjectifyService.factory().stringToKey(loanID);
		return getLoanState(leadKey);
	}
	
	public static TableMessage getLoanState(Key<ConsumerLoanSalesLead> leadKey)
	{
		Key<LoanApplicationFormData> formKey = LoanApplicationFormData.getFormKey(leadKey);
		Key<GuarantorFormData> guarantor1key = GuarantorFormData.getFormKey(leadKey, true);
		Key<GuarantorFormData> guarantor2key = GuarantorFormData.getFormKey(leadKey, false);
		Objectify ofy = ObjectifyService.begin();
		LoanApplicationFormData formData = ofy.get(formKey);
		Map<Key<GuarantorFormData>, GuarantorFormData> guarantors = ofy.get(guarantor1key, guarantor2key);
		TableMessage result = new TableMessage(3, 3, 3);
		result.setMessageId(leadKey.getString());
		result.setText(DTOConstants.LOAN_KEY_IDX, formKey.getString());
		result.setNumber(DTOConstants.LOAN_KEY_IDX, formData.isSubmmited()?1:0);
		result.setDate(DTOConstants.LOAN_KEY_IDX, formData.getTimeStamp());
		
		result.setText(DTOConstants.GUARANTOR_ONE_KEY_IDX, guarantor1key.getString());
		result.setText(DTOConstants.GUARANTOR_TW0_KEY_IDX, guarantor2key.getString());
		result.setNumber(DTOConstants.GUARANTOR_ONE_KEY_IDX, -2);
		result.setDate(DTOConstants.GUARANTOR_ONE_KEY_IDX, new Date());
		result.setNumber(DTOConstants.GUARANTOR_TW0_KEY_IDX, -2);
		result.setDate(DTOConstants.GUARANTOR_TW0_KEY_IDX, new Date());

		
		GuarantorFormData guarantor = guarantors.get(guarantor1key); 
		if( guarantor != null)
		{	
			result.setNumber(DTOConstants.GUARANTOR_ONE_KEY_IDX, guarantor.isSubmmited()?1:0);
			result.setDate(DTOConstants.GUARANTOR_ONE_KEY_IDX, guarantor.getTimeStamp());
		}
		guarantor = guarantors.get(guarantor2key);
		if( guarantor != null)
		{	
			result.setNumber(DTOConstants.GUARANTOR_TW0_KEY_IDX, guarantor.isSubmmited()?1:0);
			result.setDate(DTOConstants.GUARANTOR_TW0_KEY_IDX, guarantor.getTimeStamp());
		}
		
		return result;		
	}
	
	@Override
	public String getApplicationFormDownloadLink(String formID) {
		return PrintApplicationForm.getApplicationFormLink(formID, getThreadLocalRequest());
	}

	@Override
	public String getGuarantorFormDownloadLink(String formID) {
		return PrintApplicationForm.getGuarantorFormLink(formID, getThreadLocalRequest());
	}

	@Override
	public String getRequirementsChecklistLink(String loanID) {
		return PrintApplicationForm.getCheckListFormLink(loanID, getThreadLocalRequest());
	}

	@Override
	public TableMessage getLoanState(long loanID) {
		Key<ConsumerLoanSalesLead> leadKey = getLoanKeyFromApplicationID(loanID);
		if(leadKey == null) return null;
		return getLoanState(leadKey);
	}
	
	public static Key<ConsumerLoanSalesLead> getLoanKeyFromApplicationID(long id)
	{
		Objectify ofy = ObjectifyService.begin();
		List<Key<ConsumerLoanSalesLead>> result = ofy.query(ConsumerLoanSalesLead.class).filter("leadID =", id).listKeys();
		if(result.size() == 0)
			return null;
		else if(result.size() == 1)
			return result.get(0);
		else
		{
			String message = "";
			for(Key<ConsumerLoanSalesLead> k : result)
				message += k.toString() + " [and] ";
			throw new RuntimeException("Multiple keys found for " + id + " keys are: " + message);
		}
	}

	@Override
	public HashMap<WorkflowStateInstance, Integer> getLeadAggregates(Date startDate, Date endDate) 
	{
		return LoanMktDAO.getLeadAggregates(startDate, endDate);

	}

	@Override
	public List<TableMessage> getMapAttachments(Date startDate, Date endDate) {
	
		return LoanMktDAO.getMapAttachments(startDate, endDate);
	}

	//private final static String FEED_ID = "BASIC";
	@Override
	public HashMap<String, Integer> getOrbitTenors(String feedID) {
		Key<OrbitDataFeed> ok = OrbitDataFeed.getKey(feedID);
		OrbitDataFeed orb = ObjectifyService.begin().get(ok);
		HashMap<String, String[]> rawdata = orb.getorbitData();
		String[] tenorValues = rawdata.get(OrbitConstants.TENOR_HEADER);
		HashMap<String, Integer> tenorStats= new HashMap<>();
		for(String tenor : tenorValues)
		{
			String tenorKey = tenor + " months";
			Integer tenorCount = tenorStats.get(tenorKey);
			if(tenorCount == null)
				tenorCount = 0;
			tenorStats.put(tenorKey, tenorCount + 1);
		}
		return tenorStats;
	}
	
	@Override
	public List<TableMessage> getOrbitMap(String feedID) 
	{	
		log.warning("Commencing orbit mapping");
		Key<OrbitDataFeed> ok = OrbitDataFeed.getKey(feedID);
		Key<OrbitRimLocations> mk = OrbitRimLocations.getKey();
		Objectify ofy = ObjectifyService.begin();
		OrbitDataFeed dataFeed = ofy.get(ok);
		OrbitRimLocations locationFeed = ofy.get(mk);
		log.warning("Fetched orbit objects succesfullly from database");
		log.warning("Orbit raw data counts: " + Arrays.toString(locationFeed.getRawDataCounts()));
		log.warning("Orbit query status counts: " + Arrays.toString(locationFeed.getQueryStatusCounts()));
		
		ArrayList<TableMessage> attachments = new ArrayList<TableMessage>();
		TableMessage header = new TableMessage(1, 3, 0);
		attachments.add(header);
		// initialize google maps
		int loansCount, addressCount, queryCount;
		loansCount = addressCount = queryCount = 0;
		String addresssSamples = "";
		
		HashMap<String, String[]> rawData = dataFeed.getorbitData();
		log.warning("orbit raw data");
		HashMap<String, OrbitStruct> rimDataMap = OrbitStruct.getRimOrbitStructMap(rawData);
		log.warning("orbit rim data map");
		HashMap<String, String> addressMap = locationFeed.getRimAddresses();
		log.warning("orbit address map");
		HashMap<String, GeoPt> geoMap = locationFeed.getRimMap();
		log.warning("geo coded orbit objects succesfullly from database");
		
		for (String rim : geoMap.keySet()) {
			loansCount += 1;

			String address = addressMap.get(rim);
			if (address == null || address.length() == 0)
				throw new IllegalStateException("Address not found for rim id: " + rim);
			
			addressCount += 1;

			queryCount += 1;
			
			//debugging
			if(rim == null)
				log.severe("orbit rim null");
			
			OrbitStruct rimData = rimDataMap.get(rim);
			
			//debugging
			if(rimData == null)
				log.severe("orbit rim data null for rim: " + rim);
			
			StringBuilder desc = new StringBuilder();
			desc.append("<div style='width:180px;height:100px;color:#7b04a2;font-size:smaller'>")
					.append("<i>Address: ").append(address).append("</i>")
					.append("<br/>Compamy: [").append(rimData.company)
					.append("]<br/>Amount: [").append(rimData.amount)
					.append("]<br/>Tenor: [").append(rimData.tenure).append("]<br/>Name: [")
					.append(rimData.name).append("]<br/>Date Granted: [").append(rimData.dateGranted).append("]</div>");

			GeoPt gp = geoMap.get(rim);
			if(gp == null)
				throw new IllegalStateException("No mapping found for: " + rim);
			
			TableMessage m = new TableMessage(2, 2, 0);
			if(loansCount < 5)
				log.warning("orbit mapping loop debug6: " + loansCount + " rimData: " + rimData);
			m.setText(DTOConstants.CATEGORY_IDX, rimData.status);
			m.setText(DTOConstants.DETAIL_IDX, desc.toString());
			m.setNumber(DTOConstants.LAT_IDX, gp.getLatitude());
			m.setNumber(DTOConstants.LNG_IDX, gp.getLongitude());
			if(loansCount < 5)
				log.warning("orbit mapping loop debug7");
			m.setMessageId(rim);
			attachments.add(m);
			if(loansCount < 5)
				log.warning("orbit mapping result added: " + loansCount);

		}
		log.warning("orbit mapping loop completed for: " + loansCount);
		
		//debug header
		header.setNumber(0, loansCount);
		header.setNumber(1, geoMap.size());
		header.setNumber(2, addressMap.size());
		header.setText(0, addresssSamples);
		log.warning("Returning orbit mapping data table of size: " + attachments.size());
		return attachments;
	}
	
	@Override
	public List<TableMessage> getCurrentOrbitTable(String feedId)
	{
		Key<OrbitDataFeed> ok = OrbitDataFeed.getKey(feedId);
		Key<OrbitRimLocations> mk = OrbitRimLocations.getKey();
		Objectify ofy = ObjectifyService.begin();
		OrbitDataFeed dataFeed = ofy.get(ok);
		List<TableMessage> tableFeed = OrbitStruct.getFeedAsTable(dataFeed.getorbitData());
		TableMessageHeader header = (TableMessageHeader) tableFeed.get(0);
		header.setMessageId("Feed Name: " + feedId + " Date Uploaded: " + dataFeed.getDateCreated());
		return tableFeed;
	}

	@Override
	public List<String[]> getOrbitAmounts(String feedId) {
		Key<OrbitDataFeed> ok = OrbitDataFeed.getKey(feedId);
		OrbitDataFeed orb = ObjectifyService.begin().get(ok);
		HashMap<String, String[]> rawdata = orb.getorbitData();
		String[] amountValues = rawdata.get(OrbitConstants.AMOUNT_HEADER);
		String[] dateGranted = rawdata.get(OrbitConstants.DATE_CGRANTED_HEADER);
 		ArrayList<String[]> result = new ArrayList<>();
 		result.add(amountValues);
 		result.add(dateGranted);
 		return result;
	}

	@Override
	public List<String[]> getSangSalary(Date start, Date end) {
		Iterable<ConsumerLoanSalesLead> salesLeads = LoanMktDAO.getSalesLeadsByDate(start, end);
		ArrayList<String> salary = new ArrayList<>();
		ArrayList<String> dates = new ArrayList<>();
		for(ConsumerLoanSalesLead lead : salesLeads)
		{
			Double number = lead.getMonthlySalary();
			if(number == null)
				continue;
			
			
			String amount = OrbitConstants.NUMBER_FORMATTER.format(number);
			String dt = OrbitConstants.DATE_FORMATTER.format(lead.getDateUpdated());
			salary.add(amount);
			dates.add(dt);
			}
		String[] monthlySalaries = arrayListToArray(salary);
		String[] dateModified =  arrayListToArray(dates);
		ArrayList<String[]> result = new ArrayList<>();
		result.add(monthlySalaries);
		result.add(dateModified);
		
		return result;
	}
	
	private String[] arrayListToArray(ArrayList<String> source)
	{
		String[] target = new String[source.size()];
		for(int i = 0; i < target.length; i++)
			target[i] = source.get(i);
		return target;
	}

	@Override
	public void updateOpsQueue(String leadKeyStr, String appStatus, String comments) {
		Key<ConsumerLoanSalesLead> leadKey = ObjectifyService.factory().stringToKey(leadKeyStr);
		LoanMktDAO.changeLoanState(leadKey, WorkflowStateInstance.valueOf(appStatus), LoginHelper.getLoggedInUser(getThreadLocalRequest()), comments);

		
	}

	@Override
	public String[] getSangDisplayStatus(String leadKeyStr) {
		String[] result = new String[2];
		Key<ConsumerLoanSalesLead> leadKey = ObjectifyService.factory().stringToKey(leadKeyStr);
		ConsumerLoanSalesLead leadSales = ObjectifyService.begin().get(leadKey);
		result[0] = leadSales.getCurrentState().toString();
		result[1] = leadSales.getCurrentMessage();
		return result;
		
	}
}
