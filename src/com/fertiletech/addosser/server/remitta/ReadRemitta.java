package com.fertiletech.addosser.server.remitta;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Logger;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.fertiletech.addosser.server.login.LoginHelper;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class ReadRemitta extends HttpServlet {
	private static final Logger log = Logger.getLogger(ReadRemitta.class.getName());

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {

		ServletOutputStream out = res.getOutputStream();
		res.setContentType("text/html");

		String type = req.getParameter("type");
		if (type == null) {
			out.println("<b><font color='red'>Please specify a type</font></b>");
			return;
		}
		out.println("<b>setup starting</b><br/>");
		if (type.equals("json")) {
			URL url = new URL("http://api.icndb.com/jokes/random");
			
			JsonParser json= new JsonParser();
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
			StringBuffer result = new StringBuffer();
			String line;

			while ((line = reader.readLine()) != null) {
			  result.append(line);
			}
			reader.close();
			//{ "type": "success", "value": { "id": 483, "joke": "Bill Gates thinks he's Chuck Norris. Chuck Norris actually laughed. Once.", "categories": [] } }
			JsonElement jelem = json.parse(result.toString());
			JsonObject jobj = jelem.getAsJsonObject();
			String jt = jobj.get("type").getAsString();
			JsonObject val = jobj.getAsJsonObject("value");
		    String joke = val.get("joke").getAsString();
					
			
		

			out.println("<b>Type:</b> " + jt + "<br/>" + joke + "<hr/>");
			log.warning(result + " triggered by " + LoginHelper.getLoggedInUser(req));
		}
		else if(type.equals("postend"))
		{
			int id = 100;
			URL url = new URL("https://ftbsend.appspot.com/_ah/api/echo/v1/echo");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			// Enable output for the connection.
			conn.setDoOutput(true);
			conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			conn.setRequestProperty("Accept", "application/json");
			// Set HTTP request method.
			conn.setRequestMethod("POST");

			// Create JSON request.
			JSONObject jsonObj =
			    new JSONObject().put("message", "hello gidi").put("n", 5);

			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
			writer.write(jsonObj.toString());
			writer.close();

			int respCode = conn.getResponseCode(); // New items get NOT_FOUND on PUT
			if (respCode == HttpURLConnection.HTTP_OK || respCode == HttpURLConnection.HTTP_NOT_FOUND) {
			  req.setAttribute("error", "");
			  StringBuffer response = new StringBuffer();
			  String line;

			  // Read input data stream.
			  BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			  while ((line = reader.readLine()) != null) {
			    response.append(line);
			  }
			  reader.close();
			  req.setAttribute("response", response.toString());
			  out.println("Response--> " + response.toString());
			} else {
				out.println("An error occured: " + respCode);
			  req.setAttribute("error", conn.getResponseCode() + " " + conn.getResponseMessage());
			}
		}
		else if(type.equals("postend2"))
		{
			int id = 100;
			URL url = new URL("https://ftbsend.appspot.com/_ah/api/echo/v1/echo");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			// Enable output for the connection.
			conn.setDoOutput(true);
			conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			conn.setRequestProperty("Accept", "application/json");
			// Set HTTP request method.
			conn.setRequestMethod("PUT");

			// Create JSON request.
			JSONObject jsonObj =
			    new JSONObject().put("message", "hello gidi");

			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
			writer.write(jsonObj.toString());
			writer.close();

			int respCode = conn.getResponseCode(); // New items get NOT_FOUND on PUT
			if (respCode == HttpURLConnection.HTTP_OK || respCode == HttpURLConnection.HTTP_NOT_FOUND) {
			  req.setAttribute("error", "");
			  StringBuffer response = new StringBuffer();
			  String line;

			  // Read input data stream.
			  BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			  while ((line = reader.readLine()) != null) {
			    response.append(line);
			  }
			  reader.close();
			  req.setAttribute("response", response.toString());
			  out.println("Response--> " + response.toString());
			} else {
				out.println("An error occured: " + respCode);
			  req.setAttribute("error", conn.getResponseCode() + " " + conn.getResponseMessage());
			}
		}
		else if(type.equals("postjson"))
		{
			int id = 100;
			URL url = new URL("http://jsonplaceholder.typicode.com/posts/" + id);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			// Enable output for the connection.
			conn.setDoOutput(true);
			conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			conn.setRequestProperty("Accept", "application/json");
			// Set HTTP request method.
			conn.setRequestMethod("PUT");

			// Create JSON request.
			JSONObject jsonObj =
			    new JSONObject().put("userId", 10).put("id", id).put("title", "coming home").put("body", "it's the big apple, america yo");

			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
			writer.write(jsonObj.toString());
			writer.close();

			int respCode = conn.getResponseCode(); // New items get NOT_FOUND on PUT
			if (respCode == HttpURLConnection.HTTP_OK || respCode == HttpURLConnection.HTTP_NOT_FOUND) {
			  req.setAttribute("error", "");
			  StringBuffer response = new StringBuffer();
			  String line;

			  // Read input data stream.
			  BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			  while ((line = reader.readLine()) != null) {
			    response.append(line);
			  }
			  reader.close();
			  req.setAttribute("response", response.toString());
			  out.println("Response--> " + response.toString());
			} else {
				out.println("An error occured: " + respCode);
			  req.setAttribute("error", conn.getResponseCode() + " " + conn.getResponseMessage());
			}
		}

	}
}
