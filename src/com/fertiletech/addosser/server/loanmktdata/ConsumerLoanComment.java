/**
 * 
 */
package com.fertiletech.addosser.server.loanmktdata;

import java.util.Date;

import javax.persistence.Id;
import javax.persistence.PrePersist;

import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Unindexed;

/**
 * @author Segun Razaq Sobulo
 *
 */
@Unindexed
public class ConsumerLoanComment implements Comparable<ConsumerLoanComment>{
	@Id
	Long key;
	
	Text comment;
	@Indexed Date dateUpdated;
	@Indexed Key<ConsumerLoanSalesLead> loanKey;
	boolean showPublic;
	String user;
	
	/**
	 * 
	 */
	public ConsumerLoanComment() {}
	
	/**
	 * @param key
	 * @param comment
	 * @param loanKey
	 * @param showPublic
	 * @param user
	 */
	ConsumerLoanComment(Text comment,
			Key<ConsumerLoanSalesLead> loanKey, boolean showPublic, String user) 
	{
		this.comment = comment;
		this.loanKey = loanKey;
		this.showPublic = showPublic;
		this.user = user;
	}
	
	public Text getComment() {
		return comment;
	}
	
	void setComment(Text comment) {
		this.comment = comment;
	}
	
	public boolean isShowPublic() {
		return showPublic;
	}
	
	void setShowPublic(boolean showPublic) {
		this.showPublic = showPublic;
	}
	
	public String getUser() {
		return user;
	}
	
	void setUser(String user) {
		this.user = user;
	}
	
	public Date getDateUpdated() {
		return dateUpdated;
	}
	
	@PrePersist
	void changeDateUpdated() {
		this.dateUpdated = new Date();
	}
	
	public Key<ConsumerLoanComment> getKey()
	{
		return new Key<ConsumerLoanComment>(loanKey, ConsumerLoanComment.class, key);
	}
	
	public Key<ConsumerLoanSalesLead> getLoanKey()
	{
		return loanKey;
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(ConsumerLoanComment o) {
		int parentKeyComparison = loanKey.compareTo(o.loanKey);
		if(parentKeyComparison == 0)
			return this.dateUpdated.compareTo(o.dateUpdated);
		else
			return parentKeyComparison;
	}
}
