/**
 * 
 */
package com.fertiletech.addosser.server.loanmktdata;

import java.util.HashMap;

import com.googlecode.objectify.Key;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class GuarantorFormData extends LoanApplicationFormData{
	
	public final static String FIRST_GUARANTOR_KEY = "guarantor";
	public final static String SECOND_GUARANTOR_KEY = "guarantor2";
	
	GuarantorFormData(){}
	
	GuarantorFormData(Key<ConsumerLoanSalesLead> pKey, HashMap<String, String> data, boolean isFirstGuarantor)
	{
		super(pKey, data, isFirstGuarantor?FIRST_GUARANTOR_KEY:SECOND_GUARANTOR_KEY);
	}
	
	public static Key<GuarantorFormData> getFormKey(Key<ConsumerLoanSalesLead> leadKey, boolean isFirst)
	{
		return getKey(leadKey, GuarantorFormData.class, isFirst?FIRST_GUARANTOR_KEY:SECOND_GUARANTOR_KEY);
	}
}
