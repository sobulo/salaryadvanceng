/**
 * 
 */
package com.fertiletech.addosser.server.loanmktdata;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;

/**
 * @author Segun Razaq Sobulo
 *
 */
public final class EntityConstants {
	public final static String MARKETING_PARAM_ID = "loan-marketing-params";
	//TODO usage of controller constant is wrong,sendgrid and default mail use same ids but different to below. also currently no sms controllers. users specify controler ids in enttity setup hence use of below is wrong and will yield failures when trying to retrieve controller objects with this id
    public final static String EMAIL_CONTROLLER_ID = "EMAIL Controller";    
    public final static String SMS_CONTROLLER_ID = "SMS Controller";
    
	//number and date formats
	public final static NumberFormat NUMBER_FORMAT = NumberFormat.getInstance();
	public final static DateFormat DATE_FORMAT = new SimpleDateFormat("d MMM yyyy");
	static
	{
		NUMBER_FORMAT.setMaximumFractionDigits(2);
		NUMBER_FORMAT.setMinimumIntegerDigits(1);
	}
	
	public final static NumberFormat INT_FORMAT = NumberFormat.getIntegerInstance();
	
    
	private final static int MAX_TENOR = 12;
	private final static double INTEREST = 0.05;
	private final static double MAX_LOAN_AMT =4000000;
	private final static double MIN_LOAN_AMT = 100000;
	
    public static String getMinLoanAmount()
    {
    	return NUMBER_FORMAT.format(MIN_LOAN_AMT);
    }
    
    public static String getMaxLoanAmount( double salaryStr)
    {
    	return NUMBER_FORMAT.format(MIN_LOAN_AMT);
    }
    
    public static String getEligibleLoanAmount( String salaryStr)
    {
    	try
		{
			double salaryLoan = Double.valueOf(salaryStr);
			salaryLoan = salaryLoan / 3; //maximum monthly repayment
			Double principal = salaryLoan / (INTEREST + 1.0/MAX_TENOR);
			String stringTotal = NUMBER_FORMAT.format(principal);
			if(principal < MIN_LOAN_AMT)
				return "You're eligible for a maximum loan of " + stringTotal + " Naira. This amount is less than what we normally disburse but you may "
						+ " complete your application then visit any of the branches at our parent company, Addosser MFB (<a href='www.addosser.com'>www.addosser.com</a>) to secure a microfinance loan size of this amount";			
			principal = Math.min(principal, MAX_LOAN_AMT);
			stringTotal = NUMBER_FORMAT.format(principal);

			return "You are eligible to borrow up to " + stringTotal + " Naira.";
		}catch(Exception e)
		{
			return salaryStr + " is not a number";
		}	
    }
}
