/**
 * 
 */
package com.fertiletech.addosser.server.loanmktdata;

import java.util.Date;
import java.util.HashMap;

import javax.persistence.Id;
import javax.persistence.PrePersist;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Serialized;
import com.googlecode.objectify.annotation.Unindexed;

/**
 * @author Segun Razaq Sobulo
 *
 */

@Cached
@Unindexed
public class LoanApplicationFormData {
	/**
	 * TODO
	 * key was initially an auto-generated long id. This led to a lot of unnecassary queries in codebase
	 * These should be optimized later for efficiency but until volumes kick up, we live with old queries
	 */
	@Id
	String key;
	
	@Indexed boolean submitted;
	@Indexed Date lastModified;
	
	@Parent Key<ConsumerLoanSalesLead> parentKey;
	
	@Serialized
	HashMap<String, String> formData;
	
	private final static String FORM_KEY = "form";
	
	//empty constructor needed for objectify
	LoanApplicationFormData() {}
	
	public LoanApplicationFormData(Key<ConsumerLoanSalesLead> pKey, HashMap<String, String> data)
	{
		this(pKey, data, FORM_KEY);
	}
	
	protected LoanApplicationFormData(Key<ConsumerLoanSalesLead> pKey, HashMap<String, String> data, String formKey)
	{
		this.parentKey = pKey;
		this.formData = data;
		this.submitted = false;
		this.key = formKey;
	}

	public HashMap<String, String> getFormData() {
		return formData;
	}

	void setFormData(HashMap<String, String> formData, boolean userSubmmited) {
		this.formData = formData;
		this.submitted = userSubmmited;
	}
	
	public static Key<LoanApplicationFormData> getFormKey(Key<ConsumerLoanSalesLead> leadKey)
	{
		return getKey(leadKey, LoanApplicationFormData.class, FORM_KEY);
	}
	
	protected final static <T extends LoanApplicationFormData> Key<T>  getKey(Key<ConsumerLoanSalesLead> leadKey, Class<T> classType, String keyName)
	{
		return new Key<T>(leadKey, classType, keyName);
	}
	
	
	public Key<? extends LoanApplicationFormData> getObjectifyKey()
	{
		return getKey(parentKey, this.getClass(), getKeyString());
	}
	
	public Key<? extends LoanApplicationFormData> getKey()
	{
		return getFormKey(parentKey);
	}
	
	protected String getKeyString()
	{
		return key;
	}
	
	public Key<ConsumerLoanSalesLead> getParentKey() {
		return parentKey;
	}

	public boolean isSubmmited()
	{
		return submitted;
	}
	
	@PrePersist
	void lastModification()
	{
		lastModified = new Date();
	}
	
	public Date getTimeStamp()
	{
		return lastModified;
	}
}

