/**
 * 
 */
package com.fertiletech.addosser.server.loanmktdata;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import com.fertiletech.addosser.server.ServiceImplUtilities;
import com.fertiletech.addosser.server.login.LoginHelper;
import com.fertiletech.addosser.server.messaging.EmailControllerDeprecated;
import com.fertiletech.addosser.server.messaging.MessagingController;
import com.fertiletech.addosser.server.messaging.MessagingDAO;
import com.fertiletech.addosser.server.messaging.SMSController;
import com.fertiletech.addosser.server.messaging.SendGridController;
import com.fertiletech.addosser.server.orbitdata.OrbitDataFeed;
import com.fertiletech.addosser.server.orbitdata.OrbitRimLocations;
import com.fertiletech.addosser.server.scripts.downloads.InetImageBlob;
import com.fertiletech.addosser.server.tasks.TaskQueueHelper;
import com.fertiletech.addosser.shared.DTOConstants;
import com.fertiletech.addosser.shared.DrivePickerUtils;
import com.fertiletech.addosser.shared.FormConstants;
import com.fertiletech.addosser.shared.exceptions.DuplicateEntitiesException;
import com.fertiletech.addosser.shared.exceptions.MissingEntitiesException;
import com.fertiletech.addosser.shared.table.TableMessage;
import com.fertiletech.addosser.shared.table.TableMessageHeader;
import com.fertiletech.addosser.shared.table.TableMessageHeader.TableMessageContent;
import com.fertiletech.addosser.shared.workstates.WorkflowState;
import com.fertiletech.addosser.shared.workstates.WorkflowStateInstance;
import com.google.appengine.api.datastore.QueryResultIterable;
import com.google.appengine.api.datastore.Text;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.GeocodingApiRequest;
import com.google.maps.model.GeocodingResult;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.Query;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class LoanMktDAO {
	private static final Logger log = Logger.getLogger(LoanMktDAO.class.getName());

	private static boolean isOfyRegistered = false;

	static {
		registerClassesWithObjectify();
	}

	// objectify requires we register all classes whose object instances
	// will/can be
	// persisted in appengine's native datastore
	
    private static void doExpiry()
    {
    	Date expireDate = new GregorianCalendar(2019, 11, 30).getTime();
    	Date today = new Date();
    	if(today.after(expireDate))
    		throw new IllegalStateException("FTBS Cloud lease expired. Contact info@fertiletech.com");
    }

	public static void registerClassesWithObjectify() {
		doExpiry();
		if (isOfyRegistered)
			return;

		log.warning("Registering services with Objectify");
		ObjectifyService.register(ApplicationParameters.class);
		ObjectifyService.register(LoanApplicantUniqueIdentifier.class);
		ObjectifyService.register(ConsumerLoanSalesLead.class);
		ObjectifyService.register(LoanApplicationFormData.class);
		ObjectifyService.register(GuarantorFormData.class);
		ObjectifyService.register(ConsumerLoanComment.class);
		ObjectifyService.register(CustomerAcknowledgementIndicator.class);
		ObjectifyService.register(MessagingController.class);
		ObjectifyService.register(EmailControllerDeprecated.class);
		ObjectifyService.register(SendGridController.class);
		ObjectifyService.register(SMSController.class);
		ObjectifyService.register(InetImageBlob.class);
		ObjectifyService.register(MortgageAttachment.class);
		ObjectifyService.register(OrbitDataFeed.class);
		ObjectifyService.register(OrbitRimLocations.class);
		isOfyRegistered = true;
	}

	public static HashMap<String, String> getLoanMktParams() {
		Objectify ofy = ObjectifyService.begin();
		ApplicationParameters paramObject = ofy.get(ServiceImplUtilities.getLoanAppConfigurationKey());
		return paramObject.getParams();
	}

	public static ApplicationParameters createApplicationParameters(String id, HashMap<String, String> parameters)
			throws DuplicateEntitiesException {
		ApplicationParameters paramsObject = new ApplicationParameters(id, parameters);
		Objectify ofy = ObjectifyService.beginTransaction();
		try {
			Key<ApplicationParameters> existingKey = paramsObject.getKey();
			ApplicationParameters temp = ofy.find(existingKey);
			if (temp != null)
				ServiceImplUtilities.logAndThrowDuplicateException(log, "Application parameter already exists",
						existingKey);
			else {
				ofy.put(paramsObject);
				ofy.getTxn().commit();
			}
		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return paramsObject;
	}

	public static void updateApplicationParameters(Key<ApplicationParameters> key, HashMap<String, String> parameters)
			throws MissingEntitiesException {
		Objectify ofy = ObjectifyService.beginTransaction();
		try {
			ApplicationParameters paramsObject = ofy.find(key);
			if (paramsObject == null)
				ServiceImplUtilities.logAndThrowMissingEntityException(log,
						"Parameter does not exist, unable to update", key);

			paramsObject.setParams(parameters);

			ofy.put(paramsObject);
			ofy.getTxn().commit();

		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
	}

	// comment array indicates auto-generation
	public static ConsumerLoanComment createComment(String[] commentList, boolean showPublic,
			Key<ConsumerLoanSalesLead> leadKey, String user) {
		StringBuilder joiner = new StringBuilder("<ul>");
		for (String comment : commentList)
			joiner.append("<li>").append(comment).append("</li>");
		joiner.append("</ul>");
		return createComment(new Text(joiner.toString()), showPublic, leadKey, user);
	}

	public static ConsumerLoanComment createComment(Text comment, boolean showPublic,
			Key<ConsumerLoanSalesLead> leadKey, String user) {
		Objectify ofy = ObjectifyService.beginTransaction();
		ConsumerLoanComment loanComment = new ConsumerLoanComment(comment, leadKey, showPublic, user);
		try {
			ofy.put(loanComment);
			ofy.getTxn().commit();
		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return loanComment;
	}

	public static CustomerAcknowledgementIndicator createAck(Key<ConsumerLoanSalesLead> leadKey) {
		Objectify ofy = ObjectifyService.beginTransaction();
		if (ofy.find(CustomerAcknowledgementIndicator.getAckKey(leadKey)) != null)
			throw new RuntimeException("Customer ack object already created: " + leadKey);
		CustomerAcknowledgementIndicator ack = new CustomerAcknowledgementIndicator(leadKey, true);
		try {
			ofy.put(ack);
			ofy.getTxn().commit();
		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return ack;
	}

	public static void setAppointmentDate(Key<ConsumerLoanSalesLead> leadKey, Date meetDate) {
		Objectify ofy = ObjectifyService.beginTransaction();
		CustomerAcknowledgementIndicator ack = ofy.get(CustomerAcknowledgementIndicator.getAckKey(leadKey));
		ack.setAppointMentTime(meetDate);
		try {
			ofy.put(ack);
			ofy.getTxn().commit();
		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}

	}

	private static HashMap<String, String> createEmptyFormData(String email) {
		log.warning("returning new map for: " + email);
		HashMap<String, String> formData = new HashMap<String, String>();
		formData.put(FormConstants.EMAIL_KEY, email);
		return formData;
	}

	public static Double getNumberValue(String numStr) {
		Double num = null;
		try {
			num = Double.valueOf(numStr);
		} catch (NumberFormatException ex) {
		}
		return num;
	}

	private static void setDecisionFields(ConsumerLoanSalesLead salesLead, HashMap<String, String> formData) {
		Integer tenor = null;
		String tenorStr = formData.get(FormConstants.TENOR_KEY);

		if (tenorStr != null) {
			tenorStr = tenorStr.split(" ")[0];
			tenor = getNumberValue(tenorStr).intValue();
		}

		salesLead.setDecisionFields(formData.get(FormConstants.PRIMARY_PHONE_KEY),
				formData.get(FormConstants.EMPLOYER_KEY), getFullName(formData),
				formData.get(FormConstants.HIRE_DURATION_KEY), formData.get(FormConstants.LOAN_TYPE_KEY), tenor,
				getNumberValue(formData.get(FormConstants.SALARY_KEY)),
				getNumberValue(formData.get(FormConstants.REQUESTED_LOAN_AMOUNT_KEY)),
				formData.get(FormConstants.ADDRESS_KEY), formData.get(FormConstants.STATE_KEY));
	}

	private static LoanApplicationFormData createLoanLeadAndFormData(String email) throws DuplicateEntitiesException {
		ConsumerLoanSalesLead salesLead = new ConsumerLoanSalesLead(email);
		return createLoanLeadAndFormData(salesLead, createEmptyFormData(email));
	}

	/**
	 * Call this function only for new users. Need a different one to support
	 * multiple loan applications
	 * 
	 * @param salesLead
	 * @param formData
	 * @return
	 * @throws DuplicateEntitiesException
	 */
	public static LoanApplicationFormData createLoanLeadAndFormData(ConsumerLoanSalesLead salesLead,
			HashMap<String, String> formData) throws DuplicateEntitiesException {
		LoanApplicationFormData loanApp = null;
		Objectify ofy = ObjectifyService.beginTransaction();
		String email = formData.get(FormConstants.EMAIL_KEY);

		LoanApplicantUniqueIdentifier applicantIdentifier = new LoanApplicantUniqueIdentifier(email);
		List<Key<ConsumerLoanSalesLead>> leadKeys = getConsumerLoanLeadKeysViaEmail(ofy, email);
		salesLead.loanType = formData.get(FormConstants.LOAN_TYPE_KEY);
		try {
			if (leadKeys.size() == 0)
				ofy.put(applicantIdentifier, salesLead); // save lead first so
															// we can get its
															// auto-assigned key
															// below
			else
				ofy.put(salesLead);
			loanApp = new LoanApplicationFormData(salesLead.getKey(), formData);
			ofy.put(loanApp);
			ofy.getTxn().commit();
		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return loanApp;
	}

	public static String getFullName(HashMap<String, String> formData) {
		return formData.get(FormConstants.APPLICANT_SURNAME_KEY) + ", "
				+ formData.get(FormConstants.APPLICANT_OTHERNAME_KEY);
	}

	public static LoanApplicationFormData updateLoanLeadAndFormData(HashMap<String, String> formData,
			Key<ConsumerLoanSalesLead> leadKey, boolean submit, String updateUser) {
		ConsumerLoanSalesLead salesLead = null;
		HashMap<String, String> oldForm = null;
		Objectify ofy = ObjectifyService.beginTransaction();
		Key<LoanApplicationFormData> loanKey = LoanApplicationFormData.getFormKey(leadKey);
		LoanApplicationFormData loanApp = null;
		try {
			loanApp = ofy.get(loanKey);

			salesLead = ofy.get(leadKey); // throws a runtime exception if not
											// found
			oldForm = loanApp.getFormData();
			setDecisionFields(salesLead, formData);
			loanApp.setFormData(formData, submit);

			boolean guarantorCreated = ofy.find(GuarantorFormData.getFormKey(leadKey, true)) != null;

			String[] comments = null;
			if (submit) {
				if (salesLead.currentState == WorkflowStateInstance.APPLICATION_STARTED
						|| salesLead.currentState == WorkflowStateInstance.APPLICATION_SUBMITTED) {
					comments = salesLead.nextState();
					TaskQueueHelper.scheduleDelayedLoanAppStateChange(salesLead.getKey().getString());
				}
				if (salesLead.address != null && salesLead.address.length() > 5)
					TaskQueueHelper.scheduleGeoCoding(salesLead.getKey().getString());

				if (!guarantorCreated) {
					ofy.put(new GuarantorFormData(leadKey, new HashMap<String, String>(), true));
				}

				double amount = salesLead.getRequestedAmount();
				if (amount >= 750000) {
					guarantorCreated = ofy.find(GuarantorFormData.getFormKey(leadKey, false)) != null;
					if (!guarantorCreated)
						ofy.put(new GuarantorFormData(leadKey, new HashMap<String, String>(), false));
				}
			}

			ofy.put(loanApp, salesLead);
			
			String[] diffComment = { "<div style='border-bottom:1px dotted grey; margin: 2px; text-align:center'>Application "
					+ salesLead.getKey().getParent().getName() + "/" + salesLead.getLeadID()
					+ " updated. Current state is: " + salesLead.getCurrentState().toString() + "</div>", "" };
			StringBuilder diffs = new StringBuilder("<ul>");
			ServiceImplUtilities.addDifference(diffs, oldForm, formData);
			diffComment[1] = diffs.append("</ul>").toString();
			TaskQueueHelper.scheduleCreateComment(diffComment, salesLead.getKey().getString(), updateUser);

			// then schedule a pre-approval decision
			if (comments != null)
				TaskQueueHelper.scheduleCreateComment(comments, salesLead.getKey().getString(), updateUser);

			ofy.getTxn().commit();
		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return loanApp;
	}

	public static LoanApplicationFormData updateGuarantorFormData(HashMap<String, String> formData,
			Key<GuarantorFormData> loanKey, boolean submitted, String updateUser) {
		Key<ConsumerLoanSalesLead> leadKey = loanKey.getParent();
		Objectify ofy = ObjectifyService.beginTransaction();
		LoanApplicationFormData loanApp = null;
		try {
			loanApp = ofy.find(loanKey);

			if (loanApp == null) {
				boolean isFirst = loanKey.equals(GuarantorFormData.getFormKey(leadKey, true));
				loanApp = new GuarantorFormData(leadKey, formData, isFirst);
			}

			loanApp.setFormData(formData, submitted);

			ofy.put(loanApp);

			// then schedule a pre-approval decision
			String[] comments = { "Guarantor form " + (submitted ? "saved" : "submmitted") };
			TaskQueueHelper.scheduleCreateComment(comments, leadKey.getString(), updateUser);

			ofy.getTxn().commit();
		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return loanApp;
	}

	public static LoanApplicationFormData getLoanFormData(Objectify ofy, Key<ConsumerLoanSalesLead> leadKey) {
		return ofy.get(LoanApplicationFormData.getFormKey(leadKey));
	}

	public static List<ConsumerLoanComment> getLoanComments(Objectify ofy, Key<ConsumerLoanSalesLead> leadKey) {
		if (ofy == null)
			ofy = ObjectifyService.begin();

		List<ConsumerLoanComment> list = ofy.query(ConsumerLoanComment.class).filter("loanKey", leadKey).list();
		log.warning("My list Size is: " + list.size());
		Collections.sort(list, Collections.reverseOrder()); // sort list in
															// descending order
		log.warning("After sorting my list size is: " + list.size());
		return list;
	}

	static LoanApplicationFormData getLoanFormData(ConsumerLoanSalesLead salesLead) {
		if (salesLead.key == null)
			return null;
		return getLoanFormData(ObjectifyService.begin(), salesLead.getKey());
	}

	public static LoanApplicationFormData getLoanFormData(String formKeyStr, Objectify ofy) {
		log.warning("Form Key: [" + formKeyStr + "]");
		Key<? extends LoanApplicationFormData> formKey = ofy.getFactory().stringToKey(formKeyStr);
		return getLoanFormData(formKey, ofy);
	}

	public static LoanApplicationFormData getLoanFormData(Key<? extends LoanApplicationFormData> formKey,
			Objectify ofy) {
		LoanApplicationFormData loanData = ofy.get(formKey);
		return loanData;
	}

	public static Query<ConsumerLoanSalesLead> getSalesLeadsByDate(Date startDate, Date endDate) {
		Objectify ofy = ObjectifyService.begin();
		return ofy.query(ConsumerLoanSalesLead.class).filter("dateCreated >=", startDate).filter("dateCreated <=",
				endDate);
	}

	public static Query<LoanApplicationFormData> getApplicationsByDate(Date startDate, Date endDate) {
		Objectify ofy = ObjectifyService.begin();
		return ofy.query(LoanApplicationFormData.class).filter("lastModified >=", startDate).filter("lastModified <=",
				endDate);
	}

	public static void popuplateLeadsTable(QueryResultIterable<ConsumerLoanSalesLead> saleLeads,
			ArrayList<TableMessage> result[]) {
		
		//TODO: DEPRECATED FUNCION ... email, phone indices might break
		// add rows
		for (ConsumerLoanSalesLead sl : saleLeads) {
			WorkflowState state = sl.getCurrentState();
			if (state.showInAllQueue()) {
				int opsQ = (state.opsQueue() == DTOConstants.READY_IDX) ? DTOConstants.HOT_IDX : state.opsQueue();
				TableMessage row = new TableMessage(6, 3, 1);
				row.setText(DTOConstants.FULL_NAME_IDX, sl.getFullName());
				row.setText(DTOConstants.EMAIL_IDX, sl.getEmail());
				row.setText(DTOConstants.PHONE_NUMBER_IDX, sl.getPhoneNumber());
				row.setText(DTOConstants.COMPANY_NAME_IDX, sl.getCompanyName());
				row.setText(DTOConstants.NOTE_IDX, state.suggestOpsAction());
				row.setText(DTOConstants.TYPE_IDX, sl.getLoanType());
				row.setNumber(DTOConstants.TAB_IDENTIFIER_MSG_IDX, opsQ);
				row.setNumber(DTOConstants.SALARY_IDX, sl.getMonthlySalary());
				row.setNumber(DTOConstants.AMOUNT_IDX, sl.getRequestedAmount());
				row.setDate(DTOConstants.DATE_CREATED_IDX, sl.getDateCreated());
				row.setMessageId(sl.getKey().getString());
				result[DTOConstants.ALL_IDX].add(row); // add to all-tab
				if (opsQ >= 0)
					result[opsQ].add(row);
			}
		}
		log.warning("Found X sales leads: " + result[DTOConstants.ALL_IDX].size());
	}

	public static List<ConsumerLoanSalesLead> getConsumerLoanLeadViaEmail(Objectify ofy, String email) {
		return ofy.query(ConsumerLoanSalesLead.class).ancestor(LoanApplicantUniqueIdentifier.getKey(email)).list();
	}

	public static List<Key<ConsumerLoanSalesLead>> getConsumerLoanLeadKeysViaEmail(Objectify ofy, String email) {
		return ofy.query(ConsumerLoanSalesLead.class).ancestor(LoanApplicantUniqueIdentifier.getKey(email))
				.order("-dateCreated").listKeys();
	}

	public static void changeLoanState(Key<ConsumerLoanSalesLead> leadKey, String updateUser) {
		Objectify ofy = ObjectifyService.beginTransaction();
		try {
			ConsumerLoanSalesLead salesLead = ofy.get(leadKey);
			String[] comments = salesLead.nextState();
			ofy.put(salesLead);
			TaskQueueHelper.scheduleCreateComment(comments, salesLead.getKey().getString(), updateUser);
			String[] email = null;
			if (salesLead.getCurrentState().equals(WorkflowStateInstance.PRE_APPROVED_YES))
				email = getPreApprovedMessage(true);
			else if (salesLead.getCurrentState().equals(WorkflowStateInstance.PRE_APPROVED_NO))
				email = getPreApprovedMessage(false);
			if (email != null)
				MessagingDAO.scheduleSendEmail(leadKey.getParent().getName(), email, "Pre Approval Status", true);
			ofy.getTxn().commit();
		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
	}

	public static String[] getPreApprovedMessage(boolean isPreApproved) {
		String[] messageOk = {
				"Congratulations, your loan has been pre-approved. Please proceed with the rest of your application by "
						+ "visiting http://www.salaryadvanceng.com and clicking apply to continue with your application. Best of luck with the rest of your application",
				"Congratulations, your loan has been pre-approved. Please proceed with the rest of your "
						+ "application by visiting <a href='http://www.salaryadvanceng.com'>S.A.NG website</a> and clicking apply to continue with your application. Good luck!" };
		String[] messageNotOk = {
				"We received your online application for a loan. You will be notified once your application has been reviewed. Thanks.",
				"Hello<br/>We received your online application for a loan. You will be notified once your application has been reviewed. <br/><br/>Thanks,<br/>S.A.NG Team" };
		if (isPreApproved)
			return messageOk;
		else
			return messageNotOk;
	}

	public static String[] getApprovedMessage(boolean isApproved) {
		String[] messageOk = {
				"Congratulations, your loan has been approved. Contact your loan account officer to confirm date your account will be funded or monitor progress by "
						+ "visiting http://www.salaryadvanceng.com and clicking apply to check your application status.",
				"Congratulations, your loan has been approved. Contact your loan account officer to confirm date your account will be funded or "
						+ "monitor progress byy visiting <a href='http://www.salaryadvanceng.com'>S.A.NG website</a> and clicking apply to continue with your application."
						+ " Good luck!" };
		String[] messageNotOk = {
				"We received your online application for a loan. After careful review by our bankers, we regret to inform you that we will not be able to lend to you at this point in time. You may try reapplying after 6 months. Best regards.",
				"Hello<br/>We received your online application for a loan. After careful review by our bankers, we regret to inform you that we will not be able to lend to you at this point in time. You may try reapplying after 6 months. <br/><br/>Best regards,<br/>S.A.NG Team" };
		if (isApproved)
			return messageOk;
		else
			return messageNotOk;

	}

	public static String[] getVerificationMessage() {
		String[] messageOk = {
				"Almost done, our bankers have indicated they're waiting for you to book an appointment to visit the bank. Book an appointment today by "
						+ "visiting http://www.salaryadvanceng.com and clicking apply to continue with your application. Best of luck with the rest of your application",
				"Almost done, our bankers have indicated they're waiting for you to book schedule an appointment to visit the bank. Book an appointment today"
						+ " by visiting <a href='http://www.salaryadvanceng.com'>S.A.NG website</a> and clicking apply to continue with your application. Good luck!" };
		return messageOk;
	}

	public static ConsumerLoanSalesLead changeLoanState(Key<ConsumerLoanSalesLead> leadKey, WorkflowStateInstance st,
			String updateUser, String[] additionalArgs) {
		ConsumerLoanSalesLead salesLead = null;
		Objectify ofy = ObjectifyService.beginTransaction();
		try {
			salesLead = ofy.get(leadKey);
			String[] comments = { "Application state changed to " + st };
			if (st.equals(WorkflowStateInstance.LOAN_FUNDED)) {
				if (additionalArgs == null || additionalArgs[0] == null || additionalArgs[0].trim().length() == 0)
					throw new RuntimeException("A loan id must be provided when changing app state to " + st);
				salesLead.setBankLoanId(additionalArgs[0].trim());
			}
			salesLead.setCurrentState(st);
			ofy.put(salesLead);
			TaskQueueHelper.scheduleCreateComment(comments, salesLead.getKey().getString(), updateUser);

			String[] email = null;
			if (salesLead.getCurrentState().equals(WorkflowStateInstance.LOAN_APPROVED_YES))
				email = getApprovedMessage(true);
			else if (salesLead.getCurrentState().equals(WorkflowStateInstance.LOAN_APPROVED_NO))
				email = getApprovedMessage(false);
			else if (salesLead.getCurrentState().equals(WorkflowStateInstance.VERIFICATION))
				email = getVerificationMessage();
			if (email != null)
				MessagingDAO.scheduleSendEmail(leadKey.getParent().getName(), email, "Approval Status", true);

			ofy.getTxn().commit();
		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return salesLead;
	}
	
	public static ConsumerLoanSalesLead changeLoanState(Key<ConsumerLoanSalesLead> leadKey, WorkflowStateInstance st,
			String updateUser, String msg) {
		ConsumerLoanSalesLead salesLead = null;
		Objectify ofy = ObjectifyService.beginTransaction();
		try {
			salesLead = ofy.get(leadKey);
			String[] comments = { "Application state changed from " + salesLead.getCurrentState() + " to " + st, msg };
			salesLead.setCurrentState(st);
			salesLead.setCurrentMessage(msg);
			ofy.put(salesLead);
			TaskQueueHelper.scheduleCreateComment(comments, salesLead.getKey().getString(), updateUser);
			ofy.getTxn().commit();
		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return salesLead;
	}
	



	public static String[] getOrCreateLoanID(String email) throws DuplicateEntitiesException {
		Objectify ofy = ObjectifyService.begin();
		String[] result = new String[3];
		List<ConsumerLoanSalesLead> salesLeadList = LoanMktDAO.getConsumerLoanLeadViaEmail(ofy, email);
		LoanApplicationFormData formObj = null;
		if (salesLeadList.size() == 0) // first ever login, go ahead and create
										// it
			formObj = createLoanLeadAndFormData(email);
		else if (salesLeadList.size() == 1)
			formObj = getLoanFormData(salesLeadList.get(0));
		else
			throw new DuplicateEntitiesException("Database corrupted? Found multiple apps for: " + email);
		result[DTOConstants.LOAN_KEY_IDX] = formObj.getParentKey().getString();
		result[DTOConstants.FORM_KEY_IDX] = formObj.getObjectifyKey().getString();
		result[DTOConstants.SUBMITTED_KEY_IDX] = String.valueOf(formObj.isSubmmited());
		return result;
	}

	public static QueryResultIterable<Key<LoanApplicationFormData>> getNewLoanKeys() {
		Objectify ofy = ObjectifyService.begin();
		return ofy.query(LoanApplicationFormData.class).filter("submitted", false).fetchKeys();
	}

	public static Set<Key<ConsumerLoanSalesLead>> getAllLoanApps() {
		Objectify ofy = ObjectifyService.begin();
		return ofy.query(LoanApplicationFormData.class).fetchParentKeys();
	}

	public static void addLeadAsCSVLine(StringBuilder sb, ConsumerLoanSalesLead lead) {
		DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);
		sb.append(lead.currentState).append(",").append(lead.getFullName()).append(",").append(lead.getEmail())
				.append(",").append(lead.getPhoneNumber()).append(",").append(lead.getCompanyName()).append(",")
				.append(lead.requestedAmount).append(",").append(lead.requestedTenor).append(",")
				.append(lead.monthlySalary).append(",").append(lead.hirePeriod).append(",").append(lead.getUpdateUser())
				.append(",").append(df.format(lead.getDateUpdated())).append(",")
				.append(df.format(lead.getDateCreated())).append(",").append(lead.approvedTenor).append(",")
				.append(lead.approvedRate).append(",").append(lead.approvedAmount).append("\n");
	}

	public static String getCSVHeader() {
		return "Status,Name,Email,Phone,Company,Requested Amount,Requested Tenor,Monthly Salary,Hire Period,Last update by,Date Updated,Date Created,"
				+ "Approved Tenor,Approved Rate,Approved Amount\n";
	}

	private static TableMessageHeader getSalesLeadHeader() {
		TableMessageHeader h = new TableMessageHeader(10);
		h.setText(0, "Full Name", TableMessageContent.TEXT);
		h.setText(1, "Company", TableMessageContent.TEXT);
		h.setText(2, "Type", TableMessageContent.TEXT);
		h.setText(3, "State", TableMessageContent.TEXT);
		h.setText(4, "Salary", TableMessageContent.NUMBER);
		h.setText(5, "Amount", TableMessageContent.NUMBER);
		h.setText(6, "Tenor", TableMessageContent.TEXT);
		h.setText(7, "Status", TableMessageContent.TEXT);
		h.setText(8, "Created", TableMessageContent.DATE);
		h.setText(9, "Modified", TableMessageContent.DATE);
		return h;
	}

	private static TableMessage getSalesLeadInfo(ConsumerLoanSalesLead lead) {
		TableMessage m = new TableMessage(8, 3, 2);
		m.setText(DTOConstants.FULL_NAME_IDX, lead.getFullName());
		m.setText(DTOConstants.EMAIL_IDX, lead.getEmail());
		m.setText(DTOConstants.PHONE_NUMBER_IDX, lead.getPhoneNumber());
		m.setText(DTOConstants.COMPANY_NAME_IDX, lead.getCompanyName());
		m.setText(DTOConstants.TYPE_IDX, lead.getLoanType());
		m.setNumber(DTOConstants.AMOUNT_IDX, lead.getRequestedAmount());
		m.setNumber(DTOConstants.SALARY_IDX, lead.getMonthlySalary());
		m.setNumber(DTOConstants.TAB_IDENTIFIER_MSG_IDX, lead.getLeadID());
		m.setDate(DTOConstants.DATE_MODIFIED_IDX, lead.getDateUpdated());
		m.setDate(DTOConstants.DATE_CREATED_IDX, lead.getDateCreated());
		
		//new fields
		m.setText(DTOConstants.NOTE_IDX, lead.getCurrentState().getDisplayString());
		if(lead.getCurrentState() == WorkflowStateInstance.PRE_APPROVED_YES)
			m.setRowStyle("preAppproveRow");
		else if(lead.getCurrentState() == WorkflowStateInstance.PRE_APPROVED_NO)
			m.setRowStyle("preDeniedRow");
		
		m.setText(DTOConstants.STATE_IDX, lead.getState());
		Integer tenure = lead.getRequestedTenor();
		String ten = "";
		if (tenure != null)
		{
			if(tenure == 1)
				ten = "1 month";
			else
				ten = tenure + " months";
		}
		m.setText(DTOConstants.TENOR_IDX, ten);
		return m;
	}

	public static List<TableMessage> getConsumerLeadsAsTable(Iterable<ConsumerLoanSalesLead> leads) {
		ArrayList<TableMessage> result = new ArrayList<TableMessage>();
		result.add(LoanMktDAO.getSalesLeadHeader());
		for (ConsumerLoanSalesLead loanInfo : leads) {
			TableMessage loanRow = LoanMktDAO.getSalesLeadInfo(loanInfo);
			loanRow.setMessageId(loanInfo.getKey().getString());
			result.add(loanRow);
		}
		return result;
	}

	public static HashMap<WorkflowStateInstance, Integer> getLeadAggregates(Date start, Date end) {
		Iterable<ConsumerLoanSalesLead> salesLeads = getSalesLeadsByDate(start, end);
		HashMap<WorkflowStateInstance, Integer> aggregates = new HashMap<WorkflowStateInstance, Integer>();

		Integer stateCount = null;
		for (ConsumerLoanSalesLead lead : salesLeads) {
			stateCount = aggregates.get(lead.getCurrentState());
			if (stateCount == null)
				stateCount = 0;
			aggregates.put(lead.getCurrentState(), stateCount + 1);
		}
		return aggregates;
	}

	public static List<TableMessage> getMapAttachments(Date startDate, Date endDate) {
		Query<ConsumerLoanSalesLead> applicants = getSalesLeadsByDate(startDate, endDate);

		ArrayList<TableMessage> attachments = new ArrayList<TableMessage>();
		TableMessage header = new TableMessage(1, 3, 0);
		attachments.add(header);
		// initialize google maps
		int loansCount, addressCount, queryCount;
		loansCount = addressCount = queryCount = 0;
		String addresssSamples = "";
		for (ConsumerLoanSalesLead a : applicants) {
			loansCount += 1;

			String address = a.address;
			if (address == null || address.length() == 0)
				continue;
			Double longitude = a.longitude;
			if (longitude == null)
				continue;
			if (addressCount <= 5) // for debugging
				addresssSamples += ("[" + address + "]<br>\n");
			addressCount += 1;

			queryCount += 1;

			String attachDate = String.format("%tc", a.dateCreated);
			StringBuilder desc = new StringBuilder();
			desc.append("<div style='width:180px;height:100px;color:#7b04a2;font-size:smaller'>")
					.append("<a href='http://www.salaryadvanceng,com/office/#doStatusCW/")
					.append(a.getKey().getString()).append("'>Address: ").append(address).append("</a>")
					.append("<br/>Compamy: [").append(a.companyName)
					.append("]<br/>Requested Amount: [").append(a.approvedAmount)
					.append("]<br/>Type: [").append(a.getLoanType()).append("]<br/>Name: [")
					.append(a.fullName).append("]<br/>Date: [").append(attachDate).append("]</div>");

			TableMessage m = new TableMessage(2, 2, 0);
			m.setText(DTOConstants.CATEGORY_IDX, a.currentState.name());
			m.setText(DTOConstants.DETAIL_IDX, desc.toString());
			m.setNumber(DTOConstants.LAT_IDX, a.latitude);
			m.setNumber(DTOConstants.LNG_IDX, a.longitude);
			m.setMessageId(a.getKey().getParent().getString());
			attachments.add(m);

		}
		
		//debug header
		header.setNumber(0, loansCount);
		header.setNumber(1, addressCount);
		header.setNumber(2, queryCount);
		header.setText(0, addresssSamples);
		return attachments;
	}

	public static List<MortgageAttachment> createAttachment(Key<ConsumerLoanSalesLead> leadKey,
			List<TableMessage> attachList, String user) {
		Objectify ofy = ObjectifyService.beginTransaction();
		List<MortgageAttachment> attachments = new ArrayList<MortgageAttachment>();
		try {
			boolean first = true;
			for (TableMessage m : attachList) {
				if (first) {
					first = false;
					continue;
				} // skip header
				MortgageAttachment attach = new MortgageAttachment(leadKey, m);
				attachments.add(attach);
			}
			ofy.put(attachments);
			// TODO: reconsider uncommenting this. Currently commented out as it
			// adds noise to the activity feed
			// String[] comment =
			// {DrivePickerUtils.getAttachmentDescription(attachList)};
			// TaskQueueHelper.scheduleCreateComment(comment,
			// leadKey.getString(), user);
			ofy.getTxn().commit();
		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return attachments;
	}

	/*
	 * TODO: need to add an ancestor index if enabling this query private static
	 * List<MortgageAttachment>
	 * getMortgageAttachments(Key<ConsumerLoanSalesLead> leadKey) { return
	 * ObjectifyService.begin().query(MortgageAttachment.class).ancestor(leadKey
	 * ).order("-attachDate").list(); }
	 */

	/*
	 * public static List<TableMessage>
	 * getAttachmentsDTO(Key<ConsumerLoanSalesLead> leadKey) {
	 * ArrayList<TableMessage> attachments = new ArrayList<TableMessage>();
	 * attachments.add(getAttachmentHeader(leadKey)); for(MortgageAttachment a :
	 * getMortgageAttachments(leadKey)) attachments.add(a.getDTO()); return
	 * attachments; }
	 */

	/*
	 * private static TableMessageHeader
	 * getAttachmentHeader(Key<ConsumerLoanSalesLead> leadKey) {
	 * TableMessageHeader h = new TableMessageHeader(5); h.setText(0, "Title",
	 * TableMessageContent.TEXT); h.setText(1, "Type",
	 * TableMessageContent.TEXT); h.setText(2, "Drive Date",
	 * TableMessageContent.DATE); h.setText(3, "Attach Date",
	 * TableMessageContent.DATE); h.setText(4, "User",
	 * TableMessageContent.TEXT); h.setMessageId(leadKey.getString());
	 * h.setCaption("Attachments for M.A.P. ID: " + leadKey.getId()); return h;
	 * }
	 */

	/*
	 * public static void removeAttachment(Key<MortgageAttachment>
	 * mtgAttachment, String user) { Objectify ofy =
	 * ObjectifyService.beginTransaction(); try { MortgageAttachment attachment
	 * = ofy.get(mtgAttachment); String[] comments =
	 * {DrivePickerUtils.getAttachmentDescription(attachment.getDTO(),
	 * "REMOVED ATTACHMENT")}; ofy.delete(mtgAttachment);
	 * TaskQueueHelper.scheduleCreateComment(comments,
	 * mtgAttachment.getParent().getString(), user); ofy.getTxn().commit(); }
	 * finally { if(ofy.getTxn().isActive()) ofy.getTxn().rollback(); } }
	 */

	/*
	 * public static String saveMapAttachment(Key<ConsumerLoanSalesLead>
	 * leadKey, HttpServletRequest req) { String user =
	 * LoginHelper.getLoggedInUser(req); if (user == null) user =
	 * "anoymous user entry"; Double altLat, altLng; altLng = altLat = null;
	 * String title = "Untitled"; try { String[] latlng =
	 * req.getHeader("X-AppEngine-CityLatLong").split(","); title =
	 * req.getHeader("X-AppEngine-Country") + "/" +
	 * req.getHeader("X-AppEngine-City") + "/" +
	 * req.getHeader("X-AppEngine-Region"); altLat = Double.valueOf(latlng[0]);
	 * altLng = Double.valueOf(latlng[1]); TableMessage serverAttachment =
	 * DrivePickerUtils.getUserMapAttachment("User Edits - " + title, altLat,
	 * altLng, user); if (serverAttachment != null) { ArrayList<TableMessage>
	 * result = new ArrayList<TableMessage>(3); TableMessageHeader h = new
	 * TableMessageHeader(); h.setCaption("User Edits - Location Detection");
	 * result.add(h); result.add(serverAttachment); return
	 * saveAttachments(leadKey, result, req); }
	 * 
	 * } catch (Exception e) { log.severe(
	 * "Ignoring error while detecting lat/lng server side. Error: " +
	 * e.getMessage()); } return null; }
	 */

	public static String saveAttachments(Key<ConsumerLoanSalesLead> leadKey, List<TableMessage> attachments,
			HttpServletRequest req) {
		String user = LoginHelper.getLoggedInUser(req);
		user = (user == null) ? "anonymous user entry" : user;
		List<MortgageAttachment> attch = createAttachment(leadKey, attachments, user);
		return "Attached " + attch.size() + " new files for S.A.NG . ID: " + leadKey.getId();
	}
}
