/**
 * 
 */
package com.fertiletech.addosser.server.loanmktdata;

import java.util.HashMap;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Serialized;
import com.googlecode.objectify.annotation.Unindexed;


/**
 * @author Segun Razaq Sobulo
 *
 */
@Unindexed
@Cached
public class ApplicationParameters {
	
	@Id
	String myId;
	
	@Serialized
	HashMap<String, String> params;
	
	ApplicationParameters()
	{
		params = new HashMap<String, String>();
	}
	
	ApplicationParameters(String name)
	{
		this();
		this.myId = name;
	}
	public static Key<ApplicationParameters> getKey(String ID)
	{
		return new Key<ApplicationParameters>(ApplicationParameters.class, ID);
	}
	ApplicationParameters(String name, HashMap<String, String> params)
	{
		this.myId = name;
		this.params = params;
	}
		
	public HashMap<String, String> getParams() {
		return params;
	}

	public void setParams(HashMap<String, String> params) {
		this.params = params;
	}

	public Key<ApplicationParameters> getKey()
	{
		return new Key<ApplicationParameters>(ApplicationParameters.class, myId);
	}
}
