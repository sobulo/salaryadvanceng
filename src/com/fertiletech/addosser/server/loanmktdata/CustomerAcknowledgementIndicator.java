/**
 * 
 */
package com.fertiletech.addosser.server.loanmktdata;

import java.util.Date;

import com.googlecode.objectify.Key;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class CustomerAcknowledgementIndicator extends LoanApplicationFormData{
	Date appointMentTime;
	Date callBackDate;
	private final static String ACK_KEY = "ack";

	public CustomerAcknowledgementIndicator() {} //objectify needs an empty constructor
	public CustomerAcknowledgementIndicator(Key<ConsumerLoanSalesLead>parentKey, boolean aknowledged)
	{
		this.submitted = true;
		this.parentKey = parentKey;
		this.key = ACK_KEY;
	}
	
	public static Key<CustomerAcknowledgementIndicator> getAckKey(Key<ConsumerLoanSalesLead> leadKey)
	{
		return getKey(leadKey, CustomerAcknowledgementIndicator.class, ACK_KEY);
	}
	
	public Date getAppointMentTime() {
		return appointMentTime;
	}
	public void setAppointMentTime(Date appointMentTime) {
		this.appointMentTime = appointMentTime;
	}
	public Date getCallBackDate() {
		return callBackDate;
	}
	void setCallBackDate(Date callBackDate) {
		this.callBackDate = callBackDate;
	}
	
}
