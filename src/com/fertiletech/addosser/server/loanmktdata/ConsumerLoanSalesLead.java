/**
 * 
 */
package com.fertiletech.addosser.server.loanmktdata;

import java.util.Date;

import javax.persistence.Id;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;

import com.fertiletech.addosser.shared.DTOConstants;
import com.fertiletech.addosser.shared.workstates.WorkflowMachine;
import com.fertiletech.addosser.shared.workstates.WorkflowStateInstance;
import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Unindexed;

import sun.rmi.runtime.NewThreadAction;

/**
 * @author Segun Razaq Sobulo
 *
 */
@Unindexed
@Cached
public class ConsumerLoanSalesLead implements WorkflowMachine{
	
	@Id
	Long key;
	
	@Indexed Long leadID;
	
	//customer fields -- potential index candidates, for now useful to have these without having to load forms
	String phoneNumber;
	String companyName;
	String fullName;
	Double monthlySalary;
	Double requestedAmount;
	Integer requestedTenor;
	String hirePeriod;
	String loanType;
	//mapping support
	String address;
	String state;
	Double latitude;
	Double longitude;	
	
	WorkflowStateInstance currentState;
	Text currentMessage;

	

	Double approvedAmount;
	int approvedTenor;
	double approvedRate;
	
	//indexed fields
	@Indexed String bankLoanId;
	@Indexed Date dateCreated;
			
	//auto-populated fields
	Date dateUpdated; 
	String updateUser;	
	
	@Parent Key<LoanApplicantUniqueIdentifier> parentKey;
	
	public String getEmail(){
		return parentKey.getName();
	}
	
	public Double getApprovedAmount() {
		return approvedAmount;
	}

	void setApprovedAmount(Double approvedAmount) {
		this.approvedAmount = approvedAmount;
	}

	public String getBankLoanId() {
		return bankLoanId;
	}

	void setBankLoanId(String bankLoanId) {
		this.bankLoanId = bankLoanId;
	}
	
	public String getAddress()
	{
		return address;
	}
	
	public void setAddress(String addy)
	{
		address = addy;
	}
	
	public String getState()
	{
		return state;
	}
	
	public void setState(String st)
	{
		state = st;
	}
	
	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}	

	public Boolean getLoanApproved() {
		if(currentState.equals(WorkflowStateInstance.LOAN_APPROVED_NO))
			return false;
		else if(currentState.equals(WorkflowStateInstance.LOAN_APPROVED_YES) || currentState.equals(WorkflowStateInstance.LOAN_FUNDED))
			return true;
		
		return null;
	}

	public ConsumerLoanSalesLead() {} //empty constructor required by objectify
	
	public ConsumerLoanSalesLead(String email)
	{
		this.parentKey = new Key<LoanApplicantUniqueIdentifier>(LoanApplicantUniqueIdentifier.class, email);
		this.currentState = WorkflowStateInstance.APPLICATION_STARTED;
		this.dateCreated = new Date(); 	
	}
	
	public void setDecisionFields(
			String phoneNumber,
			String companyName,
			String fullName,
			String hirePeriod,
			String loanType,
			int requestedTenor,
			Double monthlySalary,
			Double requestedAmount,
			String address,
			String state
	)
	{
		this.phoneNumber = phoneNumber;
		this.companyName = companyName;
		this.fullName = fullName;
		this.loanType = loanType;
		this.hirePeriod = hirePeriod;
		this.requestedTenor = requestedTenor;
		this.monthlySalary = monthlySalary;
		this.requestedAmount = requestedAmount;
		this.address = address;
		this.state = state;
	}
	
	
	public String getHirePeriod() {
		return hirePeriod;
	}

	public Integer getRequestedTenor() {
		return requestedTenor;
	}

	private void setRequestedTenor(int requestedTenor) {
		this.requestedTenor = requestedTenor;
	}

	public int getApprovedTenor() {
		return approvedTenor;
	}

	void setApprovedTenor(int approvedTenor) {
		this.approvedTenor = approvedTenor;
	}

	double getApprovedRate() {
		return approvedRate;
	}

	void setApprovedRate(double approvedRate) {
		this.approvedRate = approvedRate;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	
	public String getCompanyName() {
		return companyName;
	}
		
	public String getFullName() {
		return fullName;
	}
	
	public String getLoanType() {
		return loanType;
	}	
		
	public Double getMonthlySalary() {
		return monthlySalary;
	}
		
	public Double getRequestedAmount() {
		return requestedAmount;
	}
	
	public Date getDateCreated() {
		return dateCreated;
	}
	
	public Date getDateUpdated() {
		return dateUpdated;
	}
	
	public Long getLeadID()
	{
		return leadID;
	}
	
	@PostLoad
	public void loadLoanType()
	{
		if(loanType == null)
			loanType = DTOConstants.LOAN_TYPES[0];		
	}
	
	@PrePersist
	void changeDateUpdated() {
		this.dateUpdated = new Date();
	}
	
	String getUpdateUser() {
		return updateUser;
	}
	
	public Key<ConsumerLoanSalesLead> getKey()
	{
		return new Key<ConsumerLoanSalesLead>(parentKey, ConsumerLoanSalesLead.class, key);
	}

	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.server.loanmktdata.workflow.WorkflowMachine#nextState()
	 */
	@Override
	public String[] nextState() {
		return currentState.nextState(this);
	}

	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.server.loanmktdata.workflow.WorkflowMachine#getCurrentState()
	 */
	@Override
	public WorkflowStateInstance getCurrentState() {
		return currentState;
	}

	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.server.loanmktdata.workflow.WorkflowMachine#setCurrentState(com.fertiletech.addosser.server.loanmktdata.workflow.WorkflowState)
	 */
	@Override
	public void setCurrentState(WorkflowStateInstance state) {
		this.currentState = state;
	}
	
	@PrePersist void updateApplicationID()
	{
		if(leadID == null)
			leadID = ObjectifyService.factory().allocateId(ConsumerLoanSalesLead.class);
	}

	@Override
	public String getCurrentMessage() {
		if(currentMessage == null)
			return "";
		return currentMessage.toString();
	}

	@Override
	public void setCurrentMessage(String msg) {
		currentMessage = new Text(msg);
		
	}
}
