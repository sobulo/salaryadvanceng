/**
 * 
 */
package com.fertiletech.addosser.server.loanmktdata;

import javax.persistence.Id;

import com.googlecode.objectify.Key;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class LoanApplicantUniqueIdentifier {
	@Id
	String email;

	LoanApplicantUniqueIdentifier() {}
	
	public LoanApplicantUniqueIdentifier(String email)
	{
		this.email = email.toLowerCase();
	}

	Key<LoanApplicantUniqueIdentifier> getKey()
	{
		return getKey(this.email);
	}
	
	static Key<LoanApplicantUniqueIdentifier> getKey(String email)
	{
		return new Key<LoanApplicantUniqueIdentifier>(LoanApplicantUniqueIdentifier.class, email.toLowerCase());
		
	}
}
