package com.fertiletech.addosser.server;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.fertiletech.addosser.client.LoginService;
import com.fertiletech.addosser.server.loanmktdata.ConsumerLoanComment;
import com.fertiletech.addosser.server.loanmktdata.ConsumerLoanSalesLead;
import com.fertiletech.addosser.server.loanmktdata.LoanMktDAO;
import com.fertiletech.addosser.server.login.LoginHelper;
import com.fertiletech.addosser.server.scripts.EntitySetup;
import com.fertiletech.addosser.shared.LoginInfo;
import com.fertiletech.addosser.shared.LoginRoles;
import com.fertiletech.addosser.shared.exceptions.DuplicateEntitiesException;
import com.fertiletech.addosser.shared.oauth.OurException;
import com.fertiletech.addosser.shared.oauth.SocialUser;
import com.fertiletech.addosser.shared.table.TableMessage;
import com.google.appengine.api.datastore.Text;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class LoginServiceImpl extends RemoteServiceServlet implements
		LoginService {
	static
	{
		LoanMktDAO.registerClassesWithObjectify();
	}
	private static final Logger log = Logger.getLogger(LoginServiceImpl.class.getName());

	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.client.GreetingService#login(java.lang.String)
	 */
	@Override
	public LoginInfo login(String requestUrl) {
		//TODO remove both login and loginforApplications
		throw new RuntimeException("This method is no longer supported, use oauth instead path");
		//LoginInfo info = LoginHelper.brokeredLogin(requestUrl);
		//return info;
	}

	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.client.LoginService#loginForApplications(java.lang.String)
	 */
	@Override
	public LoginInfo loginForApplications(String requestUrl) throws DuplicateEntitiesException{
		LoginInfo info = login(requestUrl);
		if(info.isLoggedIn())
		{
			List<Key<ConsumerLoanSalesLead>> leadKeys = LoanMktDAO.getConsumerLoanLeadKeysViaEmail(ObjectifyService.begin(), info.getLoginID());
	    	String[] IDs = new String[leadKeys.size()];
	    	int count = 0;
	    	for(Key<ConsumerLoanSalesLead> lk : leadKeys)
	    	{
	    		IDs[count++] = lk.getString();
	    	}
	    	info.storeLoanInfo(IDs);
	    	info.setPanelState(-1); //TODO 1 step closer to deprecating this, delete once client side no longer takes as an argument, currently meaniningless as hardcoded to -1
		}
		return info;
	}

	@Override
	public List<TableMessage> getLoanApplications(String email) 
	{
		Objectify ofy = ObjectifyService.begin();
		List<Key<ConsumerLoanSalesLead>> leadKeys = LoanMktDAO.getConsumerLoanLeadKeysViaEmail(ofy, email);
		Map<Key<ConsumerLoanSalesLead>, ConsumerLoanSalesLead> leads = ofy.get(leadKeys);
		return LoanMktDAO.getConsumerLeadsAsTable(leads.values());
	}

	@Override
	public List<TableMessage> getLoanApplications() {
		return getLoanApplications(LoginHelper.getLoggedInUser(getThreadLocalRequest()));
	}

	private final static String LAST_OPS_LOAN_ID_REQUEST = "com.fertiletech.addosser.opslastloanidviewed";
	
	public static void setLastOpsLoan(String loandIDViewed, HttpServletRequest req)
	{
		LoginRoles role = LoginHelper.getRole(req);
		HttpSession sess = req.getSession();
		if(!role.equals(LoginRoles.ROLE_PUBLIC))
		{
			//log.warning("Last OPS ID Set to: " + LAST_OPS_LOAN_ID_REQUEST);
			sess.setAttribute(LAST_OPS_LOAN_ID_REQUEST, loandIDViewed);
		}
	}
	
	public static void stampNPMBInfo(SocialUser user, HttpSession sess) throws OurException
	{
		String email = user.getEmail();
		OAuthLoginServiceImplementation.saveEmailToSession(email, sess);
    	user.role = LoginHelper.getRole(email);    	
		
    	if(user.role.equals(LoginRoles.ROLE_PUBLIC))
    	{
    		List<Key<ConsumerLoanSalesLead>> leadKeys = LoanMktDAO.getConsumerLoanLeadKeysViaEmail(ObjectifyService.begin(), email);
        	String[] IDs = new String[leadKeys.size()];
        	int count = 0;
        	
        	for(Key<ConsumerLoanSalesLead> lk : leadKeys)
        	{
        		IDs[count++] = lk.getString();
        	}
        	user.loanIDs = IDs;    		
    	}
    	else
    	{
			String[] loanIDs = {(String) sess.getAttribute(LAST_OPS_LOAN_ID_REQUEST)};
        	if(!(sess.getAttribute(LAST_OPS_LOAN_ID_REQUEST) == null))
    		{
    			if(loanIDs[0] != null)
    			{
    				user.loanIDs = loanIDs;
    				return;
    			}
    		}	    		
    	}
	}

	@Override
	public List<TableMessage> getRecentActivityComments() {
		Objectify ofy = ObjectifyService.begin();
		List<ConsumerLoanComment> comments = ofy.query(ConsumerLoanComment.class).order("-dateUpdated").limit(100).list();
		log.warning("Recent Activity Feed Request Received: " + comments.size());
		List<TableMessage> result = new ArrayList<TableMessage>();
		for(ConsumerLoanComment c : comments)
		{
			TableMessage m = new TableMessage(3, 0, 1);
			m.setMessageId(c.getKey().getString());
			Text descriptiveComment = c.getComment();
			m.setText(0, c.getUser());
			m.setText(1, descriptiveComment == null?"Oops, no comment found, contact IT":descriptiveComment.getValue());
			m.setText(2, c.getLoanKey().getString());
			//log.warning(descriptiveComment.toString());
			m.setDate(0, c.getDateUpdated());
			result.add(m);
		}
		return result;
	}	
}
