/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fertiletech.addosser.server.login;
import java.util.HashMap;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class LoginConstants
{
    public final static String COMPANY_DOMAIN = "salaryadvanceng.com";
    public final static String FTBS_DOMAIN = "fertiletech.com";
    public final static HashMap<String, String> ALLOWED_NAMESPACES = new HashMap<String, String>();

    /**
     * super user login list
     * TODO: ideally this should be stored in object database vs hardcoded here 
     */
    /*public final static String[] SUPER_USERS =
    {
        "sobulo@fertiletech.com", "intern-work@fertiletech.com"
    };*/
}
