/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fertiletech.addosser.server.tasks;


import java.util.logging.Logger;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Builder;
import com.google.appengine.api.taskqueue.TaskOptions.Method;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class TaskQueueHelper {
    private static final Logger log = Logger.getLogger(TaskQueueHelper.class.getName());
    private final static String COMMON_TASK_HANDLER = "/tasks/runner";

    private static int MAX_RETRY_COUNT = 3;
    
    public static void scheduleCreateComment(String[] comments, String loanKeyStr, String updateUser)
    {
    	boolean succeeded = false;
    	int retryCount = 0;
    	while(!succeeded && retryCount < MAX_RETRY_COUNT)
    	{
    		if(retryCount != 0)
    			log.warning("retrying schedule task, retry number " + retryCount);
    		try
    		{
	            Queue queue = QueueFactory.getDefaultQueue();
	            TaskOptions opt = Builder.withUrl(COMMON_TASK_HANDLER).
	                    param(TaskConstants.SERVICE_TYPE,
	                    TaskConstants.SERVICE_CREATE_COMMENT).
	                    param(TaskConstants.LOAN_ID_PARAM,
	                    loanKeyStr).param(TaskConstants.UPDATE_USR_PARAM, updateUser);

    	        for(String msg : comments)
    	        	if(msg != null && msg.length() > 0)
    	        		opt = opt.param(TaskConstants.MSG_BODY_PARAM, msg);	            
	            queue.add(opt.method(Method.POST));
	            succeeded = true;
    		}
    		catch(Exception e)
    		{
    			retryCount++;
    			log.warning("schedule task create string options failed: " + e.getMessage());
    		}
    	}
    	
    	if(!succeeded)
    		log.severe("Task scheduling failed, comments will not be created. This is a silent failure, best debug info: [" + loanKeyStr + "]");
    }
    
	public static void scheduleMessageSending(String controllerId, String toAddress, String messages[], String subject)
	{
    	boolean succeeded = false;
    	int retryCount = 0;
    	while(!succeeded && retryCount < MAX_RETRY_COUNT)
    	{
    		if(retryCount != 0)
    			log.warning("retrying schedule task, retry number " + retryCount);
    		try
    		{
    	    	log.warning("scheduling sending of msg to: " + toAddress);
    	        Queue queue = QueueFactory.getDefaultQueue();
    	        TaskOptions url = Builder.withUrl(COMMON_TASK_HANDLER).
    	                param(TaskConstants.SERVICE_TYPE, 
    	                		TaskConstants.SERVICE_SEND_MESSAGE).
    	                param(TaskConstants.MSG_CONTROLLER_KEY_PARAM, controllerId).
    	                param(TaskConstants.TO_ADDR_PARAM, toAddress).
    	                param(TaskConstants.MSG_SUBJECT_PARAM, subject);
    	        
    	        for(String msg : messages)
    	        	if(msg != null && msg.length() > 0)
    	        		url = url.param(TaskConstants.MSG_BODY_PARAM, msg);
    	        url.method(Method.POST);
    	        queue.add(url);    	    			
	            succeeded = true;
    		}
    		catch(Exception e)
    		{
    			retryCount++;
    			log.warning("schedule message sending failed: " + e.getMessage());
    		}
    	}
    	if(!succeeded)
    		throw new RuntimeException("Task scheduling failed, pls check logs");
	}
	
    public static void scheduleDelayedLoanAppStateChange(String loanKeyStr)
    {
    	log.warning("schedule of delayed loan app state change requested");
    	boolean succeeded = false;
    	int retryCount = 0;
    	while(!succeeded && retryCount < MAX_RETRY_COUNT)
    	{
    		if(retryCount != 0)
    			log.warning("retrying schedule task, retry number " + retryCount);
    		try
    		{
	            Queue queue = QueueFactory.getDefaultQueue(); 
	            TaskOptions opt = Builder.withUrl(COMMON_TASK_HANDLER).
	                    param(TaskConstants.SERVICE_TYPE,
	                    TaskConstants.SERVICE_CHANGE_LOAN_STATE).
	                    param(TaskConstants.LOAN_ID_PARAM,
	                    loanKeyStr).method(Method.POST);            
	            queue.add(opt);
	            succeeded = true;
    		}
    		catch(Exception e)
    		{
    			retryCount++;
    			log.warning("schedule task create string options failed: " + e.getMessage());
    		}
    	}
    	
    	if(!succeeded)
    		log.severe("Task scheduling failed, state change not ok. This is a silent failure, best debug info: [" + loanKeyStr + "]");
    	else
    		log.warning("loan state change has been scheduled");
    }
    
    public static void scheduleGeoCoding(String loanKeyStr)
    {
    	log.warning("schedule of geo coding requested");
    	boolean succeeded = false;
    	int retryCount = 0;
    	while(!succeeded && retryCount < MAX_RETRY_COUNT)
    	{
    		if(retryCount != 0)
    			log.warning("retrying schedule task, retry number " + retryCount);
    		try
    		{
	            Queue queue = QueueFactory.getDefaultQueue(); 
	            TaskOptions opt = Builder.withUrl(COMMON_TASK_HANDLER).
	                    param(TaskConstants.SERVICE_TYPE,
	                    TaskConstants.SERVICE_GEO_CODE).
	                    param(TaskConstants.LOAN_ID_PARAM,
	                    loanKeyStr).method(Method.POST);            
	            queue.add(opt);
	            succeeded = true;
    		}
    		catch(Exception e)
    		{
    			retryCount++;
    			log.warning("schedule task create string options failed: " + e.getMessage());
    		}
    	}
    	
    	if(!succeeded)
    		log.severe("Task scheduling failed, geo coding not ok. This is a silent failure, best debug info: [" + loanKeyStr + "]");
    	else
    		log.warning("geo coding has been scheduled");
    }
    
    public static void scheduleAddressUpdate(String loanKeyStr)
    {
    	log.warning("schedule of sddress requested");
    	boolean succeeded = false;
    	int retryCount = 0;
    	while(!succeeded && retryCount < MAX_RETRY_COUNT)
    	{
    		if(retryCount != 0)
    			log.warning("retrying schedule task, retry number " + retryCount);
    		try
    		{
	            Queue queue = QueueFactory.getDefaultQueue(); 
	            TaskOptions opt = Builder.withUrl(COMMON_TASK_HANDLER).
	                    param(TaskConstants.SERVICE_TYPE,
	                    TaskConstants.SERVICE_BATCH_ADDRESS).
	                    param(TaskConstants.LOAN_ID_PARAM,
	                    loanKeyStr).method(Method.POST);            
	            queue.add(opt);
	            succeeded = true;
    		}
    		catch(Exception e)
    		{
    			retryCount++;
    			log.warning("schedule task create string options failed: " + e.getMessage());
    		}
    	}
    	
    	if(!succeeded)
    		log.severe("Task scheduling failed, address upates not ok. This is a silent failure, best debug info: [" + loanKeyStr + "]");
    	else
    		log.warning("geo coding has been scheduled");
    }  
    
    public static void scheduleUpdateOrbitRim()
    {
    	log.warning("schedule of sddress requested");
    	boolean succeeded = false;
    	int retryCount = 0;
    	while(!succeeded && retryCount < MAX_RETRY_COUNT)
    	{
    		if(retryCount != 0)
    			log.warning("retrying schedule task, retry number " + retryCount);
    		try
    		{
	            Queue queue = QueueFactory.getDefaultQueue(); 
	            TaskOptions opt = Builder.withUrl(COMMON_TASK_HANDLER).
	                    param(TaskConstants.SERVICE_TYPE,
	                    TaskConstants.SERVICE_ORBIT_GEO_CODE_RIM).method(Method.POST);            
	            queue.add(opt);
	            succeeded = true;
    		}
    		catch(Exception e)
    		{
    			retryCount++;
    			log.warning("schedule task create string options failed: " + e.getMessage());
    		}
    	}
    	
    	if(!succeeded)
    		log.severe("Task scheduling failed, orbit geo coding not ok. This is a silent failure, best debug info");
    	else
    		log.warning("geo coding has been scheduled");
    }       
	
}
