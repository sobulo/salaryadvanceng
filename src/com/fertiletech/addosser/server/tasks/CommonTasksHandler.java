/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fertiletech.addosser.server.tasks;

import java.io.IOException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fertiletech.addosser.server.ServiceImplUtilities;
import com.fertiletech.addosser.server.loanmktdata.ConsumerLoanComment;
import com.fertiletech.addosser.server.loanmktdata.ConsumerLoanSalesLead;
import com.fertiletech.addosser.server.loanmktdata.LoanApplicationFormData;
import com.fertiletech.addosser.server.loanmktdata.LoanMktDAO;
import com.fertiletech.addosser.server.login.LoginHelper;
import com.fertiletech.addosser.server.messaging.MessagingController;
import com.fertiletech.addosser.server.orbitdata.OrbitRimLocations;
import com.fertiletech.addosser.shared.FormConstants;
import com.google.appengine.api.datastore.GeoPt;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.GeocodingApiRequest;
import com.google.maps.model.GeocodingResult;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class CommonTasksHandler extends HttpServlet {
	static {
		LoanMktDAO.registerClassesWithObjectify();
	}

	private static final Logger log = Logger.getLogger(CommonTasksHandler.class.getName());

	private static GeoApiContext context = null;

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		log.warning("*******TASK EXECUTION BEGINNING!!!********");
		StringBuilder requestDetails = new StringBuilder(64);
		Enumeration<String> paramNames = req.getParameterNames();
		while (paramNames.hasMoreElements()) {
			String name = paramNames.nextElement();
			requestDetails.append("Param Name: " + name.toString() + " Values: ")
					.append(Arrays.toString(req.getParameterValues(name))).append("\n");
		}
		paramNames = req.getHeaderNames();
		while (paramNames.hasMoreElements()) {
			String name = paramNames.nextElement();
			requestDetails.append("Header Name: " + name.toString() + " Values: ").append(req.getHeader(name))
					.append("\n");
		}

		// log.warning(requestDetails.toString());

		Objectify ofy = ObjectifyService.begin();

		String service = req.getParameter(TaskConstants.SERVICE_TYPE).trim();
		validateParamName(service, "No service specified");

		if (service.equals(TaskConstants.SERVICE_SEND_MESSAGE)) {
			log.warning("Running send message task");
			// TODO, test to ensure this fails if duplicate tasks scheduled
			String controllerKeyStr = req.getParameter(TaskConstants.MSG_CONTROLLER_KEY_PARAM);
			String toAddress = req.getParameter(TaskConstants.TO_ADDR_PARAM);
			String[] messageBody = req.getParameterValues(TaskConstants.MSG_BODY_PARAM);
			String msgSubject = req.getParameter(TaskConstants.MSG_SUBJECT_PARAM);
			validateParamName(controllerKeyStr, "Value must be specified for message controller key");
			validateParamName(toAddress, "Value must be specified for recipeint address");
			validateParamName(messageBody, "Value must be specified for message body");
			validateParamName(msgSubject, "Value must be specified for subject field");

			Key<? extends MessagingController> controllerKey = ofy.getFactory().stringToKey(controllerKeyStr);

			MessagingController controller = ofy.find(controllerKey);
			if (controller == null)
				throw new IllegalStateException("Unable to find controller key");
			boolean status = controller.sendMessage(toAddress, messageBody, msgSubject);

			// TODO modify log to reflect byte size of entire messageBody object
			// rather than just text component
			String msgSuffix = "message of length " + messageBody[0].length() + " to " + toAddress
					+ ". Controller Type: " + controller.getClass().getSimpleName();
			if (status)
				log.warning("Successfully sent " + msgSuffix);
			else
				log.severe("Error sending " + msgSuffix);
		} else if (service.equals(TaskConstants.SERVICE_CREATE_COMMENT)) {
			log.warning("running create comment task");
			String loanKeyStr = req.getParameter(TaskConstants.LOAN_ID_PARAM);
			Key<ConsumerLoanSalesLead> leadKey = ofy.getFactory().stringToKey(loanKeyStr);
			String[] comments = req.getParameterValues(TaskConstants.MSG_BODY_PARAM);
			String updateUser = req.getParameter(TaskConstants.UPDATE_USR_PARAM);
			validateParamName(loanKeyStr, "value most be specified for loan id");
			validateParamName(comments, "at least 1 comment must be specified");
			String loginUser = null;
			try {
				validateParamName(updateUser, "ignore");
				loginUser = updateUser;
			} catch (IllegalArgumentException EX) {
				loginUser = LoginHelper.getLoggedInUser(req);
			}

			ConsumerLoanComment storedObj = LoanMktDAO.createComment(comments, false, leadKey, loginUser);
			ServiceImplUtilities.logSystemUpdate(log, storedObj.getKey(),
					" comments associated with: " + storedObj.getLoanKey());
		} else if (service.equals(TaskConstants.SERVICE_CHANGE_LOAN_STATE)) {
			log.warning("running change loan state");
			String loanKeyStr = req.getParameter(TaskConstants.LOAN_ID_PARAM);
			Key<ConsumerLoanSalesLead> leadKey = ofy.getFactory().stringToKey(loanKeyStr);
			LoanMktDAO.changeLoanState(leadKey, LoginHelper.getLoggedInUser(req));
		} else if (service.equals(TaskConstants.SERVICE_GEO_CODE)) {
			String loanKeyStr = req.getParameter(TaskConstants.LOAN_ID_PARAM);
			Key<ConsumerLoanSalesLead> leadKey = ofy.getFactory().stringToKey(loanKeyStr);
			ConsumerLoanSalesLead lead = ofy.get(leadKey);
			String address = lead.getAddress();
			/*
			 * if(address == null) { ///might be in form LoanApplicationFormData
			 * form = ofy.get(LoanApplicationFormData.getFormKey(leadKey));
			 * address = form.getFormData().get(FormConstants.ADDRESS_KEY); }
			 */
			GeoPt location = geoCodeAddress(address);
			if ( location == null)
				return;

			lead.setLatitude((double)location.getLatitude());
			lead.setLongitude((double)location.getLongitude());
			ofy.put(lead);
			}
			else if (service.equals(TaskConstants.SERVICE_ORBIT_GEO_CODE_RIM)) {
		    	OrbitRimLocations rimLocations = ofy.get(OrbitRimLocations.getKey());
		    	Integer updateIndex = rimLocations.getNextAvailableLocation();
		    	if(updateIndex == null) return; //end recursive scheduling
		    	String address = rimLocations.getAddress(updateIndex);
		    	GeoPt location = geoCodeAddress(address);
		    	boolean success = !(location == null);
		    	rimLocations.updateQueryState(updateIndex, success);
		    	if(success)
		    	{
		    		rimLocations.setGeoPt(updateIndex, location);
		    		log.warning("Orbit Mapping Task success for: [" + address + "] loc" + location);
		    	}
		    	{
		    		log.warning("Orbit Mapping failure for : " + address);
		    	}
		    	ofy.put(rimLocations);
				TaskQueueHelper.scheduleUpdateOrbitRim(); //we keep coming back, i.e. recursive scheduling
			
		} else if (service.equals(TaskConstants.SERVICE_BATCH_ADDRESS)) {
			String loanKeyStr = req.getParameter(TaskConstants.LOAN_ID_PARAM);
			Key<ConsumerLoanSalesLead> leadKey = ofy.getFactory().stringToKey(loanKeyStr);
			ConsumerLoanSalesLead lead = ofy.get(leadKey);
			String address = lead.getAddress();
			if (address != null && address.length() > 5)
				return;

			// might be in form
			LoanApplicationFormData form = ofy.get(LoanApplicationFormData.getFormKey(leadKey));
			address = form.getFormData().get(FormConstants.ADDRESS_KEY);

			if (address == null || address.length() < 5)
				return;

			lead.setAddress(address);
			ofy.put(lead);
		}

	}
	
	private GeoPt geoCodeAddress(String address)
	{
		if (address == null || address.length() < 5)
			return null;
		address = normalizeAddress(address);
		if (context == null)
			context = new GeoApiContext().setApiKey("AIzaSyAXX2MrHVh9nVHi9qMulyKkvU_hVZxCPi8");

		GeocodingApiRequest greq = GeocodingApi.geocode(context, address);
		GeocodingResult[] results = greq.awaitIgnoreError();
		if (results != null && results.length > 0)
			return new GeoPt((float) results[0].geometry.location.lat, 
					(float) results[0].geometry.location.lng);
		return null;
	}

	private String normalizeAddress(String addy) {
		addy = addy.toLowerCase();
		if (addy.contains("state"))
			return addy;
		if (addy.contains("lagos"))
			return addy;
		return addy + ". Lagos, Nigeria";
	};

	private void validateParamName(String param, String message) {
		if (param == null || param.length() == 0)
			throw new IllegalArgumentException(message);
	}

	private void validateParamName(String[] params, String message) {
		log.warning("PARAMS: " + params);
		if (params.length == 0 || params[0] == null || params[0].length() == 0)
			throw new IllegalArgumentException(message);
	}
}
