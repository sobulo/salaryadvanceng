/**
 * 
 */
package com.fertiletech.addosser.server.messaging;

import com.googlecode.objectify.Key;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class SendGridController extends MessagingController {
	// TODO see if we can use api key instead of appengine version
	public final static String API_KEY = "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";
	String bccAddress;

	SendGridController() {
		super();
	}

	SendGridController(String key, String fromAddress, String bccAddress, int characterLimit, int dailyMessageLimit,
			int totalMessageLimit) {
		super(key, fromAddress, characterLimit, dailyMessageLimit, totalMessageLimit);
		this.bccAddress = bccAddress;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see j9educationentities.GAEPrimaryKeyEntity#getKey()
	 */
	@Override
	public Key<SendGridController> getKey() {
		return (Key<SendGridController>) super.getKey();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * j9educationentities.messaging.MessagingController#sendMessage(java.lang.
	 * String, java.lang.String)
	 */
	@Override
	public boolean sendMessage(String toAddress, String[] messageContent, String subject) {

		try 
		{
			Sendgrid sendgrid = new Sendgrid("SalaryAdvance", "hello001");
			sendgrid.setTo(toAddress).setFrom(getFromAddress());
			if(bccAddress != null)
				sendgrid.setBcc(bccAddress);
			sendgrid.setSubject(subject).setText(messageContent[0]).setHtml(messageContent[1]);
			sendgrid.addCategory("Application Portal");
			sendgrid.send();
		} catch (Exception ex) {
			throw new RuntimeException(ex.fillInStackTrace());
		}
		return true;
	}

}
