/**
 * 
 */
package com.fertiletech.addosser.server.messaging;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import javax.persistence.Id;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.Transient;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Serialized;
import com.googlecode.objectify.annotation.Unindexed;


/**
 * @author Segun Razaq Sobulo
 *
 */
@Unindexed
@Cached(expirationSeconds=1200)
public abstract class MessagingController
{	
	@Id String key;
	private String fromAddress;
	private int characterLimit;
	private int dailyMessageLimit;
	private int totalMessageLimit;
	private Date lastAccessed;
	
	@Serialized
	private HashMap<Date, Integer> dailyMessageCount;
	
	@Transient
	private Date currentDate;

	MessagingController(){
		this.dailyMessageCount = new HashMap<Date, Integer>();
		populateTransientFields();
	};
	
	
	/**
	 * @param key
	 * @param fromAddress
	 * @param characterLimit
	 * @param dailyMessageLimit
	 * @param totalMessageLimit
	 * @param currentMessageCount
	 */
	MessagingController(String key, String fromAddress,
			int characterLimit, int dailyMessageLimit, int totalMessageLimit) {
		this();
		this.key = key;
		this.fromAddress = fromAddress;
		this.characterLimit = characterLimit;
		this.dailyMessageLimit = dailyMessageLimit;
		this.totalMessageLimit = totalMessageLimit;
	}


	/* (non-Javadoc)
	 * @see j9educationentities.GAEPrimaryKeyEntity#getKey()
	 */
	public Key<? extends MessagingController> getKey() {
		return new Key(this.getClass(), key);
	}
	
	public int getTotalSentMessages()
	{
		int total = 0;
		for(int sent : dailyMessageCount.values())
			total += sent;
		return total;
	}

	@PrePersist
	public void updateLastModification()
	{
		lastAccessed = new Date();
	}
	
	@PostLoad
	public void populateTransientFields()
	{
		currentDate = getTimeAtBegginingOfDay();
		Integer todaysTotal = dailyMessageCount.get(currentDate);
		if(todaysTotal == null)
			dailyMessageCount.put(currentDate, 0);
	}
	
	private Date getTimeAtBegginingOfDay()
	{
		Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
	}
	
	int updateNumberOfMessagesSent(int total)
	{
		return updateNumberOfMessagesSent(currentDate, total);
	}
	
	public String getFromAddress() {
		return fromAddress;
	}


	public int getCharacterLimit() {
		return characterLimit;
	}


	public int getDailyMessageLimit() {
		return dailyMessageLimit;
	}


	public int getTotalMessageLimit() {
		return totalMessageLimit;
	}


	public Date getLastAccessed() {
		return lastAccessed;
	}


	public HashMap<Date, Integer> getDailyMessageCount() {
		return dailyMessageCount;
	}


	public Date getCurrentDate() {
		return currentDate;
	}


	private int updateNumberOfMessagesSent(Date sendDate, int numOfMessages)
	{
		Integer currentTotal = dailyMessageCount.get(sendDate);
		if(currentTotal == null)
			return 0;
		currentTotal += numOfMessages;
		dailyMessageCount.put(sendDate, currentTotal);
		return currentTotal;
	}
	
	public abstract boolean sendMessage(String toAddress, String[] message, String subject);

}
