/**
 * 
 */
package com.fertiletech.addosser.server.messaging;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.googlecode.objectify.Key;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class EmailControllerDeprecated extends MessagingController{
	
	EmailControllerDeprecated() {
		super();
	}
	
	EmailControllerDeprecated(String key, String fromAddress,
			int characterLimit, int dailyMessageLimit, int totalMessageLimit)
	{
		super(key, fromAddress, characterLimit, dailyMessageLimit, totalMessageLimit);
	}

	/* (non-Javadoc)
	 * @see j9educationentities.GAEPrimaryKeyEntity#getKey()
	 */
	@Override
	public Key<EmailControllerDeprecated> getKey() {
		return (Key<EmailControllerDeprecated>) super.getKey();
	}

	/* (non-Javadoc)
	 * @see j9educationentities.messaging.MessagingController#sendMessage(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean sendMessage(String toAddress, String[] messageContent, String subject) {
		//TODO hack to prevent sending messages
		if(true)
			return true;
        Properties props = new Properties(); 
        Session session = Session.getDefaultInstance(props, null); 
        try
        {
        	//create multipart message
        	Multipart multipart = new MimeMultipart();
        	
        	//add plain text
        	MimeBodyPart textPart = new MimeBodyPart();
        	textPart.setContent(messageContent[0], "text/plain");
        	multipart.addBodyPart(textPart);
        	
        	//add html for clients that support it
        	MimeBodyPart htmlPart = new MimeBodyPart();
        	htmlPart.setContent(messageContent[1], "text/html");
        	multipart.addBodyPart(htmlPart);
        	
        	//setup the message
        	Message message = new MimeMessage(session);
        	message.setFrom(new InternetAddress(getFromAddress()));
        	message.addRecipient(Message.RecipientType.TO, new InternetAddress(toAddress));
        	message.setSubject(subject);
        	message.setContent(multipart);
        	
        	//send message
        	Transport.send(message);
        }
        catch(Exception ex)
        {
        	throw new RuntimeException(ex.fillInStackTrace());
        }
        return true;
	}

}
