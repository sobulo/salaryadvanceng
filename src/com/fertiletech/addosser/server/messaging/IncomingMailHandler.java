/**
 * 
 */
package com.fertiletech.addosser.server.messaging;

import java.io.IOException;
import java.util.Properties;
import java.util.logging.Logger;

import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fertiletech.addosser.server.loanmktdata.LoanMktDAO;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class IncomingMailHandler extends HttpServlet{
	static
	{
		LoanMktDAO.registerClassesWithObjectify();
	}
	private static final Logger log = Logger.getLogger(IncomingMailHandler.class.getName());
	
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
        Properties props = new Properties(); 
        Session session = Session.getDefaultInstance(props, null); 
        try 
        {
			MimeMessage message = new MimeMessage(session, req.getInputStream());
			Address[] addresses = message.getFrom();
			for(Address addy : addresses)
				log.warning("DISCARDING email received from: " + addy.toString());
		} catch (MessagingException e) {
			log.severe("Error occurred on inbound email message: " + e.getMessage());
		}		
	}
}
