/*
 * Copyright 2008 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.fertiletech.addosser.dashboard;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import com.fertiletech.addosser.client.GUIConstants;
import com.fertiletech.addosser.client.MyAsyncCallback;
import com.fertiletech.addosser.client.MyRefreshCallback;
import com.fertiletech.addosser.client.OAuthLoginService;
import com.fertiletech.addosser.client.utils.PanelUtilities;
import com.fertiletech.addosser.dashboard.charts.DbWelcomePanel;
import com.fertiletech.addosser.dashboard.display.DashboardDisplay;
import com.fertiletech.addosser.dashboard.display.DbAccessDeniedDisplay;
import com.fertiletech.addosser.dashboard.display.DisplayWidgets;
import com.fertiletech.addosser.shared.oauth.CWArguments;
import com.fertiletech.addosser.shared.oauth.ClientUtils;
import com.fertiletech.addosser.shared.oauth.ClientUtils.BankUserCookie;
import com.fertiletech.addosser.shared.oauth.SocialUser;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.prefetch.Prefetcher;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.HeadElement;
import com.google.gwt.dom.client.LinkElement;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.Event.NativePreviewEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;

public class DashboardWrapper {
	private HashSet<String> MENU_IGNORE = new HashSet<>();

	private final static String ADMIN_PAGE_URL_PREFIX = GWT.getHostPageBaseURL();
	private final static String ADMIN_PAGE_URL;
	static {
		if (!GWT.isProdMode()) // true if dev mode
			ADMIN_PAGE_URL = ADMIN_PAGE_URL_PREFIX + GUIConstants.GWT_CODE_SERVER;
		else
			ADMIN_PAGE_URL = ADMIN_PAGE_URL_PREFIX;
	}

	// different views
	Widget requestedPanel; // last panel requested as part of history token
	String requestArgs;

	private void fetchOpsUser() {
		if (!ClientUtils.alreadyLoggedIn()) {
			GWT.log("Not logged in, updating the app");
			updateAdminApp();
			return;
		}

		final MyAsyncCallback<SocialUser> ensureLoginCallback = new MyAsyncCallback<SocialUser>() {

			@Override
			public void onSuccess(SocialUser result) {
				GWT.log("Fetch returned, making a call to update the screen");
				updateAdminApp();
			}

			@Override
			public void onFailure(Throwable caught) {
				// async callback handles this already
			}

			@Override
			protected void callService(AsyncCallback<SocialUser> cb) {
				OAuthLoginService.Util.getInstance().fetchMe(ClientUtils.getSessionIdFromCookie(), cb);
			}
		};
		// GWT.log("Making call to verify staff credentials");
		ensureLoginCallback.go("Verifying staff credentials...");
	}

	static Widget denialScreen = null;

	public void showAdminScreen() {
		if (denialScreen == null)
			denialScreen = new DbAccessDeniedDisplay();
		display.setDisplay(denialScreen);
	}

	private void updateAdminApp() {
		if (ClientUtils.alreadyLoggedIn()) {
			// GWT.log("Fetching user cookie");
			BankUserCookie usercookie = ClientUtils.BankUserCookie.getCookie();
			if (!usercookie.isOps()) {
				DbWelcomePanel firstPanel = getFirstWidget();
				if (requestedPanel != firstPanel) {

					if (usercookie.getEmail().contains("@addosser.com")) {
						PanelUtilities.errorBox.show("You do not have necessary privileges to view this page."
								+ " Contact IT to request access.");

					} else {
						PanelUtilities.errorBox
								.show("You do not have necessary privileges to view this page. Only accounts under Addosser's domain"
										+ " may log in. If you have an Addosser google account, please log out of all your other Google accounts and try accessing the portal again");
					}

					showAdminScreen();
					return;
				}
			}
			boolean showAdmin = true;
			String email = usercookie.getEmail();
			email = email.trim().toLowerCase();
			for(String okMail: BoardUsers.BOARD_USERS)
			{
				if(okMail.equals(email))
					showAdmin = false;
			}
			if(showAdmin){
				showAdminScreen();
				return;
			}

		} else {
			GWT.log("Not logged in so setting to welcome screen");
			DbWelcomePanel firstPanel = getFirstWidget();
			requestedPanel = firstPanel;
			requestArgs = null;
		}
		loadNewPanel(); // whew, finally show the screen requested.
	}

	private void loadNewPanel() {
		GWT.log("LOAD Arg: " + requestArgs);
		GWT.log("Req P:" + requestedPanel);
		if (requestedPanel instanceof MyRefreshCallback) {
			GWT.log("calling cb from load panel");
			((MyRefreshCallback) requestedPanel).updateScreen();
		}

		// set conttent
		GWT.log("Setting the dispay");
		display.setDisplay(requestedPanel);
		if (requestedPanel instanceof CWArguments && requestArgs != null) {
			((CWArguments) requestedPanel).setParameterValues(requestArgs);
		}
		Window.setTitle("Addosser MFB - S.A.NG Dashbord");
	}

	/**
	 * This is the entry point method. At least it used to be, now we call it
	 * from PortalEntryPoint
	 */
	public DashboardWrapper() {
		MENU_IGNORE.add(DisplayWidgets.ORBIT_UPLOAD);
		MENU_IGNORE.add(DisplayWidgets.ORBIT_TABLE);

		GWT.log("setting up style sheets");
		// Inject global styles.
		injectThemeStyleSheet();

		GWT.log("Setting up handlers");
		setupHandlers();

		// Always prefetch.
		GWT.log("Prefetching app ...");
		Prefetcher.start();

		OAuthLoginService.Util.handleRedirect(new MyRefreshCallback() {

			@Override
			public void updateScreen() {
				updateAdminApp();
				// Show the initial example.
				if (History.getToken().length() > 0) {
					GWT.log("About to fire an initial history of: " + History.getToken());
					History.fireCurrentHistoryState();
				} else {
					String requestUrl = Window.Location.getParameter("history");
					if (requestUrl != null && requestUrl.length() > 0) {
						History.newItem(requestUrl);
					} else {
						// TODO: not sure below necessary, looks like welcome
						// panel gets created anyway
						History.newItem(DisplayWidgets.WELCOME);
					}
				}
			}
		});

	}

	DashboardDisplay display;
	private Timer playThread;
	Image currentlyShowing;
	Image play;
	Image pause;
	FocusPanel playPause ;
	private void setupHandlers() {
		// register tokens.
		GWT.log("About to create dashboard");
		display = new DashboardDisplay();
		//fetch player controls
		Widget[] playerWidgets = display.getPlayerWidgets();
		Image backward = (Image) playerWidgets[0];
		play = (Image) playerWidgets[1];
		pause = (Image) playerWidgets[2];
		playPause = (FocusPanel) playerWidgets[3];		
		Image forward = (Image) playerWidgets[4];
		currentlyShowing = play;
		
		//setup player handlers;
		//setup click handlers
		ValueChangeHandler<Integer> durationChangeHandler = new ValueChangeHandler<Integer>() {
			
			@Override
			public void onValueChange(ValueChangeEvent<Integer> event) {
				pauseSlide();
			}
		};
		display.setDurationChangeHandler(durationChangeHandler);
		
		playPause.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				if(currentlyShowing == play)
					playSlide();
				else
					pauseSlide();
				
			}
		});
		
		forward.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				moveSlide(true);	
			}
		});
		
		backward.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				moveSlide(false);
			}
		});
		GWT.log("succesfully created the dashboard");
		;
		boolean addDivider = false; // no seperator for the first menu item
		for (String s : DisplayWidgets.MENU_LIST) {
			if(MENU_IGNORE.contains(s))
				continue;
			if (addDivider)
				display.addDivider();
			display.addMenuItem(s);
			addDivider = true; // add a divider for all items except the first
								// one.
		}
		GWT.log("initializing the dashboard");
		RootLayoutPanel rootPanel = RootLayoutPanel.get();
		rootPanel.clear();
		rootPanel.add(display);
		GWT.log("Done with init");
		// Setup a history handler to reselect the associated widget.
		final ValueChangeHandler<String> historyHandler = new ValueChangeHandler<String>() {
			public void onValueChange(ValueChangeEvent<String> event) {
				GWT.log("HISTORY: " + event.getValue());
				String[] urlParams = event.getValue().split("/");
				String token = urlParams[0];
				String args = null;
				if (urlParams.length == 2 && urlParams[1].trim().length() > 0)
					args = urlParams[1].trim();
				//Window.alert("Handler TOKEN: " + token + " Args: " + args);
				displayContentWidget(token, args);
			}
		};
		History.addValueChangeHandler(historyHandler);
		
		//setup key handlers
		Event.addNativePreviewHandler(new Event.NativePreviewHandler() {
			@Override
			public void onPreviewNativeEvent(Event.NativePreviewEvent event) {
			    NativeEvent nativeEvent = event.getNativeEvent();
			    if ("keydown".equals(nativeEvent.getType())) 
			    {
			    	int kc = nativeEvent.getKeyCode();
			        if (kc == KeyCodes.KEY_UP) 
			            playSlide();
			        else if(kc == KeyCodes.KEY_DOWN)
			        	pauseSlide();			        
			        else if(kc == KeyCodes.KEY_LEFT)
			        	moveSlide(false);
			        else if(kc == KeyCodes.KEY_RIGHT)
			        	moveSlide(true);
			        
			        
			        	
			    }
			}
		});		
	}

	DbWelcomePanel firstPanel;

	public DbWelcomePanel getFirstWidget() {
		if (firstPanel == null)
			firstPanel = new DbWelcomePanel();
		return firstPanel;
	}

	/**
	 * Set the content to the {@link DbContentWidget}.
	 *
	 * @param content
	 *            the {@link DbContentWidget} to display
	 */
	private void displayContentWidget(String token, String args) {
		//token = token.toLowerCase();
		//token = token.replace(" ", "");

		/// GWT.log("Got a panel request for: [" + token + "] with args: " +
		/// args +
		// " contains: " + contentTokenMap.containsKey(token));
		// GWT.log("Present: " + contentTokenMap.containsKey(token));
		// GWT.log("Content Table: " + contentTokenMap.keySet())
		//Window.alert("DC Token is: " + token);
		if (DisplayWidgets.CONTENT_TOKEN_MAP.containsKey(token)) {
			requestArgs = args;

			Widget content = DisplayWidgets.CONTENT_TOKEN_MAP.get(token);
			if (content == null)
			{
				content = DisplayWidgets.fetchWidget(token);
				DisplayWidgets.CONTENT_TOKEN_MAP.put(token, content);
			}
			requestedPanel = content;
			// GWT.log("Got content: " + content.getClass());

		} else {
			// GWT.log("NO Content");
			requestArgs = null;
			requestedPanel = new HTML("Unrecongnized: " + token);
		}
		// GWT.log("Requested:" + requestedPanel.getClass());
		fetchOpsUser();
	}

	/**
	 * Convenience method for getting the document's head element.
	 *
	 * @return the document's head element
	 */
	private native HeadElement getHeadElement() /*-{
		return $doc.getElementsByTagName("head")[0];
	}-*/;

	/**
	 * Inject the GWT theme style sheet based on the RTL direction of the
	 * current locale.
	 */
	private void injectThemeStyleSheet() {
		String styleSheet = "../board/LoanMarketing.css";

		// Load the GWT theme style sheet
		String modulePath = GWT.getModuleBaseURL();
		LinkElement linkElem = Document.get().createLinkElement();
		linkElem.setRel("stylesheet");
		linkElem.setType("text/css");
		linkElem.setHref(modulePath + styleSheet);
		getHeadElement().appendChild(linkElem);
	}
	
	private String[] DASHBOARD_SCREEN_NAMES = {DisplayWidgets.WELCOME, DisplayWidgets.STATUS,
			DisplayWidgets.MAP, DisplayWidgets.SANG_SAL,DisplayWidgets.ORBIT_TENOR, DisplayWidgets.ORBIT_MAP, DisplayWidgets.ORBIT_AMT};
	
	int currentSlideIdx = 0;
	private void moveSlide(boolean isForward)
	{
		History.newItem(DASHBOARD_SCREEN_NAMES[currentSlideIdx]);
		if(isForward)
			currentSlideIdx = (currentSlideIdx + 1) % DASHBOARD_SCREEN_NAMES.length; 
		else
		{
			int temp = currentSlideIdx - 1;
			if(temp < 0)
				temp = DASHBOARD_SCREEN_NAMES.length - 1;
			currentSlideIdx = temp;
		}
	}
	
	private void startSlide()
	{
		playThread = new Timer() {
			
			@Override
			public void run() {
				moveSlide(true);
			}
		};
		playThread.scheduleRepeating(display.getDuration() * 1000);
	}
	
	private void playSlide()
	{
		if(currentlyShowing == play)
		{
			play.removeFromParent();
			playPause.add(pause);
			currentlyShowing = pause;
			if(playThread == null)
				startSlide();
		}		

	}
	
	private void pauseSlide()
	{
		if(currentlyShowing == pause)
		{
			pause.removeFromParent();
			playPause.add(play);
			currentlyShowing = play;
			if(playThread != null)
			{
				playThread.cancel();
				playThread = null;
			}
		}

	}
}
