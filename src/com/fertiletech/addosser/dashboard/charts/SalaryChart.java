package com.fertiletech.addosser.dashboard.charts;


import java.util.Date;
import java.util.List;
import java.util.TreeMap;

import com.fertiletech.addosser.client.GUIConstants;
import com.fertiletech.addosser.client.MyAsyncCallback;
import com.fertiletech.addosser.client.utils.PanelUtilities;
import com.fertiletech.addosser.dashboard.charts.MortgageChartPanel;
import com.fertiletech.addosser.dashboard.display.DisplayWidgets;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.googlecode.gwt.charts.client.ChartPackage;
import com.googlecode.gwt.charts.client.ChartWidget;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.corechart.LineChart;
import com.googlecode.gwt.charts.client.corechart.LineChartOptions;
import com.googlecode.gwt.charts.client.options.Animation;
import com.googlecode.gwt.charts.client.options.HAxis;
import com.googlecode.gwt.charts.client.options.Options;
import com.googlecode.gwt.charts.client.options.VAxis;

public class SalaryChart extends MortgageChartPanel {
	public SalaryChart() {
		super();
	
		GWT.log("I got called");
		// TODO Auto-generated constructor stub
	}

	@Override
	protected ChartWidget<? extends Options> getChart() {
		LineChart barChart = new LineChart();
		LineChartOptions options = LineChartOptions.create();
		Animation aniOpt = Animation.create();
		aniOpt.setDuration(5000);
		options.setAnimation(aniOpt);
		options.setEnableInteractivity(true);
		options.setFontName("Tahoma");
		options.setHAxis(HAxis.create("Date Requested"));
		options.setVAxis(VAxis.create("Applicant Salary"));
		options.setTitle("Orbit Loan Amounts");
		//Legend legend = Legend.create();
		//legend.setPosition(LegendPosition.NONE);
		//options.setLegend(legend);
		opt = options;
		
		return barChart;
	}

	@Override
	protected ChartPackage getChartType() {
		return ChartPackage.CORECHART;
	}

	@Override
	protected void drawChart(final Date startDate, final Date endDate) {
		MyAsyncCallback<List<String[]>> statusCallBack = new MyAsyncCallback<List<String[]>>() {

			@Override
			public void onFailure(Throwable caught) {
				GWT.log("Failed without showinb");
				PanelUtilities.errorBox.show("Failed to load chart");
			}

			@Override
			public void onSuccess(List<String[]> results) {
				GWT.log("Result: " + results.size());
				GWT.log("s: " + results.get(0).length);
				GWT.log("d: " + results.get(0).length);
				
				DateTimeFormat df = DateTimeFormat.getFormat("dd/MM/yy");
				TreeMap<Date, Double> sorter = new TreeMap<Date, Double>();
				String[] amountValuesStr = results.get(0);
				String[] dateValues = results.get(1);
				double totalCheck = 0;
				double[] rawAmounts = new double [amountValuesStr.length];
				
				double min = 0;
				double max = 0;
				String maxDtStr = null;
				String minDtStr = null;
				
				for(int i = 0; i < dateValues.length; i++)
				{
					Date key = df.parse(dateValues[i]);
					double amount = GUIConstants.DEFAULT_NUMBER_FORMAT.parse(amountValuesStr[i]);
					rawAmounts[i] = amount;
					totalCheck += amount;
					if(i == 0)
					{
						max = amount;
						min = amount;
						maxDtStr = dateValues[i];
						minDtStr = dateValues[i];
						
					}
					
					if(amount < min)
					{
						min = amount;
						minDtStr = dateValues[i];
					}
					
					if(amount > max)
					{
						max = amount;
						maxDtStr = dateValues[i];
					}
					
					if(sorter.containsKey(key))
							amount += sorter.get(key);
					sorter.put(key, amount);
				}
				 
				//GWT.log("Dates: " + dateValues.length);
				//GWT.log("Amounts: " + dateValues.length);
				DataTable dataTable = DataTable.create();
				dataTable.addColumn(ColumnType.DATE, "Date");
				dataTable.addColumn(ColumnType.NUMBER, "Amount");
				addStyleColumn(dataTable);
				dataTable.addRows(sorter.size());
			
				double total = 0;
				//GWT.log("Drawing line chart");
				int row = 0;
				double[] rawDaily = new double[sorter.size()];
				for(Date d : sorter.keySet())
				{
				
					try
					{
						dataTable.setValue(row, 0, d);

						
					double amt = sorter.get(d);
					total += amt;
					rawDaily[row] = amt;
					dataTable.setValue(row, 1, amt);
		
					row++;
					}catch(Exception ex)
					{
						GWT.log("Error parsing orbit feed at row " + row + " error message");
					}
				}
				
				//sanity check
				double diff = Math.abs(totalCheck - total);
				if(diff > 0.0001)
					Window.alert("Possible bug Total Check: " + totalCheck +" vs " + " Total: " + total);
				
				
				double loanAvg = total / dateValues.length;
				double loanAvgSD = GUIConstants.calculateSD(rawAmounts, loanAvg);
				double dailyAverage = total / sorter.size();
				double dailyAvgSD = GUIConstants.calculateSD(rawDaily, dailyAverage);
	
				String msg = "Total of " + GUIConstants.DEFAULT_NUMBER_FORMAT.format(total);
				msg += " Naira was requested over " + sorter.size() + " days.";
				msg += "<br/>Average/day: " + GUIConstants.DEFAULT_NUMBER_FORMAT.format(dailyAverage) +" Naira ";
				msg += " Standard Deviation: " + GUIConstants.DEFAULT_NUMBER_FORMAT.format(dailyAvgSD);
				msg += " <br/>Number of loans requested: " + dateValues.length;
				msg += " Average/request: " + GUIConstants.DEFAULT_NUMBER_FORMAT.format(loanAvg) + " Naira ";
				msg += " Standard Deviation: " +GUIConstants.DEFAULT_NUMBER_FORMAT.format(loanAvgSD);
				msg += " <br/>Minimum monthly salary: " +GUIConstants.DEFAULT_NUMBER_FORMAT.format(min) +" Naira on " + minDtStr;
				msg += "   Maximum monthly salary: " + GUIConstants.DEFAULT_NUMBER_FORMAT.format(max) + " Naira on " + maxDtStr;
				HTML display = new HTML(msg);
				footer.clear();
				footer.add(display);
				container.setWidgetSize(footer, 100);
				container.animate(1000);
				
				Date[] dates = dateSearchPanel.getSearchDates();
				String apdx = " [" +  GUIConstants.DEFAULT_DATE_TIME_FORMAT.format(dates[0])
						+ " to " + GUIConstants.DEFAULT_DATE_TIME_FORMAT.format(dates[1]) + "]";
		

				 ((LineChartOptions) opt ).setTitle(apdx);
				//PanelUtilities.infoBox.show("Average loan amount: " + average);
				((LineChart) chart).draw(dataTable, (LineChartOptions) opt);

			}

			@Override
			protected void callService(AsyncCallback<List<String[]>> cb) {
				GWT.log("Pinging sang ...");
				DisplayWidgets.DATA_SERVICE.getSangSalary(startDate, endDate, cb);
			}
		};
		statusCallBack.go("Requesting from sang data, please wait ...");
	}

	private native void addStyleColumn(DataTable data) /*-{
		data.addColumn({
			type : 'string',
			role : 'style'
		});
	}-*/;

}
