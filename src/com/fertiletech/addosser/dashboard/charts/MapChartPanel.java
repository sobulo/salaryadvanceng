package com.fertiletech.addosser.dashboard.charts;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.fertiletech.addosser.client.MyAsyncCallback;
import com.fertiletech.addosser.client.utils.PanelUtilities;
import com.fertiletech.addosser.dashboard.display.DisplayWidgets;
import com.fertiletech.addosser.shared.DTOConstants;
import com.fertiletech.addosser.shared.table.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.googlecode.gwt.charts.client.ChartPackage;
import com.googlecode.gwt.charts.client.ChartWidget;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.map.Map;
import com.googlecode.gwt.charts.client.map.MapOptions;
import com.googlecode.gwt.charts.client.options.MapType;
import com.googlecode.gwt.charts.client.options.Options;

public class MapChartPanel extends MortgageChartPanel {
	protected ChartPackage getChartType() {
		return ChartPackage.MAP;
	}

	protected ChartWidget<? extends Options> getChart() {
		final Map map = new Map();
		MapOptions options = MapOptions.create();
		options.setShowTip(true);
		options.setMapType(MapType.NORMAL);
		options.setUseMapTypeControl(true);
		opt = options;
		return map;
	}

	protected void drawChart(final Date startDate, final Date endDate) {
		final Map mapChart = (Map) chart;
		final ListBox categoryTypes = new ListBox();
		footer.clear();
		HorizontalPanel catPanel = new HorizontalPanel();
		catPanel.setSpacing(10);
		catPanel.add(new Label("Type: "));
		catPanel.add(categoryTypes);
		footer.add(catPanel);
		container.setWidgetSize(footer, 50);
		container.animate(1000);
		MyAsyncCallback<List<TableMessage>> mapCallBack = new MyAsyncCallback<List<TableMessage>>() {

			private String getSelectedCategory() {
				return categoryTypes.getValue(categoryTypes.getSelectedIndex());
			}

			private void drawMap(List<TableMessage> catList) {
				int[] uniqueCounts = new int[1];
				mapChart.draw(getTable(catList, uniqueCounts), (MapOptions) opt);
				mapChart.setTitle("Displaying " + uniqueCounts[0] + " entries for [" + getSelectedCategory() + "]");
			}

			@Override
			public void onSuccess(List<TableMessage> result) {
				// Prepare the data
				if (result.size() >= 1) {
					GWT.log("Num of consumer objects:" + result.get(0).getNumber(0));
					GWT.log("Num of with address:" + result.get(0).getNumber(1));
					GWT.log("Num that passed google lookup:" + result.get(0).getNumber(2));
					GWT.log("first 5  address lookups: " + 
					result.get(0).getText(0));
//					PanelUtilities.errorBox.show("No results found. Try widening specified date range");
					result.remove(0); //delete header
				}

				final HashMap<String, List<TableMessage>> categoryMap = categorizeData(result);
				categoryTypes.clear();
				int largestCategory = 0;
				int count = 0;
				int initIdx = 0;
				for (String type : categoryMap.keySet()) {
					if (categoryMap.get(type).size() > largestCategory) {
						largestCategory = categoryMap.get(type).size();
						initIdx = count;
					}
					categoryTypes.addItem(type);
					count++;
				}
				categoryTypes.addChangeHandler(new ChangeHandler() {

					@Override
					public void onChange(ChangeEvent event) {
						drawMap(categoryMap.get(getSelectedCategory()));
					}
				});
				categoryTypes.setSelectedIndex(initIdx);
				drawMap(categoryMap.get(getSelectedCategory()));
			}

			@Override
			public void onFailure(Throwable caught) {
				PanelUtilities.errorBox.show("Error occured on Adodsser cloud. Details: " + caught.getMessage());
			}

			@Override
			protected void callService(AsyncCallback<List<TableMessage>> cb) {
				DisplayWidgets.DATA_SERVICE.getMapAttachments(startDate, endDate, cb);
			}
		};
		mapCallBack.go("Fetching map data, please wait ...");
	}

	private HashMap<String, List<TableMessage>> categorizeData(List<TableMessage> locations) {
		HashMap<String, List<TableMessage>> result = new HashMap<String, List<TableMessage>>();
		for (TableMessage loc : locations) {
			String cat = loc.getText(DTOConstants.CATEGORY_IDX);
			List<TableMessage> catLocations = result.get(cat);
			if (catLocations == null)
				catLocations = new ArrayList<TableMessage>();
			catLocations.add(loc);
			result.put(cat, catLocations);
		}
		//PanelUtilities.infoBox.show("Retrieved " + result.size() + "categories");
		return result;
	}

	private DataTable getTable(List<TableMessage> result, int[] uniqueCounts) {
		HashSet<String> uniqueSet = new HashSet<String>();
		DataTable dataTable = DataTable.create();
		dataTable.addColumn(ColumnType.NUMBER, "A");
		dataTable.addColumn(ColumnType.NUMBER, "B");
		dataTable.addColumn(ColumnType.STRING, "Home");
		dataTable.addRows(result.size());
		String debugMsg="";
		for (int i = 0; i < result.size(); i++) {
			double lat = result.get(i).getNumber(DTOConstants.LAT_IDX);
			double lng = result.get(i).getNumber(DTOConstants.LNG_IDX);
			String key = lat + "," + lng;
			debugMsg += "[" + key + "] - ";
			if (uniqueSet.contains(key))
				continue;
			else
				uniqueSet.add(key);
			String detail = new HTML(result.get(i).getText(DTOConstants.DETAIL_IDX)).getText();
			dataTable.setValue(i, 0, lat);
			dataTable.setValue(i, 1, lng);
			dataTable.setValue(i, 2, detail);
		}
		uniqueCounts[0] = uniqueSet.size();
		GWT.log("Raw data size: " + result.size() + 
				" Unique Count: " + uniqueSet.size());
		GWT.log(debugMsg);
		return dataTable;
	}
}
