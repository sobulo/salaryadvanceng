package com.fertiletech.addosser.dashboard.charts.orbit;

import java.util.Date;
import java.util.List;
import java.util.TreeMap;

import com.fertiletech.addosser.client.GUIConstants;
import com.fertiletech.addosser.client.MyAsyncCallback;
import com.fertiletech.addosser.client.utils.PanelUtilities;
import com.fertiletech.addosser.dashboard.charts.MortgageChartPanel;
import com.fertiletech.addosser.dashboard.display.DisplayWidgets;
import com.fertiletech.addosser.dashboard.utils.BulkOrbitUploadPanel;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.googlecode.gwt.charts.client.ChartPackage;
import com.googlecode.gwt.charts.client.ChartWidget;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.corechart.LineChart;
import com.googlecode.gwt.charts.client.corechart.LineChartOptions;
import com.googlecode.gwt.charts.client.options.Animation;
import com.googlecode.gwt.charts.client.options.HAxis;
import com.googlecode.gwt.charts.client.options.Options;
import com.googlecode.gwt.charts.client.options.VAxis;

public class AmountLineChart extends MortgageChartPanel {
	public AmountLineChart() {
		super();
		dateSearchPanel.setVisible(false);
		periodList.setVisible(true);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected ChartWidget<? extends Options> getChart() {
		LineChart barChart = new LineChart();
		LineChartOptions options = LineChartOptions.create();
		Animation aniOpt = Animation.create();
		aniOpt.setDuration(5000);
		options.setAnimation(aniOpt);
		options.setEnableInteractivity(true);
		options.setFontName("Tahoma");
		options.setHAxis(HAxis.create("Date Granted"));
		options.setVAxis(VAxis.create("Loan Amount"));
		options.setTitle("Orbit Loan Amounts");
		//Legend legend = Legend.create();
		//legend.setPosition(LegendPosition.NONE);
		//options.setLegend(legend);
		opt = options;
		
		return barChart;
	}

	@Override
	protected ChartPackage getChartType() {
		return ChartPackage.CORECHART;
	}

	@Override
	protected void drawChart(final Date startIgnored, final Date endIgnored) {
		MyAsyncCallback<List<String[]>> statusCallBack = new MyAsyncCallback<List<String[]>>() {

			@Override
			public void onFailure(Throwable caught) {
				PanelUtilities.errorBox.show("Failed to load chart");
			}

			@Override
			public void onSuccess(List<String[]> results) {
				//Window.alert("Result: " + results.size());
				DateTimeFormat df = DateTimeFormat.getFormat("dd/MM/yy");
				TreeMap<Date, Double> sorter = new TreeMap<Date, Double>();
				String[] amountValuesStr = results.get(0);
				String[] dateValues = results.get(1);
				
				double totalCheck = 0;
				double min = 0;
				double max = 0;
				String minDtStr = null;
				String maxDtStr = null;
				
				double[] rawAmounts = new double [amountValuesStr.length];
				for(int i = 0; i < dateValues.length; i++)
				{
					Date key = df.parse(dateValues[i]);
					double amount = GUIConstants.DEFAULT_NUMBER_FORMAT.parse(amountValuesStr[i]);
					totalCheck += amount;
					rawAmounts[i] = amount;
					if(i == 0)
					{
						max = amount;
						min = amount;
						maxDtStr = dateValues[i];
						minDtStr = dateValues[i];
					}
					
					if(amount < min)
					{
						min = amount;
						minDtStr = dateValues[i];
						
					}
					
					if(amount > max)
					{
						max = amount;
						maxDtStr = dateValues[i];
					}
					
					if(sorter.containsKey(key))
						amount += sorter.get(key);
					sorter.put(key, amount);
					
				}
				 
				//Window.alert("Dates: " + dateValues.length);
				//Window.alert("Amounts: " + dateValues.length);
				DataTable dataTable = DataTable.create();
				dataTable.addColumn(ColumnType.DATE, "Date");
				dataTable.addColumn(ColumnType.NUMBER, "Amount");
				addStyleColumn(dataTable);
				dataTable.addRows(sorter.size());
			
				double total = 0;
				//Window.alert("Drawing line chart");
				int row = 0;
				double[] rawDaily = new double[sorter.size()];
				for(Date d : sorter.keySet())
				{
					dataTable.setValue(row, 0, d);	
					double amt = sorter.get(d);
					total += amt;
					rawDaily[row] = amt;
					dataTable.setValue(row, 1, amt);
					row++;
				}
				
				//sanity check
				double diff = Math.abs(totalCheck - total);
				if(diff > 0.0001)
					Window.alert("Possible bug Total Check: " + totalCheck +" vs " + " Total: " + total);
				

		
				//Window.alert("Dataset completed: " + average);
				double loanAvg = total / dateValues.length;
				double loanAvgSD = GUIConstants.calculateSD(rawAmounts, loanAvg);
				double dailyAverage = total / sorter.size();
				double dailyAvgSD = GUIConstants.calculateSD(rawDaily, dailyAverage);
				
				
	
				String msg = "Total of " + GUIConstants.DEFAULT_NUMBER_FORMAT.format(total);
				msg += " Naira disbursed over " + sorter.size() + " days.";
				msg += " <br/>Average/day: " + GUIConstants.DEFAULT_NUMBER_FORMAT.format(dailyAverage) + " Naira ";
				msg += " Standard Deviation: " + GUIConstants.DEFAULT_NUMBER_FORMAT.format(dailyAvgSD);
				msg += " <br/>Number of loans granted: " + dateValues.length;	 
				msg += " <br/>Average/grant: " + GUIConstants.DEFAULT_NUMBER_FORMAT.format(loanAvg) + " Naira ";
				msg += " Standard Deviation: " +GUIConstants.DEFAULT_NUMBER_FORMAT.format(loanAvgSD); 
				msg += " <br/>Minimum loan amount: " +GUIConstants.DEFAULT_NUMBER_FORMAT.format(min) +" Naira on " + minDtStr;
				msg += "   Maximum loan amount: " + GUIConstants.DEFAULT_NUMBER_FORMAT.format(max) + " Naira on " + maxDtStr;
				HTML display = new HTML(msg);
				footer.clear();
				footer.add(display);
				container.setWidgetSize(footer, 100);
				container.animate(1000);
				
				 ((LineChartOptions) opt ).setTitle("Orbit Loan Amounts " + 
				BulkOrbitUploadPanel.getPeriodDisplay(periodList));
				//PanelUtilities.infoBox.show("Average loan amount: " + average);
				((LineChart) chart).draw(dataTable, (LineChartOptions) opt);
			}

			@Override
			protected void callService(AsyncCallback<List<String[]>> cb) {
				DisplayWidgets.DATA_SERVICE.getOrbitAmounts(periodList.getSelectedValue(), cb);
			}
		};
		statusCallBack.go("Requesting orbit from server, please wait ...");
	}

	private native void addStyleColumn(DataTable data) /*-{
		data.addColumn({
			type : 'string',
			role : 'style'
		});
	}-*/;

}
