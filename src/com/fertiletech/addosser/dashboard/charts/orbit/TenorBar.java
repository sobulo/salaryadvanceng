package com.fertiletech.addosser.dashboard.charts.orbit;

import java.util.Date;
import java.util.HashMap;
import java.util.TreeMap;

import com.fertiletech.addosser.client.GUIConstants;
import com.fertiletech.addosser.client.MyAsyncCallback;
import com.fertiletech.addosser.client.utils.PanelUtilities;
import com.fertiletech.addosser.dashboard.charts.MortgageChartPanel;
import com.fertiletech.addosser.dashboard.display.DisplayWidgets;
import com.fertiletech.addosser.dashboard.utils.BulkOrbitUploadPanel;
import com.fertiletech.addosser.shared.workstates.WorkflowStateInstance;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.googlecode.gwt.charts.client.ChartPackage;
import com.googlecode.gwt.charts.client.ChartWidget;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.corechart.ColumnChart;
import com.googlecode.gwt.charts.client.corechart.ColumnChartOptions;
import com.googlecode.gwt.charts.client.corechart.LineChartOptions;
import com.googlecode.gwt.charts.client.event.SelectEvent;
import com.googlecode.gwt.charts.client.event.SelectHandler;
import com.googlecode.gwt.charts.client.options.Animation;
import com.googlecode.gwt.charts.client.options.Legend;
import com.googlecode.gwt.charts.client.options.LegendPosition;
import com.googlecode.gwt.charts.client.options.Options;

public class TenorBar extends MortgageChartPanel {

	public TenorBar() {
		super();
		dateSearchPanel.setVisible(false);
		periodList.setVisible(true);
	}

	@Override
	protected ChartWidget<? extends Options> getChart() {
		ColumnChart barChart = new ColumnChart();
		ColumnChartOptions options = ColumnChartOptions.create();
		Animation aniOpt = Animation.create();
		aniOpt.setDuration(5000);
		options.setAnimation(aniOpt);
		options.setEnableInteractivity(true);
		options.setFontName("Tahoma");
		options.setTitle("Orbit Tenor Application");
		Legend legend = Legend.create();
		legend.setPosition(LegendPosition.NONE);
		options.setLegend(legend);
		opt = options;
		barChart.addSelectHandler(new SelectHandler() {

			@Override
			public void onSelect(SelectEvent event) {
				// PanelUtilities.infoBox.show(event.getProperties().toString());
			}
		});
		return barChart;
	}

	@Override
	protected ChartPackage getChartType() {
		return ChartPackage.CORECHART;
	}

	// hack
	final static String[] COLORS = { "color: yellow", "color: #e072df", "color: blue", "color: red", "color: brown",
			"color: pink", "color: black", "color: green", "color: gold", "color: grey", "color: indigo",
			"color: purple", "color: orange", "color: lemon", "color: #63cd1b" };

	@Override
	protected void drawChart(final Date startIgnored, final Date endIgnored) {
		MyAsyncCallback<HashMap<String, Integer>> statusCallBack = new MyAsyncCallback<HashMap<String, Integer>>() {

			@Override
			public void onFailure(Throwable caught) {
				PanelUtilities.errorBox.show("Failed to load chart");
			}

			@Override
			public void onSuccess(HashMap<String, Integer> result) {
				DataTable dataTable = DataTable.create();
				dataTable.addColumn(ColumnType.STRING, "Tenor");
				dataTable.addColumn(ColumnType.NUMBER, "# of Applications");
				addStyleColumn(dataTable);
				dataTable.addRows(result.size());
				int row = 0;
				int min = -1;
				int max = -1;
				boolean first = true;
				String maxTr = null;
				String minTr = null;
				int total = 0;
				TreeMap<Integer, String> sorter = new TreeMap<Integer, String>();
				for (String tenor : result.keySet()) {
					int tenNum = Integer.valueOf(tenor.replace(" months", ""));
					sorter.put(tenNum, tenor);
				}
				for (Integer tenNum : sorter.keySet()) {
					String tenor = sorter.get(tenNum);

					int numOfOccurences = result.get(tenor);
					if (first) {
						min = numOfOccurences;
						max = numOfOccurences;
						maxTr = tenor;
						minTr = tenor;
						first = false;
					}
					if (numOfOccurences < min) {
						min = numOfOccurences;
						minTr = tenor;

					}
					if (numOfOccurences > max) {
						max = numOfOccurences;
						maxTr = tenor;

					}

					dataTable.setValue(row, 0, tenor);
					dataTable.setValue(row, 1, numOfOccurences);
					dataTable.setValue(row, 2, COLORS[row % COLORS.length]);
					row++;
					total += numOfOccurences;
				}
				((ColumnChart) chart).draw(dataTable, (ColumnChartOptions) opt);
				String summary = "Max: " + maxTr + " " + max + " loans<br/>";
				summary += "Min: " + minTr + " " + min + " loans<br/>";
				summary += "Number of loans granted: " + total;
				HTML display = new HTML(summary);
				footer.clear();
				footer.add(display);
				container.setWidgetSize(footer, 100);
				container.animate(1000);

				((ColumnChartOptions) opt)
						.setTitle("Orbit Tenors " + BulkOrbitUploadPanel.getPeriodDisplay(periodList));
			}

			@Override
			protected void callService(AsyncCallback<HashMap<String, Integer>> cb) {
				DisplayWidgets.DATA_SERVICE.getOrbitTenors(periodList.getSelectedValue(), cb);
			}
		};
		statusCallBack.go("Requesting orbit from server, please wait ...");
	}

	private native void addStyleColumn(DataTable data) /*-{
		data.addColumn({
			type : 'string',
			role : 'style'
		});
	}-*/;
}
