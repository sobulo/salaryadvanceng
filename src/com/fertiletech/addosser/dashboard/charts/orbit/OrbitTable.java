package com.fertiletech.addosser.dashboard.charts.orbit;

import java.util.List;

import com.fertiletech.addosser.client.utils.PanelUtilities;
import com.fertiletech.addosser.dashboard.display.DisplayWidgets;
import com.fertiletech.addosser.dashboard.utils.BulkOrbitUploadPanel;
import com.fertiletech.addosser.shared.table.TableMessage;
import com.fertiletech.addosser.table.ShowcaseTable;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.ResizeComposite;

public class OrbitTable extends ResizeComposite{
	HTML headerDisplay;
	ShowcaseTable table;
	private AsyncCallback<List<TableMessage>> tableDisplay = new AsyncCallback<List<TableMessage>>() {

		@Override
		public void onFailure(Throwable caught) {
		PanelUtilities.errorBox.show("Failed to fetch recent orbit upload");
			
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			if(result == null)
				PanelUtilities.errorBox.show("Warning null return");
			if(result.size() < 2)
				PanelUtilities.errorBox.show("Insufficiend data, found just " + result.size() + " rows");
			
			headerDisplay.setHTML("Found " + (result.size()-1) + " records recently uploaded from orbit <br/><i>" + 
		  result.get(0).getMessageId() + "</i>");
			table.showTable(result);
			
		}
	};
	
	public OrbitTable()
	{
		DockLayoutPanel container = new DockLayoutPanel(Unit.PX);
		ListBox periodList = new ListBox();
		BulkOrbitUploadPanel.initOrbitLsit(periodList);
		headerDisplay = new HTML("Loading, please wait ...");
		table = new ShowcaseTable();
		container.addNorth(periodList, 50);
		container.addNorth(headerDisplay, 50);
		container.add(table);
		initWidget(container);
		DisplayWidgets.DATA_SERVICE.getCurrentOrbitTable(periodList.getSelectedValue(), tableDisplay);
		periodList.addChangeHandler(new ChangeHandler() {
			
			@Override
			public void onChange(ChangeEvent event) {
				DisplayWidgets.DATA_SERVICE.getCurrentOrbitTable(periodList.getSelectedValue(), tableDisplay);
				
			}
		});
	}
}
