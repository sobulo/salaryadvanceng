/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fertiletech.addosser.dashboard.charts;

import com.fertiletech.addosser.client.MyRefreshCallback;
import com.fertiletech.addosser.client.OAuthLoginService;
import com.fertiletech.addosser.shared.oauth.ClientUtils;
import com.fertiletech.addosser.shared.oauth.ClientUtils.BankUserCookie;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.ResizeComposite;
import com.google.gwt.user.client.ui.SimpleLayoutPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Administrator
 */
public class DbWelcomePanel extends ResizeComposite implements MyRefreshCallback {
	@UiField
	ActivityCommentCellList activityFeed;

	@UiField
	SimpleLayoutPanel anchorSlot;
	@UiField
	SimpleLayoutPanel sideBar;
	@UiField
	DockLayoutPanel container;
	Anchor logOutAnchor;
	Anchor loginAnchor;
	final int FEED_WIDTH = 300;

	private static DbWelcomePanelUiBinder uiBinder = GWT.create(DbWelcomePanelUiBinder.class);

	interface DbWelcomePanelUiBinder extends UiBinder<Widget, DbWelcomePanel> {
	}

	public DbWelcomePanel() {
		initWidget(uiBinder.createAndBindUi(this));
		logOutAnchor = new Anchor(true);
		logOutAnchor.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				OAuthLoginService.Util.logout();
			}
		});
		loginAnchor = new Anchor(true);
		loginAnchor.setText("Click here to login with your company email");
		// GWT.log("Anchor: " + loginAnchor);
		loginAnchor.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				final int authProvider = ClientUtils.BOARD;
				OAuthLoginService.Util.getAuthorizationUrl(authProvider);
			}
		});
		container.setWidgetHidden(sideBar, true);
		// GWT.log("Exiting welcome constructor");
	}

	public void updateScreen() {
		anchorSlot.clear();
		String message = "<p>Hello";
		refreshFeed(false);
		if (ClientUtils.alreadyLoggedIn()) {
			BankUserCookie userCookie = ClientUtils.BankUserCookie.getCookie();
			message += " " + userCookie.getUserName() + ",</p>";
			logOutAnchor.setText("Click here to logout of your " + userCookie.getEmail() + " "
					+ ClientUtils.getAuthProviderNameFromCookie() + " account");
			anchorSlot.add(logOutAnchor);
			if (userCookie.isOps())
				refreshFeed(true);
		} else {
			message += ", </p>";
			anchorSlot.add(loginAnchor);
		}
		// displayMessage.setHTML(message);
	}

	public void refreshFeed(boolean show) {
		if (show) {
			activityFeed.refresh();
			container.setWidgetHidden(sideBar, false);
			container.setWidgetSize(sideBar, FEED_WIDTH);
			container.animate(3000);

		}
	}
}