package com.fertiletech.addosser.dashboard.charts;

import java.util.Date;

import com.fertiletech.addosser.client.utils.SearchDateBox;
import com.fertiletech.addosser.dashboard.utils.BulkOrbitUploadPanel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.ResizeComposite;
import com.google.gwt.user.client.ui.SimpleLayoutPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.gwt.charts.client.ChartLoader;
import com.googlecode.gwt.charts.client.ChartPackage;
import com.googlecode.gwt.charts.client.ChartWidget;
import com.googlecode.gwt.charts.client.options.Options;

public abstract class MortgageChartPanel extends ResizeComposite{

	@UiField
	protected DockLayoutPanel container;
	@UiField
	protected SimplePanel footer;
	@UiField
	protected SimpleLayoutPanel content;
	@UiField
	protected SearchDateBox dateSearchPanel;
	protected ChartWidget<? extends Options> chart;
	protected Options opt;
	@UiField
	protected ListBox periodList;

	private static MortgageChartPanelUiBinder uiBinder = GWT
			.create(MortgageChartPanelUiBinder.class);

	interface MortgageChartPanelUiBinder extends
			UiBinder<Widget, MortgageChartPanel> {
	}

	public MortgageChartPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		dateSearchPanel.setEnabled(false);
		container.setWidgetSize(footer, 0);
		container.animate(1000);
		loadChartAPI();
		BulkOrbitUploadPanel.initOrbitLsit(periodList);
		periodList.addChangeHandler(new ChangeHandler() {
			
			@Override
			public void onChange(ChangeEvent event) {
				drawChart(null, null); //orbit charts ignore date arguments
				
				
			}
		});
		periodList.setVisible(false);
	}
	

	/**
	 * This is the entry point method.
	 */
	private void loadChartAPI() {
		// Create the API Loader
		final ChartPackage type = getChartType();
		//map key
		//new ApiLoader("");
		GWT.log("TYPE: " + type);
		ChartLoader chartLoader = new ChartLoader(type);
		chartLoader.loadApi(new Runnable() {

			@Override
			public void run() {
				GWT.log("Running for: " + type);
				chart = getChart();
				GWT.log("Adding cnart for: " + type);
				content.add(chart);
				GWT.log("Enabling buttons: " + type);
				dateSearchPanel.setEnabled(true);
				GWT.log("Conduct initial search");
				Date initialDates[] = dateSearchPanel.getSearchDates();
				if(initialDates != null) //sanity check, no user interaction at this point so should always be a non null value
					drawChart(initialDates[0], initialDates[1]);
				dateSearchPanel.addClickHandler(new ClickHandler() {
					
					@Override
					public void onClick(ClickEvent event) {
						Date[] queryDates = dateSearchPanel.getSearchDates();
						if(queryDates == null) return;
						drawChart(queryDates[0], queryDates[1]);
					}
				});				
			}
		});
	}

	abstract protected ChartWidget<? extends Options> getChart();
	abstract protected ChartPackage getChartType();
	abstract protected void drawChart(final Date startDate, final Date endDate);

}
