package com.fertiletech.addosser.dashboard.charts;

import java.util.Date;
import java.util.HashMap;

import com.fertiletech.addosser.client.GUIConstants;
import com.fertiletech.addosser.client.MyAsyncCallback;
import com.fertiletech.addosser.client.utils.PanelUtilities;
import com.fertiletech.addosser.dashboard.display.DisplayWidgets;
import com.fertiletech.addosser.shared.workstates.WorkflowStateInstance;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.googlecode.gwt.charts.client.ChartPackage;
import com.googlecode.gwt.charts.client.ChartWidget;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.corechart.ColumnChart;
import com.googlecode.gwt.charts.client.corechart.ColumnChartOptions;
import com.googlecode.gwt.charts.client.event.SelectEvent;
import com.googlecode.gwt.charts.client.event.SelectHandler;
import com.googlecode.gwt.charts.client.options.Animation;
import com.googlecode.gwt.charts.client.options.Legend;
import com.googlecode.gwt.charts.client.options.LegendPosition;
import com.googlecode.gwt.charts.client.options.Options;

public class StatusChartPanel extends MortgageChartPanel{

	@Override
	protected ChartWidget<? extends Options> getChart() {
		ColumnChart barChart = new ColumnChart();
		ColumnChartOptions options = ColumnChartOptions.create();
		Animation aniOpt = Animation.create();
		aniOpt.setDuration(5000);
		options.setAnimation(aniOpt);
		options.setEnableInteractivity(true);
		options.setFontName("Tahoma");
		Date[] dates = dateSearchPanel.getSearchDates();
		options.setTitle("Online Application Status (" + GUIConstants.DEFAULT_DATE_TIME_FORMAT.format(dates[0])
				+ " to " + GUIConstants.DEFAULT_DATE_TIME_FORMAT.format(dates[1]) + ")");
		Legend legend = Legend.create();
		legend.setPosition(LegendPosition.NONE);
		options.setLegend(legend);
		opt = options;
		barChart.addSelectHandler(new SelectHandler() {
			
			@Override
			public void onSelect(SelectEvent event) {
				//PanelUtilities.infoBox.show(event.getProperties().toString());
			}
		});
		return barChart;
	}

	@Override
	protected ChartPackage getChartType() {
		return ChartPackage.CORECHART;
	}

	@Override
	protected void drawChart(final Date startDate, final Date endDate) {
		MyAsyncCallback<HashMap<WorkflowStateInstance, Integer>> statusCallBack = new MyAsyncCallback<HashMap<WorkflowStateInstance,Integer>>() {

			@Override
			public void onFailure(Throwable caught) {
				PanelUtilities.errorBox.show("Failed to load chart");
			}

			@Override
			public void onSuccess(HashMap<WorkflowStateInstance, Integer> result) {
				DataTable dataTable = DataTable.create();
				dataTable.addColumn(ColumnType.STRING, "Status");
				dataTable.addColumn(ColumnType.NUMBER, "# of Applications");
				addStyleColumn(dataTable);
				dataTable.addRows(result.size());
				int row = 0;
				int total = 0;
				for(WorkflowStateInstance status : result.keySet())
				{
					int numApps = result.get(status);
					dataTable.setValue(row, 0, status.toString());
					dataTable.setValue(row, 1, numApps);
					dataTable.setValue(row, 2, status.getChartColor());
					row++;
					total += numApps;
				}
				((ColumnChart) chart).draw(dataTable, (ColumnChartOptions) opt);
				
				Date[] dates = dateSearchPanel.getSearchDates();
				((ColumnChartOptions) opt).setTitle("Online Application Status (" + GUIConstants.DEFAULT_DATE_TIME_FORMAT.format(dates[0])
						+ " to " + GUIConstants.DEFAULT_DATE_TIME_FORMAT.format(dates[1]) + ")");
				
			String msg = "Total Number of Applications: " + 
				GUIConstants.DEFAULT_INTEGER_FORMAT.format(total);
			footer.clear();
			footer.add(new HTML(msg));
			container.setWidgetSize(footer, 20);
			container.animate(1000);
			}

			@Override
			protected void callService(
					AsyncCallback<HashMap<WorkflowStateInstance, Integer>> cb) {
				DisplayWidgets.DATA_SERVICE.getLeadAggregates(startDate, endDate, cb);
				
			}
		};
		statusCallBack.go("Requesting aggregated data from server, please wait ...");
	}
	
	private native void addStyleColumn(DataTable data) /*-{
    data.addColumn({type:'string', role:'style'});
}-*/;
}
