package com.fertiletech.addosser.dashboard;

import com.fertiletech.addosser.client.GUIConstants;
import com.fertiletech.addosser.client.utils.PanelUtilities;
import com.fertiletech.addosser.dashboard.charts.DbWelcomePanel;
import com.fertiletech.addosser.dashboard.utils.BulkOrbitUploadPanel;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.google.gwt.user.client.ui.RootPanel;


/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class DashboardEntryPoint implements EntryPoint {
	
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() 
	{
		GWT.log("loading the module");
		Panel rootPanel = RootPanel.get(GUIConstants.ROOT_ID_DASHBOARD);
		
		if(rootPanel == null)
		{
			Window.alert("Code download of root panel failed, try refreshing the page." +
			" Please contact info@fertiletech.com if problem persists");
			return;
		}
		else
			rootPanel.getElement().setInnerHTML("");
		GWT.log("Setting up the module");
		setupAdminArea();
	}
			
	public void setupAdminArea()
	{
		GWT.runAsync(new RunAsyncCallback() {
			
			@Override
			public void onSuccess() {
				//refetch the panel using the more advanced layout panel
				//RootLayoutPanel rootLayoutPanel = RootLayoutPanel.get();
				//rootLayoutPanel.clear();
				//rootLayoutPanel.add(new HTML("Hello to my World!!"));
				//DbWelcomePanel display = new DbWelcomePanel();
				//display.updateScreen();
				//rootLayoutPanel.add(new BulkOrbitUploadPanel());
				GWT.log("Creating Dashboard Wrapper");
				new DashboardWrapper();
			}
			
			@Override
			public void onFailure(Throwable reason) {
				PanelUtilities.errorBox.show("Unable to refresh the page, try refreshing the page." +
						" Please contact info@fertiletech.com if problem persists");
				
			}
		});		
	}	
}
