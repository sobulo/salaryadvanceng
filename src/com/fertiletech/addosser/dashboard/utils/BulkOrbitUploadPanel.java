/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fertiletech.addosser.dashboard.utils;

import com.fertiletech.addosser.client.utils.PanelUtilities;
import com.fertiletech.addosser.client.utils.SimpleDialog;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.ResizeComposite;
import com.google.gwt.user.client.ui.SimpleLayoutPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
/**
 *
 * @author Administrator
 */
public class BulkOrbitUploadPanel extends ResizeComposite implements 
        ClickHandler, FormPanel.SubmitCompleteHandler{
    
    private final static String LOAD_PARAM_NAME = "loadData";
    private final static String GOOD_DATA_PREFIX = "<i></i>";
    public final static String INIT_DISPLAY = "Please choose a spreadsheet " +
            "and hit submit to upload</b>";
    private final static String CLASS_KEY_NAME = "classkey";
    
    
    @UiField FileUpload selectFile;
    @UiField Button submitFile;
    @UiField FormPanel studentUploadForm;
    @UiField HTML formResponse;
    @UiField SimpleLayoutPanel topLevelPanel;
    @UiField ListBox classList;
    @UiField Hidden classParam;

    FormPanel confirmationForm;
    Button confirmData;
    Button resetData;
    //HTML confirmDisplay;
    private SimpleDialog errorBox;

    private static BulkOrbitUploadPanelUiBinder uiBinder = GWT.create(BulkOrbitUploadPanelUiBinder.class);

    interface BulkOrbitUploadPanelUiBinder extends UiBinder<Widget, BulkOrbitUploadPanel> {
    }
    
    final static String BSC = "BASIC";

    private final static String QUERY_TYPES[] ={BSC, "Quarter 1 - 2018", "Quarter 2 - 2018"};
    private final static String ACTION_NAME = "/orbitupload";
    
    public static void initOrbitLsit(ListBox l)
    {
        for (String type : QUERY_TYPES)
        	l.addItem(type);	
    }
    
    public static String getPeriodDisplay(ListBox l)
    {
		String apdx = l.getSelectedValue();
		if (apdx.equals(BSC))
			apdx = " [Sep - Nov 2017]";
		else
			apdx += " [" + apdx +"]";
		return apdx;
    }
    
    public BulkOrbitUploadPanel()
    {
        initWidget(uiBinder.createAndBindUi(this));
        initOrbitLsit(classList);

        classParam.setName(CLASS_KEY_NAME);
        selectFile.setName("chooseFile");
        submitFile.addClickHandler(this);
        studentUploadForm.setAction(ACTION_NAME);
        studentUploadForm.addSubmitCompleteHandler(this);
        studentUploadForm.setMethod("post");
        studentUploadForm.setEncoding(FormPanel.ENCODING_MULTIPART);

        confirmationForm = new FormPanel();
        FlowPanel formComponents = new FlowPanel();
        confirmationForm.setAction(ACTION_NAME);
        confirmationForm.setMethod("post");
        confirmationForm.addSubmitCompleteHandler(this);
        confirmData = new Button("Save Loaded Data");
        resetData = new Button("Load new File");
        confirmData.addClickHandler(this);
        resetData.addClickHandler(this);
        //confirmDisplay = new HTML();
        formComponents.add(new Hidden(LOAD_PARAM_NAME));
        //formComponents.add(confirmDisplay);
        formComponents.add(confirmData);
        formComponents.add(resetData);
        confirmationForm.add(formComponents);
        
        errorBox = PanelUtilities.errorBox;
    }

    public void resetPanel()
    {
        studentUploadForm.reset();
        confirmationForm.reset();
        formResponse.setHTML(INIT_DISPLAY);
        topLevelPanel.remove(topLevelPanel.getWidget());
        topLevelPanel.add(studentUploadForm);
    }

    public void onClick(ClickEvent event)
    {
        if(event.getSource().equals(submitFile))
        {
        	if(!selectFile.getFilename().endsWith("xls"))
        	{
        		errorBox.show("Please select an excel 97-2003 worksheet (.xls extension)");
        		return;
        	}
        	classParam.setValue(classList.getSelectedValue());
            formResponse.setHTML("<b>Sending file to server ...</b>");
            studentUploadForm.submit();
        }
        else if(event.getSource().equals(confirmData))
        {
            //confirmDisplay.setHTML("<b>Saving confirmed data ...</b>");
        	formResponse.setHTML("<b>Saving confirmed data ...</b>");
            confirmationForm.submit();
        }
        else if(event.getSource().equals(resetData))
        {
            resetPanel();
        }
    }

    public void onSubmitComplete(SubmitCompleteEvent event)
    {
        String results = event.getResults();
        //confirmDisplay.setHTML(event.getResults());
        formResponse.setHTML(event.getResults());
        if(results.startsWith(GOOD_DATA_PREFIX)) //hacky way 2 determine state
            confirmData.setVisible(true);
        else
            confirmData.setVisible(false);
        if(topLevelPanel.getWidget().equals(studentUploadForm))
        {
            topLevelPanel.remove(studentUploadForm);
            topLevelPanel.add(confirmationForm);
        }
    }


}