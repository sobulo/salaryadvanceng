package com.fertiletech.addosser.dashboard.display;

import com.google.gwt.resources.client.CssResource;

public interface DashCss extends CssResource{
	String dashMenu();
	String dashSeperators();
}
