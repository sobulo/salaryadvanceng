package com.fertiletech.addosser.dashboard.display;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ResizeComposite;
import com.google.gwt.user.client.ui.SimpleLayoutPanel;
import com.google.gwt.user.client.ui.SplitLayoutPanel;
import com.google.gwt.user.client.ui.Widget;

public class DashboardDisplay extends ResizeComposite {

	@UiField
	SimpleLayoutPanel contentPanel;

	@UiField
	HorizontalPanel menuSlot;
	
	//slide show widgets
	@UiField
	Image backward;
	
	@UiField
	Image forward;;
	
	@UiField
	Image play;
	
	@UiField
	FocusPanel playPause;
	
	@UiField
	IntegerBox duration;
	
	@UiField
	SplitLayoutPanel dashboard;
	
	Image pause;
	
	private static DashboardDisplayUiBinder uiBinder = GWT.create(DashboardDisplayUiBinder.class);

	interface DashboardDisplayUiBinder extends UiBinder<Widget, DashboardDisplay> {
	}

	DashBoardResources res = GWT.create(DashBoardResources.class);

	public DashboardDisplay() {
		GWT.log("Setting up UI binder for display");

		initWidget(uiBinder.createAndBindUi(this));
		pause = new Image(res.pause());
		duration.setVisibleLength(2);

		GWT.log("Display binded succesfully");
		res.css().ensureInjected();
		menuSlot.setSpacing(5);
		GWT.log("Display built completed");
	}

	public void setDisplay(Widget w) {
		contentPanel.clear();
		contentPanel.add(w);
		dashboard.setWidgetHidden(contentPanel, true);
		dashboard.setWidgetHidden(contentPanel, false);
		dashboard.animate(3000);
		
		
	}

	public void addMenuItem(String token) {
		Hyperlink link = new Hyperlink(token, token);
		link.addStyleName(res.css().dashMenu());
		menuSlot.setCellWidth(link,"100px");
		menuSlot.add(link);

	}

	public void addDivider() {
		HTML divider = new HTML("/");
		divider.addStyleName(res.css().dashSeperators());
		menuSlot.add(divider);
	}
	
	public Widget[] getPlayerWidgets()
	{
		Widget[] result=  {backward, play, pause, playPause, forward};
		return result;
	}
	
	public int getDuration()
	{
		Integer elapseTime =  duration.getValue();
		if(elapseTime == null || elapseTime < 10)
			duration.setValue(30);
		return duration.getValue();
	}
	
	public void setDurationChangeHandler(ValueChangeHandler<Integer> h)
	{
		duration.addValueChangeHandler(h);
	}

}
