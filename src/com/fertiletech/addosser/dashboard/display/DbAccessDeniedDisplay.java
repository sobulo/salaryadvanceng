package com.fertiletech.addosser.dashboard.display;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class DbAccessDeniedDisplay extends Composite {

	private static DbAccessDeniedDisplayUiBinder uiBinder = GWT
			.create(DbAccessDeniedDisplayUiBinder.class);

	interface DbAccessDeniedDisplayUiBinder extends
			UiBinder<Widget, DbAccessDeniedDisplay> {
	}

	public DbAccessDeniedDisplay() {
		initWidget(uiBinder.createAndBindUi(this));
	}

}
