package com.fertiletech.addosser.dashboard.display;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import com.fertiletech.addosser.client.LoanMktService;
import com.fertiletech.addosser.client.LoanMktServiceAsync;
import com.fertiletech.addosser.client.LoginService;
import com.fertiletech.addosser.client.LoginServiceAsync;
import com.fertiletech.addosser.dashboard.charts.DbWelcomePanel;
import com.fertiletech.addosser.dashboard.charts.MapChartPanel;
import com.fertiletech.addosser.dashboard.charts.SalaryChart;
import com.fertiletech.addosser.dashboard.charts.StatusChartPanel;
import com.fertiletech.addosser.dashboard.charts.orbit.AmountLineChart;
import com.fertiletech.addosser.dashboard.charts.orbit.OrbitMap;
import com.fertiletech.addosser.dashboard.charts.orbit.OrbitTable;
import com.fertiletech.addosser.dashboard.charts.orbit.TenorBar;
import com.fertiletech.addosser.dashboard.utils.BulkOrbitUploadPanel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Widget;

public class DisplayWidgets {

	public final static String WELCOME = "SANG-Act";
	public final static String STATUS = "SANG-Chart";
	public final static String MAP = "SANG-Map";
	public final static String ORBIT_UPLOAD = "Orb-Up";
	public final static String ORBIT_TENOR = "Orb-Tnr";
	public final static String ORBIT_MAP = "Orb-Map";
	public final static String ORBIT_TABLE = "Orb-Tab";
	public final static String ORBIT_AMT = "Orb-Amt";
	public final static String SANG_SAL = "SANG-Sal";
	public final static String[] MENU_LIST = {WELCOME, STATUS, MAP, SANG_SAL, ORBIT_AMT, ORBIT_TENOR, ORBIT_MAP, ORBIT_TABLE, ORBIT_UPLOAD};
    public final static  Map<String, Widget> CONTENT_TOKEN_MAP = new HashMap<String, Widget>();	
    
    static
    {
    	for (String menu : MENU_LIST)
    		CONTENT_TOKEN_MAP.put(menu, null);
    }
	public final static LoginServiceAsync READ_SERVICE = GWT.create(LoginService.class);
	public final static LoanMktServiceAsync DATA_SERVICE = GWT.create(LoanMktService.class);

	public static Widget fetchWidget(String id) {
		if (id.equals(WELCOME))
			return new DbWelcomePanel();
		if (id.equals(STATUS))
			return new StatusChartPanel();
		if( id.equals(MAP))
			return new MapChartPanel();
		if( id.equals(SANG_SAL))
			return new SalaryChart();
		if( id.equals(ORBIT_UPLOAD))
			return new BulkOrbitUploadPanel();
		if( id.equals(ORBIT_TENOR))
			return new TenorBar();	
		if( id.equals(ORBIT_MAP))
			return new OrbitMap();	
		if( id.equals(ORBIT_TABLE))
			return new OrbitTable();
		if( id.equals(ORBIT_AMT))
			return new AmountLineChart();
		if( id.equals(ORBIT_AMT))
			return new AmountLineChart();
		return null;
	}

	public static LoginServiceAsync getReadService() {
		return READ_SERVICE;
	}

}
