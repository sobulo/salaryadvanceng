package com.fertiletech.addosser.dashboard.display;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface DashBoardResources extends ClientBundle{
	ImageResource addosser2();
	ImageResource welcome();
	
	//dashboard slide controls
	ImageResource back();
	ImageResource forward();
	ImageResource play();
	ImageResource pause();
	@Source
	("dashboard.css")
	DashCss css();	
}
