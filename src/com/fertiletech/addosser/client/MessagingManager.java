package com.fertiletech.addosser.client;

import java.util.HashMap;
import java.util.List;

import com.fertiletech.addosser.shared.FormConstants;
import com.fertiletech.addosser.shared.exceptions.MissingEntitiesException;
import com.fertiletech.addosser.shared.table.TableMessage;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("../messaging")
public interface MessagingManager extends RemoteService {
	
	public void sendMessage(FormConstants.CustomMessageTypes msgType, String[] addresses, String[] message, boolean isEmail)
		throws MissingEntitiesException;
	
	public HashMap<String, String> getMessagingControllerNames();
	
	public List<TableMessage> getControllerDetails(String controllerName) throws MissingEntitiesException;
}
