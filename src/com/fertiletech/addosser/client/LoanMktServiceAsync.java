/**
 * 
 */
package com.fertiletech.addosser.client;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.fertiletech.addosser.shared.table.TableMessage;
import com.fertiletech.addosser.shared.table.TableMessageHeader;
import com.fertiletech.addosser.shared.workstates.WorkflowStateInstance;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * @author Segun Razaq Sobulo
 *
 */
public interface LoanMktServiceAsync {

	/**
	 * 
	 * @see com.fertiletech.addosser.client.LoanMktService#getParameters()
	 */
	void getParameters(AsyncCallback<HashMap<String, String>> callback);

	void getSaleLeads(Date startDate, Date endDate, boolean useOpsQueue,
			AsyncCallback<List<TableMessage>[]> callback);

	/**
	 * 
	 * @see com.fertiletech.addosser.client.LoanMktService#updateParameters(java.util.HashMap)
	 */
	void updateParameters(HashMap<String, String> parameters,
			AsyncCallback<Void> callback);

	void getStoredLoanApplication(String formKeyStr,
			AsyncCallback<HashMap<String, String>> callback);

	void getLoanApplicationWithLoanID(String loanKeyStr,
			AsyncCallback<HashMap<String, String>> callback);	
	
	void storeLoanApplicationData(HashMap<String, String> appData,
			String loanKeyStr, boolean isSubmit, AsyncCallback<Void> callback);

	void getStoredGuarantorApplication(String loanKeyStr,
			AsyncCallback<HashMap<String, String>> callback);

	void storeGurantorApplicationData(HashMap<String, String> appData,
			String guarantorID, boolean isSubmit,
			AsyncCallback<String[]> callback);

	void loadFormDisplay(String formID, boolean isFormKey,
			AsyncCallback<String> callback);

	void getLoanID(AsyncCallback<String[]> callback);

	void loadSaleLeadComments(String leadID, AsyncCallback<String> callback);

	void saveLoanComment(String loanKeyStr, String text, boolean showPublic,
			AsyncCallback<Void> callback);

	void updateOpsQueue(String leadKey, String appStatus, String comments, AsyncCallback<Void> callback);

	void setCustomerAck(String leadKey, AsyncCallback<Void> callback);

	void getAppointmentDate(String leadKey, AsyncCallback<Date> callback);

	void setAppointmentDate(String leadKey, Date meetingDate, AsyncCallback<Void> callback);

	void getCustomerAck(String leadKey, AsyncCallback<Boolean> callback);

	void okToPrint(String loanID, AsyncCallback<Boolean> callback);

	void getNewLoanIDs(AsyncCallback<HashMap<String, String>> callback);

	void getAllSalesLeadAsCSV(AsyncCallback<String> callback);

	void startLoanApplication(HashMap<String, String> appData,
			AsyncCallback<String[]> callback);

	void getLoanState(String loanID, AsyncCallback<TableMessage> callback);

	void saveLoanApplicationData(HashMap<String, String> appData,
			String loanKeyStr, boolean isSubmit,
			AsyncCallback<String[]> callback);

	void getApplicationFormDownloadLink(String formID,
			AsyncCallback<String> callback);

	void getGuarantorFormDownloadLink(String formID,
			AsyncCallback<String> callback);

	void getRequirementsChecklistLink(String loanID,
			AsyncCallback<String> callback);

	void fetchGenericExcelLink(List<TableMessage> data,
			TableMessageHeader header, AsyncCallback<String> callback);

	void getLoanState(long loanID, AsyncCallback<TableMessage> callback);

	void getLeadAggregates(Date startDate, Date endDate,
			AsyncCallback<HashMap<WorkflowStateInstance, Integer>> callback);

	void getMapAttachments(Date startDate, Date endDate, AsyncCallback<List<TableMessage>> callback);

	void getOrbitTenors(String period, AsyncCallback<HashMap<String, Integer>> cb);

	void getOrbitMap(String type, AsyncCallback<List<TableMessage>> callback);

	void getCurrentOrbitTable(String period, AsyncCallback<List<TableMessage>> callback);

	void getOrbitAmounts(String period, AsyncCallback<List<String[]>> callback);

	void getSangSalary(Date start, Date end, AsyncCallback<List<String[]>> callback);

	void updateOpsQueue(String leadKey, int queueID, String[] arguments, AsyncCallback<Integer> callback);

	void getSangDisplayStatus(String loanKeyStr, AsyncCallback<String[]> callback);

	void getSaleLeads(Date startDate, Date endDate, AsyncCallback<List<TableMessage>> callback);

}
