package com.fertiletech.addosser.client;

import java.util.List;

import com.fertiletech.addosser.shared.LoginInfo;
import com.fertiletech.addosser.shared.exceptions.DuplicateEntitiesException;
import com.fertiletech.addosser.shared.table.TableMessage;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("../login")
public interface LoginService extends RemoteService {
	LoginInfo login(String requestUrl);
	LoginInfo loginForApplications(String requestUrl) throws DuplicateEntitiesException;
	List<TableMessage> getLoanApplications(String email);
	List<TableMessage> getLoanApplications();
	List<TableMessage> getRecentActivityComments();
}
