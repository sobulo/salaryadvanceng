package com.fertiletech.addosser.client.utils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.LongBox;
import com.google.gwt.user.client.ui.Widget;

public class SearchIDBox extends Composite implements HasClickHandlers{
	@UiField
	LongBox idBox;
	@UiField
	Button search;

	private static SearchIDBoxUiBinder uiBinder = GWT
			.create(SearchIDBoxUiBinder.class);

	interface SearchIDBoxUiBinder extends UiBinder<Widget, SearchIDBox> {
	}

	public SearchIDBox() {
		initWidget(uiBinder.createAndBindUi(this));
		idBox.getValue();
		idBox.getText();
	}

	@Override
	public HandlerRegistration addClickHandler(ClickHandler handler) {
		return search.addClickHandler(handler);
	}
	
	public Long getValue()
	{
		return idBox.getValue();
	}
	
	public void setValue(Long val)
	{
		idBox.setValue(val);
	}
	
	public String getText()
	{
		return idBox.getText();
	}
	
	public void setText(String text)
	{
		idBox.setText(text);
	}
	
	public void setEnabled(boolean enable)
	{
		idBox.setEnabled(enable);
		search.setEnabled(enable);
	}
}
