package com.fertiletech.addosser.client.utils;


import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.UIObject;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class SimpleDialog {
	protected DialogBox alertBox;
	protected HTML displayMessage;
	
	public SimpleDialog(String title)
	{
		this(title, false);
	}
	
	public SimpleDialog(String title, boolean asHtml)
	{
		alertBox = new DialogBox();
		displayMessage = new HTML();
		displayMessage.setWidth("400px");
		displayMessage.setWordWrap(true);
		
		//setup alert box
		alertBox = new DialogBox(true);
		if(asHtml)
			alertBox.setHTML(title);
		else
			alertBox.setHTML("<b>" + title + "</b>");
        alertBox.setAnimationEnabled(true);
        alertBox.setGlassEnabled(true);
        setupContentArea();
        alertBox.hide();		
	}
	
	protected void setupContentArea()
	{
		VerticalPanel contentPanel = new VerticalPanel();
		Widget buttonPanel = getButtonPanel();
		contentPanel.add(displayMessage);
		contentPanel.add(buttonPanel);
		contentPanel.setCellHorizontalAlignment(buttonPanel, HorizontalPanel.ALIGN_CENTER);
		alertBox.setWidget(contentPanel);
	}
	
	protected Widget getButtonPanel()
	{
		Button ok = new Button("OK");
		ok.addClickHandler( new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				alertBox.hide();
			}
		});
		return ok;
	}
	
	public void show(String message)
	{
		//TODO use a safehtml object with sethtml call
		displayMessage.setHTML(message);
		alertBox.show();
		alertBox.center();
	}
	
	public void show(String message, UIObject showRelativeObject)
	{
		displayMessage.setHTML(message);
		alertBox.showRelativeTo(showRelativeObject);
	}
	
	public HandlerRegistration setupCloseHandler(CloseHandler<PopupPanel> handler)
	{
		return alertBox.addCloseHandler(handler);
	}
}
