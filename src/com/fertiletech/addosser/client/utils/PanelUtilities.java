/**
 * 
 */
package com.fertiletech.addosser.client.utils;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class PanelUtilities {
	public final static SimpleDialog infoBox = new SimpleDialog("<center><b style='color:green'>INFO</b></center>", true);
	public final static SimpleDialog errorBox = new SimpleDialog("<center><b style='color:red'>Error!</b></center>", true);

}
