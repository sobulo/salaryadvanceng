/*******************************************************************************
 * Copyright 2011 Google Inc. All Rights Reserved.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.fertiletech.addosser.client;
import com.fertiletech.addosser.shared.oauth.ClientUtils;
import com.fertiletech.addosser.shared.oauth.ClientUtils.BankUserCookie;
import com.fertiletech.addosser.shared.oauth.Credential;
import com.fertiletech.addosser.shared.oauth.OurCallbackUrl;
import com.fertiletech.addosser.shared.oauth.OurException;
import com.fertiletech.addosser.shared.oauth.SocialUser;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("../oauthlogin")
public interface OAuthLoginService extends RemoteService
{
    public String     getAuthorizationUrl(Credential credential) throws OurException;
    public SocialUser verifySocialUser(Credential credential) throws OurException;
    public SocialUser fetchMe(String sessionId) throws OurException;
    public String     getAccessToken(String sessionId) throws OurException;
    public void       logout(String sessionId) throws OurException;
    /**
     * Utility class for simplifying access to the instance of async service.
     */
    public static class Util
    {
        private static OAuthLoginServiceAsync instance;

        public static OAuthLoginServiceAsync getInstance()
        {
            if (instance == null)
            {
                instance=GWT.create(OAuthLoginService.class);
            }
            return instance;
        }
        
        public static void getAuthorizationUrl(final int authProvider)
        {
            String authProviderName = ClientUtils.getAuthProviderName(authProvider);
            String cbr = ClientUtils.getCallbackUrl();
            if (authProvider == ClientUtils.BANK)
            	cbr= OurCallbackUrl.ADMIN_CALLBACK_URL;
            else if(authProvider == ClientUtils.BOARD)
            	cbr = OurCallbackUrl.BOARD_CALLBACK_URL;
            
            final String callbackUrl = cbr;
            final Credential credential = new Credential();
            credential.setRedirectUrl(callbackUrl);
            credential.setAuthProvider(authProvider);
            
            new MyAsyncCallback<String>()
            {
                @Override
                public void onSuccess(String result)
                {
                	//Window.alert("Authorization URL is: " + result);
                    String authorizationUrl = result;
                    
                    // clear all cookes first
                    ClientUtils.clearCookies(); 
                    
                    // save the auth provider to cookie
                    ClientUtils.saveAuthProvider(authProvider);
                    
                    // save the redirect url to a cookie as well
                    // we need to redirect there after logout
                    ClientUtils.saveRediretUrl(callbackUrl);
                    
                    //Window.alert("Redirecting to: " + authorizationUrl);
                    ClientUtils.redirect(authorizationUrl);
                }
                
                @Override
                protected void callService(AsyncCallback<String> cb)
                {
                    OAuthLoginService.Util.getInstance().getAuthorizationUrl(credential,cb);
                }

                @Override
                public void onFailure(Throwable caught)
                {
                    ClientUtils.handleException(caught, this.showWarning);
                }
            }.go("Getting Authorization URL from " + authProviderName + "...");
                    
        }        

        public static void logout()
        {
            final String sessionId = ClientUtils.getSessionIdFromCookie();
            
            new MyAsyncCallback<Void>()
            {

                @Override
                public void onFailure(Throwable caught)
                {
                    ClientUtils.reload(); // reload anyway otherwise we're toast! we will never be able to log out
                }

                @Override
                public void onSuccess(Void result)
                {
                    ClientUtils.reload();
                }

                @Override
                protected void callService(AsyncCallback<Void> cb)
                {
                    OAuthLoginService.Util.getInstance().logout(sessionId,cb);
                }
            }.go("Logging out..");
        }    
        
        
        private static void verifySocialUser(final MyRefreshCallback refreshCB)
        {
            final String authProviderName = ClientUtils.getAuthProviderNameFromCookie();
            final int authProvider = ClientUtils.getAuthProviderFromCookieAsInt();
            //Window.alert("Verify social user: " + authProviderName);
            new MyAsyncCallback<SocialUser>()
            {

                @Override
                public void onSuccess(SocialUser result)
                {
                    ClientUtils.saveSessionId(result.getSessionId());
                    //Window.alert("On verification, returned with id: " + result.getSessionId());
                    String name = "";
                    if (result.getName() != null)
                    {
                        name = result.getName();
                    }
                    else if (result.getNickname() != null) // yahoo
                    {
                        name = result.getNickname();
                    }
                    else if (result.getFirstName() != null) // linkedin
                    {
                        name = result.getFirstName();
                        String lastName = result.getLastName();
                        if (lastName != null)
                        {
                            name = name + " " + lastName;
                        }
                    }
                    String loanID = null;
            		if (result.loanIDs != null && result.loanIDs.length > 0)
            			loanID = result.loanIDs[0];
            		
            		//Window.alert("Loan ID set to: " + loanID + " length " + (loanID == null? "n/a":loanID.length()));

                    BankUserCookie userCookie = new BankUserCookie(name, result.getEmail(), loanID, result.role.toString());
                    ClientUtils.BankUserCookie.setCookie(userCookie);
                    //ClientUtils.BankUserCookie.showCookie("Note a cb called from verify social user after this", userCookie);
                    //Window.alert("calling CB on social verification");
                    refreshCB.updateScreen();
                }

                @Override
                protected void callService(AsyncCallback<SocialUser> cb)
                {
                    try
                    {
                        final Credential credential = ClientUtils.getCredential();
                        if (credential == null)
                        {
                            Window.alert("verifySocialUser: Could not get credential for " + authProvider + " user");
                            return;
                        }     
                        //Window.alert("Verifying creidentials with: " + credential.getAuthProvider() + "/" 
                        //		+ credential.getAuthProviderName() + " verifier: " + credential.getVerifier());
                        OAuthLoginService.Util.getInstance().verifySocialUser(credential,cb);
                    }
                    catch (Exception e)
                    {
                        Window.alert(e.getMessage());
                    }
                }

                @Override
                public void onFailure(Throwable caught)
                {
                    Window.alert("Try refreshing your browser. Coult not verify" + authProvider + " user." + caught);
                }
            }.go("Verifying " + authProviderName + " user..");
        }
        
        public static void handleRedirect(MyRefreshCallback cb)
        {
            if (!ClientUtils.alreadyLoggedIn() && ClientUtils.redirected()) 
            {
            	//Window.alert("Calling verify social user from handle redirect");
                verifySocialUser(cb);
                //Window.alert("Finished verification from within handle redirect succesfully");
            }
            else
            {
            	//Window.alert("Calling cb fn passed to handle redirect");
            	cb.updateScreen();
            	//Window.alert("Finished cb fn passed to handle redirect succesfully");
            }
        }
        
    }
    
    
}
