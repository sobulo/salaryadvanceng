/**
 * 
 */
package com.fertiletech.addosser.client;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.fertiletech.addosser.shared.exceptions.DuplicateEntitiesException;
import com.fertiletech.addosser.shared.exceptions.MissingEntitiesException;
import com.fertiletech.addosser.shared.table.TableMessage;
import com.fertiletech.addosser.shared.table.TableMessageHeader;
import com.fertiletech.addosser.shared.workstates.WorkflowStateInstance;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * @author Segun Razaq Sobulo
 *
 */
@RemoteServiceRelativePath("../loanservice")
public interface LoanMktService extends RemoteService {
	void updateParameters(HashMap<String, String> parameters) throws MissingEntitiesException;
	HashMap<String, String> getParameters();
	List<TableMessage>[] getSaleLeads(Date startDate, Date endDate, boolean useOpsQueue);
	List<TableMessage> getSaleLeads(Date startDate, Date endDate);
	HashMap<String, String> getStoredLoanApplication(String loanKeyStr);
	void storeLoanApplicationData(HashMap<String, String> appData, String loanKeyStr, boolean isSubmit);
	String[] saveLoanApplicationData(HashMap<String, String> appData, String loanKeyStr, boolean isSubmit) throws MissingEntitiesException;	
	String[] startLoanApplication(HashMap<String, String> appData) throws DuplicateEntitiesException;
	HashMap<String, String> getStoredGuarantorApplication(String guarantorKeyStr);
	String[] storeGurantorApplicationData(HashMap<String, String> appData, String guarantorID, boolean isSubmit);
	String loadFormDisplay(String formID, boolean isFormKey);
	String loadSaleLeadComments(String leadID);
	String[] getLoanID() throws DuplicateEntitiesException; //TODO will need to change once multiple apps supported
	void saveLoanComment(String loanKeyStr, String text, boolean showPublic);
	int updateOpsQueue(String leadKey, int queueID, String[] arguments);
	void updateOpsQueue(String leadKey, String appStatus, String comments);
	boolean getCustomerAck(String leadKey);
	void setCustomerAck(String leadKey);
	Date getAppointmentDate(String leadKey) throws MissingEntitiesException;
	void setAppointmentDate(String leadKey, Date meetingDate) throws MissingEntitiesException;
	Boolean okToPrint(String loanID); 
	HashMap<String, String> getNewLoanIDs();
	String getAllSalesLeadAsCSV();
	HashMap<String, String> getLoanApplicationWithLoanID(String loanKeyStr);
	TableMessage getLoanState(String loanID);
	TableMessage getLoanState(long loanID);
	String getApplicationFormDownloadLink(String formID);
	String getGuarantorFormDownloadLink(String formID);
	String getRequirementsChecklistLink(String loanID);
	String fetchGenericExcelLink(List<TableMessage> data,
			TableMessageHeader header);	
	HashMap<WorkflowStateInstance, Integer> getLeadAggregates(Date startDate,Date endDate);
	List<TableMessage> getMapAttachments(Date startDate, Date endDate);
	HashMap<String, Integer> getOrbitTenors(String period);
	List<TableMessage> getOrbitMap(String period);
	List<TableMessage> getCurrentOrbitTable(String period);
	List<String[]> getOrbitAmounts(String period);
	List<String[]> getSangSalary(Date start, Date end);
	String[] getSangDisplayStatus(String loanKeyStr);
}
