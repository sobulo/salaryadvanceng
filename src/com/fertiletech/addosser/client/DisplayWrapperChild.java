/**
 * 
 */
package com.fertiletech.addosser.client;

import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public interface DisplayWrapperChild extends HasValueChangeHandlers<Boolean>{
	public Widget getFormWidget();
	public void setLoanID(String loanID);
}
