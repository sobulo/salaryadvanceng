/**
 * 
 */
package com.fertiletech.addosser.client;


import java.util.HashMap;
import java.util.List;

import com.fertiletech.addosser.shared.table.TableMessage;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * @author Segun Razaq Sobulo
 *
 */
public interface MessagingManagerAsync {

	void sendMessage(
			com.fertiletech.addosser.shared.FormConstants.CustomMessageTypes msgType,
			String[] addresses, String[] message, boolean isEmail,
			AsyncCallback<Void> callback);

	void getMessagingControllerNames(AsyncCallback<HashMap<String, String>> callback);

	void getControllerDetails(String controllerName,
			AsyncCallback<List<TableMessage>> callback);

}
