package com.fertiletech.addosser.client;

import java.util.List;

import com.fertiletech.addosser.shared.LoginInfo;
import com.fertiletech.addosser.shared.table.TableMessage;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface LoginServiceAsync {
	void login(String requestUrl, AsyncCallback<LoginInfo> callback);

	void loginForApplications(String requestUrl,
			AsyncCallback<LoginInfo> callback);

	void getLoanApplications(String email,
			AsyncCallback<List<TableMessage>> callback);

	void getLoanApplications(AsyncCallback<List<TableMessage>> callback);

	void getRecentActivityComments(AsyncCallback<List<TableMessage>> callback);
}
