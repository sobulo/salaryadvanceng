package com.fertiletech.addosser.table;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.fertiletech.addosser.client.GUIConstants;
import com.fertiletech.addosser.client.LoanMktService;
import com.fertiletech.addosser.client.LoanMktServiceAsync;
import com.fertiletech.addosser.shared.table.TableMessage;
import com.fertiletech.addosser.shared.table.TableMessageHeader;
import com.fertiletech.addosser.shared.table.TableMessageKeyProvider;
import com.fertiletech.addosser.shared.table.TableMessageHeader.TableMessageContent;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.DateCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.NumberCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.AbstractCellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.cellview.client.RowStyles;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.cellview.client.SimplePager.TextLocation;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.ResizeComposite;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.DefaultSelectionEventManager;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.MultiSelectionModel;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SelectionModel.AbstractSelectionModel;
import com.google.gwt.view.client.SingleSelectionModel;

public class ShowcaseTable extends ResizeComposite implements
		HasValueChangeHandlers<TableMessage> {
	@UiField
	SimplePanel filterSlot;
	ShowcaseTableFilter filterBox;
	@UiField
	SimplePanel excelSlot;
	Button excelReport;
	private ListDataProvider<TableMessage> dp;
	private TableMessageHeader setupHeaderOnFirstCallBack = null;
	private final AbstractSelectionModel<TableMessage> selectionModel;
	RowStyles<TableMessage> normalRowStyle;
	HashSet<String> highlightedRows = new HashSet<String>();
	final static LoanMktServiceAsync LOAN_MKT_SERVICE = GWT.create(LoanMktService.class);
	
	@UiField
	DockLayoutPanel tableContainer;
	boolean footerVisible = true;
	
	@UiField
	HTMLPanel footer;	
	
	/**
	 * The pager used to change the range of data.
	 */
	@UiField(provided = true)
	SimplePager pager;

	/**
	 * The main CellTable.
	 */
	@UiField(provided = true)
	DataGrid<TableMessage> cellTable;

	private static ShowcaseTableUiBinder uiBinder = GWT
			.create(ShowcaseTableUiBinder.class);

	interface ShowcaseTableUiBinder extends UiBinder<Widget, ShowcaseTable> {
	}

	public ShowcaseTable() {
		this(false);
	}
	
	public ShowcaseTable(Boolean isMultiple)
	{
		this(isMultiple, true, true);
	}

	public ShowcaseTable(final Boolean isMultiple, boolean useFilters, boolean useExcel) {
		// Set a key provider that provides a unique key for each contact. If
		// key is
		// used to identify contacts when fields (such as the name and address)
		// change.
		cellTable = new DataGrid<TableMessage>(
				TableMessageKeyProvider.KEY_PROVIDER);
		cellTable.setWidth("100%");

		// Do not refresh the headers and footers every time the data is
		// updated.
		cellTable.setAutoHeaderRefreshDisabled(true);
		cellTable.setAutoFooterRefreshDisabled(true);

		// Create a Pager to control the table.
		SimplePager.Resources pagerResources = GWT
				.create(SimplePager.Resources.class);
		pager = new SimplePager(TextLocation.CENTER, pagerResources, false, 0,
				true);
		//pager.setRangeLimited(true);
		pager.setDisplay(cellTable);

		// Add a selection model so we can select cells.
		if(isMultiple == null)
			selectionModel = new NoSelectionModel<TableMessage>();
		else if(isMultiple)
			selectionModel = new MultiSelectionModel<TableMessage>(
				TableMessageKeyProvider.KEY_PROVIDER);
		else
			selectionModel = new SingleSelectionModel<TableMessage>(
						TableMessageKeyProvider.KEY_PROVIDER);
		
		cellTable.setSelectionModel(selectionModel,
				DefaultSelectionEventManager
						.<TableMessage> createCheckboxManager());

		normalRowStyle = cellTable.getRowStyles();
		cellTable.setRowStyles(new RowStyles<TableMessage>() {

			@Override
			public String getStyleNames(TableMessage row, int rowIndex) {

				if (highlightedRows.contains(row.getMessageId()))
					return "highlightRow";
				else if(row.getRowStyle() != null)
					return row.getRowStyle();
				else if (rowIndex % 2 == 0)
					return "cellTableEvenRow";
				else
					return "cellTableOddRow";
			}
		});

		selectionModel
				.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {

					@Override
					public void onSelectionChange(SelectionChangeEvent event) {
						if(isMultiple == null) return;
						else if (!isMultiple) {
							TableMessage m = ((SingleSelectionModel<TableMessage>) selectionModel)
									.getSelectedObject();
							fireChange(m);
						} else
							fireChange(new TableMessage());
					}
				});
		if(isMultiple != null)
		{
			// Initialize the columns.
			// Checkbox column. This table will uses a checkbox column for
			// selection.
			// Alternatively, you can call cellTable.setSelectionEnabled(true) to
			// enable
			// mouse selection.
			Column<TableMessage, Boolean> checkColumn = new Column<TableMessage, Boolean>(
					new CheckboxCell(true, false)) {
				@Override
				public Boolean getValue(TableMessage object) {
					// Get the value from the selection model.
					return selectionModel.isSelected(object);
				}
			};			
			cellTable.addColumn(checkColumn,
				SafeHtmlUtils.fromSafeConstant("<br/>"));
			cellTable.setColumnWidth(checkColumn, 40, Unit.PX);
		}
		//cellTable.set
		initWidget(uiBinder.createAndBindUi(this));
		footer.addDomHandler(new MouseOutHandler() {
			
			@Override
			public void onMouseOut(MouseOutEvent event) {
				if(footerVisible)
				{
					if(event.getRelativeY(pager.getElement()) > -3 ) return;
					footerVisible = false;
					tableContainer.setWidgetSize(footer, 5);
					tableContainer.animate(550);
				}
			}
		}, MouseOutEvent.getType());
		footer.addDomHandler(new MouseOverHandler() {
			
			@Override
			public void onMouseOver(MouseOverEvent event) {
				if(!footerVisible)
				{
					tableContainer.setWidgetSize(footer, 50);
					tableContainer.animate(550);
					footerVisible = true;
				}
			}
		}, MouseOverEvent.getType());
		if(useFilters)
		{
			filterBox = new ShowcaseTableFilter();
			filterSlot.add(filterBox);
			dp = new FilteredListDataProvider<TableMessage>(filterBox);
		}
		else
			dp = new ListDataProvider<TableMessage>();
		
		if(useExcel)
		{
			excelReport = new Button("Excel");
			excelSlot.add(excelReport);
		}
		dp.addDataDisplay(cellTable);

	}

	public void fireChange(TableMessage m) {
		ValueChangeEvent.<TableMessage> fire(this, m);
	}

	public void setEmptyWidget(Widget w) {
		cellTable.setEmptyTableWidget(w);
	}

	public void showTable(List<TableMessage> newTableContent) {
		List<TableMessage> tableContent = dp.getList();
		tableContent.clear();

		if (newTableContent.size() == 0)
			return;

		TableMessageHeader header = null;
		if (newTableContent.get(0) instanceof TableMessageHeader)
			header = (TableMessageHeader) newTableContent.remove(0);
		if (setupHeaderOnFirstCallBack == null) {
			if (header == null)
				throw new RuntimeException("First row should be a header row");
			// Attach a column sort handler to the
			// ListDataProvider to sort the list.
			ListHandler<TableMessage> sortHandler = new ListHandler<TableMessage>(
					tableContent);
			cellTable.addColumnSortHandler(sortHandler);
			initTable(header, sortHandler);
			if(filterBox != null)
				 filterBox.init(header, (FilteredListDataProvider<TableMessage>) dp);
			setupHeaderOnFirstCallBack = header;
			if(excelReport != null)
				initExcelExport();
		}
		tableContent.addAll(newTableContent);
	}

	private void initExcelExport() {
		excelReport.addClickHandler(new ClickHandler() {
			HTML link = new HTML();
			PopupPanel p = new PopupPanel();
			private AsyncCallback<String> callback = new AsyncCallback<String>() {

				@Override
				public void onFailure(Throwable caught) {
					link.setHTML("Excel download request failed");
					Window.alert("Excel download request failed "
							+ caught.getMessage());
				}

				@Override
				public void onSuccess(String result) {
					GWT.log(result);
					link.setHTML(result);
					if (!p.isShowing())
						p.showRelativeTo(excelReport);
				}
			};
			{
				p.add(link);
				p.setSize("150px", "150px");
				p.setAutoHideEnabled(true);
			}

			@Override
			public void onClick(ClickEvent event) {
				// RPC call that fetches link for excel download
				link.setHTML("Fetching download link, please wait");
				p.showRelativeTo(excelReport);
				GWT.log("G: " + (setupHeaderOnFirstCallBack == null) + " DP "
						+ (dp == null));
				ArrayList<TableMessage> args = new ArrayList<TableMessage>();
				args.addAll(dp.getList());
				LOAN_MKT_SERVICE.fetchGenericExcelLink(args,
						setupHeaderOnFirstCallBack, callback);
				GWT.log("Finished dispatching call");
			}
		});
	}

	private void initTable(TableMessageHeader h,
			ListHandler<TableMessage> sortHandler) {
		initTableColumns(h, cellTable, sortHandler);
	}

	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<TableMessage> handler) {
		return addHandler(handler, ValueChangeEvent.getType());
	}

	public void clear() {
		dp.getList().clear();
		refreshAll();
	}

	public void sortColumn(int colIdx) {
		cellTable.getColumnSortList().clear();
		refreshAll();
		cellTable.getColumnSortList().push(cellTable.getColumn(colIdx));
		refreshAll();
	}

	public List<TableMessage> getDataList() {
		return dp.getList();
	}

	public Set<TableMessage> getSelectedSet() {
		return ((MultiSelectionModel<TableMessage>) selectionModel)
				.getSelectedSet();
	}

	public TableMessage getSelectedRow() {
		return ((SingleSelectionModel<TableMessage>) selectionModel)
				.getSelectedObject();
	}

	public void selectNone() {
		((SingleSelectionModel<TableMessage>) selectionModel).clear();
	}

	public void refresh(TableMessage m) {
		List<TableMessage> list = dp.getList();
		int index = list.indexOf(m);
		if (index >= 0)
			list.set(index, m);
	}

	public void refreshAll() {
		dp.refresh();
	}
	
	public void tableRefreshCall()
	{
		cellTable.setRowCount(dp.getList().size());
	}

	public void highlightRow(TableMessage m) {
		highlightedRows.add(m.getMessageId());
	}

	public void clearHighlights() {
		highlightedRows.clear();
	}

	public void addColumn(Column<TableMessage, ?> col, String title) {
		cellTable.addColumn(col, title);
	}

	public boolean isInitialized()
	{
		return setupHeaderOnFirstCallBack != null;
	}
	
	/**
	 * Add the columns to the table. Column addition is done in a generic manner
	 * based on the TableHeader For custom column specification you'll have to
	 * write another init function
	 */
	public static void initTableColumns(TableMessageHeader header,
			final AbstractCellTable<TableMessage> cellTable,
			ListHandler<TableMessage> sortHandler) {
		int dateIdx, textIdx, numIdx;
		dateIdx = textIdx = numIdx = 0;
		for (int i = 0; i < header.getNumberOfHeaders(); i++) {
			TableMessageContent headerType = header.getHeaderType(i);
			GWT.log("Header: " + i + " Type: "
					+ (headerType == null ? null : headerType) + " Name: "
					+ (header.getText(i) == null ? null : header.getText(i)));
			if (headerType.equals(TableMessageContent.TEXT)) {
				final int messageIndex = textIdx++;
				Column<TableMessage, String> textColumn = new Column<TableMessage, String>(
						new TextCell()) {
					@Override
					public String getValue(TableMessage object) {
						return object.getText(messageIndex);
					}
				};
				textColumn.setSortable(true);
				textColumn.setDefaultSortAscending(false);
				sortHandler.setComparator(textColumn, TableMessage
						.getMessageComparator(messageIndex, headerType));
				cellTable.addColumn(textColumn, header.getText(i));
				// cellTable.setColumnWidth(addressColumn, 60, Unit.PCT);
			} else if (headerType.equals(TableMessageContent.NUMBER)) {
				String fmtOption = header.getFormatOption(i);
				if(fmtOption == null)
					fmtOption = "#,##0.00";
				final NumberFormat nf = NumberFormat.getFormat(fmtOption);

				final int messageIndex = numIdx++;
				Column<TableMessage, ?> columnToAdd; 
				if (header.isEditable(i)) {
					final EditTextCell ec = new EditTextCell();
					Column<TableMessage, String> textColumn = new Column<TableMessage, String>(
							ec) {

						@Override
						public String getValue(TableMessage object) {
							Double num = object.getNumber(messageIndex);
							if(num == null) return "edit";
							String numStr = nf.format(num);
							return numStr;
						}
					};
					textColumn
							.setFieldUpdater(new FieldUpdater<TableMessage, String>() {

								@Override
								public void update(int index,
										TableMessage object, String value) {
									try {
										Double newNumber = nf.parse(value);
										object.setNumber(messageIndex, newNumber);
									} catch (NumberFormatException ex) {
										Window.alert("Ignoring entry [" + value +  "]. Invalid number specified");
										object.setNumber(messageIndex, null);
									}	
									ec.clearViewData(object.getMessageId());
									cellTable.redraw();
								}
							});
					columnToAdd = textColumn;
				} else {
					Column<TableMessage, Number> numberColumn = new Column<TableMessage, Number>(
							new NumberCell(nf)) {
						@Override
						public Number getValue(TableMessage object) {
							return object.getNumber(messageIndex);
						}
					};
					columnToAdd = numberColumn;
				}
				columnToAdd.setSortable(true);
				columnToAdd.setDefaultSortAscending(false);
				sortHandler.setComparator(columnToAdd, TableMessage
						.getMessageComparator(messageIndex, headerType));
				cellTable.addColumn(columnToAdd, header.getText(i));
			} else if (headerType.equals(TableMessageContent.DATE)) {
				final int messageIndex = dateIdx++;
				Column<TableMessage, Date> dateColumn = new Column<TableMessage, Date>(
						new DateCell(GUIConstants.DEFAULT_DATE_FORMAT)) {
					@Override
					public Date getValue(TableMessage object) {
						return object.getDate(messageIndex);
					}
				};
				dateColumn.setSortable(true);
				dateColumn.setDefaultSortAscending(false);
				sortHandler.setComparator(dateColumn, TableMessage
						.getMessageComparator(messageIndex, headerType));
				cellTable.addColumn(dateColumn, header.getText(i));
			}
			/*
			 * else if(headerType.equals(TableMessageContent.HTML)) { final int
			 * messageIndex = textIdx++; Column<TableMessage, SafeHtml>
			 * htmlColumn = new Column<TableMessage, SafeHtml>(new
			 * SafeHtmlCell()) {
			 * 
			 * @Override public SafeHtml getValue(TableMessage object) { return
			 * new
			 * SafeHtmlBuilder().appendEscaped(object.getText(messageIndex)).
			 * toSafeHtml(); } }; htmlColumn.setSortable(true);
			 * htmlColumn.setDefaultSortAscending(false);
			 * sortHandler.setComparator(htmlColumn,
			 * TableMessage.getMessageComparator(messageIndex, headerType));
			 * cellTable.addColumn(htmlColumn, header.getText(i)); }
			 */
			else
				continue;
		}
	}
	
}
