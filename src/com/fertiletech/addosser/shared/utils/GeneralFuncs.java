/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fertiletech.addosser.shared.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Set;

import com.googlecode.objectify.Key;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class GeneralFuncs {

    public static <T> boolean addToHashSet( Set<T> hash, T o)
    {
        if(o == null)
            throw(new NullPointerException());
        //seems we can't rely on boolean result of add as datanucleus backed
        //hashet subclass successfully adds @ least on local server even when
        //element is already present. contains works though so check that first
        if(hash.contains(o))
            return false;
        return hash.add(o);
    }

    public static <T> boolean removeFromHashSet(Set<T> hash, T o)
    {
        if(o == null)
            throw(new NullPointerException());

        if(!hash.contains(o))
            return false;

        return hash.remove(o);
    }

    public static <K, V> boolean addToHashMap(HashMap<K, V> hashMap, K key, V value)
    {
        if(hashMap.containsKey(key))
            return false;
        hashMap.put(key, value);
        return true;
    }

    public static <K, V> boolean updateHashMap(HashMap<K, V> hashMap, K key, V val)
    {
        if(!hashMap.containsKey(key))
            return false;
        hashMap.put(key, val);
        return true;
    }

    public static <K, V> boolean removeFromHashMap(HashMap<K, V> hashMap, K key)
    {
        return( hashMap.remove(key) == null? false : true);
    }

    public static Date getDate(int year, int month, int day)
    {
        //offset month by 1 as gregorian calendar represents months from 0-11
        return (new GregorianCalendar(year, month - 1, day)).getTime();
    }

    /**
     *
     * @param <T>
     * @param a - first array
     * @param b - second array to compare with first
     * @param intersect
     * @param aOnly - elements found in aOnly
     * @param bOnly - elements found in bOnly
     */
    public static <T extends Comparable< T > > void arrayDiff(T[] a, T[] b,
            ArrayList<T> intersect, ArrayList<T> aOnly, ArrayList<T> bOnly )
    {
        Arrays.sort(a);
        Arrays.sort(b);
        int i = 0;
        int j = 0;
        while(i < a.length || j < b.length)
        {
            if(i == a.length)
            {
                bOnly.add(b[j]);
                j++;
                continue;
            }

            if(j == b.length)
            {
                aOnly.add(a[i]);
                i++;
                continue;
            }

            int comp = a[i].compareTo(b[j]);
            if(comp == 0)
            {
                intersect.add(a[i]);
                i++;
                j++;
            }
            else if(comp < 0 )
            {
                aOnly.add(a[i]);
                i++;
            }
            else
            {
                bOnly.add(b[j]);
                j++;
            }
        }
    }
    
    public static <T> void arrayDiff(Key<T>[] a, Key<T>[] b,
            ArrayList<Key<T>> intersect, ArrayList<Key<T>> aOnly, ArrayList<Key<T>> bOnly )
    {
        Arrays.sort(a);
        Arrays.sort(b);
        int i = 0;
        int j = 0;
        while(i < a.length || j < b.length)
        {
            if(i == a.length)
            {
                bOnly.add(b[j]);
                j++;
                continue;
            }

            if(j == b.length)
            {
                aOnly.add(a[i]);
                i++;
                continue;
            }

            int comp = a[i].compareTo(b[j]);
            if(comp == 0)
            {
                intersect.add(a[i]);
                i++;
                j++;
            }
            else if(comp < 0 )
            {
                aOnly.add(a[i]);
                i++;
            }
            else
            {
                bOnly.add(b[j]);
                j++;
            }
        }

    }    
}
