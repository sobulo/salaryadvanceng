/**
 * 
 */
package com.fertiletech.addosser.shared.workstates;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import com.fertiletech.addosser.shared.DTOConstants;
import com.fertiletech.addosser.shared.FormConstants;
import com.fertiletech.addosser.shared.FormConstants.HirePeriods;


/**
 * @author Segun Razaq Sobulo
 *
 */
public enum WorkflowStateInstance implements WorkflowState{
	
	APPLICATION_STARTED{
		//move from started to submitted
		public String[] nextState(WorkflowMachine context)
		{
			context.setCurrentState(APPLICATION_SUBMITTED);
			String[] comment = new String[1];
			comment[0] = getTransitionStringPrefix(APPLICATION_SUBMITTED);
			return comment;
		}

		@Override
		public Integer opsQueue() {
			return null;
		}

		@Override
		public String getChartColor() {
			return "color: yellow";
		}

		@Override
		public String getDisplayString() {
			// TODO Auto-generated method stub
			return "Started" ;
		}
	},
	
	APPLICATION_SUBMITTED{
		//move from submitted to pre-approved state of yes or no
		public String[] nextState(WorkflowMachine context)
		{
			ArrayList<String> comments = new ArrayList<String>(4);
			String[] commentHelper = new String[1];
			/*if(checkForPreApprovalYes(context, comments))
			{
				context.setCurrentState(PRE_APPROVED_YES);
				comments.add(getTransitionStringPrefix(PRE_APPROVED_YES));
			}
			else
			{
				context.setCurrentState(PRE_APPROVED_NO);
				comments.add(getTransitionStringPrefix(PRE_APPROVED_NO));
			}*/
			return comments.toArray(commentHelper);
		}

		@Override
		public Integer opsQueue() {
			return DTOConstants.HOT_IDX;
		}

		@Override
		public String getChartColor() {

			return "color: #e072df";
		}

		@Override
		public String getDisplayString() {
			return "Pending";

		}		
	},
	
	PRE_APPROVED_YES
	{

		@Override
		public Integer opsQueue() {
			return DTOConstants.HOT_IDX;
		}

		@Override
		public String getChartColor() {
			return "color: pink";
		}

		@Override
		public String getDisplayString() {
			return "Pre-Approved";
		}
	},
	
	PRE_APPROVED_NO
	{

		@Override
		public Integer opsQueue() {
			
			return DTOConstants.COLD_IDX;
		}

		@Override
		public String getChartColor() {
			return "color: brown";
		}

		@Override
		public String getDisplayString() {
			return "Not Qualified";
		}		
	},
	
	CUSTOMER_READY_FOR_VERIFICATION
	{

		@Override
		public Integer opsQueue() {
			
			return DTOConstants.READY_IDX;
		}

		@Override
		public String getChartColor() {
			return "color: purple";
		}

		@Override
		public String getDisplayString() {
			return "Documentation";
		}		
	},	

	LOAN_APPROVED_NO{

		@Override
		public Integer opsQueue() {
			return -1;
		}

		@Override
		public String suggestOpsAction() {
			return "Notice a denial pattern?";
		}

		@Override
		public String getChartColor() {
			return "color: black";
		}

		@Override
		public String getDisplayString() {
			// TODO Auto-generated method stub
			return "Denied";
		}
	}, //terminal state	
	
	VERIFICATION
	{
		public String[] nextState(WorkflowMachine context)
		{
			String[] loanComment = new String[1];
			loanComment[0] = getTransitionStringPrefix(PENDING_APPROVAL);
			return loanComment;
		}

		@Override
		public Integer opsQueue() {
			return DTOConstants.VERIFY_IDX;
		}

		@Override
		public String getChartColor() {
			return "color: magenta";
		}

		@Override
		public String getDisplayString() {
			return "Credit Check";
		}		
	},
	
	PENDING_APPROVAL
	{

		@Override
		public Integer opsQueue() {
			return DTOConstants.APPROVE_IDX;
		}

		@Override
		public String getChartColor() {
			return "color: orange";
		}

		@Override
		public String getDisplayString() {
			return "Pending Approval";
		}			
	},
	
	LOAN_APPROVED_YES{
		public String[] nextState(WorkflowMachine context)
		{
			String[] loanComment = new String[1];
			loanComment[0] = getTransitionStringPrefix(LOAN_FUNDED);
			context.setCurrentState(LOAN_FUNDED);
			return loanComment;
		}

		@Override
		public Integer opsQueue() {
			return DTOConstants.CLOSED_IDX;
		}

		@Override
		public String getChartColor() {
			return "color: green";
		}

		@Override
		public String getDisplayString() {
			return "Approved";
		}			
	},
	
	APPLY_LATER
	{

		@Override
		public Integer opsQueue() {
			return DTOConstants.APPROVE_IDX;
		}

		@Override
		public String getChartColor() {
			return "color: cyan";
		}

		@Override
		public String getDisplayString() {
			return "To apply later";
		}			
	},
	
	LOAN_FUNDED{

		@Override
		public Integer opsQueue() {
			return -1;
		}

		@Override
		public String suggestOpsAction() {
			return "Monitor loan repayments";
		}

		@Override
		public String getChartColor() {
			return "color: blue";
		}

		@Override
		public String getDisplayString() {
			return "Funded";
		}
	}; //terminal state
	
	


	@Override
	public String suggestOpsAction() {
		return DTOConstants.OPS_SUGGESTIONS[opsQueue()];
	}	
	
	//cache keys for reuse
	HashMap<String, String>[] cachedContext ;
	private final static double INTEREST = 0.05;
	private final static double SALARY_PERCENTAGE = 0.34;

	@Override
	public boolean showInAllQueue() {
		return this.opsQueue() != null;
	}
	
	String getTransitionStringPrefix(WorkflowState nextState)
	{
		return this.toString() + " [to] " + nextState.toString();
	}
	
	static double calculateEstimatedMonthlyPayment(double principal, double period)
	{
		//TODO get interest from default server parameters
		double interest = INTEREST;
		return principal * (interest / (1 - (Math.pow(1 + interest, -period))));
	}
	
	public static WorkflowStateInstance checkForPreApprovalYes(WorkflowMachine context, ArrayList<String> comment)
	{
		Double amountRequested = context.getRequestedAmount();
		String company = context.getCompanyName();
		double salary = context.getMonthlySalary();
		double tenor = context.getRequestedTenor();
		String state = context.getState();
		HirePeriods employmentDuration = HirePeriods.getEnumVal(context.getHirePeriod());
		double monthlyPayment = calculateEstimatedMonthlyPayment(amountRequested, tenor);
		
		WorkflowStateInstance result = APPLICATION_SUBMITTED;
		
		final String[] CHECK_INFO = {"check of pre-approved list of companies/employers", 
								"check salary meets minimum value", 
								"estimated monthly repayment obligations is a managable fraction of salary",
								"check for state eligibility",
								"check of hire period meets minimum requirements"};
		final String FAIL = " <span style='color:red'>failed</span> ";
		final String PASS = " <span style='color:green'>passed</span> ";
		String msgPrefix = "";
		if(Arrays.binarySearch(FormConstants.COMPANY_LIST, company) > 0)
		{
			msgPrefix = PASS;
			result = PRE_APPROVED_YES;
			comment.add(company + msgPrefix + CHECK_INFO[0]);
		}
		
		double salaryFraction = salary * SALARY_PERCENTAGE;
		/*String salStr = intCurrFmt(salary);
		String monthStr = intCurrFmt(monthlyPayment);
		String amtReqStr = intCurrFmt(amountRequested);
		String fracStr = intCurrFmt(salaryFraction);
		String pecStr = DOUBLE_FORMAT.format(SALARY_PERCENTAGE);*/
		if(salaryFraction <= monthlyPayment)
		{
			result = PRE_APPROVED_NO;
			msgPrefix = FAIL;
		}
		else
		{
			result = PRE_APPROVED_YES;
			msgPrefix = PASS;
		}
		/*comment.add("Requested: " + amtReqStr + " and calculated " + pecStr + " fraction of salary "  + salStr + " as " + fracStr + 
				" and then comparing this to estimated monthly payment of " + monthStr + msgPrefix + CHECK_INFO[2]);*/
		
		comment.add("sanguine check: " +  msgPrefix + CHECK_INFO[2]);
		

			
		if(salary < 42000)
		{
			result = PRE_APPROVED_NO;
			msgPrefix = FAIL;
			comment.add(salary + msgPrefix + CHECK_INFO[1]);
		}
		
		if(!state.equals("Lagos State"))
		{
			result = PRE_APPROVED_NO;
			msgPrefix = FAIL;
			comment.add(state + msgPrefix + CHECK_INFO[3]);
		}
		
		if(employmentDuration.equals(HirePeriods.LESS_THAN_SIX_MONTHS))
		{
			result = PRE_APPROVED_NO;
			msgPrefix = FAIL;
			comment.add(employmentDuration + msgPrefix + CHECK_INFO[4]);
		}
			
		return result;
	}
	
	//private static NumberFormat DOUBLE_FORMAT = NumberFormat.getDecimalFormat();
	//private static NumberFormat MONEY_FORMAT = NumberFormat.getFormat("###,###");
	/*private static String intCurrFmt(Double amt)
	{
		return "<span>&#8358;</span>" + MONEY_FORMAT.format(amt);
	}*/
		

	public String[] nextState(WorkflowMachine nextState)
	{
		//TODO this is a serious hack but time conscious, needs more thought later, for now we bypass the exception
		//throw new RuntimeException("This state is not supported for transitions: " + nextState.toString());
		String result[] = {nextState.getCompanyName() + ", " +", " + 
							nextState.getMonthlySalary()+ ", " + nextState.getRequestedAmount()};
		return result;
	}
}
