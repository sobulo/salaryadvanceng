/**
 * 
 */
package com.fertiletech.addosser.shared.workstates;

/**
 * @author Segun Razaq Sobulo
 *
 */
public interface WorkflowMachine {
	//http://sourcemaking.com/design_patterns/state
	public String[] nextState();
	public WorkflowStateInstance getCurrentState();
	public void setCurrentState(WorkflowStateInstance state);
	public String getCurrentMessage();
	public void setCurrentMessage(String msg);
	
	Double getMonthlySalary();
	Double getRequestedAmount();
	Integer getRequestedTenor();
	String getCompanyName();
	String getState();
	String getHirePeriod();;
}
