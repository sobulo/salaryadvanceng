/**
 * 
 */
package com.fertiletech.addosser.shared.workstates;

/**
 * @author Segun Razaq Sobulo
 *
 */
public interface WorkflowState {
	String[] nextState(WorkflowMachine context);
	Integer opsQueue();
	String suggestOpsAction();
	boolean showInAllQueue();
	String getChartColor();
	String getDisplayString();
}
