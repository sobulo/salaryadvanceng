/**
 * 
 */
package com.fertiletech.addosser.shared;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class PickedDateTime implements Serializable{
	private static final long serialVersionUID = 6121002368678217054L;
	public final static String AM = "am";
	public final static String PM = "pm";
	public Date date;
	public int hour;
	public int minute;
	

}
