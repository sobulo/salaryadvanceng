/**
 * 
 */
package com.fertiletech.addosser.shared;


/**
 * @author Segun Razaq Sobulo
 *
 */
public class FormHTMLDisplayBuilder {
	StringBuilder html;
	
	private final static String FORM_BEGINS_STYLE = "padding: 5px;text-align:left; display: inline-block; border: 1px solid gray";
	private final static String FORM_BEGINS = "<div style='" + FORM_BEGINS_STYLE + "'>";
	private final static String FORM_ENDS = "</div>";
	private final static String HEADER_STYLE = "font-weight:bold; clear:both; margin-bottom:15px";
	public final static String HEADER_BEGINS = "<div style='" + HEADER_STYLE + "'>";
	public final static String HEADER_ENDS = "</div>";
	private final static String LINE_STYLE = "margin-bottom: 15px; clear:both";
	private final static String LINE_BEGINS = "<div style='" + LINE_STYLE + "'>";
	private final static String LINE_ENDS = "</div>";
	private final static String LABEL_STYLE = "float:left; margin-right: 5px;";
	private final static String LABEL_BEGINS = "<span style='" + LABEL_STYLE + "'>";
	private final static String LABEL_ENDS = "</span>";
	private final String VALUE_STYLE;
	private final String VALUE_BEGINS;
	private final static String VALUE_ENDS = "</span>";
	private final static String SECTION_ENDS = "<br/> <hr style='clear:both' />";
	
	
	public FormHTMLDisplayBuilder()
	{
		this(false);
	}
	public FormHTMLDisplayBuilder(boolean darkTheme)
	{
		String prefix = "float:left;border-bottom:1px dotted black; margin-right: 15px; color: ";
		html = new StringBuilder();
		if(darkTheme)
			VALUE_STYLE	= prefix + "#555555;";
		else
			VALUE_STYLE = prefix + "#555555;";
		VALUE_BEGINS =  "<span style='" + VALUE_STYLE + "'>";
	}

	public FormHTMLDisplayBuilder formBegins()
	{
		html.append(FORM_BEGINS);
		return this;
	}
	
	public FormHTMLDisplayBuilder formEnds()
	{
		html.append(FORM_ENDS);
		return this;
	}
	
	public FormHTMLDisplayBuilder lineBegins()
	{
		html.append(LINE_BEGINS);
		return this;
	}
	
	public FormHTMLDisplayBuilder lineEnds()
	{
		html.append(LINE_ENDS);
		return this;
	}

	public FormHTMLDisplayBuilder headerBegins()
	{
		html.append(HEADER_BEGINS);
		return this;
	}
	
	public FormHTMLDisplayBuilder headerEnds()
	{
		html.append(HEADER_ENDS);
		return this;
	}
	
	public FormHTMLDisplayBuilder sectionEnds()
	{
		html.append(SECTION_ENDS);
		return this;
	}
	
	public FormHTMLDisplayBuilder labelBegins()
	{
		html.append(LABEL_BEGINS);
		return this;
	}
	
	public FormHTMLDisplayBuilder labelEnds()
	{
		html.append(':').append(LABEL_ENDS);
		return this;
	}	
	
	public FormHTMLDisplayBuilder backgroundBegins(String bgStyle)
	{
		html.append("<div style='" + bgStyle + "'>");
		return this;
	}	
	
	public FormHTMLDisplayBuilder backGroundEnds()
	{
		html.append("</div>");
		return this;
	}	
	
	
	public FormHTMLDisplayBuilder valueBegins()
	{
		html.append(VALUE_BEGINS);
		return this;
	}
	
	public FormHTMLDisplayBuilder valueEnds()
	{
		html.append(VALUE_ENDS);
		return this;
	}
	
	public FormHTMLDisplayBuilder appendTextBody(String txt)
	{
		html.append(txt);
		return this;
	}
	
	public FormHTMLDisplayBuilder appendTextBody(String label, String value, boolean line)
	{
		if(line)
			lineBegins();
		
		labelBegins().appendTextBody(label).labelEnds().valueBegins().appendTextBody(value).valueEnds();
		
		if(line)
			lineEnds();
		
		return this;
	}
	
	public FormHTMLDisplayBuilder appendTextBody(String label, String value)
	{
		appendTextBody(label, value, false);
		return this;
	}
	
	public String toString()
	{
		return html.toString();
	}
}
