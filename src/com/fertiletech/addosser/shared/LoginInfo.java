/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fertiletech.addosser.shared;
import java.io.Serializable;

/**
 *
 * @author Segun Razaq Sobulo
 */

public class LoginInfo implements Serializable {
    private boolean loggedIn = false;
	private String[] loginUrl;
    private String logoutUrl;
    private String providerName;
    private String loginID;
    private String message;
    private String[] loanIDs = null;
    private Integer state;
    private LoginRoles role;
    public final static String GOOGLE_PROVIDER_NAME = "google";
    public final static String YAHOO_PROVIDER_NAME = "yahoo";
    public final static String GOOGLE_CLOUD_LOGOUT = "http://accounts.google.com/Logout";
    public final static String YAHOO_CLOUD_LOGOUT = "http://login.yahoo.com/config/login?logout=1";
    public final static int GOOGLE_PROVIDER_IDX = 0; //only provider supported for admin screens
    public final static int YAHOO_PROVIDER_IDX = 1; 
    public LoginRoles getRole()
    {
    	return role;
    }
    
    public void setRole(LoginRoles role)
    {
    	this.role = role;
    }
    
    
    public void storeLoanInfo(String[] info)
    {
    	loanIDs = info;
    }
    
    public String getLoanID()
    {
    	if(loanIDs.length == 0) return null;
    	//TODO change below when multiple loan apps per customer allowed
    	return loanIDs[0]; //return most recent loan application
    }
    
    public Integer getPanelState()
    {
    	return state;
    }
    
    public void setPanelState(int st)
    {
    	state = st;
    }
    
    public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public String[] getLoginUrl() {
        return loginUrl;
    }

    public void setLoginUrl(String[] loginUrl) {
        this.loginUrl = loginUrl;
    }

    public String getLogoutUrl() {
        return logoutUrl;
    }

    public void setLogoutUrl(String logoutUrl) {
        this.logoutUrl = logoutUrl;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setName(String displayName) {
        this.providerName = displayName;
    }

    public String getLoginID() {
        return loginID;
    }

    public void setLoginID(String userKey) {
        this.loginID = userKey;
    }
}
