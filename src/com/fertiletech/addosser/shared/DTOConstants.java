/**
 * 
 */
package com.fertiletech.addosser.shared;

/**
 * @author Segun Razaq Sobulo
 *
 */
public final class DTOConstants {
	public final static String GOOGLE_CLOUD_LOGOUT = "http://accounts.google.com/Logout";
    public final static String YAHOO_CLOUD_LOGOUT = "http://login.yahoo.com/config/login?logout=1";		
	public final static String COMPANY_NAME = "ADDOSSER MFB";
	public final static String COMPANY_MANAGEMENT = "ADDOSSER Dashboard";
    public final static int TM_DIFF_HEADER = 0;
    public final static int TM_DIFF_VALS = 1;
    public final static int TM_DIFF_POS = 2;
    
    
    public final static String APP_PARAM_ADMINS = "user-admin";
    public final static String APP_PARAM_EDITOR = "user-editor";
    public final static String APP_PARAM_REVIEWER = "user-reviewer";    
    //loan mkt param strings
    public final static String MAX_PRINCIPAL_KEY = "XP";
    public final static String MIN_PRINCIPAL_KEY = "NP";
    public final static String MAX_TENOR_KEY = "XT";
    public final static String MIN_TENOR_KEY = "NT";
    public final static String INTEREST_KEY = "I";
    
    public final static int READY_IDX = 10;
    public final static int HOT_IDX = 0;
    public final static int COLD_IDX = 1;
    public final static int VERIFY_IDX = 2;
    public final static int APPROVE_IDX = 3;
    public final static int CLOSED_IDX = 4;
    public final static int ALL_IDX = 5; //this idx value should be higher than all others
    
    public final static int LOAN_DENIED_PICTURE_STATE = 2;
    
    public final static String[] OPS_SUGGESTIONS = {"Confirm as a prospect?", "Deny immediately?", "All documents received?", "Approve loan?", "Enter Orbit ID?"};
    public final static String[] LOAN_TYPES = {"Salary Advance", "Back to School", "Fly Now, Bill me Later", "Consumer Swift Lease", "Pay my Rent"};
    public final static String[] LOAN_TYPE_LABELS = {"Any Additional Comments?", "School/Tuition Fee details", "Flight/Travel Information", "Purchase Order/Invoice information", "Apartment/Rent information"};    
    private final static String[] LOAN_TYPES_URL_SUFFIX = {"salary-advance", "back-to-school", "fly-now-bill-me-later", "swift-lease", "pay-my-rent"};
    public final static String[] getLoanTypeUrl()
    {
    	String[] result = new String[LOAN_TYPES_URL_SUFFIX.length];
    	for(int i = 0; i < LOAN_TYPES_URL_SUFFIX.length; i++)
    		result[i] = "http://info.salaryadvanceng.com/products/" + LOAN_TYPES_URL_SUFFIX[i];
    	return result;
    }
	//text indices
	public final static int FULL_NAME_IDX = 0;
	public final static int COMPANY_NAME_IDX = 1;
	public final static int TYPE_IDX = 2;
	public final static int STATE_IDX = 3;
	public final static int TENOR_IDX = 4;
	public final static int NOTE_IDX = 5;
	public final static int EMAIL_IDX = 6;
	public final static int PHONE_NUMBER_IDX = 7;
	
	//number indices
	public final static int SALARY_IDX = 0;
	public final static int AMOUNT_IDX = 1;
    public final static int TAB_IDENTIFIER_MSG_IDX = 2;
    
    //date indices
    public final static int DATE_CREATED_IDX = 0;
    public final static int DATE_MODIFIED_IDX = 1;
    //------------------------------------------------ 
    
    //ID indices
    public final static int LOAN_KEY_IDX = 0;
    public final static int GUARANTOR_ONE_KEY_IDX = 1;
    public final static int GUARANTOR_TW0_KEY_IDX = 2;
    
    //below should be deprecated
    public final static int FORM_KEY_IDX = 1;
    public final static int SUBMITTED_KEY_IDX = 2; //only the form not ack objects etc 
    
    public final static int LOAN_IDX = 0;
    public final static int LOAN_ID_IDX = 1;
    
    //map indices
	public final static int CATEGORY_IDX = 0;
	public final static int DETAIL_IDX = 1;
	public final static int LAT_IDX = 0;
	public final static int LNG_IDX = 1;  
	
}
